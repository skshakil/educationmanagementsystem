package com.mysoft.systemSetupServiceApp.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import com.mysoft.core.util.CommonFunctions;



/**
 * @author Md. Jahurul Islam
 *
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	@Autowired
	@Qualifier("resourcesTokenStore")
	private TokenStore resourceTokenStore;

	@Override
	public void configure(final HttpSecurity http) throws Exception {
	  	System.out.println("### Start System Setup Resource Server Configuration ###");
        http.requestMatchers()
                .antMatchers(CommonFunctions.SECURED_PATTERN).and().authorizeRequests()
                .antMatchers(HttpMethod.POST, CommonFunctions.SECURED_PATTERN).access(CommonFunctions.SECURED_WRITE_SCOPE)
                .anyRequest().access(CommonFunctions.SECURED_READ_SCOPE);	
	}

	@Override
	public void configure(final ResourceServerSecurityConfigurer config) {
		config.tokenServices(resourceTokenServices());

	}

	@Bean
	@Primary
	public DefaultTokenServices resourceTokenServices() {
		final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(resourceTokenStore);
		return defaultTokenServices;
	}
	
	

}
