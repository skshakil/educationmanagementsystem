package com.mysoft.systemSetupServiceApp.campus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.campus.CampusService;
import com.mysoft.systemSetupServiceApp.base.SystemSetupServiceAppBaseController;

@RestController
@RequestMapping("/api/campus")
public class CampusController extends SystemSetupServiceAppBaseController {

	@Autowired
	private CampusService campusService;

	@PostMapping("/test")
	public String test() {
		return "test";
	}

	@PostMapping("/list")
	public Response list(@RequestBody(required = false) String reqObj) {
		return campusService.list(reqObj);
	}

	@PostMapping("/save")
	public Response save(@RequestBody String reqObj) {
		UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getName(),
				userDetails().getCompanyNo(), userDetails().getSessionNo(), userDetails().getCompanyNo(),
				userDetails().getOrganizationNo());
		return campusService.save(reqObj, userDetails);
	}

	@PostMapping("/delete")
	public Response delete(@RequestBody Long id) {
		return campusService.delete(id);
	}
}
