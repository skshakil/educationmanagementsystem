package com.mysoft.systemSetupServiceApp.base;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.TokenStore;

import com.mysoft.systemSetupServiceApp.util.SystemSetupServiceAppCommonFunctions;
import com.mysoft.auth.user.MyUserDetails;
import com.mysoft.core.base.BaseRepository;




/**
 * @author Md. Jahurul Islam
 *
 */
public class SystemSetupServiceAppBaseRepository extends BaseRepository implements SystemSetupServiceAppCommonFunctions {
	

	@Autowired
	private TokenStore tokenStore;
	

	public MyUserDetails userDetails() {

		MyUserDetails myUserDetails = userPrincipal();
		Map<String, Object> details = tokenStore.readAccessToken(accessToken()).getAdditionalInformation();
		
		if(null != details.get("sessionNo")) {
			myUserDetails.setSessionNo( ((Long) details.get("sessionNo")).longValue());
		}
	
		return myUserDetails;
	}



	
	
}
