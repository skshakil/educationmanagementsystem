package com.mysoft.systemSetupServiceApp.institute;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.institute.InstituteService;
import com.mysoft.systemSetupServiceApp.base.SystemSetupServiceAppBaseController;

@RestController
@RequestMapping("/api/institute")
public class InstituteController extends SystemSetupServiceAppBaseController {

	@Autowired
	private InstituteService instituteService;

	@PostMapping("/list")
	public Response getAll(@RequestBody(required = false) String reqObj) {
		return instituteService.list(reqObj);
	}

	@GetMapping("/gridList")
	public Response gridList(HttpServletRequest request) {
		return instituteService.gridList(request);
	}

	@PostMapping("/save-info")
	public Response save(@RequestBody String reqObj) {
		UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getName(),
				userDetails().getCompanyNo(), userDetails().getSessionNo(), userDetails().getCompanyNo());
		return instituteService.save(reqObj, userDetails);
	}

	@PostMapping("/update-info")
	public Response update(@RequestBody String reqObj) {
		UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getName(),
				userDetails().getCompanyNo(), userDetails().getSessionNo(), userDetails().getCompanyNo());
		
		System.out.println(reqObj);
		return instituteService.update(reqObj, userDetails);
	}

	@PostMapping("/delete-info")
	public Response delete(@RequestBody String reqObj) {
		UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getName(),
				userDetails().getCompanyNo(), userDetails().getSessionNo(), userDetails().getCompanyNo());
		return instituteService.deleteInstituteInfo(reqObj, userDetails);
	}
	
	@PostMapping("/delete")
	public Response delete(@RequestBody Long id) {
		return instituteService.delete(id);
	}
	
	@GetMapping("/find")
	public Response find(@RequestParam Long id) {
		return instituteService.find(id);
	}

}
