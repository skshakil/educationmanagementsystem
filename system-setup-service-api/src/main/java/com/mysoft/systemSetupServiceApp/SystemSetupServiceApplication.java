package com.mysoft.systemSetupServiceApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Md. Jahurul Islam
 *
 */

@SpringBootApplication
@ComponentScan({ "com.mysoft.systemSetupServiceApp", "com.mysoft.shared", "com.mysoft.core" })
public class SystemSetupServiceApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SystemSetupServiceApplication.class, args);
	}

}
