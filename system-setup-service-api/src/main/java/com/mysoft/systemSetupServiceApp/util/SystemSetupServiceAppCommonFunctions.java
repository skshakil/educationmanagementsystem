package com.mysoft.systemSetupServiceApp.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.mysoft.auth.user.MyUserDetails;

/**
 * @author Md. Jahurul Islam
 *
 */
public interface SystemSetupServiceAppCommonFunctions {
		
	default Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();

	}

	default HttpServletRequest getCurrentRequest() {

		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		Assert.state(requestAttributes != null, "Could not find current request via RequestContextHolder");
		Assert.isInstanceOf(ServletRequestAttributes.class, requestAttributes);
		HttpServletRequest servletRequest = ((ServletRequestAttributes) requestAttributes).getRequest();
		Assert.state(servletRequest != null, "Could not find current HttpServletRequest");

		return servletRequest;
	}

	default String accessToken() {

		String token = null;
		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();

		if (authentication instanceof OAuth2Authentication) {

			Object details = authentication.getDetails();

			if (details instanceof OAuth2AuthenticationDetails) {

				OAuth2AuthenticationDetails oauthsDetails = (OAuth2AuthenticationDetails) details;
				token = oauthsDetails.getTokenValue();
			}
		}

		return token;
	}

	default OAuth2AuthenticationDetails oauth2Details() {

		OAuth2AuthenticationDetails oauthsDetails = null;

		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();

		if (authentication instanceof OAuth2Authentication) {

			Object details = authentication.getDetails();

			if (details instanceof OAuth2AuthenticationDetails) {

				oauthsDetails = (OAuth2AuthenticationDetails) details;
			}
		}

		return oauthsDetails;
	}

	default OAuth2Authentication authentication() {

		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();
		return authentication;
	}

	default MyUserDetails userDetails() {

		return (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	default MyUserDetails userPrincipal() {

		return (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

}
