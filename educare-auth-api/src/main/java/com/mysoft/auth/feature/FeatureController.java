package com.mysoft.auth.feature;

import com.mysoft.core.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/features")
public class FeatureController {
	
	@Autowired
	private CoreFeatureService coreFeatureService;

	
	@PreAuthorize("hasAuthority('WJAVA_FS')") // SUBMENU_ID
	@GetMapping("/gridList")
	public Response gridList(HttpServletRequest reqObj) {
		return coreFeatureService.gridList(reqObj);
	}

	@GetMapping("/list")
	public Response getAll(@RequestBody(required = false) String reqObj) {
		return coreFeatureService.list(reqObj);
	}

	@PostMapping("/create")
	public Response create(@RequestBody String reqObj) {
		return coreFeatureService.save(reqObj);
	}

	@PutMapping("/update")
	public Response update(@RequestBody String reqObj) {
		return coreFeatureService.update(reqObj);
	}

	@DeleteMapping("/delete")
	public Response delete(@RequestParam("id") long reqId) {
		return coreFeatureService.delete(reqId);
	}

	@DeleteMapping("/remove")
	public Response remove(@RequestParam("id") long reqId) {
		return coreFeatureService.remove(reqId);
	}

	@GetMapping("/find")
	public Response findByName(@RequestParam Long id){
		return coreFeatureService.find(id);
	}

}
