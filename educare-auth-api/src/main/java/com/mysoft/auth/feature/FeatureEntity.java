package com.mysoft.auth.feature;

import com.mysoft.auth.module.ModuleEntity;
import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "sa_submenu")
public class FeatureEntity extends BaseOraEntity {

	@Id
    @Column(name = "SUBMENU_NO")
    private Long id;

    @NotNull
    @Column(name = "ACTIVE_STAT")
    private Integer activeStatus=1;

    @NotNull
    @Column(name = "HIDE_FLAG")
    private Integer hideFlag;

    @NotNull
    @Column(name = "SUBMENU_TYPE")
    private String submenuType;

    @NotNull
    @Column(name = "SUBMENU_NAME")
    private String submenuName;

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "MENU_NO", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIgnore
    private ModuleEntity menuEntity;

    @NotNull
    @Column(name = "SUBMENU_ID")
    private String submenuId;

    @NotNull
    @Column(name = "SL_NO")
    private Long slNo;

//    @Column(name = "RP_SHOW_FOOTER")
//    private Long rpShowFooter;
//
//    @Column(name = "DEF_REPORT_SERVER")
//    private String defReportServer;

    @Column(name = "PAGE_LINK")
    private String pageLink;

    @Column(name = "RP_OUTPUT")
    private String rpOutput;

    @Column(name = "SUBMENU_NAME_JAVA")
    private String submenuNameJava;

//    @Column(name = "RP_SHOW_PAGE_NO")
//    private Long rpShowPageNo;

//    @Column(name = "RP_SHOW_PRINT_DATE")
//    private Long rpShowPrintDate;

    @Column(name = "CAN_CREATE")
    private Integer canCreate;

    @Column(name = "CAN_REMOVE")
    private Integer canRemove;

    @Column(name = "CAN_MODIFY")
    private Integer canModify;

    @Column(name = "CAN_VIEW")
    private Integer canView;

//    @Column(name = "RP_SHOW_PRINTED_BY")
//    private Long rpShowPrintedBy;

//    @Column(name = "RP_SHOW_HEADER")
//    private Long rpShowHeader;

    @Column(name = "ICONNAME")
    private String iconName;

    @Transient
    private Long idNotEqual;
    
    @Transient
    private List<Long> ids;
    


	@Transient
    private List<String> submenuTypeList;
    
    
    
    public List<String> getSubmenuTypeList() {
		return submenuTypeList;
	}

	public void setSubmenuTypeList(List<String> submenuTypeList) {
		this.submenuTypeList = submenuTypeList;
	}

    public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
	

    @Transient
    private boolean isGranted = false;

    public boolean isGranted() {
        return isGranted;
    }

    public void setGranted(boolean granted) {
        isGranted = granted;
    }
}
