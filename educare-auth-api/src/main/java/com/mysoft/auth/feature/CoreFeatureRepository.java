package com.mysoft.auth.feature;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.auth.module.ModuleEntity;
import com.mysoft.auth.user.MyUserDetails;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;

@Repository
@Transactional
public class CoreFeatureRepository extends AuthBaseRepository {

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Response gridList(HttpServletRequest request) {

        DataTableResults<FeatureEntity> dataTableResults = null;
        Response response = new Response();
        FeatureEntity featureEntity = new FeatureEntity();
        List<String> submenuTypeList = new ArrayList<String>();
        submenuTypeList.add("W");
        submenuTypeList.add("A");
        featureEntity.setSubmenuTypeList(submenuTypeList);
        featureEntity.setActiveStatus(1);
        featureEntity.setCompanyNo(userDetails().getCompanyNo());

        DataTableRequest dataTableInRQ = new DataTableRequest(request);
        Long totalRowCount = totalCount(featureEntity);

        List gridList = new ArrayList<>();
        response = baseList(typedQuery(featureEntity, dataTableInRQ));

        if (response.isSuccess()) {
            if (response.getItems() != null) {
                gridList = response.getItems();
            }
            dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
        }
        response.setItems(null);
        response.setObj(dataTableResults);
        return response;
    }

    public Response list(String reqObj) {

        FeatureEntity featureEntity = null;
        if (null != reqObj) {
            featureEntity = objectMapperReadValue(reqObj, FeatureEntity.class);
        }
        return baseList(criteriaQuery(featureEntity));
    }

    public Response save(String reqObj) {
        JSONObject json = new JSONObject(reqObj);
        String parentMenuStr = Def.getString(json, "menuEntity");
        String existingFeatureCode = Def.getString(json, "submenuId");

        FeatureEntity featureEntity = objectMapperReadValue(reqObj, FeatureEntity.class);
        //featureEntity.setSubmenuType("W");

        // check if feature code is already existing
        FeatureEntity checkerEntity = new FeatureEntity();
        checkerEntity.setSubmenuId(existingFeatureCode);
        
        FeatureEntity obj = validateFeatureCode(checkerEntity);
        
        if (obj != null) {
            return getErrorResponse("Duplicate feature code found!");
        }

        MyUserDetails currentUser = userDetails();
        //set id using sequence
//		Long ogNo, Long companyNo, String tableName, String columnName, Long dataLength
        featureEntity.setId(functionFdAutoNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(),"sa_submenu","SUBMENU_NO",6l));   //featureSubmenuNo()
        featureEntity.setSsCreatedOn(new Date());
        featureEntity.setSsCreator(currentUser.getUserId());
        featureEntity.setSsCreateSession(currentUser.getSessionNo());
        featureEntity.setSsModifiedOn(new Date());
        featureEntity.setSsModifiedSession(currentUser.getSessionNo());
        featureEntity.setSsModifier(currentUser.getUserId());
        featureEntity.setCompanyNo(currentUser.getCompanyNo());
        featureEntity.setOrganizationNo(currentUser.getOrganizationNo());

        ModuleEntity moduleEntity = null;
        if (parentMenuStr != null && !parentMenuStr.equals("{}")) {
            moduleEntity = objectMapperReadValue(parentMenuStr, ModuleEntity.class);
            if (moduleEntity != null) {
                featureEntity.setMenuEntity(moduleEntity);
            }
        }

        return baseOnlySave(featureEntity);
    }

    public Response update(String reqObj) {
        FeatureEntity featureEntity = objectMapperReadValue(reqObj, FeatureEntity.class);
//        featureEntity.setSubmenuType("W");
        featureEntity.setCompanyNo(userDetails().getCompanyNo());
        featureEntity.setIdNotEqual(featureEntity.getId());
        
        FeatureEntity checkerEntity = validateFeatureCode(featureEntity);
        if (checkerEntity != null) {
            return getErrorResponse("Feature Code already exists!");
        }
        
        FeatureEntity obj = findById(featureEntity.getId());
       
        
        if (obj != null) {
            obj.setSubmenuName(featureEntity.getSubmenuName());
            obj.setSubmenuType(featureEntity.getSubmenuType());
            obj.setSlNo(featureEntity.getSlNo());
            obj.setSubmenuId(featureEntity.getSubmenuId());
            obj.setMenuEntity(featureEntity.getMenuEntity());
            obj.setActiveStatus(featureEntity.getActiveStatus());
            obj.setHideFlag(featureEntity.getHideFlag());
            obj.setIconName(featureEntity.getIconName());
            obj.setPageLink(featureEntity.getPageLink());

            MyUserDetails currentUser = userDetails();
            obj.setSsModifiedOn(new Date());
            obj.setSsModifiedSession(currentUser.getSessionNo());
            obj.setSsModifier(currentUser.getUserId());
            obj.setCompanyNo(currentUser.getCompanyNo());
            obj.setOrganizationNo(currentUser.getOrganizationNo());

            return baseUpdate(obj);
        }

        return getErrorResponse("Record not Found !!");
    }

    public Response detele(Long id) {

        FeatureEntity featureEntity = findById(id);
        if (featureEntity == null) {
            return getErrorResponse("Record not found!");
        }

        return baseDelete(featureEntity);

    }

    public Response remove(Long id) {
        FeatureEntity featureEntity = findById(id);
        if (featureEntity == null) {
            return getErrorResponse("Record not found!");
        }
        featureEntity.setActiveStatus(0);
        return baseRemove(featureEntity);
    }

    public FeatureEntity findById(Long id) {
        FeatureEntity featureEntity = new FeatureEntity();
        featureEntity.setId(id);
        featureEntity.setActiveStatus(1);
        Response response = baseFindById(criteriaQuery(featureEntity));
        if (response.isSuccess()) {

            return getValueFromObject(response.getObj(), FeatureEntity.class);
        }
        return null;
    }
    
    public FeatureEntity findByContentAccessType() {
        FeatureEntity featureEntity = new FeatureEntity();
        featureEntity.setSubmenuType("A");
        featureEntity.setActiveStatus(1);
        Response response = baseFindById(criteriaQuery(featureEntity));
        if (response.isSuccess()) {

            return getValueFromObject(response.getObj(), FeatureEntity.class);
        }
        return null;
    }

    public Response findByContentAccessTypeList() {
        FeatureEntity featureEntity = new FeatureEntity();
        featureEntity.setSubmenuType("A");
        featureEntity.setActiveStatus(1);
        //featureEntity.setCompanyNo(userDetails().getCompanyNo());
        return baseList(criteriaQuery(featureEntity));
    }

    public Response findByModule(ModuleEntity id) {
        FeatureEntity featureEntity = new FeatureEntity();
        featureEntity.setMenuEntity(id);
        return baseFindByParent(featureListCriteriaQuery(featureEntity));
    }

    public Response find(Long id) {
        FeatureEntity featureEntity = new FeatureEntity();
        featureEntity.setId(id);
        featureEntity.setActiveStatus(1);
        featureEntity.setCompanyNo(userDetails().getCompanyNo());
        return baseFindById(criteriaQuery(featureEntity));
    }


    public Response findByIds(List<Long> ids) {
        FeatureEntity featureEntity = new FeatureEntity();
        
        List<String> submenuTypeList = new ArrayList<String>();
        submenuTypeList.add("W");
        submenuTypeList.add("A");
        
        featureEntity.setSubmenuTypeList(submenuTypeList);
//        featureEntity.setCompanyNo(userDetails().getCompanyNo());
        
        featureEntity.setIds(ids);
        featureEntity.setActiveStatus(1);
        return baseList(criteriaQuery(featureEntity));
    }

    private FeatureEntity validateFeatureCode(FeatureEntity checkFeature) {

        Response response = baseSingleObject(criteriaQuery(checkFeature));
        if (response.isSuccess()) {
            return getValueFromObject(response.getObj(), FeatureEntity.class);
        }
        return null;
    }
    


    // Non API
    @SuppressWarnings({"rawtypes", "unchecked"})
    private CriteriaQuery criteriaQuery(FeatureEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    private CriteriaQuery featureListCriteriaQuery(FeatureEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = findByModuleIdCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(FeatureEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ,FeatureEntity.class);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }

        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);

        }

        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);

        }

        criteria.where(pArrayJoin.toArray(new Predicate[0]));

        return baseTypedQuery(criteria, dataTableInRQ);
    }

    private Long totalCount(FeatureEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<FeatureEntity> root = from(FeatureEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> criteriaCondition(FeatureEntity filter, CriteriaBuilder builder, Root<FeatureEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {
            if (filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }
//            if (filter.getCompanyNo() != null) {
//                Predicate condition = builder.equal(root.get("companyNo"), filter.getCompanyNo());
//                p.add(condition);
//            }

            if (filter.getId() != null && filter.getId() > 0 && filter.getIdNotEqual() == null) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }

            if (filter.getSubmenuType() != null) {
                Predicate condition = builder.equal(root.get("submenuType"), filter.getSubmenuType());
                p.add(condition);
            }
            
            if (filter.getSubmenuTypeList() != null && filter.getSubmenuTypeList().size() > 0) {
           	
                Expression<String> exp = root.get("submenuType");
                Predicate condition = exp.in(filter.getSubmenuTypeList());
                p.add(condition);
            }
            


            if (filter.getIds() != null && filter.getIds().size() > 0) {

                Expression<ModuleEntity> exp = root.get("menuEntity");
                Predicate condition = exp.in(filter.getIds());
                p.add(condition);

            }

//            if (filter.getIds() != null && filter.getIds().size() > 0) {
//                
//        		Expression<Long> exp = root.get("id");
//				Predicate condition  = exp.in(filter.getIds());
//				p.add(condition);
//               
//            }

            // for validating existing feature code input
            if (filter.getSubmenuId() != null) {
                Predicate condition = builder.equal(root.get("submenuId"), filter.getSubmenuId());
                p.add(condition);
            }

            if (filter.getIdNotEqual() != null && filter.getIdNotEqual() > 0) {
                Predicate condition = builder.notEqual(root.get("id"), filter.getIdNotEqual());
                p.add(condition);
            }

        }

        return p;
    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> findByModuleIdCondition(FeatureEntity filter, CriteriaBuilder builder, Root<FeatureEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {
            filter.setActiveStatus(1);
            if (filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }
            if (filter.getMenuEntity() != null) {
                Predicate condition = builder.equal(root.get("menuEntity"), filter.getMenuEntity());
                p.add(condition);
            }
        }

        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(FeatureEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }

    public Response findByName(String orgName) {
        if (null == orgName) {
            return getErrorResponse("please enter Module Name");
        }

        JSONObject json = new JSONObject(orgName);
        String organizationName = Def.getString(json, "menuName");

        FeatureEntity orgEntity = new FeatureEntity();

        if (null == organizationName) {
            return getErrorResponse("please enter  Organization Name");
        }

        Map<String, Object> searchFields = new HashMap<String, Object>();
        searchFields.put("menuName", organizationName);

        Response response = baseList(typedQuery(orgEntity, searchFields));

        if (response.getItems() == null) {
            return getErrorResponse("Data not found");
        }
        return response;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(FeatureEntity filter, Map<String, Object> fields) {
        init();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = basePredicate(fields);

        return typedQuery(pConjunction, pDisJunction);
    }
}
