package com.mysoft.auth.feature;

import com.mysoft.core.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Service
public class CoreFeatureService {
	
	@Autowired
	private CoreFeatureRepository coreFeatureRepository;
	
	public Response gridList(HttpServletRequest request) {
		return coreFeatureRepository.gridList(request);
	}

	public Response list(String reqObj) {
		return coreFeatureRepository.list(reqObj);
	}

	public Response save(String reqObj) {
		return coreFeatureRepository.save(reqObj);
	}

	public Response update(String reqObj) {
		return coreFeatureRepository.update(reqObj);
	}

	public Response delete(Long id) {
		return coreFeatureRepository.detele(id);
	}

	public Response remove(Long id) {
		return coreFeatureRepository.remove(id);
	}

	public Response findByName(String orgName){
		return coreFeatureRepository.findByName(orgName);
	}

    public Response find(Long id) {
		return coreFeatureRepository.find(id);
    }

	public FeatureEntity findById(Long id) {
		return coreFeatureRepository.findById(id);
	}
    
    public Response findByIds(List<Long> ids) {
  		return coreFeatureRepository.findByIds(ids);
      }
    
    public FeatureEntity findByContentAccessType() {
    	return coreFeatureRepository.findByContentAccessType();
    }

    public Response findByContentAccessTypeList() {
        return coreFeatureRepository.findByContentAccessTypeList();
    }
    
    

}
