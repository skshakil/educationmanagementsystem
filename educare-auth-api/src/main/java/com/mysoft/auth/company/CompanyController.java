package com.mysoft.auth.company;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mysoft.auth.base.AuthBaseController;
import com.mysoft.core.compnay.CoreCompanyService;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@RestController
@RequestMapping("/api/company")
public class CompanyController extends AuthBaseController {
	
	@Autowired
	private CoreCompanyService companyService;

	
	@PreAuthorize("hasAuthority('WJAVA_CSE')") //WJAVA_CSE
	@GetMapping("gridList")
	public Response gridList(HttpServletRequest request){
		UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
    	
		return companyService.gridList(request,userDetail);
	}

	@GetMapping("/list")
	public Response getAll(@RequestParam Long ogNo ) {
		
		return companyService.list(ogNo);
	}
	
	@GetMapping("/orderd-list")
	public Response getOrdereList() {
		return companyService.list4online();
	}

	@PostMapping("/create")
	public Response create(@RequestBody String reqObj) {
		return companyService.save(reqObj, new UserSignInContainter(userDetails().getUserId(),userDetails().getCompanyNo(),userDetails().getOrganizationNo(),userDetails().getSessionNo(),userDetails().getUserTypeNo()));
	}

	@PutMapping("/update")
	public Response update(@RequestBody String reqObj) {
		return companyService.update(reqObj, new UserSignInContainter(userDetails().getUserId(),userDetails().getCompanyNo(),userDetails().getOrganizationNo(),userDetails().getSessionNo(),userDetails().getUserTypeNo()));
	}

	@DeleteMapping("/delete")
	public Response delete(@RequestBody long reqId) {
		return companyService.delete(reqId);
	}

	@DeleteMapping("/remove")
	public Response remove(@RequestParam("id") long reqId) {
		return companyService.remove(reqId);
	}
	
	@GetMapping("/findCompany")
	public Response findCompanyByIds(@RequestParam("id") List<Long> companyIds) {
		return companyService.findCompanyByIds(companyIds);
	}
	
    @PostMapping("/create-with-image")
    public Response saveWithImage(@RequestParam(value = "imageFile", required = false) MultipartFile imageFile,@RequestParam(value = "logoFile", required = false) MultipartFile logoFile, @RequestParam("reqobj") String reqObj) {
    	 UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
       
        return companyService.saveWithImage(imageFile,logoFile,reqObj,userDetail);
    }
    
    @PostMapping("/update-with-image")
    public Response updateWithImage(@RequestParam(value = "imageFile", required = false) MultipartFile imageFile,@RequestParam(value = "logoFile", required = false) MultipartFile logoFile, @RequestParam("reqobj") String reqObj) {
    	UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
    	
    	return companyService.updateWithImage(imageFile,logoFile,reqObj,userDetail);
    }
    
    @GetMapping("/find-by-alias")
    public Response findCompanyByAlias(String alias) {
    	return companyService.findCompanyByAlias(alias);
    }
    
    @PostMapping("/update-serial")
    public Response updateSerial(@RequestBody String reqObj) {
    	UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
    	
    	return companyService.updateSerial(reqObj,userDetail);
    }
    
	
	@GetMapping("/company-type-list")
	public Response getCompnayTypeList() {
		return companyService.getCompanyTypeList();
	}
}
