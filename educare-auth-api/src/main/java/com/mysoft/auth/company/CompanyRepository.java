package com.mysoft.auth.company;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.core.compnay.CompanyEntity;
import com.mysoft.core.compnay.CoreCompanyService;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Repository
@Transactional
public class CompanyRepository extends AuthBaseRepository  {

	@Autowired
	CoreCompanyService coreCompanyService;
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request, UserSignInContainter userDetails) {
		
		return coreCompanyService.gridList(request,userDetails);

	/*	DataTableResults<CompanyEntity> dataTableResults = null;
		Response response = new Response();
		CompanyEntity companyEntity = new CompanyEntity();

		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(companyEntity);

		List gridList = new ArrayList<>();
		response = baseList(typedQuery(companyEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;*/
	}

	public Response list(String reqObj) {
		
		return coreCompanyService.list(Long.parseLong(reqObj));
		
		/*CompanyEntity companyEntity = null;
		if (null != reqObj) {
			companyEntity = objectMapperReadValue(reqObj, CompanyEntity.class);
		}
		return baseList(criteriaQuery(companyEntity));*/
	}

//	public Response save(String reqObj) {
//		return coreCompanyService.save(reqObj);
//		CompanyEntity companyEntity = objectMapperReadValue(reqObj, CompanyEntity.class);
//		companyEntity.setSsCreator(userDetails().getUserId());
//		return baseOnlySave(companyEntity);
//	}
//
//	public Response update(String reqObj) {
//		return coreCompanyService.update(reqObj);
//
//		CompanyEntity companyEntity = objectMapperReadValue(reqObj, CompanyEntity.class);
//		CompanyEntity obj = findById(companyEntity.getId());
//		if (obj != null) {
//			companyEntity.setSsCreator(userDetails().getUserId());
//			return baseUpdate(companyEntity);
//		}
//
//		return getErrorResponse("Record not Found !!");*/
//
//	}

	public Response detele(Long id) {
		return coreCompanyService.delete(id);
		/*
		CompanyEntity companyEntity = findById(id);
		if (companyEntity == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(companyEntity);
*/
	}

	public Response remove(Long id) {
		return coreCompanyService.remove(id);
		/*CompanyEntity companyEntity = findById(id);
		if (companyEntity == null) {
			return getErrorResponse("Record not found!");
		}
		companyEntity.setActiveStatus(3);
		return baseRemove(companyEntity);
*/	}

	public CompanyEntity findById(Long id) {
		return coreCompanyService.findCompanyById(id);
		
/*		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(id);
		Response response = baseFindById(criteriaQuery(companyEntity));
		if (response.isSuccess()) {

			return getValueFromObject(response.getObj(), CompanyEntity.class);
		}
		return null;*/
	}
	
	public Response findCompanyByIds(List<Long> companyIds) {
		return coreCompanyService.findCompanyByIds(companyIds);
		/*Response response = new Response();
		List<CompanyEntity> companyEntityList = null;
		List<Map<String,Object>> companyMapList = new ArrayList<Map<String,Object>>();
		
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setCompanyIds(companyIds);
		response = baseList(criteriaQuery(companyEntity));
		
		if(response.isSuccess() && response.getItems() !=null) {
		
			companyEntityList = getListFromObject(response.getItems(), CompanyEntity.class) ;
			
		  for(CompanyEntity compnay:companyEntityList) {
			 
			  Map<String,Object> companyMap = new HashMap<String,Object>(); 
			
			  companyMap.put("companyId", compnay.getId());
			  companyMap.put("compnayName", compnay.getCompanyName());
			  companyMap.put("compnayAddress1", compnay.getCompanyAddress1());
			  companyMap.put("compnayAddress2", compnay.getCompanyAddress2());
			  companyMap.put("compnayEmail", compnay.getCompanyEmail());
			  companyMap.put("orgId", compnay.getOgNo());
			  
			  companyMapList.add(companyMap);
				
			}
		 
		  response.setItems(companyMapList);
		}
		
		
		return  response;
	*/
	
	}
	

	/*// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(CompanyEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(CompanyEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(CompanyEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<CompanyEntity> root = from(CompanyEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(CompanyEntity filter, CriteriaBuilder builder, Root<CompanyEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			if (filter.getActiveStatus() > 0) {
				Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
				p.add(condition);
			}
			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
			
			if (filter.getCompanyIds() != null && filter.getCompanyIds().size() > 0) {
				Expression<Long> exp = root.get("id");
				Predicate condition  = exp.in(filter.getCompanyIds());
				p.add(condition);
			}
		}

		return p;
	}
	
	@SuppressWarnings({ "unused", "rawtypes" })
	private void init() {
		
		initEntityManagerBuilderCriteriaQueryRoot(CompanyEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
		
	}*/

}
