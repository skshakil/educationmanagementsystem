package com.mysoft.auth.company;

import com.mysoft.core.compnay.CompanyEntity;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class CompanyService {
	
	@Autowired
	private CompanyRepository companyRepository;
	
	public Response gridList(HttpServletRequest request, UserSignInContainter userDetails) {
		return companyRepository.gridList(request,userDetails);
	}

	public Response list(String reqObj) {
		return companyRepository.list(reqObj);
	}

	public Response delete(Long id) {
		return companyRepository.detele(id);
	}

	public Response remove(Long id) {
		
		return companyRepository.remove(id);
	}
	
	public Response findCompanyByIds(List<Long> companyIds) {
		
		return companyRepository.findCompanyByIds(companyIds);
	}
	
	
	public CompanyEntity findCompanyById(Long companyId) {
		
		return companyRepository.findById(companyId);
	}


}
