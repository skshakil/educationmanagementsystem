package com.mysoft.auth.userSession;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Response;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class UserSessionRepository extends AuthBaseRepository  {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {

		DataTableResults<UserSessionEntity> dataTableResults = null;
		Response response = new Response();
		UserSessionEntity userSessionEntity = new UserSessionEntity();

		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(userSessionEntity);

		List gridList = new ArrayList<>();
		response = baseList(typedQuery(userSessionEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response list(String reqObj) {
		
		UserSessionEntity userSessionEntity = null;
		if (null != reqObj) {
			userSessionEntity = objectMapperReadValue(reqObj, UserSessionEntity.class);
		}
		return baseList(criteriaQuery(userSessionEntity));
	}

	public Response save(String reqObj) {
		UserSessionEntity userSessionEntity = objectMapperReadValue(reqObj, UserSessionEntity.class);
		userSessionEntity.setSsCreator(userDetails().getUserId());
		return baseOnlySave(userSessionEntity);
	}

	public Response update(String reqObj) {

		UserSessionEntity userSessionEntity = objectMapperReadValue(reqObj, UserSessionEntity.class);
		UserSessionEntity obj = findById(userSessionEntity.getId());
		
		if (obj != null) {
			
			obj.setLogoutDate(userSessionEntity.getLogoutDate());
			obj.setLogoutType(userSessionEntity.getLogoutType());
			obj.setSessionStatus(userSessionEntity.getSessionStatus());
			
			return baseUpdate(obj);
		}
		

		return getErrorResponse("Record not Found !!");

	}

	public Response detele(Long id) {

		UserSessionEntity userSessionEntity = findById(id);
		if (userSessionEntity == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(userSessionEntity);

	}

	public Response remove(Long id) {
		UserSessionEntity userSessionEntity = findById(id);
		if (userSessionEntity == null) {
			return getErrorResponse("Record not found!");
		}
		//userSessionEntity.setActiveStatus(3);
		return baseRemove(userSessionEntity);
	}

	public UserSessionEntity findById(Long id) {
		UserSessionEntity userSessionEntity = new UserSessionEntity();
		userSessionEntity.setId(id);
		Response response = baseFindById(criteriaQuery(userSessionEntity));
		if (response.isSuccess()) {

			return getValueFromObject(response.getObj(), UserSessionEntity.class);
		}
		return null;
	}

	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(UserSessionEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(UserSessionEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(UserSessionEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<UserSessionEntity> root = from(UserSessionEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "unchecked" })
    private List<Predicate> criteriaCondition(UserSessionEntity filter, CriteriaBuilder builder, Root<UserSessionEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
		/*	if (filter.getActiveStatus() > 0) {
				Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
				p.add(condition);
			}*/
			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
		}

		return p;
	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(UserSessionEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

}
