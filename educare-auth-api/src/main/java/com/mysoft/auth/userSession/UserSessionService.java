package com.mysoft.auth.userSession;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;

@Service
public class UserSessionService {
	
	@Autowired
	private UserSessionRepository organizationRepository;
	
	public Response gridList(HttpServletRequest request) {
		return organizationRepository.gridList(request);
	}

	public Response list(String reqObj) {
		return organizationRepository.list(reqObj);
	}

	public Response save(String reqObj) {
		return organizationRepository.save(reqObj);
	}

	public Response update(String reqObj) {
		return organizationRepository.update(reqObj);
	}

	public Response delete(Long id) {
		return organizationRepository.detele(id);
	}

	public Response remove(Long id) {
		return organizationRepository.remove(id);
	}

}
