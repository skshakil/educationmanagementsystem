package com.mysoft.auth.userSession;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mysoft.core.base.BaseOraEntity;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
@Setter
@Entity
@Table(name = "SA_SESSION")
public class UserSessionEntity extends BaseOraEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3847520233165684834L;

	@Id
	@Column(nullable = false, name = "SESSION_NO")
	private Long id;

	@Column(name = "SESSION_STAT")
	private Integer sessionStatus;
	
	@Column(name = "USER_NO")
	private Long userNo;
	
	@Column(name = "LOGON_DT")
	private Date logonDate;
	
	@Column(name = "LOGOUT_DT")
	private Date logoutDate;
	
	@Column(name = "LOGOUT_TYPE")
	private String logoutType;
	
/*	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus;*/
	
	
	
	
	

}
