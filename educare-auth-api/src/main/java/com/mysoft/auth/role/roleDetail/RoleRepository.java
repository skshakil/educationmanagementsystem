//package com.mysoft.auth.role.roleDetail;
//
//import com.mysoft.auth.base.AuthBaseRepository;
//import com.mysoft.auth.module.ModuleEntity;
//import com.mysoft.core.pagination.DataTableRequest;
//import com.mysoft.core.pagination.DataTableResults;
//import com.mysoft.core.util.Def;
//import com.mysoft.core.util.Response;
//import org.json.JSONObject;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.CollectionUtils;
//
//import javax.persistence.TypedQuery;
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.CriteriaQuery;
//import javax.persistence.criteria.Predicate;
//import javax.persistence.criteria.Root;
//import javax.servlet.http.HttpServletRequest;
//import java.util.*;
//
//@Repository
//@Transactional
//public class RoleDetailRepository extends AuthBaseRepository {
//
//    @SuppressWarnings({"rawtypes", "unchecked"})
//    public Response gridList(HttpServletRequest request) {
//
//        DataTableResults<RoleDetailEntity> dataTableResults = null;
//        Response response = new Response();
//        RoleDetailEntity roleDetailEntity = new RoleDetailEntity();
//
//        DataTableRequest dataTableInRQ = new DataTableRequest(request);
//        Long totalRowCount = totalCount(roleDetailEntity);
//
//        List gridList = new ArrayList<>();
//        response = baseList(typedQuery(roleDetailEntity, dataTableInRQ));
//
//        if (response.isSuccess()) {
//            if (response.getItems() != null) {
//                gridList = response.getItems();
//            }
//            dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
//        }
//        response.setItems(null);
//        response.setObj(dataTableResults);
//        return response;
//    }
//
//    public Response list(String reqObj) {
//
//        RoleDetailEntity roleDetailEntity = null;
//        if (null != reqObj) {
//            roleDetailEntity = objectMapperReadValue(reqObj, RoleDetailEntity.class);
//        }
//        return baseList(criteriaQuery(roleDetailEntity));
//    }
//
//    public Response save(String reqObj) {
//        JSONObject json = new JSONObject(reqObj);
//        String parentMenuStr = Def.getString(json, "menuEntity");
//
//        RoleDetailEntity roleDetailEntity = objectMapperReadValue(reqObj, RoleDetailEntity.class);
//
//        roleDetailEntity.setSsCreator(userDetails().getUserId());
//        roleDetailEntity.setSsCreatedOn(new Date());
//        roleDetailEntity.setSsCreateSession(userDetails().getSessionNo());
//
//        ModuleEntity moduleEntity=null;
//        if (parentMenuStr != null && !parentMenuStr.equals("{}")) {
//            moduleEntity = objectMapperReadValue(parentMenuStr, ModuleEntity.class);
//            if (moduleEntity != null) {
////                roleDetailEntity.setMenuEntity(moduleEntity);
//            }
//        }
//
//        return baseOnlySave(roleDetailEntity);
//    }
//
//    public Response update(String reqObj) {
//        RoleDetailEntity roleDetailEntity = objectMapperReadValue(reqObj, RoleDetailEntity.class);
//        RoleDetailEntity obj = findById(roleDetailEntity.getId());
//        if (obj != null) {
////            roleDetailEntity.setSsCreator(userDetails().getUserId());
//            return baseUpdate(roleDetailEntity);
//        }
//
//        return getErrorResponse("Record not Found !!");
//
//    }
//
//    public Response detele(Long id) {
//
//        RoleDetailEntity roleDetailEntity = findById(id);
//        if (roleDetailEntity == null) {
//            return getErrorResponse("Record not found!");
//        }
//
//        return baseDelete(roleDetailEntity);
//
//    }
//
//    public Response remove(Long id) {
//        RoleDetailEntity roleDetailEntity = findById(id);
//        if (roleDetailEntity == null) {
//            return getErrorResponse("Record not found!");
//        }
//        return baseRemove(roleDetailEntity);
//    }
//
//    public RoleDetailEntity findById(Long id) {
//        RoleDetailEntity roleDetailEntity = new RoleDetailEntity();
//        roleDetailEntity.setId(id);
//        Response response = baseFindById(criteriaQuery(roleDetailEntity));
//        if (response.isSuccess()) {
//
//            return getValueFromObject(response.getObj(), RoleDetailEntity.class);
//        }
//        return null;
//    }
//
//    public Response findByModule(ModuleEntity id) {
//        RoleDetailEntity roleDetailEntity = new RoleDetailEntity();
//        roleDetailEntity.setMenuEntity(id);
//        return baseFindByParent(featureListCriteriaQuery(roleDetailEntity));
//    }
//
//    public Response find(Long id) {
//        RoleDetailEntity roleDetailEntity = new RoleDetailEntity();
//        roleDetailEntity.setId(id);
//        roleDetailEntity.setActiveStatus(1);
//        return baseFindById(criteriaQuery(roleDetailEntity));
//    }
//
//    // Non API
//    @SuppressWarnings({"rawtypes", "unchecked"})
//    private CriteriaQuery criteriaQuery(RoleDetailEntity filter) {
//        init();
//
//        List<Predicate> p = new ArrayList<Predicate>();
//        p = criteriaCondition(filter, null, null);
//
//        if (!CollectionUtils.isEmpty(p)) {
//            Predicate[] pArray = p.toArray(new Predicate[]{});
//            Predicate predicate = builder.and(pArray);
//            criteria.where(predicate);
//        }
//        return criteria;
//    }
//
//    private CriteriaQuery featureListCriteriaQuery(RoleDetailEntity filter) {
//        init();
//
//        List<Predicate> p = new ArrayList<Predicate>();
//        p = findByModuleIdCondition(filter, null, null);
//
//        if (!CollectionUtils.isEmpty(p)) {
//            Predicate[] pArray = p.toArray(new Predicate[]{});
//            Predicate predicate = builder.and(pArray);
//            criteria.where(predicate);
//        }
//        return criteria;
//    }
//
//    @SuppressWarnings({"rawtypes"})
//    private <T> TypedQuery typedQuery(RoleDetailEntity filter, DataTableRequest<T> dataTableInRQ) {
//        init();
//        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
//        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
//        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);
//
//        Predicate predicateAND = null;
//        Predicate predicateOR = null;
//
//        if (!CollectionUtils.isEmpty(pConjunction)) {
//            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
//            predicateAND = builder.and(pArray);
//        }
//
//        if (!CollectionUtils.isEmpty(pDisJunction)) {
//            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
//            predicateOR = builder.or(pArray);
//        }
//        if (predicateAND != null) {
//            pArrayJoin.add(predicateAND);
//
//        }
//
//        if (predicateOR != null) {
//            pArrayJoin.add(predicateOR);
//
//        }
//
//        criteria.where(pArrayJoin.toArray(new Predicate[0]));
//
//        return baseTypedQuery(criteria, dataTableInRQ);
//    }
//
//    private Long totalCount(RoleDetailEntity filter) {
//        CriteriaBuilder builder = criteriaBuilder();
//        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
//        Root<RoleDetailEntity> root = from(RoleDetailEntity.class, criteriaQuery);
//        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));
//
//    }
//
//    @SuppressWarnings({"unchecked"})
//    private List<Predicate> criteriaCondition(RoleDetailEntity filter, CriteriaBuilder builder, Root<RoleDetailEntity> root) {
//
//        if (builder == null) {
//            builder = super.builder;
//        }
//        if (root == null) {
//            root = super.root;
//        }
//
//        List<Predicate> p = new ArrayList<Predicate>();
//
//        if (filter != null) {
//            if (filter.getActiveStatus() > 0) {
//                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
//                p.add(condition);
//            }
//
//            if (filter.getId() != null && filter.getId() > 0) {
//                Predicate condition = builder.equal(root.get("id"), filter.getId());
//                p.add(condition);
//            }
//
//        }
//
//        return p;
//    }
//
//    @SuppressWarnings({"unchecked"})
//    private List<Predicate> findByModuleIdCondition(RoleDetailEntity filter, CriteriaBuilder builder, Root<RoleDetailEntity> root) {
//
//        if (builder == null) {
//            builder = super.builder;
//        }
//        if (root == null) {
//            root = super.root;
//        }
//
//        List<Predicate> p = new ArrayList<Predicate>();
//
//        if (filter != null) {
//            filter.setActiveStatus(1);
//            if (filter.getActiveStatus() > 0) {
//                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
//                p.add(condition);
//            }
//            if (filter.getMenuEntity()!=null) {
//                Predicate condition = builder.equal(root.get("menuEntity"), filter.getMenuEntity());
//                p.add(condition);
//            }
//        }
//
//        return p;
//    }
//
//    private void init() {
//        initEntityManagerBuilderCriteriaQueryRoot(RoleDetailEntity.class);
//        CriteriaBuilder builder = super.builder;
//        CriteriaQuery criteria = super.criteria;
//        Root root = super.root;
//    }
//
//    public Response findByName(String orgName) {
//        if (null == orgName) {
//            return getErrorResponse("please enter Module Name");
//        }
//
//        JSONObject json = new JSONObject(orgName);
//        String organizationName = Def.getString(json, "menuName");
//
//        RoleDetailEntity orgEntity = new RoleDetailEntity();
//
//        if (null == organizationName) {
//            return getErrorResponse("please enter  Organization Name");
//        }
//
//        Map<String, Object> searchFields = new HashMap<String, Object>();
//        searchFields.put("menuName", organizationName);
//
//        Response response = baseList(typedQuery(orgEntity, searchFields));
//
//        if (response.getItems() == null) {
//            return getErrorResponse("Data not found");
//        }
//        return response;
//    }
//
//    @SuppressWarnings({"rawtypes"})
//    private <T> TypedQuery typedQuery(RoleDetailEntity filter, Map<String, Object> fields) {
//        init();
//        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
//        List<Predicate> pDisJunction = basePredicate(fields);
//
//        return typedQuery(pConjunction, pDisJunction);
//    }
//}
