package com.mysoft.auth.role.grantRole;

import com.mysoft.core.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;


@Service
public class GrantRoleService {

    @Autowired
    private GrantRoleRepository grantRoleRepository;

    public Response save(String referal) {
        return grantRoleRepository.save(referal);
    }

    public Response list(String reqObj) {
        return grantRoleRepository.list(reqObj);
    }

    public Response update(String reqObj) {

        return grantRoleRepository.update(reqObj);
    }

    public Response delete(Long id) {
        return grantRoleRepository.remove(id);
    }

    public Response gridList(HttpServletRequest request) {
        return grantRoleRepository.gridList(request);
    }

    public Response findByUserNo(Long id) {
        return grantRoleRepository.findByUserNo(id);
    }

}
