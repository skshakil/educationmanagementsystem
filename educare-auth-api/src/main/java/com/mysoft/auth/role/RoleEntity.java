package com.mysoft.auth.role;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.mysoft.core.base.BaseOraEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "sa_role")
public class RoleEntity extends BaseOraEntity {

    @Id
    @NotNull
    @Column(name = "ROLE_NO")
    private Long id;

    @NotNull
    @Column(name = "ROLE_NAME")
    private String roleName;

    @Column(name = "ACTIVE_STAT")
    private Integer activeStatus=1;

    @Column(name = "SS_UPLOADED_ON")
    private Date ssUploadedOn;

    @Column(name = "DESCR")
    private String descr;

    @Column(name = "ROLE_ID")
    private String roleId;


	@Transient 
    List<Long> roleIds;
	
    public List<Long> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Long> roleIds) {
		this.roleIds = roleIds;
	}
}
