package com.mysoft.auth.role.grantRole;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sa_grantrole")
public class GrantRoleEntity extends BaseOraEntity {

    @Id
    @Column(name="GR_ROLE_NO")
    private Long id;

    @Column(name="USER_NO")
    private Long userNo;

    @Column(name="CAN_GRANT")
    private Long canGrant;

    @Column(name="GRANTOR")
    private Long grantor;

    @Column(name="GRANT_DT")
    private Date grantDt;

    @Column(name="ACTIVE_STAT")
    private Integer activeStatus=1;

    @Column(name="SS_UPLOADED_ON")
    private Date ssUploadedOn;

    @Column(name="ROLE_NO")
    private Long roleNo;

}
