package com.mysoft.auth.role;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Md. Jahurul Islam
 */
@Repository
@Transactional
public class RoleRepository extends AuthBaseRepository {

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Response gridList(HttpServletRequest request) {

        DataTableResults<RoleEntity> dataTableResults = null;
        Response response = new Response();
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setActiveStatus(1);

        DataTableRequest dataTableInRQ = new DataTableRequest(request);
        Long totalRowCount = totalCount(roleEntity);

        List gridList = new ArrayList<>();
        response = baseList(typedQuery(roleEntity, dataTableInRQ));

        if (response.isSuccess()) {
            if (response.getItems() != null) {
                gridList = response.getItems();
            }
            dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
        }
        response.setItems(null);
        response.setObj(dataTableResults);
        return response;
    }

    public Response list(String reqObj) {

        RoleEntity roleEntity = null;
        if (null != reqObj) {
            roleEntity = objectMapperReadValue(reqObj, RoleEntity.class);
        }
        return baseList(criteriaQuery(roleEntity));
    }

    public Response save(String reqObj, UserSignInContainter userDetails) {
        RoleEntity roleEntity = objectMapperReadValue(reqObj, RoleEntity.class);

        roleEntity.setId(roleSequenceNo());
        roleEntity.setSsCreator(userDetails.getUserId());
        roleEntity.setSsCreatedOn(new Date());
        roleEntity.setSsCreateSession(userDetails.getSessionNo());
        roleEntity.setCompanyNo(userDetails.getCompanyNo());

        return baseOnlySave(roleEntity);
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {
        RoleEntity roleEntity = objectMapperReadValue(reqObj, RoleEntity.class);
        RoleEntity obj = findById(roleEntity.getId());
        if (obj != null) {
            obj.setRoleName(roleEntity.getRoleName());
            obj.setRoleId(roleEntity.getRoleId());
            obj.setDescr(roleEntity.getDescr());

            obj.setSsModifier(userDetails.getUserId());
            obj.setSsModifiedOn(new Date());
            obj.setSsModifiedSession(userDetails.getSessionNo());

            return baseUpdate(obj);
        }

        return getErrorResponse("Record not Found !!");

    }

    public Response detele(Long id) {

        RoleEntity roleEntity = findById(id);
        if (roleEntity == null) {
            return getErrorResponse("Record not found!");
        }

        return baseDelete(roleEntity);

    }

    public Response remove(Long id) {
        RoleEntity roleEntity = findById(id);
        if (roleEntity == null) {
            return getErrorResponse("Record not found!");
        }
        roleEntity.setActiveStatus(0);
        return baseRemove(roleEntity);
    }

    public RoleEntity findById(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(id);
        roleEntity.setActiveStatus(1);
        Response response = baseFindById(criteriaQuery(roleEntity));
        if (response.isSuccess()) {

            return getValueFromObject(response.getObj(), RoleEntity.class);
        }
        return null;
    }

//    public Response findByModule(ModuleEntity id) {
//        RoleEntity roleEntity = new RoleEntity();
//        roleEntity.setMenuEntity(id);
//        return baseFindByParent(featureListCriteriaQuery(roleEntity));
//    }

    public Response find(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(id);
        roleEntity.setActiveStatus(1);
        return baseFindById(criteriaQuery(roleEntity));
    }

    // Non API
    @SuppressWarnings({"rawtypes", "unchecked"})
    private CriteriaQuery criteriaQuery(RoleEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(RoleEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }

        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);

        }

        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);

        }

        criteria.where(pArrayJoin.toArray(new Predicate[0]));

        return baseTypedQuery(criteria, dataTableInRQ);
    }

    private Long totalCount(RoleEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<RoleEntity> root = from(RoleEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> criteriaCondition(RoleEntity filter, CriteriaBuilder builder, Root<RoleEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {
            if (filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }

            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }

        }

        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(RoleEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }

    public Response findByName(String orgName) {
        if (null == orgName) {
            return getErrorResponse("please enter Module Name");
        }

        JSONObject json = new JSONObject(orgName);
        String organizationName = Def.getString(json, "menuName");

        RoleEntity orgEntity = new RoleEntity();

        if (null == organizationName) {
            return getErrorResponse("please enter  Organization Name");
        }

        Map<String, Object> searchFields = new HashMap<String, Object>();
        searchFields.put("menuName", organizationName);

        Response response = baseList(typedQuery(orgEntity, searchFields));

        if (response.getItems() == null) {
            return getErrorResponse("Data not found");
        }
        return response;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(RoleEntity filter, Map<String, Object> fields) {
        init();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = basePredicate(fields);

        return typedQuery(pConjunction, pDisJunction);
    }

}
