package com.mysoft.auth.role;

import com.mysoft.auth.base.AuthBaseController;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Md. Jahurul Islam
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/role")
public class RoleController extends AuthBaseController {
 
	@Autowired
	private RoleService roleService;

	
	
	@PreAuthorize("hasAuthority('WSA_1004')") // SUBMENU_ID
	@GetMapping("/gridList")
	public Response gridList(HttpServletRequest request){
		return roleService.gridList(request);
	}

	@PostMapping("/save")
	public Response save(@RequestBody String reqObj) {
		return roleService.save(reqObj, new UserSignInContainter(userDetails().getUserId(),userDetails().getCompanyNo(), userDetails().getSessionNo()));
	}

	@PutMapping("/update")
	public Response update(@RequestBody String reqObj) {
		return roleService.update(reqObj, new UserSignInContainter(userDetails().getUserId(),userDetails().getCompanyNo(), userDetails().getSessionNo()));
	}
	
	@PostMapping("/delete")
	public Response delete(@RequestBody Long id) {
		return roleService.delete(id);
	}

	@PostMapping("/list")
	public Response getAll(@RequestBody(required = false) String reqObj) {
		return roleService.list(reqObj);
	}
}
