package com.mysoft.auth.role;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Md. Jahurul Islam
 */
@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Response save(String reqObj, UserSignInContainter userDetails) {
        return roleRepository.save(reqObj, userDetails);
    }

    public Response list(String reqObj) {
        return roleRepository.list(reqObj);
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {

        return roleRepository.update(reqObj, userDetails);
    }

    public Response delete(Long id) {
        return roleRepository.remove(id);
    }

    public Response gridList(HttpServletRequest request) {
        return roleRepository.gridList(request);
    }

    public Response findByUserNo(Long id) {
        return roleRepository.find(id);
    }

//	public List<RoleEntity> findByIdList(List<Long> roleIds) {
//		return roleRepository.find(roleIds);
//	}

}
