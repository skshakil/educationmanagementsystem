package com.mysoft.auth.role.grantRole;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Md. Jahurul Islam
 */
@Repository
@Transactional
public class GrantRoleRepository extends AuthBaseRepository {

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Response gridList(HttpServletRequest request) {

        DataTableResults<GrantRoleEntity> dataTableResults = null;
        Response response = new Response();
        GrantRoleEntity grantRoleEntity = new GrantRoleEntity();
        grantRoleEntity.setActiveStatus(1);
        grantRoleEntity.setCompanyNo(userDetails().getCompanyNo());
        
        DataTableRequest dataTableInRQ = new DataTableRequest(request);
        Long totalRowCount = totalCount(grantRoleEntity);

        List gridList = new ArrayList<>();
        response = baseList(typedQuery(grantRoleEntity, dataTableInRQ));

        if (response.isSuccess()) {
            if (response.getItems() != null) {
                gridList = response.getItems();
            }
            dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
        }
        response.setItems(null);
        response.setObj(dataTableResults);
        return response;
    }

    public Response list(String reqObj) {

        GrantRoleEntity grantRoleEntity = null;
        if (null != reqObj) {
            grantRoleEntity = objectMapperReadValue(reqObj, GrantRoleEntity.class);
        }
        return baseList(criteriaQuery(grantRoleEntity));
    }

    public Response save(String reqObj) {
        GrantRoleEntity grantRoleEntity = objectMapperReadValue(reqObj, GrantRoleEntity.class);
//        grantRoleEntity.setId(featureSubmenuNo());
//        grantRoleEntity.setSsCreator(userDetails().getUserId());
//        grantRoleEntity.setSsCreatedOn(new Date());
//        grantRoleEntity.setSsCreateSession(userDetails().getSessionNo());
        grantRoleEntity.setCompanyNo(userDetails().getCompanyNo());
        return baseOnlySave(grantRoleEntity);
    }

    public Response update(String reqObj) {
        GrantRoleEntity grantRoleEntity = objectMapperReadValue(reqObj, GrantRoleEntity.class);
        GrantRoleEntity obj = findById(grantRoleEntity.getId());
        if (obj != null) {
//            grantRoleEntity.setSsCreator(userDetails().getUserId());
            grantRoleEntity.setCompanyNo(userDetails().getCompanyNo());
        	
            return baseUpdate(grantRoleEntity);
        }

        return getErrorResponse("Record not Found !!");

    }

    public Response detele(Long id) {

        GrantRoleEntity grantRoleEntity = findById(id);
        if (grantRoleEntity == null) {
            return getErrorResponse("Record not found!");
        }

        return baseDelete(grantRoleEntity);

    }

    public Response remove(Long id) {
        GrantRoleEntity grantRoleEntity = findById(id);
        if (grantRoleEntity == null) {
            return getErrorResponse("Record not found!");
        }
        grantRoleEntity.setActiveStatus(0);
        return baseRemove(grantRoleEntity);
    }

    public GrantRoleEntity findById(Long id) {
        GrantRoleEntity grantRoleEntity = new GrantRoleEntity();
        grantRoleEntity.setId(id);
        grantRoleEntity.setActiveStatus(1);
        Response response = baseFindById(criteriaQuery(grantRoleEntity));
        if (response.isSuccess()) {

            return getValueFromObject(response.getObj(), GrantRoleEntity.class);
        }
        return null;
    }

//    public Response findByModule(ModuleEntity id) {
//        GrantRoleEntity grantRoleEntity = new GrantRoleEntity();
//        grantRoleEntity.setMenuEntity(id);
//        return baseFindByParent(featureListCriteriaQuery(grantRoleEntity));
//    }

    public Response findByUserNo(Long id) {
        GrantRoleEntity grantRoleEntity = new GrantRoleEntity();
        grantRoleEntity.setUserNo(id);
        grantRoleEntity.setActiveStatus(1);
        return baseList(criteriaQuery(grantRoleEntity));
    }

    // Non API
    @SuppressWarnings({"rawtypes", "unchecked"})
    private CriteriaQuery criteriaQuery(GrantRoleEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(GrantRoleEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }

        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);

        }

        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);

        }

        criteria.where(pArrayJoin.toArray(new Predicate[0]));

        return baseTypedQuery(criteria, dataTableInRQ);
    }

    private Long totalCount(GrantRoleEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<GrantRoleEntity> root = from(GrantRoleEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> criteriaCondition(GrantRoleEntity filter, CriteriaBuilder builder, Root<GrantRoleEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {
            if (filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }

            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }

            if (filter.getUserNo() != null && filter.getUserNo() > 0) {
                Predicate condition = builder.equal(root.get("userNo"), filter.getUserNo());
                p.add(condition);
            }

        }

        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(GrantRoleEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }

    public Response findByName(String orgName) {
        if (null == orgName) {
            return getErrorResponse("please enter Module Name");
        }

        JSONObject json = new JSONObject(orgName);
        String organizationName = Def.getString(json, "menuName");

        GrantRoleEntity orgEntity = new GrantRoleEntity();

        if (null == organizationName) {
            return getErrorResponse("please enter  Organization Name");
        }

        Map<String, Object> searchFields = new HashMap<String, Object>();
        searchFields.put("menuName", organizationName);

        Response response = baseList(typedQuery(orgEntity, searchFields));

        if (response.getItems() == null) {
            return getErrorResponse("Data not found");
        }
        return response;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(GrantRoleEntity filter, Map<String, Object> fields) {
        init();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = basePredicate(fields);

        return typedQuery(pConjunction, pDisJunction);
    }

}
