package com.mysoft.auth.role.grantRole;

import com.mysoft.core.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Md. Jahurul Islam
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/role/grant")
public class GrantRoleController {
 
	@Autowired
	private GrantRoleService grantRoleService;

	@GetMapping("/gridList")
	public Response gridList(HttpServletRequest request){
		return grantRoleService.gridList(request);
	}

	@PostMapping("/save")
	public Response save(@RequestBody String reqObj) {
		return grantRoleService.save(reqObj);
	}

	@PutMapping("/update")
	public Response update(@RequestBody String reqObj) {
		return grantRoleService.update(reqObj);
	}
	
	@PostMapping("/delete")
	public Response delete(@RequestBody Long id) {
		return grantRoleService.delete(id);
	}

	@PostMapping("/list")
	public Response getAll(@RequestBody(required = false) String reqObj) {
		return grantRoleService.list(reqObj);
	}

	@GetMapping("/findByUserNo")
	public Response findByUserNo(@RequestParam Long id){
		return grantRoleService.findByUserNo(id);
	}
}
