package com.mysoft.auth.usersRole;

import com.mysoft.auth.role.RoleEntity;
import com.mysoft.auth.role.RoleService;
import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Md. Jahurul Islam
 *
 */
@Repository
@Transactional
public class UsersRoleRepository extends BaseRepository {

	@Autowired
	RoleService roleService;
	
	public List<UserRolesEntity> findByUserId(long userId) {
		
		Response response = new Response();
		
		UserRolesEntity userRolesEntity = new UserRolesEntity();
		UserRolesIdentity userRolesIdentity = new UserRolesIdentity();
		userRolesIdentity.setUserId(userId);
		userRolesEntity.setUserRolesIdentity(userRolesIdentity);

		List<UserRolesEntity> userRoles  = new ArrayList<UserRolesEntity>();
		
		response = baseList(criteriaQuery(userRolesEntity));
		
		if(!response.isSuccess()) {
			return null;
		}
		
		userRoles = getListFromObject(response.getItems(), UserRolesEntity.class);

		return userRoles;
	}
	
	

	public List<RoleEntity> findRoleByUserId(long userId) {
		List<Long> roleIds = new ArrayList<>();
		List<RoleEntity> roles = null;
		List<UserRolesEntity> userRolesList = findByUserId(userId);
		
		if (userRolesList.size() <= 0) {
			return roles;
		}
		for (UserRolesEntity userRoleIds : userRolesList) {
			roleIds.add(userRoleIds.getUserRolesIdentity().getRoleId());
		}

//		roles = roleService.findByIdList(roleIds);
		return roles;
	}
	
	public Response save(String reqObj) {
		UserRolesEntity userRolesEntity = objectMapperReadValue(reqObj, UserRolesEntity.class);
		return baseOnlySave(criteriaQuery(userRolesEntity));
	}

	public Response list(String reqObj) {
		UserRolesEntity userRolesEntity = objectMapperReadValue(reqObj, UserRolesEntity.class);
		return baseList(criteriaQuery(userRolesEntity));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(UserRolesEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "unchecked" })
    private List<Predicate> criteriaCondition(UserRolesEntity filter, CriteriaBuilder builder,
                                              Root<UserRolesEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			
			if (filter.getUserRolesIdentity().getUserId() > 0) {
				Predicate condition = builder.equal(root.get("userRolesIdentity").get("userId"), filter.getUserRolesIdentity().getUserId());
				p.add(condition);
			}

		}

		return p;
	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(UserRolesEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

	public Response update(String reqObj) {
		UserRolesEntity UserRolesEntity = objectMapperReadValue(reqObj, UserRolesEntity.class);
		return baseSaveOrUpdate(UserRolesEntity);
	}

	public UserRolesEntity findById(Long id) {
		UserRolesEntity UserRolesEntity = new UserRolesEntity();
//		UserRolesEntity.setId(id);
		Response response = baseFindById(criteriaQuery(UserRolesEntity));

		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), UserRolesEntity.class);
		}
		return null;
	}

	public Response delete(Long id) {
		UserRolesEntity UserRolesEntity = findById(id);
		return baseDelete(UserRolesEntity);
	}
}
