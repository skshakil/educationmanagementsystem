package com.mysoft.auth.usersRole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysoft.core.util.Response;

/**
 * @author Md. Jahurul Islam
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/usersRole")
public class UsersRoleController {

	@Autowired
	UsersRoleService roleService;

	@PostMapping("/save")
	public Response save(@RequestBody String reqObj) {
		return roleService.save(reqObj);
	}

	@PutMapping("/update")
	public Response update(@RequestBody String reqObj) {
		return roleService.update(reqObj);
	}

	@PostMapping("/delete")
	public Response delete(@RequestBody Long id) {
		return roleService.delete(id);
	}

	@PostMapping("/list")
	public Response getAll(@RequestBody(required = false) String reqObj) {
		return roleService.list(reqObj);
	}
}
