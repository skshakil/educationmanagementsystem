package com.mysoft.auth.usersRole;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class UserRolesIdentity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "user_id", nullable = false)
	private long userId;
	@Column(name = "role_id", nullable = false)
	private long roleId;
	
    @Override
    public int hashCode() {
        return Objects.hashCode(userId);
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserRolesIdentity other = (UserRolesIdentity) obj;
        return Objects.equals(userId, other.getUserId());
    }
	


}
