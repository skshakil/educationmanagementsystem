package com.mysoft.auth.usersRole;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
@Table(name = "user_roles")
public class UserRolesEntity implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = -4180377856186324154L;
	@EmbeddedId
	 private UserRolesIdentity userRolesIdentity;
}
