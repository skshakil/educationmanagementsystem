package com.mysoft.auth.usersRole;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.auth.role.RoleEntity;
import com.mysoft.core.util.Response;



/**
 * @author Md. Jahurul Islam
 *
 */
@Service
public class UsersRoleService {
	@Autowired
	UsersRoleRepository uersRoleRepository;


	public List<UserRolesEntity> findByUserId(long userId) {
		return uersRoleRepository.findByUserId(userId);
	}

	public List<RoleEntity> findRoleByUserId(long userId) {
		return uersRoleRepository.findRoleByUserId(userId);
	}
	public Response save(String reqObj) {
		return uersRoleRepository.save(reqObj);
	}

	public Response list(String reqObj) {
		return uersRoleRepository.list(reqObj);
	}

	public Response update(String reqObj) {

		return uersRoleRepository.update(reqObj);
	}

	public Response delete(Long id) {
		return uersRoleRepository.delete(id);
	}

}
