package com.mysoft.auth.module;


import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.auth.feature.CoreFeatureRepository;
import com.mysoft.auth.feature.CoreFeatureService;
import com.mysoft.auth.feature.FeatureEntity;
import com.mysoft.auth.userFeature.UserFeatureEntity;
import com.mysoft.auth.userFeature.UserFeatureService;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.user.CoreUserEntity;
import com.mysoft.core.user.CoreUserRepository;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.ModuleTree;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@Transactional
public class CoreModuleRepository extends AuthBaseRepository {

    @Autowired
    CoreFeatureRepository coreFeatureRepository;
    @Autowired
    UserFeatureService userFeatureService;
    @Autowired
    CoreFeatureService coreFeatureService;
    @Autowired
    CoreUserRepository coreUserRepository;


    List<ModuleTreeFeatureDto> moduleTreeFeatureDtoList = null;

    public Response list(String reqObj) {

        ModuleEntity moduleEntity = null;
        if (null != reqObj) {
            moduleEntity = objectMapperReadValue(reqObj, ModuleEntity.class);
        }
        return baseList(criteriaQuery(moduleEntity));
    }

    public Response findSubModule(UserSignInContainter userDetails) {

        ModuleEntity moduleEntity = new ModuleEntity();
        moduleEntity.setActiveStatus(1);
        moduleEntity.setCompanyNo(userDetails.getCompanyNo());
        moduleEntity.setParentNotNull(true);

//        moduleEntity = objectMapperReadValue(reqObj, ModuleEntity.class);

        return baseList(criteriaQuery(moduleEntity));
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Response gridList(HttpServletRequest request) {

        DataTableResults<ModuleEntity> dataTableResults = null;
        Response response = new Response();
        ModuleEntity moduleEntity = new ModuleEntity();
        moduleEntity.setActiveStatus(1);
        moduleEntity.setCompanyNo(userDetails().getCompanyNo());

        DataTableRequest dataTableInRQ = new DataTableRequest(request);
        Long totalRowCount = totalCount(moduleEntity);

        List gridList = new ArrayList<>();
        response = baseList(typedQuery(moduleEntity, dataTableInRQ));

        if (response.isSuccess()) {
            if (response.getItems() != null) {
                gridList = response.getItems();
            }
            dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
        }

        response.setItems(null);
        response.setObj(dataTableResults);
        return response;
    }

    private Map<Long, ModuleTree> createTree(List<ModuleTree> modules) {

        Map<Long, ModuleTree> mapTmp = new HashMap<>();

        //Save all nodes to a map
        for (ModuleTree current : modules) {
            mapTmp.put(current.getChildId(), current);
        }

        //loop and assign parent/child relationships
        for (ModuleTree current : modules) {

            Long parentId = current.getParentId();

            if (parentId != null) {

                ModuleTree parent = mapTmp.get(parentId);

                if (parent != null) {

                    current.setParent(parent);
                    parent.addChild(current);

                    mapTmp.put(parentId, parent);
                    mapTmp.put(current.getChildId(), current);
                }
            }

        }

        return mapTmp;

    }

    private <T> void findBottom(ModuleTree<T> node, String appender, List<UserFeatureEntity> userGrantedFeatureList) {

        //System.out.println(appender + node.getDisplayValue() + " - " + node.getChildId());
        // System.out.println(appender + node.getChildren().size());

        ModuleTreeFeatureDto moduleTreeFeatureDto = new ModuleTreeFeatureDto();
        moduleTreeFeatureDto.setId(node.getChildId());
        moduleTreeFeatureDto.setPrentId(node.getParentId());
        moduleTreeFeatureDto.setDisplayName(node.getDisplayValue());

        if (node.getChildId() > 0) {

            moduleTreeFeatureDto.setFeatureDtosList(findFeatures(node.getChildId(), userGrantedFeatureList));
        }

//        if (node.getChildren().size() == 0) {
//
//            moduleTreeFeatureDto.setFeatureDtosList(findFeatures(node.getChildId(), userGrantedFeatureList));
//        }

        moduleTreeFeatureDtoList.add(moduleTreeFeatureDto);

        node.getChildren().forEach(each -> findBottom(each, appender + appender, userGrantedFeatureList));
    }

    public Response findUserContentAccess() {

        List<UserFeatureEntity> userGrantedFeatureList = userFeatureService.gratedFeatures(userDetails().getUserId());
        FeatureEntity featureEntity = coreFeatureService.findByContentAccessType();

        Response response = new Response();
        response.setItems(findAccessFeatures(featureEntity, userGrantedFeatureList));
        return response;
    }

    public Response findUserContentAccessList() {

        List<UserFeatureEntity> userGrantedFeatureList = userFeatureService.gratedFeatures(userDetails().getUserId());
        Response featureEntity = coreFeatureService.findByContentAccessTypeList();

        List<FeatureDto> featureDtoList = new ArrayList<FeatureDto>();

        Response response = new Response();
        if (null != featureEntity.getItems()) {
            for (int i = 0; i < featureEntity.getItems().size(); i++) {
                FeatureEntity featureEntitys = (FeatureEntity) featureEntity.getItems().get(i);
                featureDtoList.add(findAccessFeaturesObj(featureEntitys, userGrantedFeatureList));
            }
        }
        response.setItems(featureDtoList);
        return response;
    }


    private List<FeatureDto> findAccessFeatures(FeatureEntity feature, List<UserFeatureEntity> userGrantedFeatureList) {

        List<FeatureDto> featureList = new ArrayList<FeatureDto>();

        if (feature != null) {

            if (userGrantedFeatureList != null && userGrantedFeatureList.size() > 0) {
                feature.setGranted(findUserGrandFeatues(userGrantedFeatureList, feature));
            }

            FeatureDto featureDto = new FeatureDto();
            featureDto.setFeatureId(feature.getId());
            featureDto.setFeatureCode(feature.getSubmenuId());

            featureDto.setFeatureName(feature.getSubmenuName());
            featureDto.setIsGranted(feature.isGranted());
            featureDto.setUpdatedStatus(feature.isGranted());
            featureList.add(featureDto);

            return featureList;
        }
        return null;

    }

    private FeatureDto findAccessFeaturesObj(FeatureEntity feature, List<UserFeatureEntity> userGrantedFeatureList) {

        List<FeatureDto> featureList = new ArrayList<FeatureDto>();

        if (feature != null) {
            if (userGrantedFeatureList != null && userGrantedFeatureList.size() > 0) {
                feature.setGranted(findUserGrandFeatues(userGrantedFeatureList, feature));
            }
            FeatureDto featureDto = new FeatureDto();
            featureDto.setFeatureId(feature.getId());
            featureDto.setFeatureCode(feature.getSubmenuId());

            featureDto.setFeatureName(feature.getSubmenuName());
            featureDto.setIsGranted(feature.isGranted());
            featureDto.setUpdatedStatus(feature.isGranted());

            return featureDto;
        }
        return null;

    }


    private List<FeatureDto> findFeatures(Long bottomId, List<UserFeatureEntity> userGrantedFeatureList) {

        List<FeatureEntity> featureEntityList = null;
        List<FeatureEntity> featureListBysubModuleId = null;

        List<Long> ids = new ArrayList<>();
        ids.add(bottomId);

        Response responseFeature = coreFeatureService.findByIds(ids);

        if (responseFeature.isSuccess() && responseFeature.getItems() != null) {

            featureEntityList = getListFromObject(responseFeature.getItems(), FeatureEntity.class);
        }


        featureListBysubModuleId = findSubMenu(featureEntityList, bottomId);

        if (featureListBysubModuleId != null && featureListBysubModuleId.size() > 0) {

            List<FeatureDto> featureList = new ArrayList<FeatureDto>();

            for (FeatureEntity feature : featureListBysubModuleId) {

                if (userGrantedFeatureList != null && userGrantedFeatureList.size() > 0) {
                    feature.setGranted(findUserGrandFeatues(userGrantedFeatureList, feature));
                }

                FeatureDto featureDto = new FeatureDto();
                featureDto.setFeatureId(feature.getId());
                featureDto.setFeatureName(feature.getSubmenuName());
                featureDto.setIsGranted(feature.isGranted());
                featureDto.setUpdatedStatus(feature.isGranted());
                featureList.add(featureDto);

            }

            return featureList;
        }
        return null;
    }

    public List<Long> findInactive() {
        ModuleEntity moduleEntity = new ModuleEntity();
        moduleEntity.setActiveStatus(0);
//        moduleEntity.setCompanyNo(userDetails().getCompanyNo());
        //moduleEntity.setFindParentOnly(true);

        Response response = baseList(findParentNo(moduleEntity, "id"));
        List<Long> moduleList = null;
        if (response.isSuccess() && response.getItems() != null) {
            moduleList = getListFromObject(response.getItems(), Long.class);
        }
        return moduleList;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private CriteriaQuery findParentNo(ModuleEntity filter, String columnName) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }

        criteria.select(root.get(columnName));
        return criteria;
    }


    @SuppressWarnings({"unchecked", "rawtypes"})
    public Response findParentWithSubModules(String reqObj) {

    	
        moduleTreeFeatureDtoList = new ArrayList<>();

        List<UserFeatureEntity> userGrantedFeatureList = null;

        if (reqObj != null && !reqObj.isEmpty()) {
        	
        	CoreUserEntity userEntity = objectMapperReadValue(reqObj, CoreUserEntity.class);
        	userEntity = coreUserRepository.findByIdReadOnly(userEntity.getId());
//            userEntity.setCompanyNo(userEntity.getCompanyNo());
            userGrantedFeatureList = userFeatureService.gratedFeatures(userEntity.getId());
        }

        List<Long> moduNoList = findInactive();


        // List<ModuleDto> moduleAndFeatures = new ArrayList<ModuleDto>();
        ModuleEntity moduleEntity = new ModuleEntity();
        if (moduNoList != null) {
            moduleEntity.setMenuNoNotInList(moduNoList);
        }
        moduleEntity.setActiveStatus(1);
//        moduleEntity.setCompanyNo(userEntity.getCompanyNo());

        List<ModuleEntity> moduleList = null;
        List<ModuleTree> modules = new ArrayList<>();

        Response response = baseList(criteriaQuery(moduleEntity));

        if (response.isSuccess() && response.getItems() != null) {
            moduleList = getListFromObject(response.getItems(), ModuleEntity.class);
            for (ModuleEntity mod : moduleList) {
                modules.add(new ModuleTree(mod.getMenuName(), mod.getId(), mod.getParentMenuNo()));
            }
        }

        Map<Long, ModuleTree> mapTmp = createTree(modules);

        for (ModuleTree node : mapTmp.values()) {
            if (node.getParent() == null) {
                findBottom(node, " ", userGrantedFeatureList);
            }
        }

        response.setItems(moduleTreeFeatureDtoList);
        return response;
    }


    public Response findParentWithSubModules1(String reqObj) {

        List<UserFeatureEntity> userGrantedFeatureList = null;

        // Long moduleNo = null;

        if (reqObj != null && !reqObj.isEmpty()) {

            JSONObject json = new JSONObject(reqObj);

            //moduleNo = Def.getLong(json, "moduleId");
        }

        if (reqObj != null && !reqObj.isEmpty()) {
            CoreUserEntity userEntity = objectMapperReadValue(reqObj, CoreUserEntity.class);
            userGrantedFeatureList = userFeatureService.gratedFeatures(userEntity.getId());
        }

        List<ModuleDto> moduleAndFeatures = new ArrayList<ModuleDto>();
        ModuleEntity moduleEntity = new ModuleEntity();
        moduleEntity.setActiveStatus(1);

//		if(moduleNo !=null && moduleNo > 0) {
//			moduleEntity.setId(1001L);
//		}

        moduleEntity.setFindParentOnly(true);

        List<ModuleEntity> moduleList = getListFromObject(baseList(criteriaQuery(moduleEntity)).getItems(), ModuleEntity.class);

        for (ModuleEntity mod : moduleList) {

            ModuleEntity subModule = new ModuleEntity();

            subModule.setParentMenuNo(mod.getId());
            subModule.setActiveStatus(1);

            List<ModuleEntity> subModuleList = getListFromObject(baseList(criteriaQuery(subModule)).getItems(), ModuleEntity.class);

            if (subModuleList != null) {

                List<Long> subModuleIds = subModuleList.stream().map(ModuleEntity::getId).collect(Collectors.toList());

                List<FeatureEntity> featureEntityList = null;
                List<FeatureEntity> featureListBysubModuleId = null;

                Response responseFeature = coreFeatureService.findByIds(subModuleIds);

                if (responseFeature.isSuccess() && responseFeature.getItems() != null) {

                    featureEntityList = getListFromObject(responseFeature.getItems(), FeatureEntity.class);
                }

                ModuleDto moduleDto = new ModuleDto();
                moduleDto.setModuleId(mod.getId());
                moduleDto.setModuleName(mod.getMenuName());

                List<SubModuleDto> subModuleDtoList = new ArrayList<SubModuleDto>();

                for (ModuleEntity sm : subModuleList) {

                    SubModuleDto subModuleDto = new SubModuleDto();
                    subModuleDto.setSubModuleId(sm.getId());
                    subModuleDto.setSubModuleName(sm.getMenuName());

                    featureListBysubModuleId = findSubMenu(featureEntityList, sm.getId());

                    if (featureListBysubModuleId != null && featureListBysubModuleId.size() > 0) {

                        List<FeatureDto> featureList = new ArrayList<FeatureDto>();

                        for (FeatureEntity feature : featureListBysubModuleId) {

                            if (userGrantedFeatureList != null && userGrantedFeatureList.size() > 0) {
                                feature.setGranted(findUserGrandFeatues(userGrantedFeatureList, feature));
                            }

                            FeatureDto featureDto = new FeatureDto();
                            featureDto.setFeatureId(feature.getId());
                            featureDto.setFeatureName(feature.getSubmenuName());
                            featureDto.setIsGranted(feature.isGranted());
                            featureDto.setUpdatedStatus(feature.isGranted());
                            featureList.add(featureDto);

                            subModuleDto.setFeatureDtoList(featureList);
                        }
                    }

                    subModuleDtoList.add(subModuleDto);
                }

                moduleDto.setSubModuleDtoList(subModuleDtoList);
                moduleAndFeatures.add(moduleDto);
            }

        }

        Response response = new Response();
        response.setItems(moduleAndFeatures);
        return response;
    }


    public List<FeatureEntity> findSubMenu(List<FeatureEntity> featureEntityList, Long suModuleId) {

        List<FeatureEntity> featureEntities = new ArrayList<FeatureEntity>();
        if (featureEntityList != null && suModuleId != null) {

            for (FeatureEntity uf : featureEntityList) {

                if (uf != null && uf.getMenuEntity() != null && uf.getMenuEntity().getId() != null) {

                    if (uf.getMenuEntity().getId().longValue() == suModuleId.longValue()) {

                        featureEntities.add(uf);
                    }

                }

            }
        }
        return featureEntities;
//        return featureEntityList.stream().filter(uf -> uf.getMenuEntity().getId().longValue() == suModuleId.longValue()).collect(Collectors.toList());

    }


    public Boolean findUserGrandFeatues(List<UserFeatureEntity> userGrantedFeatureList, FeatureEntity feature) {

        if (userGrantedFeatureList != null && userGrantedFeatureList.size() > 0 && feature != null && feature.getId() > 0) {
            for (UserFeatureEntity uf : userGrantedFeatureList) {
                if (uf != null && uf.getFeature() != null && uf.getFeature().getId() > 0) {
                    if (uf.getFeature().getId().longValue() == feature.getId().longValue()) {
                        return true;
                    }
                }

            }

        }

//        UserFeatureEntity userFeatureEntity = userGrantedFeatureList.stream().filter(uf -> uf.getFeature().getId().longValue() == feature.getId().longValue()).findAny().orElse(null);
//        if (userFeatureEntity == null) {
//            return false;
//        }
        return false;
    }


    public Response save(String reqObj, UserSignInContainter userDetails) {
        ModuleEntity moduleEntity = objectMapperReadValue(reqObj, ModuleEntity.class);

        moduleEntity.setId(functionFdAutoNo(userDetails.getOrganizationNo(),userDetails.getCompanyNo(),"sa_menu","MENU_NO",6l));    //moduleSequenceNo()
        moduleEntity.setSsCreator(userDetails.getUserId());
        moduleEntity.setSsCreatedOn(new Date());
        moduleEntity.setSsCreateSession(userDetails.getSessionNo());
        moduleEntity.setCompanyNo(userDetails.getCompanyNo());
        moduleEntity.setOrganizationNo(userDetails.getOrganizationNo());

        return baseOnlySave(moduleEntity);
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {

        ModuleEntity moduleEntity = objectMapperReadValue(reqObj, ModuleEntity.class);
        ModuleEntity obj = findById(moduleEntity.getId());
        if (obj != null) {

            obj.setMenuName(moduleEntity.getMenuName());
            obj.setParentMenuNo(moduleEntity.getParentMenuNo());
            obj.setMenuIcon(moduleEntity.getMenuIcon());

            obj.setSsModifier(userDetails.getUserId());
            obj.setSsModifiedOn(new Date());
            obj.setSsModifiedSession(userDetails.getSessionNo());

            return baseUpdate(obj);
        }

        return getErrorResponse("Record not Found !!");

    }

    public Response detele(Long id) {

        ModuleEntity moduleEntity = findById(id);
        if (moduleEntity == null) {
            return getErrorResponse("Record not found!");
        }
        return baseDelete(moduleEntity);

    }

    public Response remove(Long id) {
        ModuleEntity moduleEntity = findById(id);

        if (moduleEntity == null || moduleEntity.getActiveStatus() < 1) {
            return getErrorResponse("Record not found!");
        } else {
            Response childListResponse = findChild(id);
            if (childListResponse.getItems() != null) {
                return getErrorResponse("Features under Module Found, Please delete them before deleting this Module");
            } else {
                moduleEntity.setActiveStatus(0);
                return getSuccessResponse("Module removed successfully");
            }
        }

    }

    public ModuleEntity findById(Long id) {
        ModuleEntity moduleEntity = new ModuleEntity();
        moduleEntity.setId(id);
        Response response = baseFindById(criteriaQuery(moduleEntity));
        if (response.isSuccess()) {
            return getValueFromObject(response.getObj(), ModuleEntity.class);
        }
        return null;
    }

    public Response findChild(Long obj) {
        ModuleEntity parent = findById(obj);
        return coreFeatureRepository.findByModule(parent);
    }


    // Non API
    @SuppressWarnings({"rawtypes", "unchecked"})
    private CriteriaQuery criteriaQuery(ModuleEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(ModuleEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ,ModuleEntity.class);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }

        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);

        }

        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);

        }

        criteria.where(pArrayJoin.toArray(new Predicate[0]));

        return baseTypedQuery(criteria, dataTableInRQ);
    }

    private Long totalCount(ModuleEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<ModuleEntity> root = from(ModuleEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> criteriaCondition(ModuleEntity filter, CriteriaBuilder builder, Root<ModuleEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {

            if (filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }

            if (filter.getActiveStatus() <= 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }

            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }
//            if (filter.getCompanyNo() != null) {
//                Predicate condition = builder.equal(root.get("companyNo"), filter.getCompanyNo());
//                p.add(condition);
//            }

            if (filter.isParentNotNull()) {
                Predicate condition = builder.isNotNull(root.get("parentMenuNo"));
                p.add(condition);
            }

            if (filter.isFindParentOnly()) {
                Predicate condition = builder.isNull(root.get("parentMenuNo"));
                p.add(condition);
            }
            if (filter.getParentMenuNo() != null && filter.getParentMenuNo() > 0) {
                Predicate condition = builder.equal(root.get("parentMenuNo"), filter.getParentMenuNo());
                p.add(condition);
            }

            if (filter.getMenuNoNotInList() != null && filter.getMenuNoNotInList().size() > 0) {

                Expression<Object> exp = builder.selectCase().when(builder.isNull(root.get("parentMenuNo")), 0L).otherwise((root.get("parentMenuNo")));

                Predicate condition1 = builder.not(exp.in((filter.getMenuNoNotInList())));
                p.add(condition1);
            }
        }

        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(ModuleEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }

    public Response findByName(String orgName) {
        if (null == orgName) {
            return getErrorResponse("please enter Module Name");
        }

        JSONObject json = new JSONObject(orgName);
        String organizationName = Def.getString(json, "menuName");

        ModuleEntity orgEntity = new ModuleEntity();

        if (null == organizationName) {
            return getErrorResponse("please enter  Organization Name");
        }

        Map<String, Object> searchFields = new HashMap<String, Object>();
        searchFields.put("menuName", organizationName);

        Response response = baseList(typedQuery(orgEntity, searchFields));

        if (response.getItems() == null) {
            return getErrorResponse("Data not found");
        }
        return response;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(ModuleEntity filter, Map<String, Object> fields) {
        init();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = basePredicate(fields);

        return typedQuery(pConjunction, pDisJunction);
    }
}
