package com.mysoft.auth.module;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubModuleDto {

	Long subModuleId;
	String subModuleName;
	List<FeatureDto> featureDtoList;
	
}
