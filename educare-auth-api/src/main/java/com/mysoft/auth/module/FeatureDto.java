package com.mysoft.auth.module;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeatureDto {

	Long featureId;
	String featureCode;
	String featureName;
	Boolean isGranted;
	Boolean updatedStatus;
}
