package com.mysoft.auth.module;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ModuleTreeFeatureDto {

    Long id;
    Long prentId;
    String displayName;
    List<FeatureDto> featureDtosList;


}
