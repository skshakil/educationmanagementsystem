package com.mysoft.auth.module;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "sa_menu")
public class ModuleEntity extends BaseOraEntity {

	@Id
    @Column(name = "MENU_NO")
    private Long id;

    @NotNull
    @Column(name = "ACTIVE_STAT")
    private Integer activeStatus = 1;

    @NotNull
    @Column(name = "MENU_NAME")
    private String menuName;

    @Column(name = "MENU_LINK")
    private String menuLink;

    @Column(name = "MENU_ICON")
    private String menuIcon;

    @Column(name = "PARENT_MENU_NO")
    private Long parentMenuNo;

    @Column(name = "SL_NO")
    private Long slNo;

    @Column(name = "MENU_NAME_NLS")
    private String menuNameNls;

    // added transient property to manage filtering for finding SubModules
    @Transient
    private List<Long> menuNoNotInList;
    
    @Transient
    private boolean parentNotNull = false;

    @Transient
    private boolean findParentOnly = false;

    @Transient
    private List subModuleList;
    @Transient
    private List featureList;
    
    @Transient
    private String moduleNameAndId;
    
    public String getModuleNameAndId() {
    	
		return  getMenuName() +" ("+getId()+")";
	}

	public void setModuleNameAndId(String moduleNameAndId) {
		this.moduleNameAndId = moduleNameAndId;
	}


}
