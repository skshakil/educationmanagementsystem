package com.mysoft.auth.module;

import com.mysoft.auth.base.AuthBaseController;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/module")
public class ModuleController extends AuthBaseController {
	
	@Autowired
	private CoreModuleService moduleService;

	
	@PreAuthorize("hasAuthority('WJAVA_MS')") // SUBMENU_ID
	@GetMapping("/gridList")
	public Response gridList(HttpServletRequest reqObj) {
		return moduleService.gridList(reqObj);
	}

	@PostMapping("/module-feature-list")
	public Response list(@RequestBody (required = false) String reqObj ) {
		return moduleService.findParentWithSubModules(reqObj);
	}
	
	@PostMapping("/find-user-content-access")
	public Response findUserContentAccess() {
		return moduleService.findUserContentAccess();
	}

    @PostMapping("/find-user-content-access-list")
    public Response findUserContentAccessList() {
        return moduleService.findUserContentAccessList();
    }

	@GetMapping("/subModuleList")
	public Response getAll() {
		return moduleService.subModuleList(new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(),userDetails().getSessionNo(),userDetails().getUserTypeNo()));
	}

	@PostMapping("/create")
	public Response create(@RequestBody String reqObj) {
		return moduleService.save(reqObj, new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(),userDetails().getSessionNo(),userDetails().getUserTypeNo()));
	}

	@PutMapping("/update")
	public Response update(@RequestBody String reqObj) {
		return moduleService.update(reqObj, new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(),userDetails().getSessionNo(),userDetails().getUserTypeNo()));
	}

	@DeleteMapping("/delete")
	public Response delete(@RequestParam("id") long reqId) {
		return moduleService.delete(reqId);
	}

	@DeleteMapping("/remove")
	public Response remove(@RequestParam("id") long reqId) {
		return moduleService.remove(reqId);
	}

	@PostMapping("/findByName")
	public Response findByName(@RequestBody String orgName){
		return moduleService.findByName(orgName);
	}

//	@PostMapping("/findChild")
//	public Response findChild(@RequestBody String obj){
//		return moduleService.findModule(obj);
//	}
}
