package com.mysoft.auth.module;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;


@Service
public class CoreModuleService {

    @Autowired
    private CoreModuleRepository moduleRepository;

    public Response gridList(HttpServletRequest request) {
        return moduleRepository.gridList(request);
    }

    public Response subModuleList(UserSignInContainter userDetails) {
        return moduleRepository.findSubModule(userDetails);
    }

    public Response save(String reqObj, UserSignInContainter userDetails) {
        return moduleRepository.save(reqObj, userDetails);
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {
        return moduleRepository.update(reqObj, userDetails);
    }

    public Response delete(Long id) {
        return moduleRepository.detele(id);
    }

    public Response remove(Long id) {
        return moduleRepository.remove(id);
    }

    public Response findByName(String orgName) {
        return moduleRepository.findByName(orgName);
    }

    public Response findParentWithSubModules(String reqObj) {
        return moduleRepository.findParentWithSubModules(reqObj);
    }
    public Response findUserContentAccess() {
    	return moduleRepository.findUserContentAccess();
    }

    public Response findUserContentAccessList() {
        return moduleRepository.findUserContentAccessList();
    }

//    public Response findModule(String obj) {
//		return moduleRepository.findChild(obj);
//    }
}
