package com.mysoft.auth.module;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModuleDto {

	Long moduleId;
	String moduleName;
	List<SubModuleDto> subModuleDtoList;
	
	
}
