package com.mysoft.auth.userFeature;

import com.mysoft.auth.feature.FeatureEntity;
import com.mysoft.core.base.BaseOraEntity;
import com.mysoft.core.user.CoreUserEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sa_grantobject")
public class UserFeatureEntity extends BaseOraEntity {

    @Id
    @Column(name="GR_OBJECT_NO")
    private Long id;

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "SUBMENU_NO", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private FeatureEntity feature;

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "USER_NO", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private CoreUserEntity user;

    @Column(name="CAN_VIEW")
    private Long canView;

    @Column(name="CAN_MODIFY")
    private Long canModify;

    @Column(name="CAN_REMOVE")
    private Long canRemove;

    @Column(name="CAN_SHARE")
    private Long canShare;

    @Column(name="CAN_CREATE")
    private Long canCreate;

    @Column(name="GRANT_DT")
    private Date grantDt;

//    @Column(name="ROLE_NO")
//    private Long roleNo;

}
