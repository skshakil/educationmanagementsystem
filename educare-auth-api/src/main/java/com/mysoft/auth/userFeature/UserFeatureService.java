package com.mysoft.auth.userFeature;

import com.mysoft.core.user.CoreUserEntity;
import com.mysoft.core.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Service
public class UserFeatureService {
	
	@Autowired
	private UserFeatureRepository userFeatureRepository;
	
	public Response gridList(HttpServletRequest request) {
		return userFeatureRepository.gridList(request);
	}

	public Response list(String reqObj) {
		return userFeatureRepository.list(reqObj);
	}
	public Response findUserContentAccess() {
		return userFeatureRepository.findUserContentAccess();
	}

	public Response save(UserFeatureEntity reqObj) {
		return userFeatureRepository.save(reqObj);
	}

	public Response save(List feature, CoreUserEntity user) {
		return userFeatureRepository.save(feature, user);
	}

	public Response update(String reqObj) {
		return userFeatureRepository.update(reqObj);
	}

	public Response delete(UserFeatureEntity reqObj) {
		return userFeatureRepository.detele(reqObj);
	}

	public Response delete(Long reqObj) {
		return userFeatureRepository.detele(reqObj);
	}

	public Response delete(Long userNo, Long featureNo) {
		return userFeatureRepository.deteleByFeatureIdUserId(userNo, featureNo);
	}

	public Response remove(Long id) {
		return userFeatureRepository.remove(id);
	}


    public List<UserFeatureEntity> gratedFeatures(Long userNo) {
		return userFeatureRepository.gratedFeatureByUserNo(userNo);
    }
}
