package com.mysoft.auth.userFeature;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.auth.feature.CoreFeatureService;
import com.mysoft.auth.feature.FeatureEntity;
import com.mysoft.auth.user.UserService;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.user.CoreUserEntity;
import com.mysoft.core.user.CoreUserRepository;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class UserFeatureRepository extends AuthBaseRepository {

    @Autowired
    CoreUserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    CoreFeatureService featureService;

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Response gridList(HttpServletRequest request) {

        DataTableResults<UserFeatureEntity> dataTableResults = null;
        Response response = new Response();
        UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
        userFeatureEntity.setCompanyNo(userDetails().getCompanyNo());

        DataTableRequest dataTableInRQ = new DataTableRequest(request);
        Long totalRowCount = totalCount(userFeatureEntity);

        List gridList = new ArrayList<>();
        response = baseList(typedQuery(userFeatureEntity, dataTableInRQ));

        if (response.isSuccess()) {
            if (response.getItems() != null) {
                gridList = response.getItems();
            }
            dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
        }
        response.setItems(null);
        response.setObj(dataTableResults);
        return response;
    }

    public Response list(String reqObj) {

        // @SuppressWarnings("unused")
        // Map<String, Object> additionalInfo = tokenServices.getAccessToken(authentication()).getAdditionalInformation();

        UserFeatureEntity userFeatureEntity = null;

        if (null != reqObj) {
        	userFeatureEntity = objectMapperReadValue(reqObj, UserFeatureEntity.class);
        }

        return baseList(criteriaQuery(userFeatureEntity));
    }
    public Response findUserContentAccess() {
    	
    	UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
    	CoreUserEntity coreUserEntity = new CoreUserEntity();
    	coreUserEntity.setId(userDetails().getUserId());
    	userFeatureEntity.setUser(coreUserEntity);
    	
    	return baseList(criteriaQuery(userFeatureEntity));
    }

    public Response save(UserFeatureEntity reqObj) {
        return baseOnlySave(reqObj);
//        JSONObject json = new JSONObject(reqObj);
//        Long userNo = Def.getLong(json, "userNo");
//        String features = Def.getString(json, "features");
//
//        List<FeatureDto> featureList = objectMapperReadArrayValue(features, FeatureDto.class);
//        CoreUserEntity user = new CoreUserEntity();
//        user.setId(userNo);
//
//        Response response = null;
//        for (FeatureDto featureDto : featureList) {
//            FeatureEntity featureEntity = new FeatureEntity();
//            featureEntity.setId(featureDto.getFeatureId());
//
//            if (!featureDto.getIsGranted() && featureDto.getUpdatedStatus()) {
//                UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
//                userFeatureEntity.setId(grantObjectSequenceNo());
//                userFeatureEntity.setUser(user);
//                userFeatureEntity.setFeature(featureEntity);
//                response = baseOnlySave(userFeatureEntity);
//            }
//
//
//        }
//
//        if (!response.isSuccess()) {
//            getErrorResponse("Something went wrong");
//        }
//
//        return response;
    }

    public Response save(List feature, CoreUserEntity user) {
        List<UserFeatureEntity> grantedFeatureList = getListFromObject(feature, UserFeatureEntity.class);
        Response response = null;
        for (UserFeatureEntity singleFeature : grantedFeatureList) {

            singleFeature.setUser(user);
            singleFeature.setId(grantObjectSequenceNo());
            singleFeature.setSsCreatedOn(new Date());
            singleFeature.setSsCreator(userDetails().getUserId());
            singleFeature.setSsCreateSession(userDetails().getSessionNo());
            singleFeature.setCompanyNo(userDetails().getCompanyNo());
            response = baseOnlySave(singleFeature);
            if (!response.isSuccess()) {
                getErrorResponse("Failed to grant feature " + singleFeature);
            }
        }

        return response;
    }

    public Response update(String reqObj) {

        UserFeatureEntity employeeEntity = objectMapperReadValue(reqObj, UserFeatureEntity.class);
        UserFeatureEntity obj = findById(employeeEntity.getId());
        if (obj != null) {
            //employeeEntity.setSsCreator(userDetails().getUserId());
        	employeeEntity.setCompanyNo(userDetails().getCompanyNo());
            return baseUpdate(employeeEntity);
        }

        return getErrorResponse("Record not Found !!");

    }

    public Response detele(UserFeatureEntity reqObj) {

        return baseDelete(reqObj);

    }

    public Response deteleByFeatureIdUserId(Long userId, Long featureId) {
    	UserFeatureEntity userFeatureEntity = findByUserNoFeatureNo(userId, featureId);
        return baseDelete(userFeatureEntity);

    }

    public Response detele(Long id) {

        UserFeatureEntity userFeatureEntity = findById(id);
        if (userFeatureEntity == null) {
            return getErrorResponse("Record not found!");
        }
        return baseDelete(userFeatureEntity);

    }

    public Response remove(Long id) {

        UserFeatureEntity employeeEntity = findById(id);
        if (employeeEntity == null) {
            return getErrorResponse("Record not found!");
        }
        return baseRemove(employeeEntity);
    }


    public List<UserFeatureEntity> gratedFeatureByUserNo(Long userNo) {

        CoreUserEntity userEntity = new CoreUserEntity();
        userEntity.setId(userNo);
//        userEntity.setCompanyNo(companyNo);

        UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
        userFeatureEntity.setUser(userEntity);
//        userFeatureEntity.setCompanyNo(companyNo);

        return getListFromObject(baseList(criteriaQuery(userFeatureEntity)).getItems(), UserFeatureEntity.class);

    }

    public UserFeatureEntity findById(Long id) {

        UserFeatureEntity employeeEntity = new UserFeatureEntity();
        employeeEntity.setId(id);

        Response response = baseFindById(criteriaQuery(employeeEntity));
        if (response.isSuccess()) {

            return getValueFromObject(response.getObj(), UserFeatureEntity.class);
        }
        return null;
    }

    public UserFeatureEntity findByUserNoFeatureNo(Long userNo, Long subMenuNo) {

        CoreUserEntity userEntity = new CoreUserEntity();
        userEntity.setId(userNo);
//        userEntity.setCompanyNo(userDetails().getCompanyNo());

        FeatureEntity featureEntity = new FeatureEntity();
        featureEntity.setId(subMenuNo);
//        featureEntity.setCompanyNo(userDetails().getCompanyNo());


        UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
        userFeatureEntity.setUser(userEntity);
        userFeatureEntity.setFeature(featureEntity);
//        userFeatureEntity.setCompanyNo(userDetails().getCompanyNo());

        Response response = baseFindById(criteriaQuery(userFeatureEntity));
        
        if (response.isSuccess() && response.getObj()!=null) {
            return getValueFromObject(response.getObj(), UserFeatureEntity.class);
        }
        return null;
    }

    public Response findByNameOrId(String reqObj) {
        if (null == reqObj) {
            return getErrorResponse("please enter  Employee Name Or Emp. ID");
        }
        JSONObject json = new JSONObject(reqObj);
        String empNameOrId = Def.getString(json, "fname");

        UserFeatureEntity userFeatureEntity = new UserFeatureEntity();

        if (null == empNameOrId) {
            return getErrorResponse("please enter  Employee Name Or Emp. ID");
        }

        Map<String, Object> searchfields = new HashMap<String, Object>();
        searchfields.put("fname", empNameOrId);
        searchfields.put("empId", empNameOrId);

        Response response = baseList(typedQuery(userFeatureEntity, searchfields));

        if (response.getItems() == null) {
            return getErrorResponse("Data not found");
        }
        return response;
    }


    // Non API
    @SuppressWarnings({"rawtypes", "unchecked"})
    private CriteriaQuery criteriaQuery(UserFeatureEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(UserFeatureEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }

        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);

        }

        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);

        }

        criteria.where(pArrayJoin.toArray(new Predicate[0]));

        return baseTypedQuery(criteria, dataTableInRQ);
    }

    // for findByNameOrId method
    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(UserFeatureEntity filter, Map<String, Object> fields) {
        init();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = basePredicate(fields);

        return typedQuery(pConjunction, pDisJunction);
    }

    private Long totalCount(UserFeatureEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<UserFeatureEntity> root = from(UserFeatureEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> criteriaCondition(UserFeatureEntity filter, CriteriaBuilder builder, Root<UserFeatureEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {

            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }

            if (filter.getUser() != null) {
                Predicate condition = builder.equal(root.get("user"), filter.getUser());
                p.add(condition);
            }

            if (filter.getFeature() != null) {
                Predicate condition = builder.equal(root.get("feature"), filter.getFeature());
                p.add(condition);
            }

//            if (filter.getCompanyNo() != null) {
//                Predicate condition = builder.equal(root.get("companyNo"), filter.getCompanyNo());
//                p.add(condition);
//            }
        }

        return p;
    }

    @SuppressWarnings({"unused", "rawtypes"})
    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(UserFeatureEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }

}
