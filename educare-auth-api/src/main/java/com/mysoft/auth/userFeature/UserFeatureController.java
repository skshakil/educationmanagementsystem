package com.mysoft.auth.userFeature;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mysoft.core.util.Response;

@RestController
@RequestMapping("/api/user-features")
public class UserFeatureController {
	
	@Autowired
	private UserFeatureService userFeatureService;

	@GetMapping("/gridList")
	public Response gridList(HttpServletRequest reqObj) {
		return userFeatureService.gridList(reqObj);
	}

	@GetMapping("/list")
	public Response getAll(@RequestBody(required = false) String reqObj) {
		return userFeatureService.list(reqObj);
	}

//	@PostMapping("/create")
//	public Response create(@RequestBody String reqObj) {
//		return userFeatureService.save(reqObj);
//	}

	@PutMapping("/update")
	public Response update(@RequestBody String reqObj) {
		return userFeatureService.update(reqObj);
	}

//	@DeleteMapping("/delete")
//	public Response delete(@RequestParam("id") long reqId) {
//		return userFeatureService.delete(reqId);
//	}

	@DeleteMapping("/remove")
	public Response remove(@RequestParam("id") long reqId) {
		return userFeatureService.remove(reqId);
	}



    @GetMapping("/find-user-content-Access")
    public Response findUserContentAccess() {
      
        return userFeatureService.findUserContentAccess();
    }

	
//	@GetMapping("/find")
//	public Response findByName(@RequestParam Long id){
//		return userFeatureService.find(id);
//	}

}
