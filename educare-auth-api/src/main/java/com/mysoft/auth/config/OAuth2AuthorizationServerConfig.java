package com.mysoft.auth.config;

import java.util.Arrays;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import com.mysoft.auth.user.MyUserDetailsService;

@Configuration
@EnableAuthorizationServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import(WebSecurityConfig.class)
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Autowired
	private DataSource dataSource;

	@Autowired
	MyUserDetailsService myUserDetailsService;

	@Autowired
	private transient BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private TokenStore tokenStore;

	@Override
	public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {

		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()")
				.passwordEncoder(passwordEncoder);

	}

	@Override
	public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
		clients.jdbc(dataSource);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer()));
		endpoints.tokenStore(tokenStore).tokenEnhancer(tokenEnhancer()).tokenEnhancer(tokenEnhancerChain)
				.authenticationManager(authenticationManager).userDetailsService(myUserDetailsService)
				// .accessTokenConverter(accessTokenConverter())
				.exceptionTranslator(new MyWebResponseExceptionTranslator());
	}

	@Bean
	public DefaultTokenServices tokenServices() {

		final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore);
		defaultTokenServices.setSupportRefreshToken(true);
		defaultTokenServices.setAuthenticationManager(authenticationManager);
		defaultTokenServices.setTokenEnhancer(tokenEnhancer());

		return defaultTokenServices;
	}

	/*
	 * @Bean
	 * 
	 * @Primary public AuthorizationServerTokenServices tokenServices() {
	 * DefaultTokenServices tokenServices = new DefaultTokenServices(); // ...
	 * tokenServices.setTokenEnhancer(tokenEnhancer()); return tokenServices; }
	 */

	@Bean
	public TokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}

	/*
	 * @Bean public AccessTokenConverter accessTokenConverter() {
	 * 
	 * return new CustomAccessTokenConverter();
	 * 
	 * }
	 */

	/*
	 * @Bean public DefaultAccessTokenConverter accessTokenConverter() { return new
	 * DefaultAccessTokenConverter(); }
	 */

	// JDBC token store configuration

	/*
	 * @Bean public DataSourceInitializer dataSourceInitializer(final DataSource
	 * dataSource) { final DataSourceInitializer initializer = new
	 * DataSourceInitializer(); initializer.setDataSource(dataSource);
	 * initializer.setDatabasePopulator(databasePopulator()); return initializer; }
	 * 
	 * private DatabasePopulator databasePopulator() { final
	 * ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
	 * populator.addScript(schemaScript); populator.addScript(dataScript); return
	 * populator; }
	 */

	/*
	 * @Bean public DataSource dataSource() { final DriverManagerDataSource
	 * dataSource = new DriverManagerDataSource();
	 * dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
	 * dataSource.setUrl(env.getProperty("jdbc.url"));
	 * dataSource.setUsername(env.getProperty("jdbc.user"));
	 * dataSource.setPassword(env.getProperty("jdbc.pass")); return dataSource; }
	 */

	/*
	 * @Bean public TokenStore tokenStore() { return new JdbcTokenStore(dataSource);
	 * }
	 */

	/*
	 * public OAuth2AccessToken readAccessToken(String tokenValue) {
	 * OAuth2AccessToken accessToken = null;
	 * 
	 * try { accessToken = new DefaultOAuth2AccessToken(tokenValue);
	 * 
	 * } catch (EmptyResultDataAccessException e) { if (LOGGER.isInfoEnabled()) {
	 * LOGGER.info("Failed to find access token for token "+tokenValue); } } catch
	 * (IllegalArgumentException e) {
	 * LOGGER.warn("Failed to deserialize access token for " +tokenValue,e);
	 * //removeAccessToken(tokenValue); //OAuth2AccessToken accessToken1 =
	 * tokenStore().readAccessToken(tokenValue);
	 * tokenStore().removeAccessToken(accessToken); }
	 * 
	 * return accessToken; }
	 */

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

//    @SuppressWarnings("rawtypes")
//	@Bean
//    public WebResponseExceptionTranslator loggingExceptionTranslator() {
//    	
//        return new DefaultWebResponseExceptionTranslator() {
//            @Override
//            public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
//                // This is the line that prints the stack trace to the log. You can customise this to format the trace etc if you like
//                e.printStackTrace();
//
//                // Carry on handling the exception
//                ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
//                HttpHeaders headers = new HttpHeaders();
//                headers.setAll(responseEntity.getHeaders().toSingleValueMap());
//                OAuth2Exception excBody = responseEntity.getBody();
//                return new ResponseEntity<>(excBody, headers, responseEntity.getStatusCode());
//            }
//        };
//        
//    }

}
