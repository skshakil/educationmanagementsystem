package com.mysoft.auth.config;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.auth.user.MyUserDetails;




public class CustomTokenEnhancer extends AuthBaseRepository implements TokenEnhancer {
	  
  
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        
   	final Map<String, Object> additionalInfo = new HashMap<>();
        
   		MyUserDetails myUserDetails = (MyUserDetails)authentication.getPrincipal();
		//WebAuthenticationDetails details = (WebAuthenticationDetails) getAuthentication().getDetails();
     	//String remoteIpAddress = details.getRemoteAddress();
     	Long sessionNo = sessionNo(myUserDetails.getOrganizationNo(), myUserDetails.getCompanyNo());
     	
        //String remoteIpAddress1 =	userAgent().getRemoteAddress();
        String clientIpAdress = userAgent().getClientIpAddress();
        String macAdress = userAgent().getMacAddress();
        String osName =	userAgent().getOs();
        String browserName =	userAgent().getBrowser();
        
//        System.out.println("clientIpAdress "+ clientIpAdress);
//		  System.out.println(remoteIpAddress1);
//        System.out.println(osName);
//        System.out.println(browserName);

        additionalInfo.put("name", authentication.getName() + randomAlphabetic(4));
        additionalInfo.put("companyName", myUserDetails.getCompanyName());
        additionalInfo.put("sessionNo", sessionNo);
        
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
       
        userAccessLog(myUserDetails.getUserId(), myUserDetails.getOrganizationNo(), myUserDetails.getCompanyNo(), clientIpAdress,sessionNo,osName,browserName,macAdress);
       
        return accessToken;
    }
    
    
}
