package com.mysoft.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.mysoft.auth.user.MyUserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	MyUserDetailsService myUserDetailsService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService).passwordEncoder(passwordEncoder);
	}

	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {

		// @formatter:off
		http.authorizeRequests().antMatchers("/login").permitAll()
		//.antMatchers("/oauth/token/revokeById/**").permitAll()
		.antMatchers("/oauth/token/logout").permitAll()
		.antMatchers("/fapi/**").permitAll()
		.antMatchers("/tokens/**").permitAll()
		.anyRequest().authenticated()
		.and().formLogin().permitAll()
		.and().csrf().disable();
		// @formatter:on

		// http.oauth2Login().successHandler(new MyAuthenticationSuccessListener());

		/*
		 * //login and logout configuration http.formLogin() .loginPage("/auth/login")
		 * //.loginProcessingUrl("/authLogin") .usernameParameter("username")
		 * .passwordParameter("password")
		 * .successHandler(customAuthenticationSuccessHandler) .failureHandler(new
		 * CustomAuthenticationFailureHandler());
		 */
	}

}
