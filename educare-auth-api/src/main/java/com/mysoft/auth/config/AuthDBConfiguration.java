package com.mysoft.auth.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("spring.datasource")
@Getter
@Setter
public class AuthDBConfiguration {
	
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
    private Environment env;
	private String driverClassName;
	private String url;
	private String username;
	private String password;
	
/*  @Bean
  @Primary
    public LocalContainerEntityManagerFactoryBean verificationEntityManager() {
        LocalContainerEntityManagerFactoryBean em
          = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(verifincationDevDataSource());
        em.setPackagesToScan(
          new String[] { "org.baeldung.persistence.multiple.model.user" });
 
        HibernateJpaVendorAdapter vendorAdapter
          = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto",
          env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect",
          env.getProperty("hibernate.dialect"));
        em.setJpaPropertyMap(properties);
 
        return em;
    }*/

	@Profile("dev")
	@Bean
	public DataSource verifincationDevDataSource(){
		
		LOGGER.info("DB connection for Development");
		
		return createDataSource();
	}

	@Profile("test")
	@Bean
	public DataSource verifincationTestDataSource() throws SQLException{
		
		LOGGER.info("DB Connection fot Test");
		
		return createDataSource();
	}

	@Profile("prod")
	@Bean
	public DataSource verifincationProdDataSource(){
		LOGGER.info("DB Connection to Production");
		return createDataSource();
	}
	
	
	private DataSource createDataSource() {
		return DataSourceBuilder
				.create()
				.username(username)
				.password(password)
				.url(url)
				.driverClassName(driverClassName)
				.build();
	}

	
	
}
