package com.mysoft.auth.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import com.mysoft.core.util.CommonFunctions;
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {


    @Override
    public void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
	  	System.out.println("### Start Oauth Resource Server Configuration ###");
        http.requestMatchers()
                .antMatchers(CommonFunctions.SECURED_PATTERN).and().authorizeRequests()
                .antMatchers(HttpMethod.POST, CommonFunctions.SECURED_PATTERN).access(CommonFunctions.SECURED_WRITE_SCOPE)
                .anyRequest().access(CommonFunctions.SECURED_READ_SCOPE);
	// @formatter:on		
    }
    
 

/*    @Override
    public void configure(final ResourceServerSecurityConfigurer config) {
        config.tokenServices(resourceTokenServices());
    }*/

    
/*    @Bean
    @Primary
    public DefaultTokenServices resourceTokenServices() {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(resourceTokenStore);
        return defaultTokenServices;
    }*/


}
