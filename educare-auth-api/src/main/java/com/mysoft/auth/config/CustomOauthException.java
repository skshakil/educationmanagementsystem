package com.mysoft.auth.config;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.CollectionSerializer;



public class CustomOauthException extends OAuth2Exception {
   
	public CustomOauthException(String msg) {
       super(msg);
    }
}