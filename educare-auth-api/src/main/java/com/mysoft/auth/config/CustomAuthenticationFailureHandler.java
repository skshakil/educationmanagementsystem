/*package com.mysoft.auth.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;


*//**
 * Created by Jahurul Islam on 20-5-2018.
 * Spring Security will send control to CustomAuthenticationFailureHandler when authentication will get failed
 *//*
public class CustomAuthenticationFailureHandler  implements AuthenticationFailureHandler {
	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationFailureHandler.class);	

    @Override
    public void onAuthenticationFailure
    (HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws
    	IOException, ServletException {

    	//String contextPath = request.getContextPath();
    	logger.error(exception.toString());
    	System.out.println("######### Longin fail");

    	 if (exception instanceof AccountExpiredException) {
    		 request.getSession().setAttribute("failMsg", "Sorry, your account has expired.");
    	 }
    	 else if (exception instanceof CredentialsExpiredException) {
    		 request.getSession().setAttribute("failMsg", "Sorry, your password has expired.");
    	 }
    	 else if (exception instanceof DisabledException) {
    		 request.getSession().setAttribute("failMsg", "Sorry, your account is disabled.");
    		
    	 }
    	 else if (exception instanceof LockedException) {
    		 request.getSession().setAttribute("failMsg", "Sorry, your account is locked.");
    	 }
    	
    	 else if (exception instanceof BadCredentialsException) {
    		 request.getSession().setAttribute("failMsg", "Sorry, username or password not found");
    	 }
    	 
    	 else if (exception instanceof InternalAuthenticationServiceException) {
    		 request.getSession().setAttribute("failMsg", exception.getMessage());
    	 }
    	 

        response.sendRedirect(contextPath+"/auth/login?error=true");
    }
}
*/