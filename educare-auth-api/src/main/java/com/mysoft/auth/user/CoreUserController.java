package com.mysoft.auth.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mysoft.auth.base.AuthBaseController;
import com.mysoft.core.locationSetup.LocationService;
import com.mysoft.core.user.CoreUserService;
import com.mysoft.core.util.NodeTree;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;


/**
 * @author Md. Jahurul Islam
 */
@RestController
@RequestMapping("/api/coreUser")
public class CoreUserController extends AuthBaseController {

    @Autowired
    CoreUserService coreUserService;

    @Autowired
    UserService userService;
    
    @Autowired
    LocationService locationService;
    
    @PostMapping("/save")
    public Response save(@RequestBody String reqObj) {
    	 UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
        return userService.save(reqObj, userDetail);
    }

    @PutMapping("/update")
    public Response update(@RequestBody String reqObj) {
    	UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
        return userService.update(reqObj, userDetail);
    }
    
    @PostMapping("/create-with-image")
    public Response saveWithImage(@RequestParam(value = "file", required = false) MultipartFile file, @RequestParam("reqobj") String reqObj) {
    	 UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
       
        return userService.saveWithImage(file, reqObj, userDetail);
    }

    @PutMapping("/update-with-image")
    public Response updateWithImage(@RequestParam(value = "file", required = false) MultipartFile file, @RequestParam("reqobj") String reqObj) {
    	 UserSignInContainter userDetail = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
        return userService.updateWithImage(file, reqObj, userDetail);
    }

    @PostMapping("/delete")
    public Response delete(@RequestBody Long id) {
        return coreUserService.delete(id);
    }

    @PostMapping("/list")
    public Response getAll(@RequestBody(required = false) String reqObj) {
        return coreUserService.list(reqObj);
    }

    @GetMapping("/user-details")
    public Response getUserDetails() {
        return coreUserService.getUserDetails(new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(), userDetails().getCompanyName()));
    }

    @GetMapping("/find-user-menu")
    public Response findUserMenu() {
        UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
        List<NodeTree> nodeTree = userMenuList(userDetails.getUserId(), userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "W");
        return coreUserService.findUserMenu(userDetails, nodeTree);
    }
    
    @GetMapping("/find-user-preference")
    public Response findUserPreference() {
        return coreUserService.findUserPreference(new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo()));
    }

    @PostMapping("/save-user-preference")
    public Response saveUserPreference(@RequestBody(required = true) String reqObj) {
        return coreUserService.saveUserPreference(reqObj, new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo()));
    }

//    @GetMapping("/save-user-preference")
//    public Response saveUserPreference(@RequestBody(required = true) String reqObj) {
//		Response response = new Response();
//		response = coreUserService.changePassword(reqObj);
//		return response;
//	}

//    @GetMapping("/find-by-user-name")
//    public Response findByUserName(@RequestParam Long id) {
//        return coreUserService.findByUserId(id);
//    }

//    @PreAuthorize("hasAuthority('WSA_1028')") //SUBMENU_ID
    @GetMapping("gridList")
    public Response gridList(HttpServletRequest request) {
    	MyUserDetails userDetail = userDetails();
    	UserSignInContainter user = new UserSignInContainter(userDetail.getUserId(), userDetail.getCompanyNo(), userDetail.getOrganizationNo(), userDetail.getSessionNo(), userDetail.getUserTypeNo());
        return coreUserService.gridList(request, user);
    }
    
    @GetMapping("patient-portal-user-GridList")
    public Response patientUserGridList(HttpServletRequest request) {
        return coreUserService.patientUserGridList(request);
    }
    

    @GetMapping("/find")
    public Response find(@RequestParam Long id) {
        return coreUserService.find(id);
    }

    @PostMapping("/findByNameOrId")
    public Response findByNameOrId(@RequestBody String obj) {
    	MyUserDetails userDetail = userDetails();
    	UserSignInContainter user = new UserSignInContainter(userDetail.getUserId(), userDetail.getCompanyNo(), userDetail.getOrganizationNo(), userDetail.getSessionNo(), userDetail.getUserTypeNo());
        return coreUserService.untaggedEmpNo(obj,user);
    }

    @PostMapping("/validateUserId")
    public Response validateUserId(@RequestBody String obj) {
        return userService.validateUserId(obj);
    }

    @PostMapping("/validateUserName")
    public Response validateUserName(@RequestBody String obj) {
        return userService.validateUserName(obj);
    }

    @PostMapping("/validateExisting")
    public Response validateExistingUser(@RequestBody String obj) {
        return userService.validateUserExisting(obj);
    }


    @PutMapping("/update-user-info")
    public Response updateUserInfo(@RequestParam(value = "file", required = false) MultipartFile file, @RequestParam(value = "reqobj", required = false) String reqObj) {
        UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getName(), userDetails().getCompanyNo(), userDetails().getSessionNo(),userDetails().getEmpNo(), userDetails().getDoctorNo());
        return userService.updateUserInfo(file, reqObj, userDetails);
//        return userService.updateWithImage(file, reqObj, userDetails);
    }
    

    @PostMapping("/reset-password")
    public Response resetPassword(@RequestBody String regObj) {
        String plainPass = getRandomNumber();
        String ecodPass =  new BCryptPasswordEncoder().encode(plainPass);
        return coreUserService.resetPassword(regObj,plainPass,ecodPass,new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo()));
    }
    
    
    @PostMapping("/update-user-info")
    public Response updateUserInfo(@RequestBody String regObj) {
    	
        return coreUserService.updateUserInfo(regObj, new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo()));
    }
    
    @GetMapping("/init-update-user-data")
    public Response initUpdateUserData() {
    	
    	return locationService.findCountryDistricThana();
        
    }

}
