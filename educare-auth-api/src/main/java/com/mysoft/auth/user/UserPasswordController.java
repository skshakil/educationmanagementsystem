package com.mysoft.auth.user;

import com.mysoft.auth.base.AuthBaseController;
import com.mysoft.core.user.CoreUserService;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



/**
 * @author Md. Jahurul Islam
 *
 */
@RestController
public class UserPasswordController extends AuthBaseController {
	 
	@Autowired
	CoreUserService coreUserService;
	@Autowired
	UserService userService;
    @RequestMapping(method = RequestMethod.POST, value = "/tokens/getPassword")
	public Response getPassword(@RequestBody(required = true) String reqObj) {
		UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUsername(), userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(),  userDetails().getSessionNo());
		return userService.forgotPassword(reqObj, userDetails);
	}

    @RequestMapping(method = RequestMethod.POST, value = "/api/changePassword")
	public Response changePassword(@RequestBody(required = true) String reqObj) {
		UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUsername(), userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(),  userDetails().getSessionNo());
		return userService.changePassword(reqObj, userDetails);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/changePasswordByAdmin")
	public Response changePasswordByAdmin(@RequestBody(required = true) String reqObj) {
		UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUsername(), userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(),  userDetails().getSessionNo());
		return userService.changePasswordByAdmin(reqObj, userDetails);
	}
}
