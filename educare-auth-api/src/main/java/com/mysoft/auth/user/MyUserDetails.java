package com.mysoft.auth.user;

import java.util.Collection;

import com.mysoft.core.user.CoreUserEntity;
import org.springframework.security.core.GrantedAuthority;



public class MyUserDetails extends MyUser {
	private static final long serialVersionUID = 1L;
	
	public MyUserDetails(CoreUserEntity user, Collection<? extends GrantedAuthority> authorities) {
		super(
				user.getUserName(), 
				user.getPassword(), 
				user.isEnabled(), 
				!user.isAccountExpired(),
				!user.isPasswordExpired(), 
				!user.isAccountLocked(), 
				authorities, 
				user.getId(), 
				user.getCompanyNo(), 
				user.getCompnayName(), 
				user.getDoctorNo(), 
				user.getOrganizationNo(),
				user.getUserTypeNo());
}

}
