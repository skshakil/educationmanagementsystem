//package com.mysoft.auth.user;
//
//import java.util.Date;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedStoredProcedureQuery;
//import javax.persistence.ParameterMode;
//import javax.persistence.StoredProcedureParameter;
//import javax.persistence.Table;
//import com.mysoft.core.base.BaseOraEntity;
//import lombok.Getter;
//import lombok.Setter;
//
//@NamedStoredProcedureQuery(
//	    name = "addUserAccessLog", 
//	    procedureName = "pd_ins_session_web",
//	    parameters = {
//		@StoredProcedureParameter(name = "userNo", type = Long.class, mode = ParameterMode.IN),
//		@StoredProcedureParameter(name = "userCompanyNo", type = Long.class, mode = ParameterMode.IN),
//		@StoredProcedureParameter(name = "userOsName", type = String.class, mode = ParameterMode.IN),
//		@StoredProcedureParameter(name = "userRegion", type = String.class, mode = ParameterMode.IN),
//		@StoredProcedureParameter(name = "userTimezone", type = String.class, mode = ParameterMode.IN),
//		@StoredProcedureParameter(name = "userTerminal", type = String.class, mode = ParameterMode.IN),
//		@StoredProcedureParameter(name = "userPcName", type = String.class, mode = ParameterMode.IN),
//		@StoredProcedureParameter(name = "userIdaddress", type = String.class, mode = ParameterMode.IN)
//		}
//)
//@Setter
//@Getter
//@Entity
//@Table(name="sa_session")
//public class userAccessLog extends BaseOraEntity {
//	/*
//	  @Id
//	  @Column(name="SESSION_NO", nullable = false)
//	  private long id;
//	   
//	  @Column(name="SESSION_STAT", nullable = true)
//	  private long sessionStatus;
//	  
//	  @Column(name="USER_NO", nullable = true)
//	  private long userNo;
//	  
//	  @Column(name="SID", nullable = true)
//	  private String sid;
//	  
//	  @Column(name="SERIAL#", nullable = true)
//	  private String serial;
//	  
//	  @Column(name="LOGON_DT", nullable = true)
//	  private Date logonDate;
//	  
//	  @Column(name="LOGOUT_DT", nullable = true)
//	  private Date logoutDate;
//	  
//	  @Column(name="LOGOUT_TYPE", nullable = true)
//	  private String logoutType;
//	  
//	  @Column(name="OS_USER_NAME", nullable = true)
//	  private String osUserName;
//	  
//	  @Column(name="OS_TIMEZONE", nullable = true)
//	  private String osTimeZone;
//	  
//	  @Column(name="OS_REGION", nullable = true)
//	  private String osRegion;
//	  
//	  
//	  @Column(name="IP_ADDR_OWN", nullable = true)
//	  private String ipAddrOwn;
//	  
//	  @Column(name="IP_ADDR_CURR", nullable = true)
//	  private String ipAddrCurrent;
//	  
//	  
//	  @Column(name="STATUS", nullable = true)
//	  private String status;
//	  
//	  @Column(name="LOGON_PC_NAME", nullable = true)
//	  private String logonPcName;
//	  
//	  @Column(name="MAC_ADDRESS", nullable = true)
//	  private String macAddress;
//	  
//	  @Column(name="DB_USERNAME", nullable = true)
//	  private String dbUserName;
//	  
//	  @Column(name="DB_CONNECTION_SERVER", nullable = true)
//	  private String dbConnectionServer;
//	  
//	  @Column(name="DB_SCHEMANAME", nullable = true)
//	  private String dbSchemaName;
//	  
//	  @Column(name="DB_PROGRAM", nullable = true)
//	  private String dbProgram;
//	  
//	  @Column(name="DB_PREV_SQL_ID", nullable = true)
//	  private String dbPrevSqlId;*/
//		  
//	  
//}
//
//
