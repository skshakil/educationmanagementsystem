package com.mysoft.auth.user;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.auth.company.CompanyService;
import com.mysoft.auth.role.RoleEntity;
import com.mysoft.auth.role.RoleService;
import com.mysoft.auth.usersRole.UsersRoleService;
import com.mysoft.core.user.CoreUserEntity;
import com.mysoft.core.user.CoreUserService;
import com.mysoft.core.viewfeature.ViewFeatureEntity;
import com.mysoft.core.viewfeature.ViewFeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author Md. Jahurul Islam
 *
 */
@Service("myUserDetailsService")
@Transactional
public class MyUserDetailsService extends AuthBaseRepository implements UserDetailsService {

	@Autowired
	CoreUserService userService;

	@Autowired
	CompanyService companyService;

	@Autowired
	RoleService roleService;

	@Autowired
	UsersRoleService userRoleService;

	@Autowired
	ViewFeatureService viewFeatureService;

	Collection<? extends GrantedAuthority> authorities;

	@Override
	public MyUserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {

		if (username.isEmpty()) {
			throw new UsernameNotFoundException("User not found" + username);
		}

		CoreUserEntity user = userService.findByUserName(username.toUpperCase());

		if (user == null)
			throw new UsernameNotFoundException("User not found" + username);
		Date userExpireDate = user.getAccountExpireDate();

		if (userExpireDate.before(new Date())) {
			throw new AccountExpiredException("Your License is expired. please contact with Administrator.");
		}

		// List<RoleEntity> AllRoles = userRoleService.findRoleByUserId(user.getId());
		List<ViewFeatureEntity> features = viewFeatureService.findByUserId(user.getId());

		Collection<? extends GrantedAuthority> authories = setAuthorities(null, features);

//		user.setDoctorNo(employeeService.getDoctorNoByEmpNo(user.getEmpNo()));

		com.mysoft.core.compnay.CompanyEntity company = companyService.findCompanyById(user.getCompanyNo());

		if (null != company) {

			user.setOrganizationNo(company.getOgNo());
			user.setCompnayName(company.getCompanyName());

		}

		/*
		 * Map<String,ViewFeatureEntity> featuremap = new HashMap<>(); List<Role>
		 * AllRoles =userRoleService.findRoleByUserId(user.getId());
		 * 
		 * for (ViewFeatureEntity featureEntity : viewfeature) {
		 * featuremap.put(featureEntity.getCode(), featureEntity); }
		 */

		/*
		 * SaCompany company = companyService.findById(user.getCompanyId()); if (company
		 * == null) throw new UsernameNotFoundException("Company not found for user: " +
		 * username);
		 * 
		 * OrganizationEntity org = organizationService.findById(company.getOgNo());
		 * Collection<? extends GrantedAuthority> roles = getAuthorities(AllRoles);
		 * boolean hasDoctor = roles.stream().anyMatch(role ->
		 * role.getAuthority().equals("ROLE_DOCTOR"));
		 * 
		 * if (hasDoctor) { Long doctorNo = user.getAgentId(); if (doctorNo != null) {
		 * DoctorInfo doctorInfo = doctorInfoService.findByDoctorNo(doctorNo); return
		 * new MyUserDetails(user, roles,featuremap, company.getCompanyNo(),
		 * company.getCompanyName(), org.getOgNo(), org.getOgName(), doctorInfo); } }
		 * 
		 * return new MyUserDetails(user, roles, featuremap, company.getCompanyNo(),
		 * company.getCompanyName(), org.getOgNo(), org.getOgName());
		 */

		return new MyUserDetails(user, authories);
	}

	Set<GrantedAuthority> setAuthorities(List<RoleEntity> roles, List<ViewFeatureEntity> features) {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		if (roles != null) {

			for (RoleEntity role : roles) {
				GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRoleName());
				authorities.add(grantedAuthority);
			}

		}

		if (features != null) {
			for (ViewFeatureEntity feature : features) {
				GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(feature.getSubMenuId());
				authorities.add(grantedAuthority);
			}
		}

		return authorities;
	}

}
