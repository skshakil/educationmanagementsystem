package com.mysoft.auth.user;

import com.mysoft.auth.base.AuthBaseRepository;
import com.mysoft.auth.feature.FeatureEntity;
import com.mysoft.auth.module.FeatureDto;
import com.mysoft.auth.userFeature.UserFeatureEntity;
import com.mysoft.auth.userFeature.UserFeatureService;
import com.mysoft.core.user.CoreUserEntity;
import com.mysoft.core.user.CoreUserRepository;
import com.mysoft.core.user.CoreUserService;
import com.mysoft.core.userCompany.CoreUserCompanyEntity;
import com.mysoft.core.userCompany.CoreUserCompanyRepository;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class UserRepository extends AuthBaseRepository {

    @Autowired
    CoreUserRepository coreUserRepository;
    @Autowired
    CoreUserService coreUserService;
    @Autowired
    CoreUserCompanyRepository coreUserCompanyRepository;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;


    @Autowired
    UserFeatureService userFeatureService;

    @Autowired
    EntityManager entityManager;



    public Response save(String reqObj, UserSignInContainter userDetails) {
        JSONObject json = new JSONObject(reqObj);
        System.out.println(reqObj);
        String user = Def.getString(json, "user");
//        String features = Def.getString(json, "features");

        System.out.println(user);
        Response userValidation = validateUserName(user);
        Response idValidation = validateUserId(user);
        Response response = null;

        if (userValidation.isSuccess() && idValidation.isSuccess()) {
          
            CoreUserEntity coreUserEntity = objectMapperReadValue(user, CoreUserEntity.class);

            coreUserEntity.setUserFullName(coreUserEntity.getUserFullName() != null? coreUserEntity.getUserFullName() : coreUserEntity.getUserName());
            
            coreUserEntity.setId(functionFdAutoNo(userDetails.getOrganizationNo(),userDetails.getCompanyNo(),"sa_user","USER_NO",6l));
            coreUserEntity.setCompnayName(userDetails.getCompanyName());
            coreUserEntity.setSsCreator(userDetails.getUserId());
            coreUserEntity.setSsCreatedOn(new Date());
            coreUserEntity.setSsCreateSession(userDetails.getSessionNo());
            coreUserEntity.setSsModifier(userDetails.getUserId());
            coreUserEntity.setSsModifiedOn(new Date());
            coreUserEntity.setSsModifiedSession(userDetails.getSessionNo());
            coreUserEntity.setCompanyNo(userDetails.getCompanyNo());
            coreUserEntity.setOrganizationNo(userDetails.getOrganizationNo());

            // Making userId and userName uppercase
            coreUserEntity.setUserId(coreUserEntity.getUserId().toUpperCase());
            coreUserEntity.setUserName(coreUserEntity.getUserName().toUpperCase());
            // sets legacy password for Oracle form login
//            coreUserEntity.setPassword(functionGenerateLegacyPassword(coreUserEntity.getUserName().toUpperCase(), coreUserEntity.getPassword()));
            coreUserEntity.setPassword(passwordEncoder.encode(coreUserEntity.getPassword()));

            coreUserRepository.save(coreUserEntity);
           
            CoreUserCompanyEntity userCompanyEntity = new CoreUserCompanyEntity();
            userCompanyEntity.setUserNo(coreUserEntity.getId());
            userCompanyEntity.setId(grantCompanyfunctionFdAutoNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo()));
            userCompanyEntity.setDefaultFlag(1);
            userCompanyEntity.setAllComapnyNo(userDetails.getCompanyNo());
            userCompanyEntity.setNumOfCon(1L);   

            userCompanyEntity.setSsCreator(userDetails.getUserId());
            userCompanyEntity.setSsCreatedOn(new Date());
            userCompanyEntity.setSsCreateSession(userDetails.getSessionNo());
            userCompanyEntity.setCompanyNo(userDetails.getCompanyNo());
            userCompanyEntity.setOrganizationNo(userDetails.getOrganizationNo()); 
            
            response = coreUserCompanyRepository.saveUser(userCompanyEntity);

            if (!response.isSuccess()){
                getErrorResponse("User creation failed");
            }

//            List<FeatureDto> featureList = objectMapperReadArrayValue(features, FeatureDto.class);
//
//            for (FeatureDto featureDto : featureList) {
//                FeatureEntity featureEntity = new FeatureEntity();
//                featureEntity.setId(featureDto.getFeatureId());
//                UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
//                if (!featureDto.getIsGranted() && featureDto.getUpdatedStatus()) {
//                    userFeatureEntity.setId(functionFdAutoNo(userDetails.getOrganizationNo(),userDetails.getCompanyNo(),"sa_grantobject", "gr_object_no",6l));
//                    userFeatureEntity.setUser(coreUserEntity);
//                    userFeatureEntity.setFeature(featureEntity);
//                    userFeatureEntity.setSsCreateSession(userDetails.getSessionNo());
//                    userFeatureEntity.setSsCreator(userDetails.getUserId());
//                    userFeatureEntity.setSsCreatedOn(new Date());
//                    userFeatureEntity.setCompanyNo(userDetails.getCompanyNo());
//                    userFeatureEntity.setOrganizationNo(userDetails.getOrganizationNo());
//                    response = userFeatureService.save(userFeatureEntity);
//                }
//
//            }
//
//            if (!response.isSuccess()) {
//                getErrorResponse("Something went wrong");
//            }
            return response;

        } else {
            return userValidation;
        }
    }

    public Response changePassword(String reqObj, UserSignInContainter userDetails) {

        JSONObject json = new JSONObject(reqObj);
        String currentPassword = Def.getString(json, "currentPassword");
        String newPassword = Def.getString(json, "newPassword");

        if (currentPassword == null || currentPassword.isEmpty()) {
            return getErrorResponse("Please enter your current password !!");
        }

        if (newPassword == null || newPassword.isEmpty()) {
            return getErrorResponse("Please enter your new password !!");
        }


        CoreUserEntity coreUserEntity = coreUserRepository.findById(userDetails.getUserId());

        if (coreUserEntity == null) {

            return getErrorResponse("User not found !!");
        }

        boolean isMatche = passwordEncoder.matches(currentPassword, coreUserEntity.getPassword());

        if (!isMatche) {
            return getErrorResponse("Your current password not matche,Please enter current password !!");
        }

        String changePassword = passwordEncoder.encode(newPassword);

        coreUserEntity.setPassword(changePassword);
        coreUserEntity.setSsModifiedOn(new Date());
        coreUserEntity.setSsCreator(userDetails.getUserId());
        coreUserEntity.setSsCreateSession(userDetails.getSessionNo());
        coreUserEntity.setCompanyNo(userDetails.getCompanyNo());
      
        Response response = coreUserRepository.update(coreUserEntity);

        if (!response.isSuccess() || response.getObj() == null) {
            return getErrorResponse("Change password fail !!");
        }

        return getSuccessResponse("Change password saved successfully");

    }

    public Response changePasswordbyAdmin(String reqObj, UserSignInContainter userDetails) {

        JSONObject json = new JSONObject(reqObj);
        String newPassword = Def.getString(json, "password");
        String userName = Def.getString(json, "userName");

        if (newPassword == null || newPassword.isEmpty()) {
            return getErrorResponse("Please enter your new password !!");
        }

        if (newPassword.length() < 6) {
            return getErrorResponse("Password must be at least 06 character");
        }

        if (userName == null || userName.isEmpty()) {
            return getErrorResponse("Username must not be empty");
        }

        CoreUserEntity coreUserEntity = coreUserRepository.findByUserName(userName);

        if (coreUserEntity == null) {
            return getErrorResponse("User not found !!");
        }

        String changePassword = passwordEncoder.encode(newPassword);

        coreUserEntity.setPassword(changePassword);
        coreUserEntity.setSsModifiedOn(new Date());
        coreUserEntity.setSsModifier(userDetails.getUserId());
        coreUserEntity.setSsModifiedSession(userDetails.getSessionNo());
       
        Response response = coreUserRepository.update(coreUserEntity);

        if (!response.isSuccess() || response.getObj() == null) {
            return getErrorResponse("Changing password failed !!");
        }

        return getSuccessResponse("Password changed successfully");
    }

    public Response forgotPassword(String reqObj, UserSignInContainter userDetails) {


        JSONObject json = new JSONObject(reqObj);
        String userName = Def.getString(json, "userName");

        if (userName == null || userName.isEmpty()) {
            return getErrorResponse("Please enter your user name  !!");
        }


        CoreUserEntity coreUserEntity = coreUserRepository.findByUserName(userName.toUpperCase());

        if (coreUserEntity == null) {

            return getErrorResponse("User not found !!");
        }

        String existenceUserName = coreUserEntity.getUserName().toLowerCase();
        String newPassword = existenceUserName + "#";
        String newEncodePassword = passwordEncoder.encode(newPassword);

        coreUserEntity.setPassword(newEncodePassword);
        // change password for legacy password
        coreUserEntity.setPassword(functionGenerateLegacyPassword(coreUserEntity.getUserName().toUpperCase(), newPassword));
        coreUserEntity.setSsModifiedOn(new Date());
        coreUserEntity.setCompanyNo(userDetails.getCompanyNo());
        
        coreUserRepository.update(coreUserEntity);

        Response response = new Response();
        response.setObj(newPassword);


        return getSuccessResponse("Your new password", response);

    }

    public Response validateUserName(String reqObj) {
        Response response = new Response();
        CoreUserEntity coreUserEntity = objectMapperReadValue(reqObj, CoreUserEntity.class);
        if (coreUserEntity.getUserName() != null) {
            CoreUserEntity checkerEntity2 = coreUserRepository.findByUserName(coreUserEntity.getUserName().toUpperCase());
            if (checkerEntity2 != null) {
                response = getErrorResponse("User Name must be unique");
            } else {
                response.setSuccess(true);
            }
        }

        return response;
    }

    public Response validateUserId(String reqObj) {
        Response response = new Response();
            CoreUserEntity coreUserEntity = objectMapperReadValue(reqObj, CoreUserEntity.class);

        if (coreUserEntity.getUserId() != null) {
            CoreUserEntity checkerEntity = coreUserRepository.findByUserId(coreUserEntity.getUserId().toUpperCase());
            if (checkerEntity != null) {
                response = getErrorResponse("User ID must be unique");
            } else {
                response.setSuccess(true);
            }
        }

        return response;
    }

    public Response validateExisting(String reqObj) {
        Response response = new Response();
        CoreUserEntity coreUserEntity = objectMapperReadValue(reqObj, CoreUserEntity.class);

        if (coreUserEntity.getId() != null) {
            CoreUserEntity checkerEntity3 = coreUserRepository.findById(coreUserEntity.getId());
            if (coreUserEntity.getUserId() != null && !checkerEntity3.getUserId().equalsIgnoreCase(coreUserEntity.getUserId())) {
                return response = validateUserId(reqObj);
            }
            if (coreUserEntity.getUserName() != null && !checkerEntity3.getUserName().equalsIgnoreCase(coreUserEntity.getUserName())) {
                return response = validateUserName(reqObj);
            }
        }
        return response;
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {
        JSONObject json = new JSONObject(reqObj);
        String user = Def.getString(json, "user");
//        String features = Def.getString(json, "features");

        Response validationResponse = validateExisting(user);
        Response response = null;

        if (validationResponse == null || validationResponse.isSuccess()) {
            CoreUserEntity coreUserEntity = objectMapperReadValue(user, CoreUserEntity.class);
            CoreUserEntity originalUser = coreUserRepository.findByIdReadOnly(coreUserEntity.getId());
            if (originalUser == null) {
                return getErrorResponse("User not found !!");
            }
            originalUser.setUserId(coreUserEntity.getUserId().toUpperCase());
            originalUser.setUserName(coreUserEntity.getUserName().toUpperCase());
            originalUser.setAccountExpireDate(coreUserEntity.getAccountExpireDate());
            originalUser.setActiveStatus(coreUserEntity.getActiveStatus());
            originalUser.setEnabled(coreUserEntity.isEnabled());
            originalUser.setDefaultPageLink(coreUserEntity.getDefaultPageLink());
            originalUser.setSsModifiedOn(new Date());
            originalUser.setSsModifier(userDetails.getUserId());
            originalUser.setSsModifiedSession(userDetails.getSessionNo());

            response = coreUserRepository.update(originalUser);

            if (!response.isSuccess()){
                getErrorResponse("User update failed");
            }

//            List<FeatureDto> featureList = objectMapperReadArrayValue(features, FeatureDto.class);
//
//            for (FeatureDto featureDto : featureList) {
//                FeatureEntity featureEntity = new FeatureEntity();
//                featureEntity.setId(featureDto.getFeatureId());
//                UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
//                if (featureDto.getUpdatedStatus()) {
//                    // calling function to generate primary key
//                    userFeatureEntity.setId(functionFdAutoNo(originalUser.getOrganizationNo(),originalUser.getCompanyNo(),"sa_grantobject", "gr_object_no",6l));
//                    userFeatureEntity.setUser(coreUserEntity);
//                    userFeatureEntity.setFeature(featureEntity);
//                    userFeatureEntity.setSsCreateSession(userDetails.getSessionNo());
//                    userFeatureEntity.setSsCreator(userDetails.getUserId());
//                    userFeatureEntity.setSsCreatedOn(new Date());
//                    userFeatureEntity.setCompanyNo(originalUser.getCompanyNo());
//                    userFeatureEntity.setOrganizationNo(originalUser.getOrganizationNo());
//                    response = userFeatureService.save(userFeatureEntity);
//                }
//                if (!featureDto.getUpdatedStatus()) {
//                    response = userFeatureService.delete(coreUserEntity.getId(), featureEntity.getId());
//                }
//            }
//            if (!response.isSuccess()) {
//                getErrorResponse("Something went wrong");
//            }
            return response;
        } else {
            return validationResponse;
        }
    }

    
    public Response updateUserInfo(MultipartFile file, String reqObj,UserSignInContainter userDetails) {
    	return coreUserService.updateUserInfo(file,reqObj,userDetails);
    }

    public String functionGenerateLegacyPassword(String username, String password) {
        String legacyPassword;
        legacyPassword = (String) entityManager
                .createNativeQuery("SELECT f_password(:pUserName,:pPassword) FROM DUAL")
                .setParameter("pUserName", username)
                .setParameter("pPassword", password)
                .getSingleResult();

        return legacyPassword;

    }
    

    public Response saveWithImage(MultipartFile file, String reqObj, UserSignInContainter userDetails) {
        JSONObject json = new JSONObject(reqObj);
        String user = Def.getString(json, "user");
        String features = Def.getString(json, "features");

        Response userValidation = validateUserName(user);
        Response idValidation = validateUserId(user);
        Response response = null;
        
        CoreUserEntity coreUserEntity = objectMapperReadValue(user, CoreUserEntity.class);

        if (userValidation.isSuccess() && idValidation.isSuccess()) {

            coreUserEntity.setUserFullName(coreUserEntity.getUserFullName() != null? coreUserEntity.getUserFullName() : coreUserEntity.getUserName());
            
            coreUserEntity.setId(functionFdAutoNo(coreUserEntity.getOrganizationNo(),coreUserEntity.getCompanyNo(),"sa_user","USER_NO",6l));
            coreUserEntity.setSsCreator(userDetails.getUserId());
            coreUserEntity.setSsCreatedOn(new Date());
            coreUserEntity.setSsCreateSession(userDetails.getSessionNo());
            coreUserEntity.setSsModifier(userDetails.getUserId());
            coreUserEntity.setSsModifiedOn(new Date());
            coreUserEntity.setSsModifiedSession(userDetails.getSessionNo());

            // Making userId and userName uppercase
            coreUserEntity.setUserId(coreUserEntity.getUserId().toUpperCase());
            coreUserEntity.setUserName(coreUserEntity.getUserName().toUpperCase());
            coreUserEntity.setPassword(passwordEncoder.encode(coreUserEntity.getPassword()));
            
            String fileName = coreUserEntity.getUserId()+"_"+coreUserEntity.getOrganizationNo()+"_"+coreUserEntity.getCompanyNo();
    		// Image file
    		if (file != null) {
    			coreUserEntity.setUserPhoto(Def.customFileName(file, fileName));
    		}

            Response userRes = coreUserRepository.save(coreUserEntity);
            
            if(userRes.isSuccess()) {

    			if (file != null) {
    				storePhotoToFile(file, fileName);
    			}
            }
           
            CoreUserCompanyEntity userCompanyEntity = new CoreUserCompanyEntity();
            userCompanyEntity.setUserNo(coreUserEntity.getId());
            userCompanyEntity.setId(grantCompanyfunctionFdAutoNo(coreUserEntity.getOrganizationNo(), coreUserEntity.getCompanyNo()));
            userCompanyEntity.setDefaultFlag(1);
            userCompanyEntity.setAllComapnyNo(1L);
            userCompanyEntity.setNumOfCon(1L);   

            userCompanyEntity.setSsCreator(userDetails.getUserId());
            userCompanyEntity.setSsCreatedOn(new Date());
            userCompanyEntity.setSsCreateSession(userDetails.getSessionNo());
            userCompanyEntity.setCompanyNo(coreUserEntity.getCompanyNo());
            userCompanyEntity.setOrganizationNo(coreUserEntity.getOrganizationNo()); 
            
            response = coreUserCompanyRepository.saveUser(userCompanyEntity);

            if (!response.isSuccess()){
                getErrorResponse("User creation failed");
            }

            List<FeatureDto> featureList = objectMapperReadArrayValue(features, FeatureDto.class);

            for (FeatureDto featureDto : featureList) {
                FeatureEntity featureEntity = new FeatureEntity();
                featureEntity.setId(featureDto.getFeatureId());
                UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
                if (!featureDto.getIsGranted() && featureDto.getUpdatedStatus()) {
                    userFeatureEntity.setId(functionFdAutoNo(coreUserEntity.getOrganizationNo(),coreUserEntity.getCompanyNo(),"sa_grantobject", "gr_object_no",6l));
                    userFeatureEntity.setUser(coreUserEntity);
                    userFeatureEntity.setFeature(featureEntity);
                    userFeatureEntity.setSsCreateSession(userDetails.getSessionNo());
                    userFeatureEntity.setSsCreator(userDetails.getUserId());
                    userFeatureEntity.setSsCreatedOn(new Date());
                    userFeatureEntity.setCompanyNo(coreUserEntity.getCompanyNo());
                    userFeatureEntity.setOrganizationNo(coreUserEntity.getOrganizationNo());
                    response = userFeatureService.save(userFeatureEntity);
                }

            }

            if (!response.isSuccess()) {
                getErrorResponse("Something went wrong");
            }
            return response;

        } else {
            return userValidation;
        }
    }
    
    public Response updateWithImage(MultipartFile file, String reqObj, UserSignInContainter userDetails) {
        JSONObject json = new JSONObject(reqObj);
        String user = Def.getString(json, "user");
        String features = Def.getString(json, "features");

        Response validationResponse = validateExisting(user);
        Response response = null;

        if (validationResponse == null || validationResponse.isSuccess()) {
            CoreUserEntity coreUserEntity = objectMapperReadValue(user, CoreUserEntity.class);
            CoreUserEntity originalUser = coreUserRepository.findByIdReadOnly(coreUserEntity.getId());
            if (originalUser == null) {
                return getErrorResponse("User not found !!");
            }

            coreUserEntity.setUserName(coreUserEntity.getUserName().toUpperCase());
            coreUserEntity.setSsModifiedOn(new Date());
            coreUserEntity.setSsModifiedSession(userDetails.getSessionNo());
            coreUserEntity.setSsModifier(userDetails.getUserId());

            String fileName = coreUserEntity.getUserId()+"_"+coreUserEntity.getOrganizationNo()+"_"+coreUserEntity.getCompanyNo();
    		// Image file
    		if (file != null) {
    			coreUserEntity.setUserPhoto(Def.customFileName(file, fileName));
    		}

            response = coreUserRepository.update(coreUserEntity);

            if (!response.isSuccess()){
                getErrorResponse("User update failed");
            }else {
            	if (file != null) {
    				storePhotoToFile(file, fileName);
    			}
            }

            List<FeatureDto> featureList = objectMapperReadArrayValue(features, FeatureDto.class);

            for (FeatureDto featureDto : featureList) {
                FeatureEntity featureEntity = new FeatureEntity();
                featureEntity.setId(featureDto.getFeatureId());
                UserFeatureEntity userFeatureEntity = new UserFeatureEntity();
                if (featureDto.getUpdatedStatus()) {
                    // calling function to generate primary key
                    userFeatureEntity.setId(functionFdAutoNo(originalUser.getOrganizationNo(),originalUser.getCompanyNo(),"sa_grantobject", "gr_object_no",6l));
                    userFeatureEntity.setUser(coreUserEntity);
                    userFeatureEntity.setFeature(featureEntity);
                    userFeatureEntity.setSsCreateSession(userDetails.getSessionNo());
                    userFeatureEntity.setSsCreator(userDetails.getUserId());
                    userFeatureEntity.setSsCreatedOn(new Date());
                    userFeatureEntity.setCompanyNo(coreUserEntity.getCompanyNo());
                    userFeatureEntity.setOrganizationNo(coreUserEntity.getOrganizationNo());
                    response = userFeatureService.save(userFeatureEntity);
                }
                if (!featureDto.getUpdatedStatus()) {
                    response = userFeatureService.delete(coreUserEntity.getId(), featureEntity.getId());
                }
            }
            if (!response.isSuccess()) {
                getErrorResponse("Something went wrong");
            }
            return getSuccessResponse("User Update Successful.", response);
        } else {
            return validationResponse;
        }
    }
}
