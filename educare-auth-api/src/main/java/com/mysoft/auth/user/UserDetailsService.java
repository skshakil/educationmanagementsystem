package com.mysoft.auth.user;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;




public interface UserDetailsService extends org.springframework.security.core.userdetails.UserDetailsService{
	MyUserDetails loadUserByUsername(String username)throws UsernameNotFoundException, DataAccessException;
}
