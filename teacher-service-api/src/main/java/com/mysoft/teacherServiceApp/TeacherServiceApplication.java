package com.mysoft.teacherServiceApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Md. Jahurul Islam
 *
 */
@SpringBootApplication
@ComponentScan({ "com.mysoft.teacherServiceApp", "com.mysoft.shared", "com.mysoft.core" })
public class TeacherServiceApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(TeacherServiceApplication.class, args);
	}

}
