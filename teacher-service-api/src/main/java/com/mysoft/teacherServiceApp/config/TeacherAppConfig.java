package com.mysoft.teacherServiceApp.config;


import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Md. Jahurul Islam
 *
 */
@Configuration
public class TeacherAppConfig implements WebMvcConfigurer {

	@Autowired
	private Environment env;

	@Autowired
	private DataSource dataSource;

	@Bean
	public TokenStore resourcesTokenStore() {
		return new JdbcTokenStore(dataSource);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplateBuilder().build();
	}

	@Bean
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource);
		em.setPackagesToScan(new String[] { "com.mysoft.teacherServiceApp", "com.mysoft.shared", "com.mysoft.core" });
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());
		return em;
	}

	protected Properties additionalProperties() {
		final Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
		hibernateProperties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
		hibernateProperties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		hibernateProperties.put("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
		hibernateProperties.put("hibernate.proc.param_null_passing",
				env.getProperty("hibernate.proc.param_null_passing"));
		hibernateProperties.put("hibernate.schema_update.unique_constraint_strategy",
				env.getProperty("hibernate.schema_update.unique_constraint_strategy"));
		hibernateProperties.put("hibernate.temp.use_jdbc_metadata_defaults",
				env.getProperty("hibernate.temp.use_jdbc_metadata_defaults"));
		return hibernateProperties;
	}

}
