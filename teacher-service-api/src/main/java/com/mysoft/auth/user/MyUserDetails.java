package com.mysoft.auth.user;


import com.mysoft.core.user.CoreUserEntity;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author Md. Jahurul Islam
 *
 */
public class MyUserDetails extends MyUser {
	private static final long serialVersionUID = 1L;

	public MyUserDetails(CoreUserEntity user, Collection<? extends GrantedAuthority> authorities) {
		super(user.getUserName(), user.getPassword(), user.isEnabled(), !user.isAccountExpired(),
				!user.isPasswordExpired(), !user.isAccountLocked(), authorities, user.getId(), user.getCompanyNo(), user.getCompnayName());
}


}

