package com.mysoft.shared.util;

import com.mysoft.core.util.CommonFunctions;

public interface SharedCommonFunctions extends CommonFunctions {
	
	
    float prescriptionAsenceDelayOrder  = 3.1F;
	
	default float absenceDelayOrder() {
		return SharedCommonFunctions.prescriptionAsenceDelayOrder;
	}
	
    long dosagePreDiagnosisValType = 26L;
    long realationWithMealPreDiagnosisValType = 34L;
    long routePreDiagnosisValType = 50L;
    long durationPreDiagnosisValType = 51L;
    long durationMuPreDiagnosisValType = 52L;

}
