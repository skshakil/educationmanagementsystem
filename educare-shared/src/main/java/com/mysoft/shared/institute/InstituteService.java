package com.mysoft.shared.institute;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Service
public class InstituteService {

	@Autowired
	private InstituteRepository instituteRepository;

	public Response list(String reqObj) {
		return instituteRepository.list(reqObj);
	}

	public Response gridList(HttpServletRequest request) {
		return instituteRepository.gridList(request);
	}

	public Response save(String reqObj, UserSignInContainter userDetails) {
		return instituteRepository.save(reqObj, userDetails);
	}

	public Response update(String reqObj, UserSignInContainter userDetails) {
		return instituteRepository.update(reqObj, userDetails);
	}

	public Response delete(Long id) {
		return instituteRepository.delete(id);
	}

	public Response deleteInstituteInfo(String reqObj, UserSignInContainter userDetails) {
		return instituteRepository.deleteInstituteInfo(reqObj, userDetails);
	}

	public Response find(Long id) {
		return instituteRepository.find(id);
	}

}
