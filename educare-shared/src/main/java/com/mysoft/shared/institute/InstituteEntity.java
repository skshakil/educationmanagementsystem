package com.mysoft.shared.institute;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mysoft.core.base.BaseOraEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "SA_INSTITUTE")
public class InstituteEntity extends BaseOraEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "INSTITUTE_NO")
	private Long id;

	@Column(name = "INSTITUTE_ID")
	private String instituteId;

	@Column(name = "INSTITUTE_NAME")
	private String instituteName;

	@Column(name = "INSTITUTE_CAMPUS")
	private String instituteCampus;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "PRE_ADDRESS")
	private String presentAddress;

	@Column(name = "PRE_VILLAGE")
	private String presentVillage;

	@Column(name = "PRE_UNION")
	private String presentUnion;

	@Column(name = "PRE_POFFICE")
	private String presentPostOffice;

	@Column(name = "PRE_THANA")
	private String presentThana;

	@Column(name = "PRE_DISTRICT")
	private String presentDistrict;

	@Column(name = "PRE_DIVISION")
	private String presentDivision;

	@Column(name = "PRE_COUNTRY")
	private String presentCountry;

	@Column(name = "PER_ADDRESS")
	private String permanentAddress;

	@Column(name = "PER_VILLAGE")
	private String permanentVillage;

	@Column(name = "PER_UNION")
	private String permanentUnion;

	@Column(name = "PER_POFFICE")
	private String permanentPostOffice;

	@Column(name = "PER_THANA")
	private String permanentThana;

	@Column(name = "PER_DISTRICT")
	private String permanentDistrict;

	@Column(name = "PER_DIVISION")
	private String permanentDivision;

	@Column(name = "PER_COUNTRY")
	private String permanentCountry;

	@Column(name = "CONTACT")
	private String contact;

	@Column(name = "MOBILE_NO")
	private String mobileNo;

	@Column(name = "PHONE_NO")
	private String phoneNo;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "FAX")
	private String fax;

	@Column(name = "WEB")
	private String web;

	@Column(name = "ESTABLISHED")
	private Long established;

	@Column(name = "CODE")
	private Long code;

	@Column(name = "REG_NO")
	private String regNo;

	@Column(name = "LICENSE_NO")
	private String licenseNo;

//	@Column(name = "LOGO")
//	private byte [] logo;
//
//	@Column(name = "FAVICON")
//	private byte [] favicon;

	@Column(name = "INS_HISTORY")
	private String instituteHistory;

//	@Column(name = "SS_UPLOADED_ON")
//	private Date uploadedOn;

}
