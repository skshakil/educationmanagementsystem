package com.mysoft.shared.institute.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.shared.institute.util.InstituteAppCommonFunctions;

@Repository
@Transactional
public class InstituteAppBaseRepository extends BaseRepository implements InstituteAppCommonFunctions {
    private final Logger logger = LoggerFactory.getLogger(getClass());
}
