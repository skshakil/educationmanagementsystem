package com.mysoft.shared.institute;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.user.CoreUserEntity;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.faculty.facultyInfo.FacultyEntity;
import com.mysoft.shared.institute.base.InstituteAppBaseRepository;

@Repository
@Transactional
public class InstituteRepository extends InstituteAppBaseRepository {

	public Response list(String reqObj) {
		InstituteEntity instituteObj = objectMapperReadValue(reqObj, InstituteEntity.class);
		return baseList(criteriaQuery(instituteObj));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {
		DataTableResults<FacultyEntity> dataTableResults = null;
		Response response = new Response();
		InstituteEntity instituteObj = new InstituteEntity();
		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(instituteObj);
		List gridList = new ArrayList<>();
		response = baseList(typedQuery(instituteObj, dataTableInRQ));
		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response save(String reqObj, UserSignInContainter userDetails) {
		boolean porcContinueFlag = true;
		Response response = new Response();
		InstituteEntity instituteObj = null;
		JSONObject json = new JSONObject(reqObj);
		String instituteObjStr = Def.getString(json, "instituteObj");
		if (instituteObjStr != null && !instituteObjStr.equals("{}")) {
			instituteObj = objectMapperReadValue(instituteObjStr, InstituteEntity.class);
		}
		if (instituteObj != null) {
			instituteObj.setSsCreator(userDetails.getUserId());
			instituteObj.setSsCreatedOn(new Date());
			instituteObj.setSsCreateSession(userDetails.getSessionNo());
			instituteObj.setSsModifier(userDetails.getUserId());
			instituteObj.setSsModifiedOn(new Date());
			instituteObj.setSsModifiedSession(userDetails.getSessionNo());
			instituteObj.setCompanyNo(userDetails.getCompanyNo());
			instituteObj.setOrganizationNo(userDetails.getOrganizationNo());

			response = baseOnlySave(instituteObj);

			response.setObj(instituteObj);
			response.setMessage("Institute information save successfully");
			return response;
		}
		response.setSuccess(false);
		response.setObj(instituteObj);
		response.setMessage("Institute information not found!");
		return response;
	}

	public Response update(String reqObj, UserSignInContainter userDetails) {
		boolean porcContinueFlag = true;
		Response response = new Response();
		InstituteEntity instituteObj = null;
		JSONObject json = new JSONObject(reqObj);
		String instituteObjStr = Def.getString(json, "instituteObj");
		if (instituteObjStr != null && !instituteObjStr.equals("{}")) {
			instituteObj = objectMapperReadValue(instituteObjStr, InstituteEntity.class);
		}
		InstituteEntity obj = findById(instituteObj.getId());
		if (obj != null) {
			obj.setInstituteId(instituteObj.getInstituteId());
			obj.setInstituteName(instituteObj.getInstituteName());
			obj.setInstituteCampus(instituteObj.getInstituteCampus());
			obj.setPresentAddress(instituteObj.getPresentAddress());
			obj.setContact(instituteObj.getContact());
			obj.setRegNo(instituteObj.getRegNo());
			obj.setEmail(instituteObj.getEmail());

			obj.setSsModifier(userDetails.getUserId());
			obj.setSsModifiedOn(new Date());
			obj.setSsModifiedSession(userDetails.getSessionNo());
			response = baseUpdate(obj);
			if (response.isSuccess()) {
				response.setObj(obj);
				response.setMessage("Faculty Information update successfully.");
				return response;
			}
		}
		return getErrorResponse("Update failed, Record not Found!");
	}

	private InstituteEntity findById(Long id) {
		
		System.err.println("ID:  "+id);
		InstituteEntity instituteObj = new InstituteEntity();
		instituteObj.setId(id);
		Response response = baseFindById(criteriaQuery(instituteObj));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), InstituteEntity.class);
		}
		return null;
	}

	public Response remove(Long id) {
		InstituteEntity instituteObj = findById(id);
		if (instituteObj == null) {
			return getErrorResponse("Record Not Found");
		}
		return baseRemove(instituteObj);
	}
	
	public Response delete(Long id) {
		InstituteEntity instituteObj = findById(id);
		return baseDelete(instituteObj);
	}

	public Response deleteInstituteInfo(String reqObj, UserSignInContainter userDetails) {
		boolean porcContinueFlag = true;
		Response response = new Response();
		JSONObject json = new JSONObject(reqObj);
		InstituteEntity instituteObj = null;
		if (reqObj != null && reqObj.equals("{}")) {
			String instituteObjStr = Def.getString(json, "instituteObj");
			if (instituteObjStr != null && !instituteObjStr.equals("{}")) {
				instituteObj = objectMapperReadValue(instituteObjStr, InstituteEntity.class);
			}
			InstituteEntity obj = findById(instituteObj.getId());
			if (obj != null) {
				obj.setSsModifier(userDetails.getUserId());
				obj.setSsModifiedOn(new Date());
				obj.setSsModifiedSession(userDetails.getSessionNo());
				response = baseRemove(obj);

				if (response.isSuccess()) {
					response.setMessage("Institute Information delete succussfully done");
					return response;
				}
			}
		}
		return getErrorResponse("Delete failed, Record not found!");
	}

	public InstituteEntity findByUserNo(Long id) {
		InstituteEntity instituteObj = new InstituteEntity();
		instituteObj.setId(id);
//		instituteObj.setActiveStatus(1);
		Response response = baseFindById(criteriaQuery(instituteObj));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), InstituteEntity.class);
		}
		return null;
	}

	public Response find(Long id) {
		InstituteEntity instituteEntity = new InstituteEntity();
		instituteEntity.setId(id);
		Response response = baseFindById(criteriaQuery(instituteEntity));
		if (response.isSuccess()) {
			InstituteEntity user = getValueFromObject(response.getObj(), InstituteEntity.class);

			response.setObj(user);
			return response;
		} else {
			return getErrorResponse("User Not found");
		}
	}

	/* Non API */

	private CriteriaQuery criteriaQuery(InstituteEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	private <T> TypedQuery typedQuery(InstituteEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}
		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);
		}
		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);
		}
		criteria.where(pArrayJoin.toArray(new Predicate[0]));
		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(InstituteEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<InstituteEntity> root = from(InstituteEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	private List<Predicate> criteriaCondition(InstituteEntity filter, CriteriaBuilder builder,
			Root<InstituteEntity> root) {
		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}
		List<Predicate> p = new ArrayList<Predicate>();
		if (filter != null) {
//            if (filter.getActiveStatus() > 0) {
//                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
//                p.add(condition);
//            }
            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }
		}
		return p;
	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(InstituteEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

}
