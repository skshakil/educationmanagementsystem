package com.mysoft.shared.base;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.util.SharedCommonFunctions;

public class SharedBaseRepository extends BaseRepository implements SharedCommonFunctions {

	@Autowired
	private EntityManager entityManager;

	public Long opdAppointmentNo(UserSignInContainter userDetails) {
		return storedProcedureGenerateNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "SEQ_APPOINT_NO");
	}

	public Long opdAppointmentVitalNo(UserSignInContainter userDetails) {
		return storedProcedureGenerateNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "SEQ_VITAL_NO");
	}

	public Long itemNo(UserSignInContainter userDetails) {
		return functionFdAutoNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "IN_ITEM", "ITEM_NO", 6L);
	}
	
	public Long getPrescriptionMedicineNo(UserSignInContainter userDetails) {
		return storedProcedureGenerateNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "SEQ_PRES_MED_NO");
	}
	
	public Long getPrescriptionTempMedicationNo(UserSignInContainter userDetails) {
		return storedProcedureGenerateNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "SEQ_PRES_TEMP_MEDICINE_NO");
	}
	public Long getPrescriptionTempMedicationNoDtl(UserSignInContainter userDetails) {
		return storedProcedureGenerateNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "SEQ_PRES_TEMP_MEDDTL_NO");
	}
	
	

	public Response opdAppointmentValidation(String personalNo, Long departmentNo, UserSignInContainter userDetails) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("pkg_cmh_policy.pd_appoint_validate")

				.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, Long.class, ParameterMode.OUT)
				.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT)

				.setParameter(1, userDetails.getOrganizationNo()).setParameter(2, userDetails.getCompanyNo())
				.setParameter(3, personalNo).setParameter(4, departmentNo);

		query.execute();

		Long action = query.getOutputParameterValue(5) != null ? (Long) query.getOutputParameterValue(5) : null;
		String message = query.getOutputParameterValue(6) != null ? (String) query.getOutputParameterValue(6) : "";

		Response response = new Response();
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("action", action);
		resMap.put("message", message);

		response.setObj(resMap);

		return response;

	}

	public Integer opdAppointmentSerial(Long departmentNo, Long roomNo, String appointmentDate) {
		BigDecimal maxValue = null;
		maxValue = (BigDecimal) entityManager
				.createNativeQuery("SELECT fd_get_roomwise_sl_dt(:p_bu_no, :p_room_no,:p_dt) FROM DUAL")
				.setParameter("p_bu_no", departmentNo).setParameter("p_room_no", roomNo)
				.setParameter("p_dt", appointmentDate).getSingleResult();

		if (maxValue == null) {
			return null;
		}
		return maxValue.intValue();

	}

	public Response storedProcedurePatientValidate(String personalNo) {

		int index = 0;

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("pkg_cmh_policy.pd_validate_patient")

				.registerStoredProcedureParameter(++index, String.class, ParameterMode.IN) // 1.p_personal_no

				.registerStoredProcedureParameter(++index, Integer.class, ParameterMode.OUT)
				.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)
				.registerStoredProcedureParameter(++index, Integer.class, ParameterMode.OUT)

				.setParameter(1, personalNo);

		query.execute();

		Integer action = query.getOutputParameterValue(2) != null ? (Integer) query.getOutputParameterValue(2) : null;
		String message = query.getOutputParameterValue(3) != null ? (String) query.getOutputParameterValue(3) : "";
		Integer regType = query.getOutputParameterValue(4) != null ? (Integer) query.getOutputParameterValue(4) : null;

		Response response = new Response();
		Map<String, Object> resMap = new HashMap<String, Object>();

		resMap.put("action", action);
		resMap.put("message", message);
		resMap.put("regType", regType);

		response.setObj(resMap);

		return response;

	}

	public Long getStampNo() {
		Long consDtlNo = null;
		Connection con = getOraConnection();
		CallableStatement cstm = null;
		try {
			cstm = con.prepareCall("{call K_GENERAL.PD_GENARATE_NO(?,?,?,?,?)}");
			cstm.setString(1, "SEQ_OPD_CONSULTATIONDLT_NO"); // "SEQ_OPD_CONSULTATIONDLT_NO"
			cstm.setLong(2, 1);
			cstm.registerOutParameter(3, java.sql.Types.NUMERIC);
			cstm.setString(4, "YYMM");// "YYMM"
			cstm.setLong(5, 10);// 10
			cstm.execute();

			consDtlNo = cstm.getLong(3);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (con != null) {
					con.close();
				}
				if (cstm != null) {
					cstm.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return consDtlNo;
	}

    public Response storedProcedureMedicationValidation(String personalNo, Long itemNo) {

		int index = 0;
		
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PD_PRES_MEDICINE_STATE")
	
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.IN)  //1.in_personal_number
		.registerStoredProcedureParameter(++index, Long.class, ParameterMode.IN)//2.in_item_no
		.registerStoredProcedureParameter(++index, Date.class, ParameterMode.IN) //3.in_date
		.registerStoredProcedureParameter(++index, Integer.class, ParameterMode.OUT)//4.out_item_stock
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)//5.out_item_status
		.registerStoredProcedureParameter(++index, Date.class, ParameterMode.OUT)//6.out_prescribedon
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)//7.out_itemname
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)//8.out_department
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)//9.out_dosage
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)//10.out_duration
		.registerStoredProcedureParameter(++index, Date.class, ParameterMode.OUT)//11.out_frmdate
		.registerStoredProcedureParameter(++index, Date.class, ParameterMode.OUT)//12.out_todate
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)//13.out_err_message
		.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)//14.out_doctor_name
				
		
		.setParameter(1, personalNo)
		.setParameter(2, itemNo)
		.setParameter(3, clearTime(new Date()));

	
		query.execute();

		Integer itemStock = query.getOutputParameterValue(4) != null ? (Integer) query.getOutputParameterValue(4) : null;
		String itemStatus = query.getOutputParameterValue(5) != null ? (String) query.getOutputParameterValue(5) : "";
		Date prescribedOn = query.getOutputParameterValue(6) != null ? (Date) query.getOutputParameterValue(6) : null;
		String itemName = query.getOutputParameterValue(7) != null ? (String) query.getOutputParameterValue(7) : "";
		String departmentName = query.getOutputParameterValue(8) != null ? (String) query.getOutputParameterValue(8) : "";
		String dosage = query.getOutputParameterValue(9) != null ? (String) query.getOutputParameterValue(9) : "";
		String duration = query.getOutputParameterValue(10) != null ? (String) query.getOutputParameterValue(10) : "";
		Date fromDate = query.getOutputParameterValue(11) != null ? (Date) query.getOutputParameterValue(11) : null;
		Date toDate = query.getOutputParameterValue(12) != null ? (Date) query.getOutputParameterValue(12) : null;
		String errMessage = query.getOutputParameterValue(13) != null ? (String) query.getOutputParameterValue(13) : "";
		String doctorName = query.getOutputParameterValue(14) != null ? (String) query.getOutputParameterValue(14) : "";

		Response response = new Response();
		Map<String, Object> resMap = new HashMap<String, Object>();
		
	
		resMap.put("itemStock", itemStock);
		resMap.put("itemStatus", itemStatus);
		resMap.put("prescribedOn", prescribedOn);
		resMap.put("itemName", itemName);
		resMap.put("departmentName", departmentName);
		resMap.put("dosage", dosage);
		resMap.put("duration", duration);
		resMap.put("fromDate", fromDate);
		resMap.put("toDate", toDate);
		resMap.put("errMessage", errMessage);
		resMap.put("doctorName", doctorName);

		response.setObj(resMap);

		return response;

	}

    public Long getTherapyModalityNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "RPS_MODALITY_SETUP", "MODALITY_NO", 6l);
	}

	public Long getTherapyTypeNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "RPS_THERAPY_TYPE_SETUP", "THERAPY_TYPE_NO", 6l);
	}

	public Long getTherapyNo(Long ogNo, Long conpanyNO) {
		return storedProcedureGenerateNo(ogNo, conpanyNO, "SEQ_THERAPY_NO");
	}

	public Long getTherapyDtlNo(Long ogNo, Long conpanyNO) {
		return storedProcedureGenerateNo(ogNo, conpanyNO, "SEQ_THERAPY_DTL_NO");
	}

	public Long getTherapyReqNo(Long ogNo, Long conpanyNO) {
		return storedProcedureGenerateNo(ogNo, conpanyNO, "SEQ_RP_THERAPY_REQ_NO");
	}

	public Long getIpdsWardNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "IPDS_WARD", "WARD_NO", 6l);
	}

	public Long getIpdsWardFacilityNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "IPDS_WARDFACCILITIES", "WARDFACCILITIES_NO", 6l);
	}

	public Long getPrescriptionDetailNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_DTL_NO");
	}

	public Long getPrescriptionMedicineNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_MED_NO");
	}

	public Long getPrescriptionNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRESCRIPTION_NO");
	}

	public Long getPrescriptionEyeNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_EYE_NO");
	}

	public Long getPrescriptionPhysicalExamNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_PHY_CLI_NO");
	}

	public Long getPresLookupGroupSeqNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_LOOKUP_GROUP_NO");
	}

	public Long getPresLookupGroupDetailSeqNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRS_LOOKUP_GROUP_DETAIL_NO");
	}

	public Long getPrescriptionTemplateNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_TEMPLATE_NO");
	}

	public Long getPrescriptionTempDetailsNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_TEMP_DETAILS_NO");
	}

	public Long getPrescriptionTempMedicineNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_TEMP_MEDICINE_NO");
	}

	public Long getPreDiagnosisNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_PRE_DIAGNOSIS_NO");
	}

	public Long getFavouriteNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_PRES_FAV_NO");
	}

	public Long userPreferenceNo(Long ogNo, Long conpanyNo) {
		return storedProcedureGenerateNo(ogNo, conpanyNo, "SEQ_USER_PREF_NO");
	}

	public Long getCaseHistoryNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_CASE_HISTORY_NO");
	}

	public Long otRequisitionNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "OT_REQ", "OT_REQ_NO", 6l);
	}
	
	public Long generateReqDtlNo(UserSignInContainter userDetails) {
        return storedProcedureGenerateNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "seq_ot_reqdtl_no");
    }
	
	public Long otRequisitionDtlNo(Long ogNo, Long conpanyNO) {
        return functionFdAutoNo(ogNo, conpanyNO, "OT_REQDTL", "OT_REQDTL_NO", 6l);
    }
	public Long generateStoreCaveragerNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "IN_STORE_COVERAGE", "STORE_COV_NO", 6l);
	}

	public Long ipdBedHistoryNo(Long ogNo, Long conpanyNo) {
		return functionFdAutoNo(ogNo, conpanyNo, "IPD_BEDHISTORY", "BEDHISTORY_NO", 13l);
	}

	public Long ipdsBedNo(Long ogNo, Long conpanyNO) {
        return functionFdAutoNo(ogNo, conpanyNO, "IPDS_BED", "BED_NO", 6l);
    }
	
	public Long grantNsDsNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "SA_GRANTNS", "GRANTNS_NO", 6l);
	}
	
	public Long nsCoverageNo(Long ogNo, Long conpanyNO) {
		return functionFdAutoNo(ogNo, conpanyNO, "IPDS_NS_COVERAGE", "COV_NO", 6l);
	}

	public Long getDisSumNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_DISCHARGE_SUM_NO");
	}
	public Long getHandoverSumNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_DH_SUM_NO");
	}
	
	public Long getDisSumDtlNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_DIS_DTL_NO");
	}
	
	public Long getHandoverSumDtlNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_DH_DTL_NO");
	}
	
	public Long getDisSumDtlTranNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_TRANSACTION_NO");
	}

	public Long getDisSumMedNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_DIS_MED_NO");
	}
	
	public Long getDisSumMedDtlNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_DIS_MED_DTL_NO");
	}
	

	public Long getFileSharedNo(UserSignInContainter userDetails) {
		return functionFdAutoNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(), "FILE_SHARED", "FILE_SHARED_NO", 6L);
	}
	
}
