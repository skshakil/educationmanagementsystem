package com.mysoft.shared.faculty.facultyEducationInfo;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = "FA_FACULTY_EDUCATION")
public class FacultyEducationEntity extends BaseOraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FACULTY_EDUCATION_NO")
    private Long id;

    @Column(name = "FACULTY_EDUCATION_ID")
    private String facultyEducationId;

    @Column(name = "FACULTY_NO")
    private Long facultyNo;

    @Column(name = "COURSE_NAME")
    private String courseName;

    @Column(name = "INSTITUTION_NAME")
    private String institutionName;

    @Column(name = "BOARD_NAME")
    private String boardName;

    @Column(name = "MAJOR_SUBJECT")
    private String majorSubject;

    @Column(name = "ADMISSION_YEAR")
    private String admissionYear;

    @Column(name = "PASSING_YEAR")
    private String passingYear;

    @Column(name = "REG_NO")
    private String regNo;

    @Column(name = "ROOL_NO")
    private String rollNo;

    @Column(name = "GPA_GRADE")
    private String grade;

    @Column(name = "ACTIVE_STAT")
    private Integer activeStatus = 1;

    @Transient
    private Integer isDeleted;
}
