package com.mysoft.shared.faculty.facultyFamily;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyFamilyService {

    @Autowired
    private FacultyFamilyRepository facultyFamilyRepository;

    public Response findByFacultyId(String reqObj) {
        return facultyFamilyRepository.findByFacultyId(reqObj);
    }

    public Response saveFamilyMember(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        return facultyFamilyRepository.saveFamilyMember(reqObj, facultyNo, userDetails);
    }

    public Response familyMemberUpdate(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        return facultyFamilyRepository.familyMemberUpdate(reqObj, facultyNo, userDetails);
    }
}
