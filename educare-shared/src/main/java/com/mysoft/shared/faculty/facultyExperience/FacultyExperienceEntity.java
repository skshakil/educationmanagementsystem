package com.mysoft.shared.faculty.facultyExperience;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@ToString
@Table(name = "FA_FACULTY_EXPERIENCE")
public class FacultyExperienceEntity extends BaseOraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FACULTY_EXPERIENCE_NO")
    private Long id;

    @Column(name = "FACULTY_EXPERIENCE_ID")
    private String facultyExperienceId;

    @Column(name = "FACULTY_NO")
    private Long facultyNo;

    @Column(name = "JOB_DESCRIPTION")
    private String jobDescription;

    @Column(name = "JOB_DESIGNATION")
    private String jobDesignation;

    @Column(name = "INSTITUTION_NAME")
    private String institutionName;

    @Column(name = "JOIN_DATE")
    private Date joinDate;

    @Column(name = "RESIGN_DATE")
    private Date resignDate;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ACTIVE_STAT")
    private Integer activeStatus = 1;

    @Transient
    private Integer isDeleted;
}
