package com.mysoft.shared.faculty.facultyEducationInfo;

import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.base.SharedBaseRepository;
import com.mysoft.shared.faculty.facultyFamily.FacultyFamilyEntity;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class FacultyEducationRepository extends SharedBaseRepository {

    public Response saveFacultyEducation(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        List<FacultyEducationEntity> educationInfoList = null;
//        if (reqObj != null && !reqObj.equals("[]")) {
//            educationInfoList = objectMapperReadArrayValue(reqObj, FacultyEducationEntity.class);
//        }

        JSONObject json = new JSONObject(reqObj);
        String educationObjStr = Def.getString(json, "educationInfoList");
        if (educationObjStr != null && !educationObjStr.equals("[]")) {
            educationInfoList = objectMapperReadArrayValue(educationObjStr, FacultyEducationEntity.class);

            for (FacultyEducationEntity educationEntity : educationInfoList) {
                educationEntity.setFacultyNo(facultyNo);
                educationEntity.setSsCreator(userDetails.getUserId());
                educationEntity.setSsCreatedOn(new Date());
                educationEntity.setSsCreateSession(userDetails.getSessionNo());
                educationEntity.setSsModifier(userDetails.getUserId());
                educationEntity.setSsModifiedOn(new Date());
                educationEntity.setSsModifiedSession(userDetails.getSessionNo());
                educationEntity.setCompanyNo(userDetails.getCompanyNo());
                educationEntity.setOrganizationNo(userDetails.getOrganizationNo());

                baseOnlySave(educationEntity);
            }
        }
        return getSuccessResponse("Faculty education save successfully.");
    }

    public Response educationInfoUpdate(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        List<FacultyEducationEntity> educationInfoList = null;
        JSONObject json = new JSONObject(reqObj);
        String educationObjStr = Def.getString(json, "educationInfoList");

        if (educationObjStr != null && !educationObjStr.equals("[]")) {
            educationInfoList = objectMapperReadArrayValue(educationObjStr, FacultyEducationEntity.class);

            for (FacultyEducationEntity educationEntity : educationInfoList) {
                educationEntity.setFacultyNo(facultyNo);

                educationEntity.setSsModifier(userDetails.getUserId());
                educationEntity.setSsModifiedOn(new Date());
                educationEntity.setSsModifiedSession(userDetails.getSessionNo());

                return baseUpdate(educationEntity);
            }
        }
        return getErrorResponse("Faculty Education update successfully.");
    }

    public Boolean saveUpdateOrDelete(List<FacultyEducationEntity> batch, Long facultyNo) {
        Response response = null;
        Boolean flag = true;

        for (FacultyEducationEntity obj : batch) {
            obj.setFacultyNo(facultyNo);
            if (obj.getIsDeleted() != null && obj.getIsDeleted() == 1) {
                response = removeById(obj.getId());
                if (!response.isSuccess()) {
                    flag = false;
                    break;
                }
            } else {
                if (obj.getId() != null) {
                    response = updateObj(obj);
                    if (!response.isSuccess()) {
                        flag = false;
                        break;
                    }
                } else {
                    response = saveObj(obj);
                    if (!response.isSuccess()) {
                        flag = false;
                        break;
                    }
                }
            }
        }
        return flag;
    }

    /* Non API */
    public Response saveObj(FacultyEducationEntity facultyInfoObj) {
//        facultyInfoObj.setSsCreator(userDetails().getUserId());
//        facultyInfoObj.setSsCreatedOn(new Date());
//        facultyInfoObj.setSsCreateSession(userDetails().getSessionNo());
//        facultyInfoObj.setCompanyNo(userDetails().getCompanyNo());
//        facultyInfoObj.setOrganizationNo(userDetails().getOrganizationNo());
        return baseOnlySave(facultyInfoObj);
    }

    public Response removeById(Long id) {
        FacultyEducationEntity obj = findByIdObj(id);
        if (obj != null) {
//            obj.setSsModifier(userDetails().getUserId());
//            obj.setSsModifiedOn(new Date());
//            obj.setSsModifiedSession(userDetails().getSessionNo());
            obj.setActiveStatus(0);
            return baseRemove(obj);
        }
        return getErrorResponse("Faculty info not found !");
    }

    public FacultyEducationEntity findByIdObj(Long id) {
        FacultyEducationEntity facultyInfoObj = new FacultyEducationEntity();
        facultyInfoObj.setId(id);
        Response response = baseFindById(criteriaQuery(facultyInfoObj));
        if (response.isSuccess()) {
            return getValueFromObject(response.getObj(), FacultyEducationEntity.class);
        }
        return null;
    }

    public Response updateObj(FacultyEducationEntity reqObj) {
        FacultyEducationEntity obj = findByIdObj(reqObj.getId());
        if (obj != null) {
            obj.setAdmissionYear(reqObj.getAdmissionYear());
            obj.setCourseName(reqObj.getCourseName());
            obj.setInstitutionName(reqObj.getInstitutionName());
            obj.setBoardName(reqObj.getBoardName());
            obj.setMajorSubject(reqObj.getMajorSubject());

//            obj.setSsModifier(userDetails().getUserId());
//            obj.setSsModifiedOn(new Date());
//            obj.setSsModifiedSession(userDetails().getSessionNo());

            return baseUpdate(obj);
        }
        return getErrorResponse("Record not Found !!");
    }

    private CriteriaQuery criteriaQuery(FacultyEducationEntity filter) {
        init();
        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);
        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    private List<Predicate> criteriaCondition(FacultyEducationEntity filter, CriteriaBuilder builder,
                                              Root<FacultyEducationEntity> root) {
        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }
        List<Predicate> p = new ArrayList<Predicate>();
        if (filter != null) {
            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }
            if (filter.getFacultyNo() != null && filter.getFacultyNo() > 0) {
                Predicate condition = builder.equal(root.get("facultyNo"), filter.getFacultyNo());
                p.add(condition);
            }
        }
        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(FacultyEducationEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }
}
