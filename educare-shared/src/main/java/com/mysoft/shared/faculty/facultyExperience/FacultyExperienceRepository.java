package com.mysoft.shared.faculty.facultyExperience;

import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.base.SharedBaseRepository;
import com.mysoft.shared.faculty.facultyEducationInfo.FacultyEducationEntity;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class FacultyExperienceRepository extends SharedBaseRepository {

    public Response saveFacultyExperience(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        List<FacultyExperienceEntity> experienceInfoList = null;
//        if (reqObj != null && !reqObj.equals("[]")) {
//            experienceInfoList = objectMapperReadArrayValue(reqObj, FacultyExperienceEntity.class);
//        }

        JSONObject json = new JSONObject(reqObj);
        String experienceObjStr = Def.getString(json, "experienceInfoList");
        if (experienceObjStr != null && !experienceObjStr.equals("[]")) {
            experienceInfoList = objectMapperReadArrayValue(experienceObjStr, FacultyExperienceEntity.class);

            for (FacultyExperienceEntity experienceEntity : experienceInfoList) {
                experienceEntity.setFacultyNo(facultyNo);
                experienceEntity.setSsCreator(userDetails.getUserId());
                experienceEntity.setSsCreatedOn(new Date());
                experienceEntity.setSsCreateSession(userDetails.getSessionNo());
                experienceEntity.setSsModifier(userDetails.getUserId());
                experienceEntity.setSsModifiedOn(new Date());
                experienceEntity.setSsModifiedSession(userDetails.getSessionNo());
                experienceEntity.setCompanyNo(userDetails.getCompanyNo());
                experienceEntity.setOrganizationNo(userDetails.getOrganizationNo());

                baseOnlySave(experienceEntity);
            }
        }
        return getSuccessResponse("Faculty experience save successfully.");
    }

    public Response experienceInfoUpdate(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        List<FacultyExperienceEntity> experienceInfoList = null;
        JSONObject json = new JSONObject(reqObj);
        String experienceObjStr = Def.getString(json, "experienceInfoList");

        if (experienceObjStr != null && !experienceObjStr.equals("[]")) {
            experienceInfoList = objectMapperReadArrayValue(experienceObjStr, FacultyExperienceEntity.class);

            for (FacultyExperienceEntity experienceEntity : experienceInfoList) {
                experienceEntity.setFacultyNo(facultyNo);

                experienceEntity.setSsModifier(userDetails.getUserId());
                experienceEntity.setSsModifiedOn(new Date());
                experienceEntity.setSsModifiedSession(userDetails.getSessionNo());

                return baseUpdate(experienceEntity);
            }
        }
        return getErrorResponse("Faculty experience update successfully.");
    }

    public boolean saveUpdateOrDelete(List<FacultyExperienceEntity> batch, Long facultyNo) {
        Response response = null;
        Boolean flag = true;

        for (FacultyExperienceEntity obj : batch) {
            obj.setFacultyNo(facultyNo);
            if (obj.getIsDeleted() != null && obj.getIsDeleted() == 1) {
                response = removeById(obj.getId());
                if (!response.isSuccess()) {
                    flag = false;
                    break;
                }
            } else {
                if (obj.getId() != null) {
                    response = updateObj(obj);
                    if (!response.isSuccess()) {
                        flag = false;
                        break;
                    }
                } else {
                    response = saveObj(obj);
                    if (!response.isSuccess()) {
                        flag = false;
                        break;
                    }
                }
            }
        }
        return flag;
    }

    public Response saveObj(FacultyExperienceEntity facultyInfoObj) {
//        facultyInfoObj.setSsCreator(userDetails().getUserId());
//        facultyInfoObj.setSsCreatedOn(new Date());
//        facultyInfoObj.setSsCreateSession(userDetails().getSessionNo());
//        facultyInfoObj.setCompanyNo(userDetails().getCompanyNo());
//        facultyInfoObj.setOrganizationNo(userDetails().getOrganizationNo());
        return baseOnlySave(facultyInfoObj);
    }

    public Response removeById(Long id) {
        FacultyExperienceEntity obj = findByIdObj(id);
        if (obj != null) {
//            obj.setSsModifier(userDetails().getUserId());
//            obj.setSsModifiedOn(new Date());
//            obj.setSsModifiedSession(userDetails().getSessionNo());
            obj.setActiveStatus(0);
            return baseRemove(obj);
        }
        return getErrorResponse("Faculty info not found !");
    }

    public FacultyExperienceEntity findByIdObj(Long id) {
        FacultyExperienceEntity facultyInfoObj = new FacultyExperienceEntity();
        facultyInfoObj.setId(id);
        Response response = baseFindById(criteriaQuery(facultyInfoObj));
        if (response.isSuccess()) {
            return getValueFromObject(response.getObj(), FacultyExperienceEntity.class);
        }
        return null;
    }

    public Response updateObj(FacultyExperienceEntity reqObj) {
        FacultyExperienceEntity obj = findByIdObj(reqObj.getId());
        if (obj != null) {
//            obj.setExpTitle(reqObj.getExpTitle());
//            obj.setExpInstitute(reqObj.getExpInstitute());
//            obj.setExpDesc(reqObj.getExpDesc());
//            obj.setExpCertificDate(reqObj.getExpCertificDate());
//            obj.setExpStartDate(reqObj.getExpStartDate());
//            obj.setExpEndDate(reqObj.getExpEndDate());

//            obj.setSsModifier(userDetails().getUserId());
//            obj.setSsModifiedOn(new Date());
//            obj.setSsModifiedSession(userDetails().getSessionNo());

            return baseUpdate(obj);
        }
        return getErrorResponse("Record not Found !!");
    }

    /* Non API */
    private CriteriaQuery criteriaQuery(FacultyExperienceEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> criteriaCondition(FacultyExperienceEntity filter, CriteriaBuilder builder,
                                              Root<FacultyExperienceEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {

            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }

            if (filter.getFacultyNo() != null && filter.getFacultyNo() > 0) {
                Predicate condition = builder.equal(root.get("studentNo"), filter.getFacultyNo());
                p.add(condition);
            }

        }

        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(FacultyExperienceEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }
}
