package com.mysoft.shared.faculty.facultyInfo;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class FacultyService {

    @Autowired
    private FacultyRepository facultyRepository;

    public Response list(String reqObj) {
        return facultyRepository.list(reqObj);
    }

    public Response gridList(HttpServletRequest request) {
        return facultyRepository.gridList(request);
    }

    public Response save(String reqObj, UserSignInContainter userDetails) {
        return facultyRepository.save(reqObj, userDetails);
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {
        return facultyRepository.update(reqObj, userDetails);
    }

    public Response delete(Long id) {
        return facultyRepository.remove(id);
    }

    public Response deleteFacultyInfo(String reqObj, UserSignInContainter userDetails) {
        return facultyRepository.deleteFacultyInfo(reqObj, userDetails);
    }

    public Response findByUserNo(Long id) {
        return facultyRepository.find(id);
    }

}
