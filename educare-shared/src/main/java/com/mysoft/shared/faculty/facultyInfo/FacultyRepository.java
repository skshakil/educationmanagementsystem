package com.mysoft.shared.faculty.facultyInfo;

import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.base.SharedBaseRepository;
import com.mysoft.shared.faculty.facultyEducationInfo.FacultyEducationEntity;
import com.mysoft.shared.faculty.facultyEducationInfo.FacultyEducationService;
import com.mysoft.shared.faculty.facultyExperience.FacultyExperienceEntity;
import com.mysoft.shared.faculty.facultyExperience.FacultyExperienceService;
import com.mysoft.shared.faculty.facultyFamily.FacultyFamilyEntity;
import com.mysoft.shared.faculty.facultyFamily.FacultyFamilyService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class FacultyRepository extends SharedBaseRepository {

    @Autowired
    private FacultyFamilyService facultyFamilyService;
    @Autowired
    private FacultyEducationService facultyEducationService;
    @Autowired
    private FacultyExperienceService facultyExperienceService;

    public Response list(String reqObj) {
        FacultyEntity facultyObj = new FacultyEntity();
        return baseList(criteriaQuery(facultyObj));
    }

    public Response gridList(HttpServletRequest request) {
        DataTableResults<FacultyEntity> dataTableResults = null;
        Response response = new Response();
        FacultyEntity facultyObj = new FacultyEntity();
        DataTableRequest dataTableInRQ = new DataTableRequest(request);
        Long totalRowCount = totalCount(facultyObj);
        List gridList = new ArrayList<>();
        response = baseList(typedQuery(facultyObj, dataTableInRQ));
        if (response.isSuccess()) {
            if (response.getItems() != null) {
                gridList = response.getItems();
            }
            dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
        }
        response.setItems(null);
        response.setObj(dataTableResults);
        return response;
    }

    public Response save(String reqObj, UserSignInContainter userDetails) {
        boolean porcContinueFlag = true;
        Response response = new Response();
        FacultyEntity facultyObj = null;
        JSONObject json = new JSONObject(reqObj);
        String facultyObjStr = Def.getString(json, "facultyObj");
        if (facultyObjStr != null && !facultyObjStr.equals("{}")) {
            facultyObj = objectMapperReadValue(facultyObjStr, FacultyEntity.class);
        }
        if (facultyObj != null) {
            facultyObj.setSsCreator(userDetails.getUserId());
            facultyObj.setSsCreatedOn(new Date());
            facultyObj.setSsCreateSession(userDetails.getSessionNo());
            facultyObj.setSsModifier(userDetails.getUserId());
            facultyObj.setSsModifiedOn(new Date());
            facultyObj.setSsModifiedSession(userDetails.getSessionNo());
            facultyObj.setCompanyNo(userDetails.getCompanyNo());
            facultyObj.setOrganizationNo(userDetails.getOrganizationNo());

            response = baseOnlySave(facultyObj);

            if (response.isSuccess() && response.getObj() != null) {
                facultyObj = (FacultyEntity) response.getObj();
                facultyFamilyService.saveFamilyMember(reqObj, facultyObj.getId(), userDetails);
                facultyEducationService.saveFacultyEducation(reqObj, facultyObj.getId(), userDetails);
                facultyExperienceService.saveFacultyExperience(reqObj, facultyObj.getId(), userDetails);
            }
            response.setObj(facultyObj);
            response.setMessage("Faculty information save successfully");
            return response;
        }
        response.setSuccess(false);
        response.setObj(facultyObj);
        response.setMessage("Faculty information not save!");
        return response;
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {
        boolean porcContinueFlag = true;
        Response response = new Response();
        FacultyEntity facultyObj = null;
        JSONObject json = new JSONObject(reqObj);
        String facultyObjStr = Def.getString(json, "facultyObj");
        if (facultyObjStr != null && !facultyObjStr.equals("{}")) {
            facultyObj = objectMapperReadValue(facultyObjStr, FacultyEntity.class);
        }
        FacultyEntity obj = findById(facultyObj.getId());
        if (obj != null) {
            obj.setFirstName(facultyObj.getFirstName());
            obj.setDateOfBirth(facultyObj.getDateOfBirth());
            obj.setLastName(facultyObj.getLastName());
            obj.setGender(facultyObj.getGender());

            obj.setSsModifier(userDetails.getUserId());
            obj.setSsModifiedOn(new Date());
            obj.setSsModifiedSession(userDetails.getSessionNo());

            response = baseUpdate(obj);
            if (response.isSuccess()) {
                facultyFamilyService.familyMemberUpdate(reqObj, facultyObj.getId(), userDetails);
                facultyEducationService.educationInfoUpdate(reqObj, facultyObj.getId(), userDetails);
                facultyExperienceService.experienceInfoUpdate(reqObj, facultyObj.getId(), userDetails);

                response.setObj(facultyObj);
                response.setMessage("Faculty Information update successfully.");
                return response;
            }
        }
        return getErrorResponse("Update failed, Record not Found!");
    }

    public Response deleteFacultyInfo(String reqObj, UserSignInContainter userDetails) {
        boolean porcContinueFlag = true;
        Response response = new Response();
        FacultyEntity facultyObj = null;
        if (reqObj != null && !reqObj.equals("{}")) {
            facultyObj = objectMapperReadValue(reqObj, FacultyEntity.class);
        }
        FacultyEntity obj = findById(facultyObj.getId());
        if (obj != null) {
            obj.setActiveStatus(0);
            obj.setSsModifier(userDetails.getUserId());
            obj.setSsModifiedOn(new Date());
            obj.setSsModifiedSession(userDetails.getSessionNo());
            response = baseRemove(obj);
            if (response.isSuccess()) {
                response.setMessage("Faculty Information delete succussfully done");
                return response;
            }
        }
        return getErrorResponse("Delete failed, Record not found!");
    }

    private FacultyEntity findById(Long id) {
        FacultyEntity facultyObj = new FacultyEntity();
        facultyObj.setId(id);
        Response response = baseFindById(criteriaQuery(facultyObj));
        if (response.isSuccess()) {
            return getValueFromObject(response.getObj(), FacultyEntity.class);
        }
        return null;
    }

    public Response remove(Long id) {
        FacultyEntity facultyObj = findById(id);
        if (facultyObj == null) {
            return getErrorResponse("Record Not Found");
        }
        return baseRemove(facultyObj);
    }

    public Response find(Long id) {
        FacultyEntity facultyObj = new FacultyEntity();
        facultyObj.setId(id);
        facultyObj.setActiveStatus(1);
        return baseFindById(criteriaQuery(facultyObj));
    }

    /* Non API */

    private CriteriaQuery criteriaQuery(FacultyEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    private <T> TypedQuery typedQuery(FacultyEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }
        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);
        }
        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);
        }
        criteria.where(pArrayJoin.toArray(new Predicate[0]));
        return baseTypedQuery(criteria, dataTableInRQ);
    }

    private Long totalCount(FacultyEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<FacultyEntity> root = from(FacultyEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }

    private List<Predicate> criteriaCondition(FacultyEntity filter, CriteriaBuilder builder, Root<FacultyEntity> root) {
        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }
        List<Predicate> p = new ArrayList<Predicate>();
        if (filter != null) {
            if (filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }
            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }
        }
        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(FacultyEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }
}
