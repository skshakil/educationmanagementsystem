package com.mysoft.shared.faculty.facultyEducationInfo;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyEducationService {

    @Autowired
    private FacultyEducationRepository facultyEducationRepository;

    public Response saveFacultyEducation(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        return facultyEducationRepository.saveFacultyEducation(reqObj, facultyNo, userDetails);
    }

    public Response educationInfoUpdate(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        return facultyEducationRepository.educationInfoUpdate(reqObj, facultyNo, userDetails);
    }

}
