package com.mysoft.shared.faculty.facultyFamily;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = "FA_FACULTY_FAMILY")
public class FacultyFamilyEntity extends BaseOraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FACULTY_FAMILY_NO")
    private Long id;

    @Column(name = "FACULTY_FAMILY_ID")
    private Long facultyFamilyId;

    @Column(name = "FACULTY_NO")
    private Long facultyNo;

    @Column(name = "MEMBER_NAME")
    private String memberName;

    @Column(name = "RELATION_NO")
    private String relationNo;

    @Column(name = "MOBILE_NO")
    private String mobileNo;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "NATIONAL_ID")
    private String NID;

    @Column(name = "OCCUPATION_NAME")
    private String occupationName;

    @Column(name = "ACTIVE_STAT")
    private Integer activeStatus = 1;

    @Transient
    private Integer isDeleted;
}
