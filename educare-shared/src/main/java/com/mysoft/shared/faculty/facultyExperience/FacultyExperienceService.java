package com.mysoft.shared.faculty.facultyExperience;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyExperienceService {

    @Autowired
    private FacultyExperienceRepository facultyExperienceRepository;

    public Response saveFacultyExperience(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        return facultyExperienceRepository.saveFacultyExperience(reqObj, facultyNo, userDetails);
    }

    public Response experienceInfoUpdate(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        return facultyExperienceRepository.experienceInfoUpdate(reqObj, facultyNo, userDetails);
    }

}
