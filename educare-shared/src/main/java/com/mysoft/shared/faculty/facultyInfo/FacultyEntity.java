package com.mysoft.shared.faculty.facultyInfo;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@Entity
@ToString
@Table(name = "FA_FACULTY_INFO")
public class FacultyEntity extends BaseOraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FACULTY_NO")
    private Long id;

    @Column(name = "FACULTY_ID")
    private String facultyId;

    @Column(name = "SALUTATION")
    private String salutation;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "FATHER_NAME")
    private String fatherName;

    @Column(name = "MOTHER_NAME")
    private String motherName;

    @Column(name = "SPOUSE_NAME")
    private String spouseName;

    @Column(name = "DOB")
    private Date dateOfBirth;

    @Column(name = "GENDER")
    private String gender;

    @Column(name = "RELIGION")
    private String religion;

    @Column(name = "MOBILE_NO")
    private String mobileNo;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ADDRESS")
    private String presentAddress;

    @Column(name = "PRE_ADDRESS")
    private String permanentAddress;

    @Column(name = "NATIONALITY")
    private String nationality;

    @Column(name = "ACTIVE_STAT")
    private Integer activeStatus = 1;
}
