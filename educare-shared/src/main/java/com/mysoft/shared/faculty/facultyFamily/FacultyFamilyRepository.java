package com.mysoft.shared.faculty.facultyFamily;

import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.base.SharedBaseRepository;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class FacultyFamilyRepository extends SharedBaseRepository {

    public Response findByFacultyId(String reqObj) {
        FacultyFamilyEntity facultyFamilyObj = new FacultyFamilyEntity();
        JSONObject json = new JSONObject(reqObj);
        Long facultyNo = Def.getLong(json, "facultyNo");
        if (facultyNo == null) {
            return getErrorResponse("Faculty Not found!");
        }
        facultyFamilyObj.setFacultyNo(facultyNo);
        return baseList(criteriaQuery(facultyFamilyObj));
    }

    public Response saveFamilyMember(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        List<FacultyFamilyEntity> familyInfoList = null;
//        if (reqObj != null && !reqObj.equals("[]")) {
//            familyInfoList = objectMapperReadArrayValue(reqObj, FacultyFamilyEntity.class);
//        }

        JSONObject json = new JSONObject(reqObj);
        String familyObjStr = Def.getString(json, "familyInfoList");
        if (familyObjStr != null && !familyObjStr.equals("[]")) {
            familyInfoList = objectMapperReadArrayValue(familyObjStr, FacultyFamilyEntity.class);

            for (FacultyFamilyEntity familyEntity : familyInfoList) {
                familyEntity.setFacultyNo(facultyNo);
                familyEntity.setSsCreator(userDetails.getUserId());
                familyEntity.setSsCreatedOn(new Date());
                familyEntity.setSsCreateSession(userDetails.getSessionNo());
                familyEntity.setSsModifier(userDetails.getUserId());
                familyEntity.setSsModifiedOn(new Date());
                familyEntity.setSsModifiedSession(userDetails.getSessionNo());
                familyEntity.setCompanyNo(userDetails.getCompanyNo());
                familyEntity.setOrganizationNo(userDetails.getOrganizationNo());

                baseOnlySave(familyEntity);
            }
        }
        return getSuccessResponse("Faculty family save successfully.");
    }

    public Response familyMemberUpdate(String reqObj, Long facultyNo, UserSignInContainter userDetails) {
        List<FacultyFamilyEntity> familyInfoList = null;
        JSONObject json = new JSONObject(reqObj);
        String familyObjStr = Def.getString(json, "familyInfoList");

        if (familyObjStr != null && !familyObjStr.equals("[]")) {
            familyInfoList = objectMapperReadArrayValue(familyObjStr, FacultyFamilyEntity.class);

            for (FacultyFamilyEntity familyEntity : familyInfoList) {
                familyEntity.setFacultyNo(facultyNo);

                familyEntity.setSsModifier(userDetails.getUserId());
                familyEntity.setSsModifiedOn(new Date());
                familyEntity.setSsModifiedSession(userDetails.getSessionNo());

                return baseUpdate(familyEntity);
            }
        }
        return getErrorResponse("Faculty Family update successfully.");
    }

    public Response updateObj(FacultyFamilyEntity reqObj, UserSignInContainter userDetails) {
        FacultyFamilyEntity familyEntity = findById(reqObj.getId());
        if (familyEntity != null) {
            familyEntity.setMemberName(reqObj.getMemberName());
            familyEntity.setRelationNo(reqObj.getRelationNo());
            familyEntity.setMobileNo(reqObj.getMobileNo());
            familyEntity.setAddress(reqObj.getAddress());

            familyEntity.setSsModifier(userDetails.getUserId());
            familyEntity.setSsModifiedOn(new Date());
            familyEntity.setSsModifiedSession(userDetails.getSessionNo());

            return baseUpdate(familyEntity);
        }
        return getErrorResponse("Record not Found !!");
    }

    public FacultyFamilyEntity findById(Long id) {
        FacultyFamilyEntity facultyInfoObj = new FacultyFamilyEntity();
        facultyInfoObj.setId(id);
        Response response = baseFindById(criteriaQuery(facultyInfoObj));
        if (response.isSuccess()) {
            return getValueFromObject(response.getObj(), FacultyFamilyEntity.class);
        }
        return null;
    }

//    public Boolean saveFamilyMember(List<FacultyFamilyEntity> batch, Long facultyNo, UserSignInContainter userDetails) {
//        Response response = null;
//        boolean flag = true;
//        for (FacultyFamilyEntity obj : batch) {
//            obj.setFacultyNo(facultyNo);
//            response = saveObj(obj, userDetails);
//            if (!response.isSuccess()) {
//                flag = false;
//                break;
//            }
//        }
//        return flag;
//    }

    public Response saveObj(FacultyFamilyEntity familyInfoObj, UserSignInContainter userDetails) {
        familyInfoObj.setSsCreator(userDetails.getUserId());
        familyInfoObj.setSsCreatedOn(new Date());
        familyInfoObj.setSsCreateSession(userDetails.getSessionNo());
        familyInfoObj.setCompanyNo(userDetails.getCompanyNo());
        familyInfoObj.setOrganizationNo(userDetails.getOrganizationNo());
        return baseOnlySave(familyInfoObj);
    }

    /* Non API */
    private CriteriaQuery criteriaQuery(FacultyFamilyEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    private <T> TypedQuery typedQuery(FacultyFamilyEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }
        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);
        }
        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);
        }
        criteria.where(pArrayJoin.toArray(new Predicate[0]));
        return baseTypedQuery(criteria, dataTableInRQ);
    }

    private Long totalCount(FacultyFamilyEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<FacultyFamilyEntity> root = from(FacultyFamilyEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }

    private List<Predicate> criteriaCondition(FacultyFamilyEntity filter, CriteriaBuilder builder, Root<FacultyFamilyEntity> root) {
        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }
        List<Predicate> p = new ArrayList<Predicate>();
        if (filter != null) {
            if (filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }
            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }
        }
        return p;
    }

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(FacultyFamilyEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }
}
