package com.mysoft.shared.campus.base;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.shared.campus.util.CampusAppCommonFunction;

@Repository
@Transactional
public class CampusAppBaseRepository extends BaseRepository implements CampusAppCommonFunction {

	private final Logger logger = LoggerFactory.getLogger(getClass());
}
