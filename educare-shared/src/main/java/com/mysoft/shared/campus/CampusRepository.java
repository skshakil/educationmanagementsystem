package com.mysoft.shared.campus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.campus.base.CampusAppBaseRepository;

@Repository
@Transactional
public class CampusRepository extends CampusAppBaseRepository {

	public Response list(String reqObj) {
		CampusEntity campusEntity = objectMapperReadValue(reqObj, CampusEntity.class);
		return baseList(criteriaQuery(campusEntity));
	}

	public Response save(String reqObj, UserSignInContainter userDetails) {
		boolean porcContinueFlag = true;
		Response response = new Response();
		CampusEntity campusObj = null;
		JSONObject json = new JSONObject(reqObj);
		String campusObjStr = Def.getString(json, "campusObj");
		if (campusObjStr != null && !campusObjStr.equals("{}")) {
			campusObj = objectMapperReadValue(campusObjStr, CampusEntity.class);
		}
		if (campusObj != null) {
			campusObj.setSsCreator(userDetails.getUserId());
			campusObj.setSsCreatedOn(new Date());
			campusObj.setSsCreateSession(userDetails.getSessionNo());
			campusObj.setSsModifier(userDetails.getUserId());
			campusObj.setSsModifiedOn(new Date());
			campusObj.setSsModifiedSession(userDetails.getSessionNo());
			campusObj.setCompanyNo(userDetails.getCompanyNo());
			campusObj.setOrganizationNo(userDetails.getOrganizationNo());

			response = baseOnlySave(campusObj);
			response.setObj(campusObj);
			response.setMessage("Campus information save successfully.");
			return response;
		}
		response.setSuccess(false);
		response.setObj(campusObj);
		response.setMessage("Campus information not saved.");

		return response;
	}

	private CampusEntity findById(Long id) {
		System.err.println("ID:  " + id);
		CampusEntity campusObj = new CampusEntity();
		campusObj.setId(id);
		Response response = baseFindById(criteriaQuery(campusObj));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), CampusEntity.class);
		}
		return null;
	}

	public Response delete(Long id) {
		CampusEntity campusObj = findById(id);
		return baseDelete(campusObj);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(CampusEntity filter) {

		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);
		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings("unchecked")
	private List<Predicate> criteriaCondition(CampusEntity filter, CriteriaBuilder builder, Root<CampusEntity> root) {
		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}
		List<Predicate> p = new ArrayList<Predicate>();
		if (filter != null) {
			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
		}
		return p;
	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(CampusEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery query = super.criteria;
		Root root = super.root;
	}

}
