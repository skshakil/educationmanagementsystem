package com.mysoft.shared.campus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Service
public class CampusService {
	
	@Autowired
	private CampusRepository campusRepository;
	
	public Response list(String reqObj) {
		return campusRepository.list(reqObj);
	}
	
	public Response save(String reqObj,UserSignInContainter userDetails) {
		return campusRepository.save(reqObj,userDetails);
	}
	
	public Response delete(Long id) {
		return campusRepository.delete(id);
	}

}
