package com.mysoft.shared.campus.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import com.mysoft.core.util.CommonFunctions;
import com.mysoft.core.util.UserSignInContainter;

public interface CampusAppCommonFunction extends CommonFunctions {
	
	default Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	default UserSignInContainter userDetails() {
		return (UserSignInContainter) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	default UserSignInContainter userPrincipal() {
		return (UserSignInContainter) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	default String accessToken() {
		String token = null;
		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();
		if (authentication instanceof OAuth2Authentication) {
			Object details = authentication.getDetails();
			if (details instanceof OAuth2AuthenticationDetails) {
				OAuth2AuthenticationDetails oauthsDetails = (OAuth2AuthenticationDetails) details;
				token = oauthsDetails.getTokenValue();
			}
		}
		return token;
	}

}
