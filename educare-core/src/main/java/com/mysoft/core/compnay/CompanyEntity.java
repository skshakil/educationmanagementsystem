package com.mysoft.core.compnay;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Md. Jahurul Islam
 * 
 */
@Getter
@Setter
@Entity
@Table(name = "sa_company")
public class CompanyEntity implements Serializable {

	private static final long serialVersionUID = 3746039792618057221L;

	@Id
	@Column(nullable = false, name = "company_no")
	Long id;
	
	@Column(nullable = false, name = "company_id")
	String companyId;

	@Column(nullable = false, name = "company_name")
	String companyName;

	@Column(nullable = true, name = "company_alias")
	String companyAlias;

	@Column(nullable = true, name = "company_slogan")
	String companySlogan;

	@Column(nullable = true, name = "COMPANY_ADDRESS")
	String companyAddress;
	
	@Column(nullable = true, name = "COMPANY_PROVINCE")
	String companyProvince;

	@Column(name = "COMPANY_VILLAGE")
	private String companyVillage;

	@Column(name = "COMPANY_UNION")
	private String companyUnion;

	@Column(name = "COMPANY_THANA")
	private String companyThana;

	@Column(name = "COMPANY_DISTRICT")
	private String companyDistrict;

	@Column(name = "COMPANY_DIVISION")
	private String companyDivision;

	@Column(name = "COMPANY_COUNTRY")
	private String companyCountry;

	@Column(name = "COMPANY_TEL") //COMPANY_PHONE
	private String companyPhone;

	@Column(name = "COMPANY_FAX")
	private String companyFax;

	@Column(name = "COMPANY_EMAIL")
	private String companyEmail;

	@Column(name = "COMPANY_WEBSITE")
	private String companyWebsite;

	@Column(name = "COMPANY_LOGO")
	private String companyLogo;

	@Column(name = "CONTACT_PERSON")
	private String contactPerson;
	
	@Column(name = "CONTACT_PERSON_DESIG")
	private String contactPersonDesig;
	
	@Column(name = "CONTACT_NO")
	private String contactNo;	
	
	@Column(name = "SL_NO")
	private Long slNo;
	
	@Column(name = "CONTRACT_DATE")
	private Date contractDate;
	
	@Column(name = "LCN_VAL")
	private String lcnVal;

	@Column(nullable = false, name = "EXPIRED_ON")
	private Date expiredOn;

	@Column(nullable = false, name = "ACTIVE_STAT")
	private Integer activeStatus = 1;

	@Column(name = "LOGON_URL")
	private String logonUrl;

	@Column(nullable = false, name = "LCN_EXP_MSG_DAY")
	private Integer lcnExpMsgDay;

	@Column(nullable = false, name = "OG_NO")
	private long ogNo;

	@Transient
//	@Column(name = "OG_NAME",nullable = true)
	private String ogName;

	@Column(name = "SS_CREATOR")
	private Long ssCreator;

	@Column(name = "SS_CREATED_ON")
	private Date ssCreatedOn;

	@Column(name = "SS_CREATED_SESSION")
	private Long ssCreateSession;

	@Column(name = "SS_MODIFIER")
	private Long ssModifier;

	@Column(name = "SS_MODIFIED_ON")
	private Date ssModifiedOn;

	@Column(name = "SS_MODIFIED_SESSION")
	private Long ssModifiedSession;

	@Column(name = "COMPANY_IMAGE")
	private String companyImage;
	
	@Column(name = "PROMOTIONAL_UPDATES ")
	private String promotionalUpdates;
	
	@Column(name = "PROMOTIONAL_UPDATES_FLAG ")
	private String promotionalUpdatesFlag;
	
	@Column(name = "COMPANY_TYPE ")
	private Long companyType;
	
	@Column(name = "PLACE_LATITUDE ")
	private Double placeLatitude;
	
	@Column(name = "PLACE_LONGITUDE ")
	private Double placeLongitude;
	
	@Transient List<Long> companyIds; 
	@Transient String photoImg;
	@Transient String photoLogo;
	@Transient Double placeDistance;

	public void setCompanyIds(List<Long> companyIds) {
		this.companyIds = companyIds;
	}
}
