package com.mysoft.core.compnay;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.CommonFunctions;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Repository
@Transactional
public class CoreCompanyRepository extends BaseRepository {

	@Autowired
	EntityManager entityManager;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request, UserSignInContainter userDetails) {

		DataTableResults<CompanyEntity> dataTableResults = null;
		Response response = new Response();

		CompanyEntity companyEntity = new CompanyEntity();

		String ogNo = request.getParameter("ogNo");

		if (ogNo != null && !ogNo.isEmpty()) {
			companyEntity.setOgNo(Long.parseLong(ogNo));
		}
		companyEntity.setActiveStatus(0);
		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(companyEntity);

		List gridList = new ArrayList<>();
		response = baseList(typedQuery(companyEntity, dataTableInRQ));

		if (response.isSuccess()) {
//			gridList = response.getItems();
			if (response.getItems() != null) {
				List<CompanyEntity> companyObjList = getListFromObject(response.getItems(), CompanyEntity.class);
				for (CompanyEntity company : companyObjList) {

					if (company.getCompanyImage() != null && !company.getCompanyImage().isEmpty()
							&& !StringUtils.isBlank(company.getCompanyImage())) {
						Response imageRes = findCompanyImage(company.getCompanyImage());
						if (imageRes.isSuccess() && imageRes.getObj() != null) {
							company.setPhotoImg((String) imageRes.getObj());
						}

					}
					if (company.getCompanyLogo() != null && !company.getCompanyLogo().isEmpty()
							&& !StringUtils.isBlank(company.getCompanyLogo())) {
						Response logoRes = findCompanyLogo(company.getCompanyLogo());
						if (logoRes.isSuccess() && logoRes.getObj() != null) {
							company.setPhotoLogo((String) logoRes.getObj());
						}
					}

					gridList.add(company);
				}

			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response list() {
		CompanyEntity companyEntity = new CompanyEntity();

		Response response = baseList(criteriaQuery(companyEntity));
		List<CompanyEntity> companyList = new ArrayList<>();
		if (response.isSuccess() && response.getItems() != null) {
			List<CompanyEntity> companyObjList = getListFromObject(response.getItems(), CompanyEntity.class);
			for (CompanyEntity company : companyObjList) {

				if (company.getCompanyImage() != null && !company.getCompanyImage().isEmpty()
						&& !StringUtils.isBlank(company.getCompanyImage())) {
					Response imageRes = findCompanyImage(company.getCompanyImage());
					if (imageRes.isSuccess() && imageRes.getObj() != null) {
						company.setPhotoImg((String) imageRes.getObj());
					}

				}
				if (company.getCompanyLogo() != null && !company.getCompanyLogo().isEmpty()
						&& !StringUtils.isBlank(company.getCompanyLogo())) {
					Response logoRes = findCompanyLogo(company.getCompanyLogo());
					if (logoRes.isSuccess() && logoRes.getObj() != null) {
						company.setPhotoLogo((String) logoRes.getObj());
					}
				}

				companyList.add(company);
			}
			response.setItems(companyList);
		}
		return response;

	}

	@SuppressWarnings("unchecked")
	public Response logoList() {
		CompanyEntity companyEntity = new CompanyEntity();

		Map<String, String> columnMap = new HashMap<String, String>();

		columnMap.put("id", "id");
		columnMap.put("companyLogo", "companyLogo");

		Response response = baseTupleList(columnMap, findByColumns(companyEntity, columnMap));

		List<CompanyEntity> companyList = new ArrayList<>();

		if (response.isSuccess() && response.getItems() != null) {

			List<Map<String, Object>> companyObjList = response.getItems(); // getListFromObject(response.getItems(),
																			// CompanyEntity.class);

			for (Map<String, Object> map : companyObjList) {

				CompanyEntity company = new CompanyEntity();
				company.setId((Long) map.get("id"));
				company.setCompanyLogo((String) map.get("companyLogo"));

				if (company.getCompanyLogo() != null && !company.getCompanyLogo().isEmpty()
						&& !StringUtils.isBlank(company.getCompanyLogo())) {
					Response logoRes = findCompanyLogo(company.getCompanyLogo());
					if (logoRes.isSuccess() && logoRes.getObj() != null) {
						company.setPhotoLogo((String) logoRes.getObj());
					}
				}

				companyList.add(company);
			}
			response.setItems(companyList);
		}
		return response;

	}

	@SuppressWarnings("unchecked")
	public Response imageList() {
		CompanyEntity companyEntity = new CompanyEntity();

		Map<String, String> columnMap = new HashMap<String, String>();

		columnMap.put("id", "id");
		columnMap.put("companyImage", "companyImage");

		Response response = baseTupleList(columnMap, findByColumns(companyEntity, columnMap));

		List<CompanyEntity> companyList = new ArrayList<>();

		if (response.isSuccess() && response.getItems() != null) {

			List<Map<String, Object>> companyObjList = response.getItems();

			for (Map<String, Object> map : companyObjList) {

				CompanyEntity company = new CompanyEntity();
				company.setId((Long) map.get("id"));
				company.setCompanyImage((String) map.get("companyImage"));

				if (company.getCompanyImage() != null && !company.getCompanyImage().isEmpty()
						&& !StringUtils.isBlank(company.getCompanyImage())) {
					Response imageRes = findCompanyImage(company.getCompanyImage());
					if (imageRes.isSuccess() && imageRes.getObj() != null) {
						company.setPhotoImg((String) imageRes.getObj());
					}

				}

				companyList.add(company);
			}
			response.setItems(companyList);
		}
		return response;

	}

	public Response list4online() {
		CompanyEntity companyEntity = new CompanyEntity();
		return getNonReadonlyList(criteriaQuery(companyEntity));
	}

	
	@SuppressWarnings("unchecked")
	public Response listNearByUser(Double userLatitude, Double userLongitude) {
		
		CompanyEntity companyEntity = new CompanyEntity();
		
		List<CompanyEntity> companyList = new ArrayList<CompanyEntity>();
		Response response = getNonReadonlyList(criteriaQuery(companyEntity));
		

		
		if(response.getItems()!= null) {
			companyList = response.getItems();
			
			companyList.remove(0);
			
			for (CompanyEntity company : companyList) {
				Double distance = calculatePlaceDistance(userLatitude, userLongitude,
						company.getPlaceLatitude(), company.getPlaceLongitude());
		
				company.setPlaceDistance(distance);
			}
			
			List<CompanyEntity> sortedList = companyList.stream()
			        .sorted(Comparator.comparingDouble(CompanyEntity::getPlaceDistance))
			        .collect(Collectors.toList());
			
			response.setItems(sortedList);
		}
		
		return response;
	}

	public Response list(Long ogNo) {
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setOgNo(ogNo);
		return baseList(criteriaQuery(companyEntity));
	}

	public String findAliasById(Long id) {
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(id);

		Response response = new Response();

		Map<String, String> columnMap = new HashMap<String, String>();

		columnMap.put("companyAlias", "companyAlias");

		response = baseTupleSingleColumn(columnMap, findByColumns(companyEntity, columnMap));
		if (response.getObj() != null) {
			return response.getObj().toString();
		}

		return "";

	}

	public String findLogoById(Long id) {
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(id);

		Response response = new Response();

		Map<String, String> columnMap = new HashMap<String, String>();

		columnMap.put("companyLogo", "companyLogo");

		response = baseTupleSingleColumn(columnMap, findByColumns(companyEntity, columnMap));
		if (response.getObj() != null) {
			return response.getObj().toString();
		}

		return null;

	}

	public String findImageById(Long id) {
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(id);

		Response response = new Response();

		Map<String, String> columnMap = new HashMap<String, String>();

		columnMap.put("companyImage", "companyImage");

		response = baseTupleSingleColumn(columnMap, findByColumns(companyEntity, columnMap));
		if (response.getObj() != null) {
			return response.getObj().toString();
		}

		return null;

	}

	public Response save(String reqObj, UserSignInContainter userDetails) {
		CompanyEntity companyEntity = objectMapperReadValue(reqObj, CompanyEntity.class);

		companyEntity.setId(functionFdAutoNo(null, null, "sa_company", "company_no", 6l));
		companyEntity.setCompanyId(companyEntity.getId().toString());
		companyEntity.setSsCreator(userDetails.getUserId());
		companyEntity.setSsCreatedOn(new Date());
		companyEntity.setSsCreateSession(userDetails.getSessionNo());

		// should be dynamic
		companyEntity.setLcnExpMsgDay(20);

		return baseOnlySave(companyEntity);
	}

	public Response update(String reqObj, UserSignInContainter userDetails) {

		CompanyEntity companyEntity = objectMapperReadValue(reqObj, CompanyEntity.class);
		CompanyEntity obj = findById(companyEntity.getId());
		if (obj != null) {

			obj.setCompanyName(companyEntity.getCompanyName());
			obj.setCompanyPhone(companyEntity.getCompanyPhone());
			obj.setOgNo(companyEntity.getOgNo());
			obj.setCompanyAddress(companyEntity.getCompanyAddress());

			obj.setSsModifier(userDetails.getUserId());
			obj.setSsModifiedOn(new Date());
			obj.setSsModifiedSession(userDetails.getSessionNo());

			return baseUpdate(obj);
		}

		return getErrorResponse("Record not Found !!");

	}

	public Response detele(Long id) {

		CompanyEntity companyEntity = findById(id);
		if (companyEntity == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(companyEntity);

	}

	public Response remove(Long id) {
		CompanyEntity companyEntity = findById(id);
		if (companyEntity == null) {
			return getErrorResponse("Record not found!");
		}
		companyEntity.setActiveStatus(3);
		return baseRemove(companyEntity);
	}

	public CompanyEntity findById(Long id) {
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(id);
		Response response = baseFindById(criteriaQuery(companyEntity));
		if (response.isSuccess()) {
			
			companyEntity = getValueFromObject(response.getObj(), CompanyEntity.class);
			
//			if (companyEntity.getCompanyLogo() != null && !companyEntity.getCompanyLogo().isEmpty()
//					&& !StringUtils.isBlank(companyEntity.getCompanyLogo())) {
//				Response logoRes = findCompanyLogo(companyEntity.getCompanyLogo());
//				if (logoRes.isSuccess() && logoRes.getObj() != null) {
//					companyEntity.setPhotoLogo((String) logoRes.getObj());
//				}
//			}
			

			return companyEntity;
		}
		return null;
	}

	public String findCompanyInfoById(Long id, String type) {

		CompanyEntity companyEntity = findById(id);

		if (null != companyEntity && type.equalsIgnoreCase("HTML")) {

			StringBuilder companyInfo = new StringBuilder();
			companyInfo.append("<span style=\"font-family:Arial; font-size:12px; font-weight: bold;\">"
					+ companyEntity.getCompanyName() + "</span>");
			companyInfo.append("<br/>");
			companyInfo.append("<span style=\"font-size:10px;\">" + companyEntity.getCompanyAddress() + "</span>");

			return companyInfo.toString();

		} else if (null != companyEntity) {

			StringBuilder companyInfo = new StringBuilder();
			companyInfo.append(companyEntity.getCompanyName());
			companyInfo.append("\n");
			companyInfo.append(companyEntity.getCompanyAddress());

			return companyInfo.toString();
		}

		return null;
	}

	public String findCompanyInfoHTMLById(Long id) {
		return findCompanyInfoById(id, "HTML");
	}

	public String findCompanyInfoPlainTextById(Long id) {
		return findCompanyInfoById(id, "PlainText");
	}

	public Response findCompanyByIds(List<Long> companyIds) {

		Response response = new Response();
		List<CompanyEntity> companyEntityList = null;
		List<Map<String, Object>> companyMapList = new ArrayList<Map<String, Object>>();

		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setCompanyIds(companyIds);
		response = baseList(criteriaQuery(companyEntity));

		if (response.isSuccess() && response.getItems() != null) {

			companyEntityList = getListFromObject(response.getItems(), CompanyEntity.class);

			for (CompanyEntity compnay : companyEntityList) {

				Map<String, Object> companyMap = new HashMap<String, Object>();

				companyMap.put("companyId", compnay.getId());
				companyMap.put("compnayName", compnay.getCompanyName());
				companyMap.put("compnayAddress1", compnay.getCompanyAddress());
				companyMap.put("compnayEmail", compnay.getCompanyEmail());
				companyMap.put("orgId", compnay.getOgNo());
				companyMap.put("alias", compnay.getCompanyAlias());
				companyMap.put("companyPhone", compnay.getCompanyPhone());

				companyMapList.add(companyMap);
			}

			response.setItems(companyMapList);
		}

		return response;

	}

	public Response findCompanyByAlias(String alias) {

		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setActiveStatus(0);
		companyEntity.setCompanyAlias(alias.toUpperCase());

		return baseFindById(criteriaQuery(companyEntity));

	}

	// Non API
	private CriteriaQuery<Tuple> findByColumns(CompanyEntity filter, Map<String, String> columnNameMap) {

		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Tuple> criteriaQuery = baseCiteriaQueryTuple(builder);
		Root<CompanyEntity> root = baseFrom(CompanyEntity.class, criteriaQuery);

		return baseMultiSeletCriteria(builder, criteriaQuery, root, columnNameMap,
				criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(CompanyEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		criteria.orderBy(builder.asc(root.get("slNo")));

		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(CompanyEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ, CompanyEntity.class);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		if (dataTableInRQ.getOrder().getName() != null && !dataTableInRQ.getOrder().getName().isEmpty()) {
			if (dataTableInRQ.getOrder().getSortDir().equals("ASC")) {
				criteria.orderBy(builder.asc(root.get(dataTableInRQ.getOrder().getName())));
			} else {
				criteria.orderBy(builder.desc(root.get(dataTableInRQ.getOrder().getName())));
			}
		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(CompanyEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<CompanyEntity> root = from(CompanyEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(CompanyEntity filter, CriteriaBuilder builder, Root<CompanyEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			if (filter.getActiveStatus() > 0) {
				Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
				p.add(condition);
			}
			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}

			if (filter.getCompanyId() != null) {
				p.add(builder.equal(root.get("companyId"), filter.getCompanyId()));
			}

			if (filter.getCompanyIds() != null && filter.getCompanyIds().size() > 0) {
				Expression<Long> exp = root.get("id");
				Predicate condition = exp.in(filter.getCompanyIds());
				p.add(condition);
			}

			if (filter.getOgNo() != 0) {
				Predicate condition = builder.equal(root.get("ogNo"), filter.getOgNo());
				p.add(condition);
			}

			if (filter.getCompanyAlias() != null) {
				p.add(builder.equal(builder.upper(root.get("companyAlias")), filter.getCompanyAlias()));
			}

		}

		return p;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	private void init() {

		initEntityManagerBuilderCriteriaQueryRoot(CompanyEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;

	}

	public Response saveWithImage(MultipartFile imageFile, MultipartFile logoFile, String reqObj,
			UserSignInContainter userDetails) {

		if (reqObj == null) {
			return getErrorResponse("Company Data Not Found!!");
		}
		CompanyEntity companyEntity = objectMapperReadValue(reqObj, CompanyEntity.class);

		companyEntity.setId(functionFdAutoNo(null, null, "sa_company", "company_no", 6l));
		companyEntity.setCompanyId(companyEntity.getId().toString());
		companyEntity.setExpiredOn(clearTime(companyEntity.getExpiredOn()));
		companyEntity.setSsCreator(userDetails.getUserId());
		companyEntity.setSsCreatedOn(new Date());
		companyEntity.setSsCreateSession(userDetails.getSessionNo());
		companyEntity.setSsModifier(userDetails.getUserId());
		companyEntity.setSsModifiedOn(new Date());
		companyEntity.setSsModifiedSession(userDetails.getSessionNo());

		// should be dynamic
		companyEntity.setLcnExpMsgDay(2);

		// Image file
		if (imageFile != null) {
			String imageName = Def.customFileName(imageFile,
					"companyImage_" + companyEntity.getOgNo() + "_" + companyEntity.getId());

			Response imgRes = uploadCompanyImage(imageFile, imageName);

			if (imgRes.isSuccess()) {
				companyEntity.setCompanyImage((String) imgRes.getObj());

			}
		}
		if (logoFile != null) {
			String logoName = Def.customFileName(logoFile,
					"companyLogo_" + companyEntity.getOgNo() + "_" + companyEntity.getId());

			Response logoRes = uploadCompanyLogo(logoFile, logoName);

			if (logoRes.isSuccess()) {
				companyEntity.setCompanyLogo((String) logoRes.getObj());
			}
		}

		Response response = baseOnlySave(companyEntity);
		if (response.isSuccess() && response.getObj() != null) {
			CompanyEntity foundCompanyObj = getValueFromObject(response.getObj(), CompanyEntity.class);
			response = ogCompanyWiseInsert(foundCompanyObj);
		}
		if (response.isSuccess()) {
			return getSuccessResponse("Company Save Successfully.", response);
		} else {
			return getErrorResponse("Company Save Failed!!", response);
		}

	}

	public Response updateWithImage(MultipartFile imageFile, MultipartFile logoFile, String reqObj,
			UserSignInContainter userDetails) {

		Response response = new Response();
		Response imgRes = null;
		Response logoRes = null;
		CompanyEntity companyEntity = objectMapperReadValue(reqObj, CompanyEntity.class);
		if (companyEntity != null) {

			companyEntity.setExpiredOn(clearTime(companyEntity.getExpiredOn()));
			companyEntity.setSsModifier(userDetails.getUserId());
			companyEntity.setSsModifiedOn(new Date());
			companyEntity.setSsModifiedSession(userDetails.getSessionNo());

			// Image file upload
			if (imageFile != null) {
				String imageName = companyEntity.getCompanyImage() != null ? companyEntity.getCompanyImage()
						: Def.customFileName(imageFile,
								"companyImage_" + companyEntity.getOgNo() + "_" + companyEntity.getId());

				imgRes = uploadCompanyImage(imageFile, imageName);

				if (imgRes.isSuccess()) {
					companyEntity.setCompanyImage((String) imgRes.getObj());

				}
			}
			// Logo file upload
			if (logoFile != null) {
				String logoName = companyEntity.getCompanyLogo() != null ? companyEntity.getCompanyLogo()
						: Def.customFileName(logoFile,
								"companyLogo_" + companyEntity.getOgNo() + "_" + companyEntity.getId());

				logoRes = uploadCompanyLogo(logoFile, logoName);

				if (logoRes.isSuccess()) {
					companyEntity.setCompanyLogo((String) logoRes.getObj());

				}
			}

			response = baseUpdate(companyEntity);
			return getSuccessResponse("Company update successful.", response);
		}

		return getErrorResponse("Company updte failed!", response);
	}

	/*
	 * insert lookup detail data and sms configuration
	 * 
	 * @input : aogNo, companyNo
	 * 
	 */
	public Response ogCompanyWiseInsert(CompanyEntity companyObj) {

		int index = 0;
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PD_OG_COMPANY_WISE_INS_DATA")

				.registerStoredProcedureParameter(++index, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(++index, Long.class, ParameterMode.IN)

				.registerStoredProcedureParameter(3, String.class, ParameterMode.OUT)

				.setParameter(1, companyObj.getId()).setParameter(2, companyObj.getOgNo());

		query.execute();

		String message = query.getOutputParameterValue(3) != null ? (String) query.getOutputParameterValue(3) : null;
		Response response = new Response();

		if (message == null) {
			response.setSuccess(true);
			response.setMessage("Og Company Wise Data Insert Successfully.");
			return response;
		} else {
			response.setSuccess(false);
			response.setMessage(message);
			return response;
		}

	}

	public Response updateSerial(String reqObj, UserSignInContainter userDetail) {

		List<CompanyEntity> reqCompanyList = objectMapperReadArrayValue(reqObj, CompanyEntity.class);

		List<CompanyEntity> companyList = new ArrayList<CompanyEntity>();
		List<Object> finalList4Update = new ArrayList<Object>();
		Response companyListRes = list4online();
		Response updateRes = null;

		if (companyListRes.isSuccess() && companyListRes.getItems().size() > 0) {
			
			companyList = getListFromObject(companyListRes.getItems(), CompanyEntity.class);

			companyList.forEach(coreCompany -> reqCompanyList.stream()
					.filter(company -> coreCompany.getId().equals(company.getId()))
					.forEach(company -> coreCompany.setSlNo(company.getSlNo())));
			
			for (CompanyEntity companyEntity : companyList) {
				companyEntity.setSsModifiedOn(new Date());
				companyEntity.setSsModifiedSession(userDetail.getSessionNo());
				companyEntity.setSsModifier(userDetail.getUserId());
				finalList4Update.add(companyEntity);
			}
			
			return baseBatchSaveOrUpdate(finalList4Update);
		}
		return getErrorResponse("Update Failed!!");
	}

	public Response getCompanyTypeList(){
		return getLookupMList(CommonFunctions.conpanyTypeLookupNo);
	}
	
	
}
