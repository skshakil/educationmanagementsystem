package com.mysoft.core.compnay;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class CoreCompanyService {

    @Autowired
    private CoreCompanyRepository companyRepository;

    public Response gridList(HttpServletRequest request, UserSignInContainter userDetails) {
        return companyRepository.gridList(request,userDetails);
    }

    public Response list(Long reqObj) {
        return companyRepository.list(reqObj);
    }
    
    public Response list() {
        return companyRepository.list();
    }
    
    public Response logoList() {
    	return companyRepository.logoList();
    }
    
    public Response imageList() {
    	return companyRepository.imageList();
    }
    
    public Response list4online() {
    	return companyRepository.list4online();
    }
    
	public Response getCompanyListNearUser(Double userLatitude, Double userLongitude) {
		return companyRepository.listNearByUser(userLatitude, userLongitude);
	}

    public Response save(String reqObj, UserSignInContainter userDetails) {
        return companyRepository.save(reqObj, userDetails);
    }

    public Response update(String reqObj, UserSignInContainter userDetails) {
        return companyRepository.update(reqObj, userDetails);
    }

    public Response delete(Long id) {
        return companyRepository.detele(id);
    }

    public Response remove(Long id) {
        return companyRepository.remove(id);
    }

    public Response findCompanyByIds(List<Long> companyIds) {
        return companyRepository.findCompanyByIds(companyIds);
    }

    public CompanyEntity findCompanyById(Long companyId) {
        return companyRepository.findById(companyId);
    }
    
    public String findCompanyInfoById(Long companyId, String dataFormat) {
        return companyRepository.findCompanyInfoById(companyId,dataFormat);
    }
    
    public String findCompanyInfoHTMLById(Long companyId) {
        return companyRepository.findCompanyInfoHTMLById(companyId);
    }
    
    public String findCompanyInfoPlainTextById(Long companyId) {
        return companyRepository.findCompanyInfoPlainTextById(companyId);
    }

    public String findAliasById(Long companyId) {
    	return companyRepository.findAliasById(companyId);
    }

	public Response saveWithImage(MultipartFile imageFile,MultipartFile logoFile, String reqObj, UserSignInContainter userDetail) {
		return companyRepository.saveWithImage(imageFile,logoFile,reqObj,userDetail);
	}

	public Response updateWithImage(MultipartFile imageFile,MultipartFile logoFile, String reqObj, UserSignInContainter userDetail) {
		return companyRepository.updateWithImage(imageFile,logoFile,reqObj,userDetail);
	}

	public String findLogoById(Long companyNo) {
		return companyRepository.findLogoById(companyNo);
	}
	
	public String findImageById(Long companyNo) {
		return companyRepository.findImageById(companyNo);
	}
	
	public Response findCompanyByAlias(String alias) {
		return companyRepository.findCompanyByAlias(alias);
	}

	public Response updateSerial( String reqObj, UserSignInContainter userDetail) {
		return companyRepository.updateSerial(reqObj,userDetail);
	}

	public Response getCompanyTypeList() {
		return companyRepository.getCompanyTypeList();
	}
	
	public CompanyEntity findById(Long companyNo) {
		return companyRepository.findById(companyNo);
	}
}
