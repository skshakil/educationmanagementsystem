package com.mysoft.core.grantDeliveryStatus;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Immutable
@Table(name = "SA_GRANTDELIVERY_STATUS_V")
public class ViewGrantDeliveryStatusEntity implements Serializable {

	private static final long serialVersionUID = -8968110403413350309L;

	@Id
	@Column(name = "ID", insertable = false, updatable = false, nullable = true)
	Long id;

	@Column(name = "USER_NO", insertable = false, updatable = false, nullable = true)
	Long userNo;

	@Column(name = "EMP_NO", insertable = false, updatable = false, nullable = true)
	Long empNo;

	@Column(name = "DELIVERY_STATUS_NO", insertable = false, updatable = false, nullable = true)
	Long deliveryStatusNo;

	@Column(name = "DELIVERY_STATUS_NAME", insertable = false, updatable = false, nullable = false)
	String deliveryStatusName;

	@Column(name = "ALL_DELIVERY_STATUS", insertable = false, updatable = false, nullable = true)
	Long allDeliveryStatus;

	@Column(name = "DEFAULT_FLAG", insertable = false, updatable = false, nullable = false)
	Long defaultFalg;

	@Column(name = "COMPANY_NO", insertable = false, updatable = false, nullable = false)
	Long companyNo;

	@Column(name = "COMPANY_NAME", insertable = false, updatable = false, nullable = false)
	String companyName;

}
