package com.mysoft.core.grantDeliveryStatus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.util.Response;

@Repository
@Transactional
public class GrantDeliveryStatusRepository extends BaseRepository {

	public Response list(String reqObj) {
		ViewGrantDeliveryStatusEntity viewGrantdeliveryStatusEntity = new ViewGrantDeliveryStatusEntity();
		if (reqObj != null) {
			viewGrantdeliveryStatusEntity = objectMapperReadValue(reqObj, ViewGrantDeliveryStatusEntity.class);
		}
		return baseList(criteriaQuery(viewGrantdeliveryStatusEntity));
	}

	public List<Long> findGrantSingleCollByUserNo(Long userNo) {

		ViewGrantDeliveryStatusEntity grantns = new ViewGrantDeliveryStatusEntity();
		grantns.setUserNo(userNo);

		Response response = baseList(findByColumnName(grantns, "deliveryStatusNo"));

		List<Long> deliveryStatusNoList = getListFromObject(response.getItems(), Long.class);

		return deliveryStatusNoList;
	}

	public List<ViewGrantDeliveryStatusEntity> grantObjListByUserNo(Long userNo) {
		ViewGrantDeliveryStatusEntity viewGrantdeliveryStatusEntity = new ViewGrantDeliveryStatusEntity();
		viewGrantdeliveryStatusEntity.setUserNo(userNo);
		Response response = baseList(criteriaQuery(viewGrantdeliveryStatusEntity));
		List<ViewGrantDeliveryStatusEntity> grantObjList = getListFromObject(response.getItems(),
				ViewGrantDeliveryStatusEntity.class);
		return grantObjList;
	}

	public Response listfindByuserNo(Long userNo) {
		ViewGrantDeliveryStatusEntity viewGrantdeliveryStatusEntity = new ViewGrantDeliveryStatusEntity();
		viewGrantdeliveryStatusEntity.setUserNo(userNo);
		return baseList(criteriaQuery(viewGrantdeliveryStatusEntity));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery findByColumnName(ViewGrantDeliveryStatusEntity filter, String columnName) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		criteria.select(root.get(columnName));
		return criteria;
	}

	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(ViewGrantDeliveryStatusEntity filter) {
		init();
		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(ViewGrantDeliveryStatusEntity filter, CriteriaBuilder builder,
			Root<ViewGrantDeliveryStatusEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {

			if (filter.getUserNo() != null && filter.getUserNo() > 0) {
				Predicate condition = builder.equal(root.get("userNo"), filter.getUserNo());
				p.add(condition);
			}
			if (filter.getDeliveryStatusName() != null && !filter.getDeliveryStatusName().isEmpty()) {
				Predicate condition = builder.equal(root.get("deloveryStatusName"), filter.getDeliveryStatusName());
				p.add(condition);
			}

		}

		return p;
	}

	@SuppressWarnings("rawtypes")
	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(ViewGrantDeliveryStatusEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}
}
