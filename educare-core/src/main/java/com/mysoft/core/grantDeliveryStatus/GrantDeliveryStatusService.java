package com.mysoft.core.grantDeliveryStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;

@Service
public class GrantDeliveryStatusService {

	@Autowired
	private GrantDeliveryStatusRepository grantDeliveryStatusRepository;

	public Response list(String reqObj) {
		return grantDeliveryStatusRepository.list(reqObj);
	}

	public Response listfindByuserNo(Long userNo) {
		return grantDeliveryStatusRepository.listfindByuserNo(userNo);
	}
}