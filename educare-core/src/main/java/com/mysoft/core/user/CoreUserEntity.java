package com.mysoft.core.user;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "SA_USER")
public class CoreUserEntity extends BaseOraEntity implements Serializable {

    private static final long serialVersionUID = -1537111554670173093L;

    @Id
    @Column(name = "USER_NO", nullable = false)
    private Long id;

    @Column(name = "USER_ID", nullable = false)
    private String userId;

    @Column(name = "USER_NAME", nullable = true)
    private String userName;

    @Column(name = "USER_PWD", nullable = true)
    private String password;

    @Column(name = "ACTIVE_STAT", nullable = false)
    private Integer activeStatus = 1;
    
    @Column(name = "USER_TYPE_NO", nullable = false)
    private Integer userTypeNo;
    
    @Column(name = "USER_FULL_NAME", nullable = false)
    private String userFullName;
    
    @Column(name = "USER_DESIGNATION", nullable = false)
    private String userDesignation;
    
    @Column(name = "USER_DEPARTMENT", nullable = false)
    private String userDepartment;
    
    @Column(name = "USER_SIGNATURE", nullable = false)
    private String userSignature;
    
    @Column(name = "USER_MOBILE", nullable = false)
    private String userMobile;
    
    @Column(name = "USER_PHONE", nullable = false)
    private String userPhone;
    
    @Column(name = "USER_EMAIL", nullable = false)
    private String userEmail;
    
    @Column(name = "USER_ADDRESS_PRE", nullable = false)
    private String userAddressPre;
    
    @Column(name = "USER_VILLAGE_PRE", nullable = false)
    private String userVillagePre;
    
    @Column(name = "USER_UNION_PRE", nullable = false)
    private String userUnionPre;
    
    @Column(name = "USER_THANA_PRE", nullable = false)
    private String userThanaPre;
    
    @Column(name = "USER_DISTRICT_PRE", nullable = false)
    private String userDistrictPre;
    
    @Column(name = "USER_DIVISION_PRE", nullable = false)
    private String userDivisionPre;
    
    @Column(name = "USER_PROVINCE_PRE", nullable = false)
    private String userProvincePre;
    
    @Column(name = "USER_COUNTRY_PRE", nullable = false)
    private String userCountryPre;
    
    @Column(name = "USER_ADDRESS_PER", nullable = false)
    private String userAddressPer;
    
    @Column(name = "USER_VILLAGE_PER", nullable = false)
    private String userVillagePer;
    
    @Column(name = "USER_UNION_PER", nullable = false)
    private String userUnionPer;
    
    @Column(name = "USER_THANA_PER", nullable = false)
    private String userThanaPer;
    
    @Column(name = "USER_DISTRICT_PER", nullable = false)
    private String userDistrictPer;
    
    @Column(name = "USER_DIVISION_PER", nullable = false)
    private String userDivisionPer;
    
    @Column(name = "USER_PROVINCE_PER", nullable = false)
    private String userProvincePer;
    
    @Column(name = "USER_COUNTRY_PER", nullable = false)
    private String userCountryPer;
    
    @Column(name = "FATHER_NAME", nullable = false)
    private String fatherName;
    
    @Column(name = "MOTHER_NAME", nullable = false)
    private String motherName;
    
    @Column(name = "SPOUSE_NAME", nullable = false)
    private String spouseName;
    
    @Column(name = "GENDER", nullable = false)
    private String gender;
    
    @Column(name = "DOB", nullable = false)
    private Date dob;
    
    @Column(name = "MARITAL_STATUS", nullable = false)
    private String maritalStatus;
    
    @Column(name = "BLOOD_GROUP", nullable = false)
    private String bloodGroup;
    
    @Column(name = "RELIGION", nullable = false)
    private String religion;
    
    @Column(name = "NATIONALITY", nullable = false)
    private String nationality;
    
    @Column(name = "NID", nullable = false)
    private String nid;
    
    @Column(name = "SS_NO", nullable = false)
    private String ssNo;
    
    @Column(name = "DRIVINGLC_NO", nullable = false)
    private String drivinglcNo;
    
    @Column(name = "TIN_NO", nullable = false)
    private String tinNo;
    
    @Column(name = "PLACE_OF_BIRTH", nullable = false)
    private String placeOfBirth;
    
    @Column(name = "INTEREST", nullable = false)
    private String interest;
    
    @Column(name = "BIRTH_REG_NO", nullable = false)
    private String birthRegNo;
    
    @Column(name = "IDENTIFICATION_SIGN", nullable = false)
    private String identificationSign;
    
    @Column(name = "CARD_NO", nullable = false)
    private String cardNo;
    
    @Column(name = "CONTRACT_PERSON_NAME", nullable = false)
    private String contractPersonName;
    
    @Column(name = "CONTRACT_RELATION", nullable = false)
    private String contractRelation;
    
    @Column(name = "CONTRACT_PHONE", nullable = false)
    private String contractPhone;
    
    @Column(name = "CONTRACT_MOBILE", nullable = false)
    private String contractMobile;
    
    @Column(name = "CONTRACT_EMAIL", nullable = false)
    private String contractEmail;

    @Column(name = "ACCOUNT_EXPIRE_DATE", nullable = true)
    private Date accountExpireDate;    

    @Column(name = "ENABLED", nullable = true)
    private boolean enabled;

    @Column(name = "ACCOUNT_LOCKED", nullable = true)
    private boolean accountLocked;

    @Column(name = "ACCOUNT_EXPIRED", nullable = true)
    private boolean accountExpired;

    @Column(name = "PASSWORD_EXPIRED", nullable = true)
    private boolean passwordExpired;    
    
    @Column(name = "BLACK_LISTED", nullable = true)
    private boolean blackListed;    
    
    @Column(name = "BLACK_LIST_BY", nullable = true)
    private String blackListBy;    
    
    @Column(name = "BLACK_LIST_DATE", nullable = true)
    private Date blackListDate;    
    
    @Column(name = "DOCTOR_NO", nullable = true)
    private Long doctorNo;    
    
    @Column(name = "USER_PHOTO", nullable = true)
    private String userPhoto;    
    
    @Transient
    private String compnayName;
    
    @Column(name = "DEFAULT_PAGE_LINK", nullable = true)
    private String defaultPageLink;
    
    @Transient
    private String likeUserName;
     
    @Transient
    private Long billUnitNo;
    
    @Transient
    private Long b2bLabNo;
    
    @Transient
    private String photo;

}
