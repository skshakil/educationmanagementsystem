package com.mysoft.core.user;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Getter
@Setter
@Entity
@Table(name = "sa_user_v_web")
public class ViewUserEntity  {

    @Id
    @Column(name = "USER_NO", nullable = false, insertable = false, updatable = false)
    private Long id;

    @Column(name = "USER_ID", insertable = false, updatable = false)
    private String userId;

    @Column(name = "USER_NAME", insertable = false, updatable = false)
    private String userName;
    
    @Column(name = "USER_FULL_NAME", insertable = false, updatable = false)
    private String userFullName;

    @Column(name = "USER_TYPE_NO", insertable = false, updatable = false)
    private Integer userTypeNo;

    @Column(name = "USER_PWD", insertable = false, updatable = false)
    private String userPwd;
    
    @Column(name = "ACCOUNT_EXPIRE_DATE", insertable = false, updatable = false)
    private Date accountExpireDate;
    
    @Column(name = "ACCOUNT_EXPIRED", insertable = false, updatable = false)
    private Integer accountExpird;

    @Column(name = "ACCOUNT_LOCKED", insertable = false, updatable = false)
    private Integer accountLocked;    
    
    @Column(name = "ENABLED", insertable = false, updatable = false)
    private Integer enabled;
    
    @Column(name = "PASSWORD_EXPIRED", insertable = false, updatable = false)
    private Integer passwordExpired;
    
    @Column(name = "BLACK_LISTED", insertable = false, updatable = false)
    private Integer blackListed;
    
    @Column(name = "BLACK_LIST_BY", insertable = false, updatable = false)
    private Long blackListBy;
    
    @Column(name = "BLACK_LIST_DATE", insertable = false, updatable = false)
    private Date blackListDate;

    @Column(name = "USER_CREATED_ON", insertable = false, updatable = false)
    private Date userCreatedOn;

    @Column(name = "USER_MODIFIED_ON", insertable = false, updatable = false)
    private Date userModifiedOn;    

    @Column(name = "USER_DESIGNATION", nullable = true)
    private String userDesignation;

    @Column(name = "USER_DEPARTMENT", nullable = true)
    private String userDepartment;

    @Column(name = "USER_SIGNATURE", insertable = false, updatable = false)
    private String userSignatre;

    @Column(name = "USER_MOBILE", insertable = false, updatable = false)
    private String userMobile;    
    
    @Column(name = "USER_PHONE", insertable = false, updatable = false)
    private String userPhone;    
    
    @Column(name = "USER_EMAIL", insertable = false, updatable = false)
    private String userEmail;    

    @Column(name = "GENDER", insertable = false, updatable = false)
    private String gender;
    
    @Column(name = "DOB", insertable = false, updatable = false)
    private Date dob;
    
    @Column(name = "MARITAL_STATUS", insertable = false, updatable = false)
    private String maritalStatus;
    
    @Column(name = "BLOOD_GROUP", insertable = false, updatable = false)
    private String bloodGroup;
    
    @Column(name = "RELIGION", insertable = false, updatable = false)
    private String religion;
    
    @Column(name = "NATIONALITY", insertable = false, updatable = false)
    private String nationality;
    
    @Column(name = "NID", insertable = false, updatable = false)
    private String nid;
    
    @Column(name = "JOBTITLE_NO", insertable = false, updatable = false)
    private Long jobtitleNo;
    
    @Column(name = "JOBTITLE", insertable = false, updatable = false)
    private String jobtitle;
    
    @Column(name = "BU_NO", insertable = false, updatable = false)
    private Long buNo;
    
    @Column(name = "BU_NAME", insertable = false, updatable = false)
    private String buName;

    @Column(name = "ACTIVE_STAT", insertable = false, updatable = false)
    private Integer activeStatus;
    
    @Column(name = "COMPANY_NO", insertable = false, updatable = false)
    private Long companyNo;
    
    @Column(name = "OG_NO", insertable = false, updatable = false)
    private Long ogNo;
    
    @Column(name = "USER_PHOTO", insertable = false, updatable = false)
    private String userPhoto;
	
	@Transient
    private List<Long> userNoList;

}
