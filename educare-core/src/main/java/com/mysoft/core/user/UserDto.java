package com.mysoft.core.user;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserDto {

    Long id;

    private String userId;

    private String userName;

    private Integer activeStatus;

    private String rem;

    private Long empNo;

    private Long defaultModuleId;

    private boolean enabled;

    private boolean accountLocked;

    private boolean accountExpired;

    private boolean passwordExpired;

    private Date accountExpireDate;

    private String defaultPageLink;

    private String personalId;
    
    private Long billUnitNo;
    
    private Long b2bLabNo;

}
