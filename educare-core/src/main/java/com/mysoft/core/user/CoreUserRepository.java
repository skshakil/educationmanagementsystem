
package com.mysoft.core.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.userCompany.UserCompanyService;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.NodeTree;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.core.viewfeature.ViewMenuService;

@Repository
@Transactional
public class CoreUserRepository extends BaseRepository {

	@Autowired
	UserCompanyService userCompanyService;
	@Autowired
	ViewMenuService viewMenuService;

	@Autowired
	ViewUserRepository viewUserRepository;

	
	public Response gridList(HttpServletRequest request) {

		DataTableResults<CoreUserEntity> dataTableResults = null;
		Response response = new Response();
		CoreUserEntity coreUserEntity = new CoreUserEntity();
		coreUserEntity.setActiveStatus(1);

		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(coreUserEntity);

		List gridList = new ArrayList<>();
		response = baseList(typedQuery(coreUserEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response viewGridList(HttpServletRequest request, UserSignInContainter userDetail) {
		return viewUserRepository.gridList(request, userDetail);
	}

	public Response patientUserGridList(HttpServletRequest request) {
		return viewUserRepository.patientUserGridList(request);
	}

	public Response save(CoreUserEntity entity) {

		return baseOnlySave(entity);
	}

	public CoreUserEntity findByUserName(String userName) {
		
		CoreUserEntity coreUserEntity = new CoreUserEntity();
		coreUserEntity.setUserName(userName);
		Response response = baseSingleObject(criteriaQuery(coreUserEntity));

		if (!response.isSuccess()) {
			return null;
		}

		coreUserEntity = getValueFromObject(response.getObj(), CoreUserEntity.class);
		return coreUserEntity;

	}

	public CoreUserEntity findByUserId(String id) {
		Response response = new Response();
		CoreUserEntity coreUserEntity = new CoreUserEntity();
		coreUserEntity.setUserId(id);
		response = baseSingleObject(criteriaQuery(coreUserEntity));

		if (!response.isSuccess()) {
			return null;
		}
		coreUserEntity = getValueFromObject(response.getObj(), CoreUserEntity.class);
		return coreUserEntity;

	}

	public Response untaggedEmpNo(String reqObj, UserSignInContainter userDetils) {

//		Response response = employeeService.findByNameOrId(reqObj);
//		Response response = doctorService.findByDoctorNameOrId(reqObj,userDetils);
		return null;
	}

//	public List<Long> userWithEmpNo() {
//		CoreUserEntity userEntity = new CoreUserEntity();
//		userEntity.setHasEmp(true);
//		Response result = baseList(criteriaQuery(userEntity));
//
//		List<Long> empNoList;
//
//		if (result.isSuccess() && result.getItems() != null) {
//			List<CoreUserEntity> userList = getListFromObject(result.getItems(), CoreUserEntity.class);
//			empNoList = userList.stream().map(CoreUserEntity::getEmpNo).collect(Collectors.toList());
//			return empNoList;
//		}
//
//		return null;
//	}

//    public Response forgotPassword(String reqObj, UserSignInContainter userDetails) {
//
//
//        JSONObject json = new JSONObject(reqObj);
//        String userName = Def.getString(json, "userName");
//
//        if (userName == null || userName.isEmpty()) {
//            return getErrorResponse("Please enter your user name  !!");
//        }
//
//
//        CoreUserEntity coreUserEntity = findByUserName(userName.toUpperCase());
//
//        if (coreUserEntity == null) {
//
//            return getErrorResponse("User not found !!");
//        }
//
//        String existenceUserName = coreUserEntity.getUserName().toLowerCase();
//        String newPassword = existenceUserName + "#";
//        String newEncodePassword = passwordEncoder.encode(newPassword);
//
//        coreUserEntity.setPassword(newEncodePassword);
//        coreUserEntity.setSsModifiedOn(new Date());
//
//        update(objectToJson(coreUserEntity), userDetails);
//
//        Response response = new Response();
//        response.setObj(newPassword);
//
//
//        return getSuccessResponse("Your new password", response);
//
//    }

//    public Response changePassword(String reqObj, UserSignInContainter userDetails) {
//
//        JSONObject json = new JSONObject(reqObj);
//        String currentPassword = Def.getString(json, "currentPassword");
//        String newPassword = Def.getString(json, "newPassword");
//
//        if (currentPassword == null || currentPassword.isEmpty()) {
//            return getErrorResponse("Please enter your current password !!");
//        }
//
//        if (newPassword == null || newPassword.isEmpty()) {
//            return getErrorResponse("Please enter your new password !!");
//        }
//
//
//        CoreUserEntity coreUserEntity = findByUserName(userDetails.getName());
//
//        if (coreUserEntity == null) {
//
//            return getErrorResponse("User not found !!");
//        }
//
//        boolean isMatche = passwordEncoder.matches(currentPassword, coreUserEntity.getPassword());
//
//        if (!isMatche) {
//            return getErrorResponse("Your current password not matche,Please enter current password !!");
//        }
//
//        String changePassword = passwordEncoder.encode(newPassword);
//
//        coreUserEntity.setPassword(changePassword);
//        coreUserEntity.setSsModifiedOn(new Date());
//        coreUserEntity.setSsCreator(userDetails.getUserId());
//        coreUserEntity.setSsCreateSession(userDetails.getSessionNo());
//        Response response = update(objectToJson(coreUserEntity), userDetails);
//
//        if (!response.isSuccess() || response.getObj() == null) {
//            return getErrorResponse("Change password fail !!");
//        }
//
//        return getSuccessResponse("Change password save succesfully");
//
//    }

	public Response list(String reqObj) {
		CoreUserEntity coreUserEntity = objectMapperReadValue(reqObj, CoreUserEntity.class);
		return baseList(criteriaQuery(coreUserEntity));

	}

	public Response findUserMenu(UserSignInContainter userDetails, List<NodeTree> nodeTree) {

		List<NodeTree> nodeTree2 = nodeTree;
		Response response = new Response();
		response.setItems(nodeTree2);

		return response;
	}

	public Response findUserPreference(UserSignInContainter userDetails) {

		CoreUserEntity coreUserEntity = findById(userDetails.getUserId());

		if (coreUserEntity == null) {
			return getErrorResponse("User not found !!");
		}

		Response response = new Response();

		CoreUserEntity userPreference = new CoreUserEntity();
		userPreference.setDefaultPageLink(coreUserEntity.getDefaultPageLink());

		response.setObj(userPreference);

		return response;
	}

	public Response saveUserPreference(String reqObj, UserSignInContainter userDetails) {

		JSONObject json = new JSONObject(reqObj);
		String defaultPageLink = Def.getString(json, "defaultPageLink");

		if (defaultPageLink == null || defaultPageLink.isEmpty()) {
			return getErrorResponse("Please enter your value !!");
		}

		CoreUserEntity coreUserEntity = findById(userDetails.getUserId());

		if (coreUserEntity == null) {

			return getErrorResponse("User not found !!");
		}

		coreUserEntity.setDefaultPageLink(defaultPageLink);
		coreUserEntity.setSsModifiedOn(new Date());
		coreUserEntity.setSsCreateSession(userDetails.getSessionNo());

		Response response = update(coreUserEntity);

		if (!response.isSuccess() || response.getObj() == null) {
			return getErrorResponse("Update fail !!");
		}

		return getSuccessResponse("Save succesfully");

	}

	public Response getUserDetails(UserSignInContainter userDetails) {
		Map<String, Object> userDeatials = new HashMap<String, Object>();
		Response response = new Response();
		CoreUserEntity userPreference = null;
		CoreUserEntity coreUserEntity = findById(userDetails.getUserId());
		if (coreUserEntity != null) {

			Response userPreferenceResponse = findUserPreference(userDetails);
			
//			if (coreUserEntity.getUserPhoto() != null && !coreUserEntity.getUserPhoto().isEmpty() && !StringUtils.isBlank(coreUserEntity.getUserPhoto())) {
//				Response responsePhoto = findByPhotoName(coreUserEntity.getUserPhoto());
//				if (responsePhoto.isSuccess() && responsePhoto.getObj() != null) {
//					coreUserEntity.setPhoto((String) responsePhoto.getObj());
//				}
//
//			}

			if (userPreferenceResponse.isSuccess()) {
				userPreference = getValueFromObject(userPreferenceResponse.getObj(), CoreUserEntity.class);

				userDeatials.put("name", coreUserEntity.getUserName());
				userDeatials.put("defaultCompanyId", userDetails.getCompanyNo());
				userDeatials.put("defaultCompanyName", userDetails.getCompanyName());
				userDeatials.put("userDefaultPageLink", userPreference.getDefaultPageLink());
				userDeatials.put("userTypeNo", coreUserEntity.getUserTypeNo());
				userDeatials.put("organizationNo", coreUserEntity.getOrganizationNo());
				userDeatials.put("doctorNo", coreUserEntity.getDoctorNo());
				userDeatials.put("photo", coreUserEntity.getPhoto());
				userDeatials.put("userMobile", coreUserEntity.getUserMobile());
				userDeatials.put("userEmail", coreUserEntity.getUserEmail());
				userDeatials.put("userId", coreUserEntity.getId());

				Response responseCompanyList = userCompanyService.getUserCompany(userDetails);
				if (responseCompanyList.isSuccess() && responseCompanyList.getItems() != null) {

					userDeatials.put("companyList", responseCompanyList.getItems());

				}

				response.setObj(userDeatials);

				return response;
			}
		}

		return getErrorResponse(" Information not found !!");

	}

	public Response update(CoreUserEntity coreUserEntity) {
		return baseSaveOrUpdate(coreUserEntity);
	}

	public Response find(Long id) {
		CoreUserEntity coreUserEntity = new CoreUserEntity();
		coreUserEntity.setId(id);
		coreUserEntity.setActiveStatus(null);
		Response response = baseFindById(criteriaQuery(coreUserEntity));
		if (response.isSuccess()) {
			CoreUserEntity user = getValueFromObject(response.getObj(), CoreUserEntity.class);
			
//			if (user.getUserPhoto() != null && !user.getUserPhoto().isEmpty() && !StringUtils.isBlank(user.getUserPhoto())) {
//				Response responsePhoto = findByPhotoName(user.getUserPhoto());
//				if (responsePhoto.isSuccess() && responsePhoto.getObj() != null) {
//					user.setPhoto((String) responsePhoto.getObj());
//				}
//			}
			response.setObj(user);
			return response;
		} else {
			return getErrorResponse("User Not found");
		}
	}

	public CoreUserEntity findById(Long id) {
		CoreUserEntity coreUserEntity = new CoreUserEntity();
		coreUserEntity.setId(id);
		coreUserEntity.setActiveStatus(null);
		Response response = baseFindById(criteriaQuery(coreUserEntity));

		if (response.isSuccess() && response.getObj() != null) {
			return getValueFromObject(response.getObj(), CoreUserEntity.class);
		}
		return null;
	}

	public CoreUserEntity findByIdReadOnly(Long id) {
		CoreUserEntity coreUserEntity = new CoreUserEntity();
		coreUserEntity.setId(id);
		coreUserEntity.setActiveStatus(null);
		Response response = baseFindByIdReadOnly(criteriaQuery(coreUserEntity));

		if (response.isSuccess() && response.getObj() != null) {
			return getValueFromObject(response.getObj(), CoreUserEntity.class);
		}
		return null;
	}

	public Response updateUserInfo(MultipartFile file, String reqObj, UserSignInContainter userDetails) {
		
		JSONObject json = new JSONObject(reqObj);
		 Def.getLong(json, "id");
		
		CoreUserEntity coreUserEntity = findById(Def.getLong(json, "id"));
		
		coreUserEntity.setUserMobile(Def.getString(json, "userMobile"));
		coreUserEntity.setUserEmail(Def.getString(json, "userEmail"));
		coreUserEntity.setUserName(Def.getString(json, "name"));
		
		coreUserEntity.setSsModifiedOn(new Date());
		coreUserEntity.setSsModifiedSession(userDetails.getSessionNo());
		coreUserEntity.setSsModifier(userDetails.getUserId());
		
		
		 String fileName = coreUserEntity.getUserId()+"_"+coreUserEntity.getOrganizationNo()+"_"+coreUserEntity.getCompanyNo();
 		
		 // Image file
 		if (file != null) {
 			coreUserEntity.setUserPhoto(Def.customFileName(file, fileName));
 		}
 		
 		Response response = update(coreUserEntity);
 		
 		if(response.isSuccess()) {
 	 		
 	 		if (file != null) {
 				storePhotoToFile(file, fileName);
 			}
 	 		return response;
 		}

		return getErrorResponse("Update Not Execution!!");
	}

	public Response delete(Long id) {
		CoreUserEntity CoreUserEntity = findById(id);
		return baseDelete(CoreUserEntity);
	}

//	public Response updateWithBillUnit(String reqObj, UserSignInContainter userDetails) {
//
//		if (reqObj != null) {
//
//			JSONObject json = new JSONObject(reqObj);
//
//			Long id = Def.getLong(json, "id");
//			Long unitNo = Def.getLong(json, "billUnitNo");
//
//			CoreUserEntity user = findById(id);
//
//			Response response = new Response();
//
//			user.setBillUnitNo(unitNo);
//			user.setSsModifiedOn(new Date());
//			user.setSsModifier(userDetails.getUserId());
//			user.setSsModifiedSession(userDetails.getSessionNo());
//
//			response = update(user);
//			return response;
//		}
//
//		return getErrorResponse("Update fail !!");
//
//	}

//	public Response updateWithB2bLab(String reqObj, UserSignInContainter userDetails) {
//
//		if (reqObj != null) {
//
//			JSONObject json = new JSONObject(reqObj);
//
//			Long id = Def.getLong(json, "id");
//			Long b2bLabNo = Def.getLong(json, "b2bLabNo");
//
//			CoreUserEntity user = findById(id);
//
//			Response response = new Response();
//
//			user.setB2bLabNo(b2bLabNo);
//			user.setSsModifiedOn(new Date());
//			user.setSsModifier(userDetails.getUserId());
//			user.setSsModifiedSession(userDetails.getSessionNo());
//
//			response = update(user);
//			return response;
//		}
//
//		return getErrorResponse("Update fail !!");
//
//	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(CoreUserEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ,CoreUserEntity.class);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(CoreUserEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(CoreUserEntity filter, CriteriaBuilder builder,
			Root<CoreUserEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			if (filter.getActiveStatus()  != null && filter.getActiveStatus() > 0) {
				p.add(builder.equal(root.get("activeStatus"), filter.getActiveStatus()));
			}
			if (filter.getUserName() != null && !filter.getUserName().isEmpty()) {
//				p.add(builder.equal(root.get("userName"), filter.getUserName()));
				Predicate condition = builder.equal(builder.upper(root.get("userName")),
						filter.getUserName().toUpperCase());
				p.add(condition);
			}
			if (filter.getLikeUserName() != null && !filter.getLikeUserName().isEmpty()) {
				Predicate condition = builder.like(builder.upper(root.get("userName")),
						filter.getLikeUserName().toUpperCase() + "%");
				p.add(condition);
			}
			if (filter.getId() != null && filter.getId() > 0) {
				p.add(builder.equal(root.get("id"), filter.getId()));
			}
			if (filter.getUserId() != null && !filter.getUserId().isEmpty()) {
				p.add(builder.equal(root.get("userId"), filter.getUserId()));
			}
			if (filter.getCompanyNo() != null) {
				p.add(builder.equal(root.get("companyNo"), filter.getCompanyNo()));
			}
			if (filter.getOrganizationNo() != null) {
				p.add(builder.equal(root.get("organizationNo"), filter.getOrganizationNo()));
			}
			if (filter.getDoctorNo() != null) {
				p.add(builder.equal(root.get("doctorNo"), filter.getDoctorNo()));
			}
		}

		return p;
	}

	private Long totalCount(CoreUserEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<CoreUserEntity> root = from(CoreUserEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(CoreUserEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

	public Response resetPassword(String obj, String plainPass, String encodePass, UserSignInContainter userDetails) {

		CoreUserEntity userEntity = findById(Long.parseLong(obj));
		
		if(userEntity == null) {
			getErrorResponse("Invalid User!!");
		}
		
		userEntity.setPassword(encodePass);
		userEntity.setSsModifiedSession(userDetails.getUserId());
		userEntity.setSsModifiedOn(new Date());
		userEntity.setSsCreateSession(userDetails.getSessionNo());
		Response response = update(userEntity);
		if (response.isSuccess()) {
			return getSuccessResponse("Reset Password Successfully done");
		}

		return getErrorResponse("Reset Password fail !!");
	}

	public Response updateUserInfo(String obj, UserSignInContainter userDetails) {

		Response response = new Response();

//		EmployeeEntity employeeObj = objectMapperReadValue(obj, EmployeeEntity.class);
//
//		EmployeeEntity employeeEntity = coreEmployeeService.findById(employeeObj.getId());
//
//		employeeEntity.setPeCareOfAddr(employeeObj.getPeCareOfAddr());
//		employeeEntity.setPeAddr1(employeeObj.getPeAddr1());
//		employeeEntity.setPeAddr2(employeeObj.getPeAddr2());
//		employeeEntity.setPeAddrPost(employeeObj.getPeAddrPost());
//		employeeEntity.setPeAddr3(employeeObj.getPeAddr3());
//		employeeEntity.setPeAddrDist(employeeObj.getPeAddrDist());
//		employeeEntity.setPeAddrCountry(employeeObj.getPeAddrCountry());
//
//		employeeEntity.setPrCareOfAddr(employeeObj.getPrCareOfAddr());
//		employeeEntity.setPrAddr1(employeeObj.getPrAddr1());
//		employeeEntity.setPrAddr2(employeeObj.getPrAddr2());
//		employeeEntity.setPrAddrPost(employeeObj.getPrAddrPost());
//		employeeEntity.setPrAddr3(employeeObj.getPrAddr3());
//		employeeEntity.setPrAddrDist(employeeObj.getPrAddrDist());
//		employeeEntity.setPrAddrCountry(employeeObj.getPrAddrCountry());
//
//		employeeEntity.setSsModifiedOn(new Date());
//		employeeEntity.setSsModifiedSession(userDetails.getSessionNo());
//		employeeEntity.setSsModifier(userDetails.getUserId());

//		response = baseUpdate(employeeEntity);

		if (response.isSuccess()) {
			return getSuccessResponse("Address Updated.");
		} else {
			return getErrorResponse("Address Update Failed.");
		}

	}

	public CoreUserEntity findByDoctorNo(Long doctorNo) {
		CoreUserEntity coreUserEntity = new CoreUserEntity();
		coreUserEntity.setDoctorNo(doctorNo);
		coreUserEntity.setActiveStatus(null);
		Response response = baseFindById(criteriaQuery(coreUserEntity));

		if (response.isSuccess() && response.getObj() != null) {
			CoreUserEntity user = getValueFromObject(response.getObj(), CoreUserEntity.class);
			
			if (user.getUserPhoto() != null && !user.getUserPhoto().isEmpty() && !StringUtils.isBlank(user.getUserPhoto())) {
				Response responsePhoto = findByPhotoName(user.getUserPhoto());
				if (responsePhoto.isSuccess() && responsePhoto.getObj() != null) {
					user.setPhoto((String) responsePhoto.getObj());
				}
			}
			
			return user;
		}
		return null;
	}

	
}