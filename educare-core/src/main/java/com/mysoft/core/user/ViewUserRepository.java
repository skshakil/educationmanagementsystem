package com.mysoft.core.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.apache.commons.lang.StringUtils;


/**
 * 
 */
@Repository
@Transactional
public class ViewUserRepository extends BaseRepository {
	@SuppressWarnings({ "rawtypes", "unchecked" })

    public Response gridList(HttpServletRequest request, UserSignInContainter userDetail) {
		List<ViewUserEntity> viewUserEntityList; 
        DataTableResults<ViewUserEntity> dataTableResults = null;
        Response response = new Response();
        
    	String companyNo = request.getParameter("companyNo");
    	String ogNo = request.getParameter("ogNo");
		
        ViewUserEntity viewUserEntity = new ViewUserEntity();
        
//        viewUserEntity.setActiveStatus(1);
        
        if (companyNo != null && !companyNo.isEmpty()) {
        	viewUserEntity.setCompanyNo(Long.parseLong(companyNo));
		}
        if (ogNo != null && !ogNo.isEmpty()) {
        	viewUserEntity.setOgNo(Long.parseLong(ogNo));
        }
        DataTableRequest dataTableInRQ = new DataTableRequest(request);

        List gridList = new ArrayList<>();
        Long totalRowCount = countTypedQuery(viewUserEntity,dataTableInRQ);
        response = baseList(typedQuery(viewUserEntity, dataTableInRQ));

        if (response.isSuccess() && response.getItems() != null) {

        	viewUserEntityList = getListFromObject(response.getItems(), ViewUserEntity.class);
			
			for (ViewUserEntity user : viewUserEntityList) {

//				if (user.getUserPhoto() != null && !user.getUserPhoto().isEmpty() && !StringUtils.isBlank(user.getUserPhoto())) {
//					Response responsePhoto = findByPhotoName(user.getUserPhoto());
//					if (responsePhoto.isSuccess() && responsePhoto.getObj() != null) {
//						user.setUserPhoto((String) responsePhoto.getObj());
//					}
//				}
				gridList.add(user);
			}
        }
        
        dataTableResults = dataTableResults(dataTableInRQ, countTypedQuery(viewUserEntity, dataTableInRQ), gridList, totalRowCount);
        response.setItems(null);
        response.setObj(dataTableResults);
        return response;
    }
	
	public Response findEmpByUserNoList(List<Long> userNoList) {

		ViewUserEntity viewUser = new ViewUserEntity();
		viewUser.setUserNoList(userNoList);
		Response response = baseList(criteriaQuery(viewUser));

        if (response.getItems() == null) {
            return getErrorResponse("User not found");
        }
        return response;
	}
	
	public Response findByUserNoList(List<Long> userNoList) {
		
		ViewUserEntity viewUser = new ViewUserEntity();
		viewUser.setUserNoList(userNoList);
		
  	  
  	 Response response = new Response();

  	 Map<String, String> columnMap = new HashMap<String, String>();
     
      columnMap.put("id", "id");
      columnMap.put("userId", "userId");
      columnMap.put("userName","userName");
      columnMap.put("activeStatus","activeStatus");
      
      response =  baseTupleList(columnMap,findByColumns(viewUser,columnMap));
			
	if(response.getItems() == null) {
			return getErrorResponse("User not found");
	}
		
		return response;
	}
    
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    
    public Response patientUserGridList(HttpServletRequest request) {

    	 DataTableResults<ViewUserEntity> dataTableResults = null;
         Response response = new Response();
         ViewUserEntity viewUserEntity = new ViewUserEntity();
         viewUserEntity.setActiveStatus(1);

         DataTableRequest dataTableInRQ = new DataTableRequest(request);


         List gridList = new ArrayList<>();
         Long totalRowCount = countTypedQuery(viewUserEntity,dataTableInRQ);
         response = baseList(typedQuery(viewUserEntity, dataTableInRQ));

         if (response.isSuccess()) {
             if (response.getItems() != null) {
                 gridList = response.getItems();
             }
            
         }
         
         dataTableResults = dataTableResults(dataTableInRQ, countTypedQuery(viewUserEntity, dataTableInRQ), gridList, totalRowCount);
         response.setItems(null);
         response.setObj(dataTableResults);
         return response;
    }

	private CriteriaQuery<Tuple> findByColumns(ViewUserEntity filter, Map<String, String> columnNameMap) {

		CriteriaBuilder builder = criteriaBuilder();
    	CriteriaQuery<Tuple> criteriaQuery = baseCiteriaQueryTuple(builder);
		Root<ViewUserEntity> root = baseFrom(ViewUserEntity.class,criteriaQuery);

		return baseMultiSeletCriteria(builder, criteriaQuery, root, columnNameMap, criteriaCondition(filter, builder, root));

	}

	private CriteriaQuery<Tuple> findByColumns(ViewUserEntity filter, Map<String, String> columnNameMap, Map<String, Object> fields) {

		CriteriaBuilder builder = criteriaBuilder();
  	    CriteriaQuery<Tuple> criteriaQuery = baseCiteriaQueryTuple(builder);
		Root<ViewUserEntity> root = baseFrom(ViewUserEntity.class,criteriaQuery);

		return baseMultiSeletCriteria(builder, criteriaQuery, root, columnNameMap, criteriaCondition(filter, builder, root), basePredicate(fields,builder,root));

	}
	
    @SuppressWarnings({"rawtypes"})
    private <T> TypedQuery typedQuery(ViewUserEntity filter, DataTableRequest<T> dataTableInRQ) {
        init();
        List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
        List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ,ViewUserEntity.class);

        Predicate predicateAND = null;
        Predicate predicateOR = null;

        if (!CollectionUtils.isEmpty(pConjunction)) {
            Predicate[] pArray = pConjunction.toArray(new Predicate[]{});
            predicateAND = builder.and(pArray);
        }

        if (!CollectionUtils.isEmpty(pDisJunction)) {
            Predicate[] pArray = pDisJunction.toArray(new Predicate[]{});
            predicateOR = builder.or(pArray);
        }
        if (predicateAND != null) {
            pArrayJoin.add(predicateAND);

        }

        if (predicateOR != null) {
            pArrayJoin.add(predicateOR);

        }

        criteria.where(pArrayJoin.toArray(new Predicate[0]));

        return baseTypedQuery(criteria, dataTableInRQ);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private CriteriaQuery criteriaQuery(ViewUserEntity filter) {
        init();

        List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

        if (!CollectionUtils.isEmpty(p)) {
            Predicate[] pArray = p.toArray(new Predicate[]{});
            Predicate predicate = builder.and(pArray);
            criteria.where(predicate);
        }
        return criteria;
    }

    @SuppressWarnings({"unchecked"})
    private List<Predicate> criteriaCondition(ViewUserEntity filter, CriteriaBuilder builder,
                                              Root<ViewUserEntity> root) {

        if (builder == null) {
            builder = super.builder;
        }
        if (root == null) {
            root = super.root;
        }

        List<Predicate> p = new ArrayList<Predicate>();

        if (filter != null) {
            if (filter.getActiveStatus() != null && filter.getActiveStatus() > 0) {
                Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
                p.add(condition);
            }
            
            if (filter.getId() != null && filter.getId() > 0) {
                Predicate condition = builder.equal(root.get("id"), filter.getId());
                p.add(condition);
            }

            if (filter.getUserName() != null && !filter.getUserName().isEmpty()) {
                Predicate condition = builder.equal(root.get("userName"), filter.getUserName());
                p.add(condition);
            }
 
            if (filter.getUserId() != null && !filter.getUserId().isEmpty()) {
                Predicate condition = builder.equal(root.get("userId"), filter.getUserId());
                p.add(condition);
            }
            
            if (filter.getUserNoList() != null) {
            	Expression<Long> exp = root.get("id");
            	Predicate condition = exp.in(filter.getUserNoList());
                p.add(condition);            	
            }
            
            if (filter.getCompanyNo() != null) {
            	p.add(builder.equal(root.get("companyNo"), filter.getCompanyNo()));            	
            }
            if (filter.getOgNo() != null) {
            	p.add(builder.equal(root.get("ogNo"), filter.getOgNo()));            	
            }

        }

        return p;
    }

    private Long totalCount(ViewUserEntity filter) {
        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<ViewUserEntity> root = from(ViewUserEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

    }
    
    private <T> Long countTypedQuery(ViewUserEntity filter, DataTableRequest<T> dataTableInRQ) {

        CriteriaBuilder builder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
        Root<ViewUserEntity> root = from(ViewUserEntity.class, criteriaQuery);

        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root), dataTablefilter(dataTableInRQ, builder, root, ViewUserEntity.class));
    }

    

    private void init() {
        initEntityManagerBuilderCriteriaQueryRoot(ViewUserEntity.class);
        CriteriaBuilder builder = super.builder;
        CriteriaQuery criteria = super.criteria;
        Root root = super.root;
    }


}
