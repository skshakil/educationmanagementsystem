package com.mysoft.core.user;

import com.mysoft.core.util.NodeTree;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Md. Jahurul Islam
 *
 */
@Service
public class CoreUserService {
	@Autowired
	CoreUserRepository coreUserRepository;
	
	@Autowired
	ViewUserRepository viewUserRepository;

	public CoreUserEntity findByUserName(String userName) {
		
		return coreUserRepository.findByUserName(userName);
	}
	
	public CoreUserEntity findByUserId(String userName) {
		return coreUserRepository.findByUserId(userName);
	}
	
	public Response list(String reqObj) {
		return coreUserRepository.list(reqObj);
	}

//	public Response update(String reqObj, UserSignInContainter userDetails) {
//
//		return coreUserRepository.update(reqObj,userDetails);
//	}

	public Response delete(Long id) {
		return coreUserRepository.delete(id);
	}

	public Response getUserDetails(UserSignInContainter userDetails) {
		return coreUserRepository.getUserDetails(userDetails);
	}
//	public Response findUserMenu() {
//		return coreUserRepository.findUserMenu();
//	}
	
//	public Response forgotPassword(String reqObj, UserSignInContainter userDetails) {
//
//		return coreUserRepository.forgotPassword(reqObj,userDetails );
//	}
	
//	public Response changePassword(String reqObj, UserSignInContainter userDetails) {
//
//		return coreUserRepository.changePassword(reqObj, userDetails);
//	}

	public Response untaggedEmpNo(String obj, UserSignInContainter userDetils) {
		return coreUserRepository.untaggedEmpNo(obj, userDetils);
	}

    public Response gridList(HttpServletRequest request, UserSignInContainter userDetail) {
		return  coreUserRepository.viewGridList(request,userDetail);
    }
    
    public Response patientUserGridList(HttpServletRequest request) {
		return  coreUserRepository.patientUserGridList(request);
    }
    
    
	public Response findUserMenu(UserSignInContainter userDetails, List<NodeTree> nodeTree) {
		return coreUserRepository.findUserMenu(userDetails, nodeTree);
	}

	
	public Response findUserPreference(UserSignInContainter userDetails) {
		return coreUserRepository.findUserPreference(userDetails);
	}
	
	public Response saveUserPreference(String regObj, UserSignInContainter userDetails) {
		return coreUserRepository.saveUserPreference(regObj, userDetails);
	}

    public Response find(Long id) {
		return coreUserRepository.find(id);
    }
    
    public Response updateUserInfo(MultipartFile file, String reqObj,UserSignInContainter userDetails) {
    	return coreUserRepository.updateUserInfo(file,reqObj,userDetails);
    }
    
//    public Response updateWithBillUnit(String reqObj,UserSignInContainter userDetails) {
//    	return coreUserRepository.updateWithBillUnit(reqObj,userDetails);
//    }
    
//    public Response updateWithB2bLab(String reqObj,UserSignInContainter userDetails) {
//    	return coreUserRepository.updateWithB2bLab(reqObj,userDetails);
//    }
        
    public Response resetPassword(@RequestBody String obj, String plainPass, String encodePass,UserSignInContainter userDetails) {
		return  coreUserRepository.resetPassword(obj,plainPass,encodePass, userDetails);
    }
        
    public Response updateUserInfo(@RequestBody String obj,  UserSignInContainter userDetails) {
		return  coreUserRepository.updateUserInfo(obj, userDetails);
    }
    
    public Response findEmpByUserNoList(List<Long> userNoList) {
    	return viewUserRepository.findEmpByUserNoList(userNoList);
    }
    
    public Response findByUserNoList(List<Long> userNoList) {
    	return viewUserRepository.findByUserNoList(userNoList);
    }

    
}
