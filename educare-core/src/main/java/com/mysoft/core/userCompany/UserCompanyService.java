package com.mysoft.core.userCompany;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserCompanyService {
	
	@Autowired
	private UserCompanyRepository userCompanyRepository;
	
	public Response gridList(HttpServletRequest request) {
		return userCompanyRepository.gridList(request);
	}

	public Response list(String reqObj) {
		return userCompanyRepository.list(reqObj);
	}

	public Response save(String reqObj,Long generatedgrCompanyNo,UserSignInContainter userDetails) {
		return userCompanyRepository.save(reqObj,generatedgrCompanyNo,userDetails);
	}

	public Response update(String reqObj) {
		return userCompanyRepository.update(reqObj);
	}

	public Response delete(Long id) {
		return userCompanyRepository.detele(id);
	}

/*	public Response remove(Long id) {
		return userCompanyRepository.remove(id);
	}*/
	
	public Response getUserCompany(UserSignInContainter userDetails) {
		return userCompanyRepository.getUserCompany(userDetails);
	}


}
