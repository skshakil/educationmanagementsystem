package com.mysoft.core.userCompany;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.compnay.CoreCompanyService;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Repository
@Transactional
public class UserCompanyRepository extends BaseRepository {
	
	@Autowired
	CoreCompanyService companyService;
	@Autowired
	CoreUserCompanyService userCompanyService;
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {

		return userCompanyService.gridList(request);
	/*	DataTableResults<CoreUserCompanyEntity> dataTableResults = null;
		Response response = new Response();
		CoreUserCompanyEntity companyEntity = new CoreUserCompanyEntity();

		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(companyEntity);

		List gridList = new ArrayList<>();
		response = baseList(typedQuery(companyEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;*/
	}

	public Response list(String reqObj) {
		
		return userCompanyService.list(reqObj);
		
	/*	CoreUserCompanyEntity userCompanyEntity = null;
		if (null != reqObj) {
			userCompanyEntity = objectMapperReadValue(reqObj, CoreUserCompanyEntity.class);
		}
		return baseList(criteriaQuery(userCompanyEntity));*/
	}

	public Response save(String reqObj,Long generatedgrCompanyNo,UserSignInContainter userDetails) {
		return userCompanyService.save(reqObj,generatedgrCompanyNo,userDetails);
	/*	CoreUserCompanyEntity userCompanyEntity = objectMapperReadValue(reqObj, CoreUserCompanyEntity.class);
		userCompanyEntity.setSsCreator(userDetails().getUserId());
		return baseOnlySave(userCompanyEntity);*/
	}

	public Response update(String reqObj) {
		
		return userCompanyService.update(reqObj);
/*		CoreUserCompanyEntity userCompanyEntity = objectMapperReadValue(reqObj, CoreUserCompanyEntity.class);
		CoreUserCompanyEntity obj = findById(userCompanyEntity.getId());
		if (obj != null) {
			userCompanyEntity.setSsCreator(userDetails().getUserId());
			return baseUpdate(userCompanyEntity);
		}

		return getErrorResponse("Record not Found !!");*/

	}

	public Response detele(Long id) {
		return userCompanyService.delete(id);

		/*CoreUserCompanyEntity userCompanyEntity = findById(id);
		if (userCompanyEntity == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(userCompanyEntity);*/

	}

/*	public Response remove(Long id) {
		CoreUserCompanyEntity userCompanyEntity = findById(id);
		if (userCompanyEntity == null) {
			return getErrorResponse("Record not found!");
		}
		userCompanyEntity.setActiveStatus(3);
		return baseRemove(userCompanyEntity);
	}*/

/*	public CoreUserCompanyEntity findById(Long id) {
		CoreUserCompanyEntity userCompanyEntity = new CoreUserCompanyEntity();
		userCompanyEntity.setId(id);
		Response response = baseFindById(criteriaQuery(userCompanyEntity));
		if (response.isSuccess()) {

			return getValueFromObject(response.getObj(), CoreUserCompanyEntity.class);
		}
		return null;
	}*/
	
	public Response getUserCompany(UserSignInContainter userDetails) {
		
		return userCompanyService.getUserCompany(userDetails.getUserId());
		
	/*	CoreUserCompanyEntity userCompanyEntity = new CoreUserCompanyEntity();
		userCompanyEntity.setUserNo(userDetails().getUserId());
		userCompanyEntity.setCompanyNo(null);
		Response response =  baseList(criteriaQuery(userCompanyEntity));
		
		if(response.isSuccess() && response.getItems() !=null) {
			
			List<CoreUserCompanyEntity>  userCompanyList =	getListFromObject(response.getItems(), CoreUserCompanyEntity.class);
			List<Long> companyIds = userCompanyList.stream().map(CoreUserCompanyEntity::getCompanyNo).collect(Collectors.toList());
			
			return companyService.findCompanyByIds(companyIds);
		}*/


		//return getErrorResponse("Data not found ");
		
		
		

	}	
	
	

/*	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(CoreUserCompanyEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(CoreUserCompanyEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(CoreUserCompanyEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<CoreUserCompanyEntity> root = from(CoreUserCompanyEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(CoreUserCompanyEntity filter, CriteriaBuilder builder, Root<CoreUserCompanyEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
		
				if (filter.getActiveStatus() > 0) {
				Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
				p.add(condition);
			}
			
			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("userNo"), filter.getId());
				p.add(condition);
			}
			
			if (filter.getUserNo() != null && filter.getUserNo() > 0) {
				Predicate condition = builder.equal(root.get("userNo"), filter.getUserNo());
				p.add(condition);
			}
			
			if (filter.getCompanyNo() != null && filter.getCompanyNo() > 0) {
				Predicate condition = builder.equal(root.get("companyNo"), filter.getCompanyNo());
				p.add(condition);
			}
		}

		return p;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(CoreUserCompanyEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}*/

}
