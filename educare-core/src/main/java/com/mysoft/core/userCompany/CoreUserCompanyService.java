package com.mysoft.core.userCompany;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Service
public class CoreUserCompanyService {
	
	@Autowired
	private CoreUserCompanyRepository userCompanyRepository;
	
	public Response gridList(HttpServletRequest request) {
		return userCompanyRepository.gridList(request);
	}

	public Response list(String reqObj) {
		return userCompanyRepository.list(reqObj);
	}

	public Response save(String reqObj,Long generatedgrCompanyNo,UserSignInContainter userDetails) {
		return userCompanyRepository.save(reqObj,generatedgrCompanyNo,userDetails);
	}

	public Response update(String reqObj) {
		return userCompanyRepository.update(reqObj);
	}

	public Response delete(Long id) {
		return userCompanyRepository.detele(id);
	}

/*	public Response remove(Long id) {
		return userCompanyRepository.remove(id);
	}*/
	
	public Response getUserCompany(Long userId) {
		return userCompanyRepository.getUserCompany(userId);
	}


}
