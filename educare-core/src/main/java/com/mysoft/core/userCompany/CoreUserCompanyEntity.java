package com.mysoft.core.userCompany;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
@Setter
@Entity
@Table(name = "sa_grantcompany")
public class CoreUserCompanyEntity extends BaseOraEntity implements Serializable {

	private static final long serialVersionUID = 3746039792618057221L;

	@Id
	@Column(name = "GR_COMPANY_NO")
	Long id;

	@Column(name = "USER_NO")
	private Long userNo;

	@Column(name = "DEFAULT_FLAG")
	private Integer defaultFlag;

	@Column(name = "ALL_COMPANY_NO")
	private Long allComapnyNo;

	@Column(name = "NUM_OF_CON")
	private Long numOfCon;
	
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus = 1;

}
