/**
 * 
 */
package com.mysoft.core.util;

import lombok.Getter;

/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
public enum AppReportName {

	NON_LAB_REPORT("NLR-1001",              "Non Lab Report");

	
	private java.lang.String name;
	private java.lang.String id;

	AppReportName(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public static String getById(String id) {
		
		for (AppReportName r : AppReportName.values()) {
			if (r.getId().equals(id))
				return r.getName();
		}
		return "unknown";
	}
	

	
	public static String getByName(String name) {
		for (AppReportName r : AppReportName.values()) {
			if (r.getName().equals(name))
				return r.getId();
		}
		return "0";
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

}
