package com.mysoft.core.util;

public enum TagTypeEnum {
	
	Doctor("doctor", 1),
	Department("department", 2),
	Employee("employee", 3),
	DischargeEditPrintPolicy("discharge Edit & Print Policy", 4);

	private String tagTypeName;
	private int tagTypeVal;

	private TagTypeEnum(String tagTypeName, int tagTypeVal) {
		this.tagTypeName = tagTypeName;
		this.tagTypeVal = tagTypeVal;
	}
	
	public String getTagTypeName() {
		return tagTypeName;
	}

	public void setTagTypeName(String tagTypeName) {
		this.tagTypeName = tagTypeName;
	}

	public int getTagTypeVal() {
		return tagTypeVal;
	}

	public void setTagTypeVal(int tagTypeVal) {
		this.tagTypeVal = tagTypeVal;
	}
	

}
