package com.mysoft.core.util;

public class ProcedureParams {
	
	private String paramName;
	private String paramType;
	private String dataType;
	
	 public ProcedureParams(String paramName, String paramType, String dataType ) {
	       this.paramName = paramName;
	       this.paramType = paramType;
	       this.dataType = dataType;
	    }
	 
	
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamType() {
		return paramType;
	}
	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	
	

}
