package com.mysoft.core.util;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.mysoft.core.report.JasperExportFormat;
//import com.mysoft.prescriptionApp.prescriptionDetails.PrescriptionDetailsEntity;
import com.mysoft.core.report.ReportPathAndName;

import oracle.jdbc.internal.OracleCallableStatement;
import oracle.sql.Datum;

/**
 * @author Md. Jahurul Islam
 *
 */
public class CommonFunction {

	@Autowired
	HttpSession httpSession;
	static final List<ReportPathAndName> reportNamList = new ArrayList<ReportPathAndName>();

	static {
		if (reportNamList.size() == 0) {
			reportNamList
					.add(new ReportPathAndName("wahab", "1", "/report/prescription/wahab", "prescriptionLayout_1"));
			reportNamList
					.add(new ReportPathAndName("wahab", "2", "/report/prescription/wahab", "prescriptionLayout_2"));
			reportNamList.add(new ReportPathAndName("gen", "1", "/report/prescription", "generalPrescriptionLayout_1"));
		}
	}

	public static JasperExportFormat printFormat(String printFormat) {
		if (printFormat == null) {
			printFormat = "PDF";
		}
		if (printFormat.equalsIgnoreCase("PDF")) {
			return JasperExportFormat.PDF_FORMAT;
		}
		if (printFormat.equalsIgnoreCase("HTML")) {
			return JasperExportFormat.HTML_FORMAT;
		} else if (printFormat.equalsIgnoreCase("DOCX")) {
			return JasperExportFormat.DOCX_FORMAT;
		} else if (printFormat.equalsIgnoreCase("XLSX")) {
			return JasperExportFormat.XLSX_FORMAT;
		}

		return JasperExportFormat.PDF_FORMAT;
	}

	public static String getReportPath(HttpServletRequest request, String path) {
		return request.getServletContext().getRealPath(path);
		// return request.getSession().getServletContext().getRealPath(path);
		// path = this.getClass().getClassLoader().getResource("").getPath();
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getResoucePath(String filePath) {
		Resource resource = new ClassPathResource(filePath);
		try {
			return resource.getFile().getAbsolutePath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getResoucePath4REG(String filePath) {
		Resource resource = new ClassPathResource(filePath);
		
		try {
			return resource.getFile().getAbsolutePath();
		} catch (IOException e) {

			resource = new ClassPathResource("/report/registrationCard/");

			try {
				return resource.getFile().getAbsolutePath();
			} catch (IOException e1) {

				e1.printStackTrace();

			}

		}
		return null;
	}
	
	
	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getResoucePath4REG(String filePath, String fileName) {
		String path = null;

		path = findPath(filePath, fileName);

		if (path != null) {
			return findPath(filePath, null);
		} else {
			return findPath("/report/registrationCard/", null);
		}

	}

	private static String findPath(String filePath, String fileName) {

		try {
			if (fileName != null) {
				return new ClassPathResource(filePath + "/" + fileName+".jasper").getFile().getAbsolutePath();
			} else {
				return new ClassPathResource(filePath).getFile().getAbsolutePath();
			}

		} catch (IOException e1) {
			return null;
		}
	}

	public static long generateRandom(int length) {
		while (true) {
			long numb = (long) (Math.random() * 100000000 * 1000000);
			if (String.valueOf(numb).length() == length)
				return numb;
		}
	}

	public void setHttpSession(String key, Object obj) {

		httpSession.setAttribute(key, obj);
	}

	public Object getHttpSession(String key) {
		System.out.println("uuuuuuuuuuuuu " + httpSession.getAttributeNames());
		return httpSession.getAttribute(key);
	}

	public static ReportPathAndName reportPathName(String pClient, String pLayout) {
		return reportNamList.stream().filter(rp -> rp.getpClient().equals(pClient) && rp.getpLayout().equals(pLayout))
				.findAny().orElse(null);
	}

}
