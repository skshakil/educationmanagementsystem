package com.mysoft.core.util;

public enum TagForEnum {

	Discharge("discharge", 1), Oncology("oncology", 2);

	private String tagForName;
	private int tagForVal;

	private TagForEnum(String tagForName, int tagForVal) {
		this.tagForName = tagForName;
		this.tagForVal = tagForVal;
	}

	public String getTagForName() {
		return tagForName;
	}

	public void setTagForName(String tagForName) {
		this.tagForName = tagForName;
	}

	public int getTagForVal() {
		return tagForVal;
	}

	public void setTagForVal(int tagForVal) {
		this.tagForVal = tagForVal;
	}

}
