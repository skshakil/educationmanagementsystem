/**
 * 
 */
package com.mysoft.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
public enum B2BNonLabReportType {

	WITHOUT_HEADER_WITHOUT_SIGNATURE(0,"Without header without signature"),
	WITH_HEADER_WITHOUT_SIGNATURE(1,"With header without signature"),
	WITHOUT_HEADER_WITH_SIGNATURE(2,"Without header with signature"),
	WITH_HEADER_WITH_SIGNATURE(3,"With header with signature");

	
	private java.lang.String name;
	private int id;

	B2BNonLabReportType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public static String getById(int id) {
		
		for (B2BNonLabReportType r : B2BNonLabReportType.values()) {
			if (r.getId() == id)
				return r.getName();
		}
		return "unknown";
	}
	

	
	public static int getByName(String name) {
		for (B2BNonLabReportType r : B2BNonLabReportType.values()) {
			if (r.getName().equals(name))
				return r.getId();
		}
		return -1;
	}

	public static List<Map<String, Object>> findB2BNonLabReportTypeList() {
		 
		 List<Map<String, Object>> b2bNonLabReportTypeList = new ArrayList<Map<String, Object>>();
		
		for (B2BNonLabReportType r : B2BNonLabReportType.values()) {
			
			Map<String, Object> registrationTypeMap = new HashMap<String, Object>();
			registrationTypeMap.put("id", r.getId());
			registrationTypeMap.put("name", r.getName());
			
			b2bNonLabReportTypeList.add(registrationTypeMap);
		}
		
		return b2bNonLabReportTypeList;
		
	}
	
	
	
	public void setName(java.lang.String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

}
