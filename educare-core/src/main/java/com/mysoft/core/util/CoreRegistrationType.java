/**
 * 
 */
package com.mysoft.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
public enum CoreRegistrationType {

	SERVICEHOLDER(1, "Service holder"), FAMILY(2, "Family"), RE(3, "Re"), CNE(4, "Cne"), MISCELLENOUS(5, "Mise"), QUICK(6, "NR");
	
	private java.lang.String name;
	private java.lang.Integer id;

	CoreRegistrationType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public static String getById(int id) {
		for (CoreRegistrationType r : CoreRegistrationType.values()) {
			if (r.getId() == id)
				return r.getName();
		}
		return "unknown";
	}
	
	public static List<Map<String, Object>> getRegTypeList() {
		 
		 List<Map<String, Object>> registrationTypes = new ArrayList<Map<String, Object>>();
		
		for (CoreRegistrationType r : CoreRegistrationType.values()) {
			
			Map<String, Object> registrationTypeMap = new HashMap<String, Object>();
			registrationTypeMap.put("id", r.getId());
			registrationTypeMap.put("name", r.getName());
			
			registrationTypes.add(registrationTypeMap);
		}
		
		return registrationTypes;
		
	}
	
	public static int getByName(String name) {
		for (CoreRegistrationType r : CoreRegistrationType.values()) {
			if (r.getName() == name)
				return r.getId();
		}
		return 0;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public void setId(java.lang.Integer id) {
		this.id = id;
	}

}
