package com.mysoft.core.util;

import java.util.ArrayList;
import java.util.List;


public class ModuleTree<T> {

    private Long childId;
    private Long parentId;

    private String displayValue;
    private ModuleTree parent;
    private List<ModuleTree> children;
    private List<FeatureDto> featureList;


    public ModuleTree() {
        super();
        this.children = new ArrayList<>();
    }


    public ModuleTree(String displayVal, Long childId, Long parentId) {
        this.displayValue = displayVal;
        this.childId = childId;
        this.parentId = parentId;
        this.children = new ArrayList<>();
    }

    public Long getChildId() {
        return childId;
    }

    public void setChildId(Long childId) {
        this.childId = childId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }


    public ModuleTree getParent() {
        return parent;
    }

    public void setParent(ModuleTree parent) {
        this.parent = parent;
    }

    public List<ModuleTree> getChildren() {
        return children;
    }

    public void setChildren(List<ModuleTree> children) {
        this.children = children;
    }


    public void addChild(ModuleTree child) {
        if (!this.children.contains(child) && child != null)
            this.children.add(child);
    }


    public List<FeatureDto> getFeatureList() {
        return featureList;
    }


    public void setFeatureList(List<FeatureDto> featureList) {
        this.featureList = featureList;
    }


}
