package com.mysoft.core.util;

public enum DischargePolicyEnum {

	ProvisionalForConsultant(1, "provisional For Consultant", 360, 1),
	ProvisionalForDoctor(2, "provisional For Doctor", 12, 2),
	FinalizeForConsultant(3, "finalize For Consultant", 24, 1), FinalizeForDoctor(4, "finalize For Doctor", 6, 2);

	private Integer policyId;
	private String policyName;
	private Integer policyDuration;
	private Integer policyFor; // consultant = 1; doctor = 2;

	private DischargePolicyEnum(Integer policyId, String policyName, Integer policyDuration, Integer policyFor) {
		this.policyId = policyId;
		this.policyName = policyName;
		this.policyDuration = policyDuration;
		this.policyFor = policyFor;
	}

	public Integer getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public Integer getPolicyDuration() {
		return policyDuration;
	}

	public void setPolicyDuration(Integer policyDuration) {
		this.policyDuration = policyDuration;
	}

	public Integer getPolicyFor() {
		return policyFor;
	}

	public void setPolicyFor(Integer policyFor) {
		this.policyFor = policyFor;
	}
	


}
