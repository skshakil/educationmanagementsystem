package com.mysoft.core.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeatureDto {

    Long featureId;
    String featureName;
    Boolean isGranted;
    Boolean updatedStatus;
}
