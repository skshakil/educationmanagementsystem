package com.mysoft.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum NotificationType {

	SIGN_UP		 	(101L, "SIGN UP"),
	APPOINTMENT	 	(102L, "APPOINTMENT"),
	CONSULTATION 	(103L, "CONSULTATION"),
	PRESCRIPTION	(104L, "PRESCRIPTION"),
	FILE_ATTACHMENT	(105L, "FILE ATTACHMENT");
	

	private Long typeNo;
	private String typeName;

	NotificationType(Long typeNo, String typeName) {
		this.typeNo = typeNo;
		this.typeName = typeName;
	}

	public Long getTypeNo() {
		return typeNo;
	}

	public String getTypeName() {
		return typeName;
	}
	
	

	public void setTypeNo(Long typeNo) {
		this.typeNo = typeNo;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public static List<Map<String, Object>> getNotificationList() {

		List<Map<String, Object>> attachTypeList = new ArrayList<Map<String, Object>>();

		for (NotificationType r : NotificationType.values()) {
			
//			NotificationType type = null;
//			type.setTypeNo(r.getTypeNo());
//			type.setTypeName(r.getTypeName());
			
			Map<String, Object> getDateCriteriaMap = new HashMap<String, Object>();
			getDateCriteriaMap.put("typeNo", r.getTypeNo());
			getDateCriteriaMap.put("typeName", r.getTypeName());

			attachTypeList.add(getDateCriteriaMap);
		}
		return attachTypeList;
	}
	
	public static String getNotifiTypeByNo(Long typeNo) {

		for (NotificationType r : NotificationType.values()) {
			if (r.getTypeNo().longValue() == typeNo.longValue())
				return r.getTypeName();
		}
		return "unknown";
	}

}
