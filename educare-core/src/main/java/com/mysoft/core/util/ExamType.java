package com.mysoft.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

/**
 * @author Md. jitu
 *
 */
@Getter
public enum ExamType {
	
	
	PHATHOLOGY(1L,"Pathology"),
	RADIOLOGY(2L,"Radiology"),	
	SERVICE(3L,"Service"),
	VACCINATION(11L,"MSU");

	
	private java.lang.String name;
	private Long id;

	ExamType(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public static String getById(int id) {
		
		for (ItemType r : ItemType.values()) {
			if (r.getId() == id)
				return r.getName();
		}
		return "unknown";
	}
	

	
	public static Long getByName(String name) {
		for (ItemType r : ItemType.values()) {
			if (r.getName().equals(name))
				return r.getId();
		}
		return -1L;
	}

	public static List<Map<String, Object>> findItemTypeList() {
		 
		 List<Map<String, Object>> itemTypeList = new ArrayList<Map<String, Object>>();
		
		for (ItemType r : ItemType.values()) {
			
			Map<String, Object> itemTypeMap = new HashMap<String, Object>();
			itemTypeMap.put("id", r.getId());
			itemTypeMap.put("name", r.getName());
			
			itemTypeList.add(itemTypeMap);
		}
		
		return itemTypeList;
		
	}
	
	public static List<Long> findItemTypeNoList() {
		 
		 List<Long> itemTypeList = new ArrayList<Long>();
		
		for (ItemType r : ItemType.values()) {
			itemTypeList.add(r.getId());
		}
		
		return itemTypeList;
		
	}
	
	
	
	public void setName(java.lang.String name) {
		this.name = name;
	}

	public void setId(Long id) {
		this.id = id;
	}
	


}
