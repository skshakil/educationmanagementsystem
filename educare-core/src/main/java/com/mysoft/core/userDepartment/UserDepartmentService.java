package com.mysoft.core.userDepartment;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.base.BaseService;
import com.mysoft.core.util.Response;

@Service
public class UserDepartmentService extends BaseService {
	
	@Autowired
	UserDepartmentRepository userDepartmentRepository;
	
	public Response list(String reqObj) {
		return userDepartmentRepository.list(reqObj);
	}
	
	public Response gridList(HttpServletRequest reqObj) {
		return userDepartmentRepository.gridList(reqObj);
	}
	
	public Response listFindByuserNo(Long userNo) {
		return userDepartmentRepository.listFindByuserNo(userNo);
	}

}
