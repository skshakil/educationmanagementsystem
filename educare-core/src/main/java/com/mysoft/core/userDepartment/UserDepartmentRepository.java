package com.mysoft.core.userDepartment;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.grantDeliveryStatus.ViewGrantDeliveryStatusEntity;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Response;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class UserDepartmentRepository extends BaseRepository {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {

		List<ViewUserDepartmentEntity> viewDepartmentEntityList = new ArrayList<ViewUserDepartmentEntity>();
		DataTableResults<ViewUserDepartmentEntity> dataTableResults = null;

		Response response = new Response();
		ViewUserDepartmentEntity viewDepartmentEntity = new ViewUserDepartmentEntity();
		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(viewDepartmentEntity);

		List gridList = new ArrayList<>();

		response = baseList(typedQuery(viewDepartmentEntity, dataTableInRQ));

		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response list(String reqObj) {
		ViewUserDepartmentEntity viewDepartmentEntity = new ViewUserDepartmentEntity();
		if (reqObj != null) {
			viewDepartmentEntity = objectMapperReadValue(reqObj, ViewUserDepartmentEntity.class);
		}
		return baseList(criteriaQuery(viewDepartmentEntity));
	}

	public Response listFindByuserNo(Long userNo) {
		ViewUserDepartmentEntity viewDepartmentEntity = new ViewUserDepartmentEntity();
		viewDepartmentEntity.setUserNo(userNo);
		return baseList(criteriaQuery(viewDepartmentEntity));
	}
	
	public List<ViewUserDepartmentEntity> objListByUserNo(Long userNo) {
		ViewUserDepartmentEntity  viewUserDepartmentEntity = new ViewUserDepartmentEntity();
		viewUserDepartmentEntity.setUserNo(userNo);
		Response response = baseList(criteriaQuery(viewUserDepartmentEntity));
		List<ViewUserDepartmentEntity> objList = getListFromObject(response.getItems(),
				ViewUserDepartmentEntity.class);
		return objList;
	}

//	=============================================================================

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(ViewUserDepartmentEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(ViewUserDepartmentEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
        List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);
		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}
		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);
		}
		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);
		}
		criteria.where(pArrayJoin.toArray(new Predicate[0]));
		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(ViewUserDepartmentEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<ViewUserDepartmentEntity> root = from(ViewUserDepartmentEntity.class, criteriaQuery);
        return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));
	}

	@SuppressWarnings({ "unchecked" })
    private List<Predicate> criteriaCondition(ViewUserDepartmentEntity filter, CriteriaBuilder builder,
                                              Root<ViewUserDepartmentEntity> root) {
		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}
		List<Predicate> p = new ArrayList<Predicate>();
		if (filter != null) {
			if (filter.getUserNo() != null && filter.getUserNo() > 0) {
				Predicate condition = builder.equal(root.get("userNo"), filter.getUserNo());
				p.add(condition);
			}
			
			if (filter.getBuName() != null && !filter.getBuName().isEmpty()) {
				Predicate condition = builder.equal(root.get("buName"), filter.getBuName());
				p.add(condition);
			}
		}
		return p;

	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(ViewUserDepartmentEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}
}
