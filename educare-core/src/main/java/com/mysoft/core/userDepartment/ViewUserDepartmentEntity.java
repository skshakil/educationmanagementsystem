package com.mysoft.core.userDepartment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Immutable
@Table(name = "SA_GRANTBU_V")
public class ViewUserDepartmentEntity implements Serializable {

	private static final long serialVersionUID = 6094582050531571959L;
	
	
	
	@Id
	@Column(name = "ID", insertable = false, updatable = false, nullable = true)
	Long id;


	@Column(name = "USER_NO", insertable = false, updatable = false, nullable = true)
	Long userNo;

	@Column(name = "EMP_NO", insertable = false, updatable = false, nullable = true)
	Long empNo;
	
	@Column(name = "BU_NO", insertable = false, updatable = false, nullable = true) 
	Long buNo;
	
	@Column(name = "BU_NAME", insertable = false, updatable = false, nullable = false)
	String buName;
	
	@Column(name = "DEFAULT_FLAG", insertable = false, updatable = false, nullable = true)
	Integer defaultFlag;
	
	@Column(name = "SAM_RCV", insertable = false, updatable = false, nullable = true)
	Integer samRcv;
	
	@Column(name = "RESULT_EDIT", insertable = false, updatable = false, nullable = true)
	Integer resultEdit;
	
	@Column(name = "SAVE_FLAG", insertable = false, updatable = false, nullable = true)
	Integer saveFlag;
	
	@Column(name = "VERIFY_FLAG", insertable = false, updatable = false, nullable = true)
	Integer verifyFlag;
	
	@Column(name = "FINALIZE_FLAG", insertable = false, updatable = false, nullable = true)
	Integer FinalizeFlag;
	
	@Column(name = "PRINT_FINAL_FLAG", insertable = false, updatable = false, nullable = true)
	Integer printFinalFlag;
	
	@Column(name = "PREVIEW_FLAG", insertable = false, updatable = false, nullable = true)
	Integer previewFlag;
	
	@Column(name = "ALL_BU_NO", insertable = false, updatable = false, nullable = true)
	Integer allBuNo;
	
	@Column(name = "COMPANY_NO", insertable = false, updatable = false, nullable = true)
	Integer companyNo;
	
	@Column(name = "COMPANY_NAME", insertable = false, updatable = false, nullable = true)
	String companyName;

}
