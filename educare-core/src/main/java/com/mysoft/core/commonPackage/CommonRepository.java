package com.mysoft.core.commonPackage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.lookupDetail.LookupDetailService;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;

import oracle.jdbc.OracleTypes;

@Repository
@Transactional
public class CommonRepository extends BaseRepository {

	@Autowired
	JdbcTemplate db;

//	@Autowired
//	LookupDetailService lookupDetailService;

	public Response salutGenderMaritalList(String reqObj) {
		Response response = new Response();

		Map<String, List> resMap = new HashMap<String, List>();

		JSONObject sJson = new JSONObject();
		sJson.put("lookupNo", 1005);
		JSONObject gJson = new JSONObject();
		gJson.put("lookupNo", 5007);
		JSONObject mJson = new JSONObject();
		mJson.put("lookupNo", 1084);

//        LookupDetailEntity salutationEntity = new LookupDetailEntity();
//        salutationEntity.setLookupNo(1005L);
//        
//        LookupDetailEntity genderEntity = new LookupDetailEntity();
//        genderEntity.setLookupNo(5007L);
//        
//        LookupDetailEntity meritalEntity = new LookupDetailEntity();
//        meritalEntity.setLookupNo(1084L);

//		Response salutationResponse = lookupDetailService.list(sJson.toString());
//		Response genderResponse = lookupDetailService.list(gJson.toString());
//		Response meritalResponse = lookupDetailService.list(mJson.toString());
//
//		resMap.put("salutationList", salutationResponse.getItems());
//		resMap.put("genderList", genderResponse.getItems());
//		resMap.put("maritalStatusList", meritalResponse.getItems());
		response.setObj(resMap);

		return getSuccessResponse("List Found", response);
	}

	public Response getPayModeList() {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PayModeDto> payModeDtoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.payModeStmt());
			while (rs.next()) {
				PayModeDto payModeDto = new PayModeDto();
				payModeDto.setPayModeNo(rs.getLong("PAY_MODE_NO"));
				payModeDto.setPayModeName(rs.getString("PAY_MODE_NAME"));
				payModeDtoList.add(payModeDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setItems(payModeDtoList);
		return getSuccessResponse("Pay modeList Found", response);
	}

	public Response getPatientTypeListByFlagColumn(String flagColumn) {
		boolean errorFlag = false;
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientTypeDto> pateintTypeList = new ArrayList<PatientTypeDto>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.patientTypeListStmtByFlagColumn(flagColumn));
			while (rs.next()) {
				PatientTypeDto pateintType = new PatientTypeDto();
				pateintType.setPatientTypeNo(rs.getLong("pat_type_no"));
				pateintType.setPatientTypeName(rs.getString("pat_type_name"));
				pateintTypeList.add(pateintType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			errorFlag = true;
			// return getErrorResponse(" patient list found");
		} finally {
			finalyConStmRs(con, stm, rs);
		}
		if (errorFlag) {
			return getErrorResponse("Patient list not found");
		}
		response.setItems(pateintTypeList);
		return getSuccessResponse("Patient Type List Found", response);
	}

	public Response getPatientTypeList() {
		boolean errorFlag = false;
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientTypeDto> pateintTypeList = new ArrayList<PatientTypeDto>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.patientTypeListStmt());
			while (rs.next()) {
				PatientTypeDto pateintType = new PatientTypeDto();
				pateintType.setPatientTypeNo(rs.getLong("pat_type_no"));
				pateintType.setPatientTypeName(rs.getString("pat_type_name"));
				pateintTypeList.add(pateintType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			errorFlag = true;
			// return getErrorResponse(" patient list found");
		} finally {
			finalyConStmRs(con, stm, rs);
		}
		if (errorFlag) {
			return getErrorResponse(" patient list not found");
		}
		response.setItems(pateintTypeList);
		return getSuccessResponse("Patient Type List Found", response);
	}

	public Response getPatientTypeListIPD() {
		boolean errorFlag = false;
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientTypeDto> pateintTypeList = new ArrayList<PatientTypeDto>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.patientTypeListIPD());
			while (rs.next()) {
				PatientTypeDto pateintType = new PatientTypeDto();
				pateintType.setPatientTypeNo(rs.getLong("pat_type_no"));
				pateintType.setPatientTypeName(rs.getString("pat_type_name"));
				pateintTypeList.add(pateintType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			errorFlag = true;
			// return getErrorResponse(" patient list found");
		} finally {
			finalyConStmRs(con, stm, rs);
		}
		if (errorFlag) {
			return getErrorResponse(" patient list not found");
		}
		response.setItems(pateintTypeList);
		return getSuccessResponse("IPD Patient Type List Found", response);
	}

	public Response getPatientListAttr(Long patientTypeNo) {
		Response response = new Response();
		Map<String, Object> patTypeattr = new HashMap<>();
		SimpleJdbcCall procReadActor = new SimpleJdbcCall(db).withProcedureName("WEB_PD_PATIENT_TYPE_ATTR")
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(new SqlParameter("P_PATIENT_TYPE", Types.NUMERIC),
						new SqlOutParameter("P_PATIENT_TYPE_ATTR", OracleTypes.CURSOR));
		SqlParameterSource in = new MapSqlParameterSource().addValue("P_PATIENT_TYPE", patientTypeNo, Types.NUMERIC);
		Map out = procReadActor.execute(in);
		patTypeattr.put("items", out.get("P_PATIENT_TYPE_ATTR"));
		response.setModel(patTypeattr);
		return response;
	}

	@SuppressWarnings("unchecked")
	public Response getPatientInfoByType(String reqObj) {
		JSONObject json = new JSONObject(reqObj);
		Response response = new Response();
		Long patientTypeNo = Def.getLong(json, "patientTypeNo");

		if (patientTypeNo == 2) {
//            Response hnpatientRes = getHNPatient(reqObj);
			Response hnpatientRes = getPatientDetailsByHospitalNUmber(reqObj);
			List<PatientInfoDto> patientInfoList = hnpatientRes.getItems();
			if (null != patientInfoList && patientInfoList.size() > 0) {
				Long regNo = patientInfoList.get(0).getRegNo();
				if (isAdmitted(regNo)) {
					response.setSuccess(false);
					response.setMessage("Ipd Admitted Patient Not Allowed!");
				} else {
//                     response = getHNPatient(reqObj);
					response = getPatientDetailsByHospitalNUmber(reqObj);
				}
			} else {
				response = getPatientDetailsByHospitalNUmber(reqObj);
			}
			return response;
		} else if (patientTypeNo == 3 || patientTypeNo == 16) {
			String empName = Def.getString(json, "empName");
			return getStaffEmpPatient(empName);
		} else if (patientTypeNo == 7) {
			return ipdPatientList(reqObj);
		} else if (patientTypeNo == 11) {
			String empName = Def.getString(json, "empName");
			return getCorporatePatient(empName);
		} else if (patientTypeNo == 18) {
			String cardId = Def.getString(json, "cardId");
			return getCardHolderPatient(cardId);
		} else {
			response.setSuccess(false);
			response.setMessage("No Data Found!");
			response.setItems(new ArrayList<>());
			return response;
		}

	}

	@SuppressWarnings({ "unused", "unused", "unchecked" })
	public Response getPatientDetailsByHospitalNUmber(String reqObj) {

		Map<String, Object> result = new HashMap<>();
		Response response = new Response();

		JSONObject json = new JSONObject(reqObj);

		SimpleJdbcCall procReadActor = new SimpleJdbcCall(db).withProcedureName("pd_hn_details")
				.withoutProcedureColumnMetaDataAccess().declareParameters(

						new SqlParameter("p_hn", Types.VARCHAR),

						new SqlOutParameter("p_action", Types.NUMERIC), new SqlOutParameter("p_error", Types.VARCHAR),
						new SqlOutParameter("p_details", OracleTypes.CURSOR));

		SqlParameterSource in = new MapSqlParameterSource().addValue("p_hn", Def.getString(json, "hnNumber"),
				Types.VARCHAR);

		Map out = procReadActor.execute(in);

		result.put("actionFlag", out.get("p_action"));
		result.put("errorMsg", out.get("p_error"));
		result.put("patientDetails", out.get("p_details"));

		List<PatientInfoDto> patientInfoDtoList = new ArrayList<PatientInfoDto>();

		Long actionFlag = null;
		String errorMsg = null;

		if (null != result.get("actionFlag")) {
			actionFlag = Long.parseLong(result.get("actionFlag").toString());
		}
		if (null != result.get("errorMsg")) {
			errorMsg = result.get("errorMsg").toString();
		}

		if (null != actionFlag && actionFlag == 1L) {
			List patientDetailList = (List) result.get("patientDetails");

			patientInfoDtoList = new ArrayList<PatientInfoDto>();

			for (int i = 0; i < patientDetailList.size(); i++) {

				PatientInfoDto patientInfoDto = new PatientInfoDto();

				Map mList = (Map) patientDetailList.get(i);

				if (null != mList.get("REG_NO")) {
					patientInfoDto.setRegNo(Long.parseLong(mList.get("REG_NO").toString()));
				}
				;
				if (null != mList.get("HOSPITAL_NUMBER")) {
					patientInfoDto.setHospitalNo(mList.get("HOSPITAL_NUMBER").toString());
				}
				;
				if (null != mList.get("SALUTATION")) {
					patientInfoDto.setSalutation(mList.get("SALUTATION").toString());
				}
				;
				if (null != mList.get("FNAME")) {
					patientInfoDto.setFName(mList.get("FNAME").toString());
				}
				;
				if (null != mList.get("PATIENT_NAME")) {
					patientInfoDto.setPatientName(mList.get("PATIENT_NAME").toString());
				}
				;
				if (null != mList.get("GENDER")) {
					patientInfoDto.setGender(mList.get("GENDER").toString());
				}
				;
				if (null != mList.get("M_STATUS")) {
					patientInfoDto.setMaritalStatus(mList.get("M_STATUS").toString());
				}
				;
				if (null != mList.get("AGE_DD")) {
					patientInfoDto.setAgeDD(mList.get("AGE_DD").toString());
				}
				;
				if (null != mList.get("AGE_MM")) {
					patientInfoDto.setAgeMM(mList.get("AGE_MM").toString());
				}
				;
				if (null != mList.get("AGE_YY")) {
					patientInfoDto.setAgeYY(mList.get("AGE_YY").toString());
				}
				;
				if (null != mList.get("AGE")) {
					patientInfoDto.setAge(mList.get("AGE").toString());
				}
				;
				if (null != mList.get("DOB")) {
					patientInfoDto.setDob((Date) (mList.get("DOB")));
				}
				;
				if (null != mList.get("BLOOD_GROUP")) {
					patientInfoDto.setBloodGroup(mList.get("BLOOD_GROUP").toString());
				}
				;
				if (null != mList.get("ADDRESS")) {
					patientInfoDto.setAddress(mList.get("ADDRESS").toString());
				}
				;
				if (null != mList.get("PHONE_MOBILE")) {
					patientInfoDto.setPhoneMobile(mList.get("PHONE_MOBILE").toString());
				}
				;
				if (null != mList.get("EMAIL")) {
					patientInfoDto.setEmail(mList.get("EMAIL").toString());
				}
				;
				if (null != mList.get("COR_CLIENT_CARD_NO")) {
					patientInfoDto.setCardNo(Long.parseLong(mList.get("COR_CLIENT_CARD_NO").toString()));
				}
				;
				if (null != mList.get("RELATION_NO")) {
					patientInfoDto.setRelationNo(Long.parseLong(mList.get("RELATION_NO").toString()));
				}
				;
				if (null != mList.get("RELATION")) {
					patientInfoDto.setRelationName(mList.get("RELATION").toString());
				}
				;
				if (null != mList.get("EMP_NO")) {
					patientInfoDto.setEmpNo(Long.parseLong(mList.get("EMP_NO").toString()));
				}
				;
				if (null != mList.get("EMP_ID")) {
					patientInfoDto.setEmpId(mList.get("EMP_ID").toString());
				}
				;
				if (null != mList.get("EMP_NAME")) {
					patientInfoDto.setEmpName(mList.get("EMP_NAME").toString());
				}
				;
				if (null != mList.get("COR_CLIENT_NO")) {
					patientInfoDto.setCorClientNo(Long.parseLong(mList.get("COR_CLIENT_NO").toString()));
				}
				;
				if (null != mList.get("COR_CLIENT_ID")) {
					patientInfoDto.setCorClientId(mList.get("COR_CLIENT_ID").toString());
				}
				;
				if (null != mList.get("COR_CLIENT_NAME")) {
					patientInfoDto.setCorClientName(mList.get("COR_CLIENT_NAME").toString());
				}
				;

				patientInfoDtoList.add(patientInfoDto);
			}

		} else {
			patientInfoDtoList = new ArrayList<PatientInfoDto>();
			response.setSuccess(false);
		}

		response.setMessage(errorMsg);
		response.setItems(patientInfoDtoList);
		return response;
	}

//    public 

	@SuppressWarnings({ "unused", "unused" })
	public Response getHNPatient(String reqObj) {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientInfoDto> patientInfoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.hnPatientStmt(reqObj));
			while (rs.next()) {
				PatientInfoDto patientInfoDto = new PatientInfoDto();
				patientInfoDto.setRegNo(rs.getLong("REG_NO"));
				patientInfoDto.setHospitalNo(rs.getString("HOSPITAL_NUMBER"));
				patientInfoDto.setSalutation(rs.getString("SALUTATION"));
				patientInfoDto.setFName(rs.getString("FNAME"));
				patientInfoDto.setLName(rs.getString("LNAME"));
				patientInfoDto.setPatientName(rs.getString("PATIENT_NAME"));
				patientInfoDto.setGender(rs.getString("GENDER"));
				patientInfoDto.setMaritalStatus(rs.getString("M_STATUS"));
				patientInfoDto.setAgeDD(rs.getString("AGE_DD"));
				patientInfoDto.setAgeMM(rs.getString("AGE_MM"));
				patientInfoDto.setAgeYY(rs.getString("AGE_YY"));
				patientInfoDto.setAge(rs.getString("AGE"));
				patientInfoDto.setDob(rs.getDate("DOB"));
				patientInfoDto.setBloodGroup(rs.getString("BLOOD_GROUP"));
				patientInfoDto.setAddress(rs.getString("ADDRESS"));
				patientInfoDto.setPhoneMobile(rs.getString("PHONE_MOBILE"));
				patientInfoDto.setEmail(rs.getString("EMAIL"));

//                patientInfoDto.setCardNo(rs.getLong("COR_CLIENT_CARD_NO"));
//                patientInfoDto.setCorClientNo(rs.getLong("COR_CLIENT_NO"));
//                patientInfoDto.setRelationNo(rs.getLong("RELATION_NO"));

				patientInfoList.add(patientInfoDto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setItems(patientInfoList);
		return getSuccessResponse("Patient List Found", response);
	}

	public Boolean isAdmitted(Long regNo) {
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		Boolean result = null;

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT COUNT(ADMISSION_NO) AD_COUNT ");
		sqlQuery.append("FROM IPD_ADMISSION ");
		sqlQuery.append("WHERE REG_NO = ");
		sqlQuery.append(regNo);
		sqlQuery.append(" AND NVL(DIS_FLAG,0) = 0 ");
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(sqlQuery.toString());
			while (rs.next()) {
				result = rs.getLong("AD_COUNT") != 0;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	@SuppressWarnings({ "unused", "unused" })
	public Response getStaffEmpPatient(String empName) {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientInfoDto> patientInfoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.staffEmpListStm(empName));
			while (rs.next()) {
				PatientInfoDto patientInfoDto = new PatientInfoDto();
				patientInfoDto.setEmpNo(rs.getLong("EMP_NO"));
				patientInfoDto.setEmpId(rs.getString("EMP_ID"));
				patientInfoDto.setEmpName(rs.getString("EMP_NAME"));
				patientInfoDto.setPhoneMobile(rs.getString("PHONE_MOBILE"));
				patientInfoDto.setAddress(rs.getString("PE_ADDR1"));
				patientInfoList.add(patientInfoDto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setItems(patientInfoList);
		return getSuccessResponse("Patient List Found", response);
	}

	@SuppressWarnings({ "unused", "unused" })
	public Response getStaffEmployeeList(String empName) {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientInfoDto> patientInfoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.staffEmpListStm(empName));
			while (rs.next()) {
				PatientInfoDto patientInfoDto = new PatientInfoDto();
				patientInfoDto.setEmpNo(rs.getLong("EMP_NO"));
				patientInfoDto.setEmpId(rs.getString("EMP_ID"));
				patientInfoDto.setEmpName(rs.getString("EMP_NAME"));
				patientInfoList.add(patientInfoDto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.setItems(patientInfoList);
		return getSuccessResponse("Patient List Found", response);
	}

	@SuppressWarnings({ "unused", "unused" })
	public Response getDependentRelation() {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientInfoDto> patientInfoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.dependentRelationStmt());
			while (rs.next()) {

				PatientInfoDto patientInfoDto = new PatientInfoDto();
				patientInfoDto.setRelationNo(rs.getLong("RELATION_NO"));
				patientInfoDto.setRelationId(rs.getString("RELATION_ID"));
				patientInfoDto.setRelationName(rs.getString("RELATION"));
				patientInfoList.add(patientInfoDto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setItems(patientInfoList);
		return getSuccessResponse("Patient List Found", response);
	}

	@SuppressWarnings({ "unused", "unused" })
	public Response getCorporatePatient(String empName) {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientInfoDto> patientInfoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.corporateListStm(empName));
			while (rs.next()) {
				PatientInfoDto patientInfoDto = new PatientInfoDto();

				patientInfoDto.setCorClientNo(rs.getLong("COR_CLIENT_NO"));
				patientInfoDto.setCorClientId(rs.getString("COR_CLIENT_ID"));
				patientInfoDto.setCorClientName(rs.getString("COR_CLIENT_NAME"));
				patientInfoList.add(patientInfoDto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setItems(patientInfoList);
		return getSuccessResponse("Patient List Found", response);
	}

//	SELECT CARD_NO, CARD_ID, ENTRY_DATE, SALUTATION, 
//    FNAME, LNAME, GENDER, M_STATUS, 
//    AGE_DD, AGE_MM, AGE_YY, DOB, 
//    BLOOD_GROUP, PHONE_MOBILE, PE_ADDR1, NVL(ACTIVE_STAT,1) ACTIVE_STAT
//    FROM V_BILLS_PATIENT_CARD  ; 

	@SuppressWarnings({ "unused", "unused" })
	public Response getCardHolderPatient(String cardId) {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientInfoDto> patientInfoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.cardHolderListStm(cardId));
			while (rs.next()) {
				PatientInfoDto patientInfoDto = new PatientInfoDto();

				patientInfoDto.setCardNo(rs.getLong("CARD_NO"));
				patientInfoDto.setCardId(rs.getString("CARD_ID"));
				patientInfoDto.setEntryDate(rs.getDate("ENTRY_DATE"));
				patientInfoDto.setSalutation(rs.getString("SALUTATION"));
				patientInfoDto.setFName(rs.getString("FNAME"));
				patientInfoDto.setLName(rs.getString("LNAME"));
				patientInfoDto.setGender(rs.getString("GENDER"));
				patientInfoDto.setGender(rs.getString("GENDER"));
				patientInfoDto.setMaritalStatus(rs.getString("M_STATUS"));
				patientInfoDto.setAgeDD(rs.getString("AGE_DD"));
				patientInfoDto.setAgeMM(rs.getString("AGE_MM"));
				patientInfoDto.setAgeYY(rs.getString("AGE_YY"));
				patientInfoDto.setDob(rs.getDate("DOB"));
				patientInfoDto.setBloodGroup(rs.getString("BLOOD_GROUP"));
				patientInfoDto.setAddress(rs.getString("PE_ADDR1"));
				patientInfoDto.setPhoneMobile(rs.getString("PHONE_MOBILE"));
				patientInfoDto.setEmail(rs.getString("EMAIL_PERSONAL"));
				patientInfoDto.setActiveStatus(rs.getLong("ACTIVE_STAT"));
				patientInfoList.add(patientInfoDto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setItems(patientInfoList);
		return getSuccessResponse("Patient List Found", response);
	}

	public Response ipdPatientList(String reqObj) {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List<PatientInfoDto> patientInfoList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.ipdAdmissionPatientStmt(reqObj));
			while (rs.next()) {
				PatientInfoDto patientInfoDto = new PatientInfoDto();
				patientInfoDto.setRegNo(rs.getLong("REG_NO"));
				patientInfoDto.setHospitalNo(rs.getString("HOSPITAL_NUMBER"));
				patientInfoDto.setSalutation(rs.getString("SALUTATION"));
				patientInfoDto.setFName(rs.getString("FNAME"));
				patientInfoDto.setLName(rs.getString("LNAME"));
				patientInfoDto.setPatientName(rs.getString("PATIENT_NAME"));
				patientInfoDto.setGender(rs.getString("GENDER"));
				patientInfoDto.setMaritalStatus(rs.getString("M_STATUS"));
				patientInfoDto.setAgeDD(rs.getString("AGE_DD"));
				patientInfoDto.setAgeMM(rs.getString("AGE_MM"));
				patientInfoDto.setAgeYY(rs.getString("AGE_YY"));
				patientInfoDto.setAge(rs.getString("AGE"));
				patientInfoDto.setDob(rs.getDate("DOB"));
				patientInfoDto.setBloodGroup(rs.getString("BLOOD_GROUP"));
				patientInfoDto.setAddress(rs.getString("ADDRESS"));
				patientInfoDto.setPhoneMobile(rs.getString("PHONE_MOBILE"));
				patientInfoDto.setEmail(rs.getString("EMAIL"));

				patientInfoDto.setAdmissionNo(rs.getLong("ADMISSION_NO"));
				patientInfoDto.setAdmissionId(rs.getString("ADMISSION_ID"));
				patientInfoDto.setRegNo(rs.getLong("REG_NO"));
				patientInfoDto.setDoctorNo(rs.getLong("DOCTOR_NO"));
				patientInfoDto.setRefDoctorNo(rs.getLong("REF_DOCTOR_NO"));
				patientInfoDto.setBedNo(rs.getLong("BED_NO"));
				patientInfoDto.setBuNo(rs.getLong("BU_NO"));
				patientInfoList.add(patientInfoDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stm != null) {
					stm.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setItems(patientInfoList);
		return getSuccessResponse("Ipd Patient List Found", response);
	}

	public Response ipdWardList(String flagName, String flagValue) {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List wardList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.wardList(flagName, flagValue));
			while (rs.next()) {
				Map<String, Object> singelWard = new HashMap<>();
				singelWard.put("ward_name", rs.getString("ward_name"));
				singelWard.put("ward_no", rs.getString("ward_no"));
				wardList.add(singelWard);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			finalyConStmRs(con, stm, rs);
		}
		response.setItems(wardList);
		return getSuccessResponse("Ward List Found", response);
	}

	public Response primaryDoctorList() {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List priDocList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.primaryDoctorList());
			while (rs.next()) {
				Map<String, Object> singleDoctor = new HashMap<>();
				singleDoctor.put("doctorName", rs.getString("DOCTOR_NAME"));
				singleDoctor.put("doctorNo", rs.getString("DOCTOR_NO"));
				singleDoctor.put("doctorId", rs.getString("DOCTOR_ID"));
				singleDoctor.put("docDegree", rs.getString("DOC_DEGREE"));
				singleDoctor.put("phoneMobile", rs.getString("PHONE_MOBILE"));
				singleDoctor.put("buNo", rs.getString("BU_NO"));
				singleDoctor.put("buName", rs.getString("BU_NAME"));
				priDocList.add(singleDoctor);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			finalyConStmRs(con, stm, rs);
		}
		response.setItems(priDocList);
		return getSuccessResponse("Primary Doctor List Found", response);
	}

	public Response ipdDeptList() {
		Response response = new Response();
		Connection con = null;
		ResultSet rs = null;
		Statement stm = null;
		List deptList = new ArrayList<>();
		try {
			con = getOraConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(CommonStatement.ipdDepartmentList());
			while (rs.next()) {
				Map<String, Object> singleDept = new HashMap<>();
				singleDept.put("BU_NO", rs.getString("BU_NO"));
				singleDept.put("BU_ID", rs.getString("BU_ID"));
				singleDept.put("BU_NAME", rs.getString("BU_NAME"));
				deptList.add(singleDept);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			finalyConStmRs(con, stm, rs);
		}
		response.setItems(deptList);
		return getSuccessResponse("Department List Found", response);
	}
}
