package com.mysoft.core.commonPackage;

import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonService {
	
	@Autowired
	CommonRepository commonRepository;

	public Response salutGenderMaritalList(String reqObj) {
		return commonRepository.salutGenderMaritalList(reqObj);
	}
	
	public Response getPayModeList() {
		return commonRepository.getPayModeList();
	}
	
	public Response getPatientTypeListByFlagColumn(String flagColumn) {
		return commonRepository.getPatientTypeListByFlagColumn(flagColumn);
	}

	public Response getPatientTypeList() {
		return commonRepository.getPatientTypeList();
	}

	public Response getPatientTypeListIpd() {
		return commonRepository.getPatientTypeListIPD();
	}
	
	public Response getPatientListAttr(String reqObj) {
		JSONObject json = new JSONObject(reqObj);
		return commonRepository.getPatientListAttr(Def.getLong(json, "patientTypeNo"));
	}
	
	public Response getDependentRelation() {
		return commonRepository.getDependentRelation();
	}
	
	
	public Response getPatientInfoByType(String reqObj) {
		return commonRepository.getPatientInfoByType(reqObj);
	}

    public Response wardList(String flagName, String flagValue) {
        return commonRepository.ipdWardList(flagName, flagValue);
    }

    public Response primaryDoctorList() {
        return commonRepository.primaryDoctorList();
    }

    public Response ipdDepartmentList() {
        return commonRepository.ipdDeptList();
    }

//	public Response admitedPatientInfo(String reqObj) {
//		return commonRepository.admitedPatientInfo(reqObj);
//	}
//	
//	public Response dayCarePatientList(String reqObj) {
//		return commonRepository.dayCarePatientList(reqObj);
//	}
//
//	public Response patientList(String reqObj) {
//		return commonRepository.hnPatientList(reqObj);
//	}
//	
//	public Response storeUserList(String reqObj) {
//		return commonRepository.storeUserList(reqObj);
//	}
//	
//	public Response refDocList(String reqObj) {
//		return commonRepository.refDocList(reqObj);
//	}
//	
//	public Response maxDiscount(String reqObj) {
//		return commonRepository.maxDiscount(reqObj);
//	}
//	public Response discDistribution(String reqObj) {
//		return commonRepository.discDistribution(reqObj);
//	}
//	
	
	
}
