package com.mysoft.core.commonPackage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientTypeDto {
	private Long patientTypeNo;
	private String patientTypeName;
}
