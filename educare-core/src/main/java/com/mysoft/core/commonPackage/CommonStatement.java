package com.mysoft.core.commonPackage;

import com.mysoft.core.util.CommonFunctions;
import com.mysoft.core.util.CommonUtils;
import com.mysoft.core.util.Def;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class CommonStatement implements CommonFunctions {
	
	
	public static String payModeStmt() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT PAY_MODE_NO,PAY_MODE_NAME ");
		sqlQuery.append("FROM BILLS_PAYMODE ");
		sqlQuery.append("WHERE ACTIVE_STAT = 1");
		return sqlQuery.toString();
	}
	
	 /**
	 * @ Patient Type List
	 *  
	 * */
	public static String patientTypeListStmtByFlagColumn(String flagColumn) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT pat_type_name, pat_type_no ");
		sqlQuery.append("FROM hpms_patient_type ");
		sqlQuery.append("WHERE "+flagColumn+" = 1 ");
		sqlQuery.append("ORDER BY pat_type_no ");
		return sqlQuery.toString();
	}
	
	 /**
	 * @ Patient Type List
	 *  
	 * */
	public static String patientTypeListStmt() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT pat_type_name, pat_type_no ");
		sqlQuery.append("FROM hpms_patient_type ");
		sqlQuery.append("WHERE dig_flag = 1 ");
		sqlQuery.append("ORDER BY pat_type_no ");
		return sqlQuery.toString();
	}

	/**
	 * @IPD Patient Type List
	 */
	public static String patientTypeListIPD() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT pat_type_name, pat_type_no ");
		sqlQuery.append("FROM hpms_patient_type ");
		sqlQuery.append("WHERE ipd_flag = 1 ");
		sqlQuery.append("ORDER BY pat_type_no ");
		return sqlQuery.toString();
	}
	/**
	 * @ HN Patient List
	 * 
	 * */
	public static String hnPatientStmt(String reqObj) {
		JSONObject json = new JSONObject(reqObj);
		StringBuilder sqlQuery = new StringBuilder();
		
		sqlQuery.append("SELECT REG_NO,HOSPITAL_NUMBER,SALUTATION,FNAME,LNAME,PATIENT_NAME, ");
		sqlQuery.append("GENDER AS GENDER,M_STATUS AS M_STATUS, ");
		sqlQuery.append("AGE_DD,AGE_MM,AGE_YY,AGE,DOB,BLOOD_GROUP,ADDRESS,PHONE_MOBILE,EMAIL, ");
		sqlQuery.append("COR_CLIENT_CARD_NO, COR_CLIENT_NO, RELATION_NO ");
		sqlQuery.append("FROM OPD_REGISTRATION ");
		sqlQuery.append("WHERE HOSPITAL_NUMBER = ");
		sqlQuery.append("'"+Def.getString(json, "hnNumber")+"' ");
		
		System.out.println(sqlQuery+ " ++ sqlQuery");
		return sqlQuery.toString();

	}
	
	/**
	 * @ staffEmpListStmr Patient List
	 * 
	 * */
	public static String staffEmpListStm(String empName) {
//		JSONObject json = new JSONObject(reqObj);
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append("SELECT EMP_ID, EMP_NAME, EMP_NO,PHONE_MOBILE,PE_ADDR1 ");
		sqlQuery.append("FROM V_HR_EMP ");
		sqlQuery.append("WHERE ");
		sqlQuery.append("UPPER(EMP_NAME) LIKE ");
		sqlQuery.append("('" + CommonUtils.PERCENTAGE_SIGN );
		sqlQuery.append(empName.toUpperCase() );
		sqlQuery.append( CommonUtils.PERCENTAGE_SIGN + "') ");
		sqlQuery.append("AND NVL(ACTIVE_STAT,0) = 1");
		
		System.out.println(sqlQuery+ " ++ sqlQuery");
		return sqlQuery.toString();

	}
	public static String dependentRelationStmt() {
//		JSONObject json = new JSONObject(reqObj);
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append("SELECT RELATION_NO,RELATION_ID, RELATION ");
		sqlQuery.append("FROM V_HPMS_RELATION ");
		sqlQuery.append("WHERE NVL(ACTIVE_STAT,0) = 1 ");
		sqlQuery.append("AND RELATION_NO <> 0 " );
		sqlQuery.append("ORDER BY RELATION_NO " );
		
		System.out.println(sqlQuery+ " ++ sqlQuery");
		return sqlQuery.toString();

	}
	
//	SELECT RELATION, TO_CHAR(RELATION_NO) REL_NO
//	FROM V_HPMS_RELATION
//	WHERE NVL(ACTIVE_STAT,0) = 1
//	AND RELATION_NO <> 0
//	ORDER BY RELATION_NO;
	
	/**
	 * @ staffEmpListStmr Patient List
	 * 
	 * */
	public static String corporateListStm(String empName) {
//		JSONObject json = new JSONObject(reqObj);
		StringBuilder sqlQuery = new StringBuilder();
		
		sqlQuery.append("SELECT COR_CLIENT_NAME, COR_CLIENT_ID, COR_CLIENT_NO ");
		sqlQuery.append("FROM V_BILLS_CORPORATE_CLIENT ");
		sqlQuery.append("WHERE ");
		sqlQuery.append("UPPER(COR_CLIENT_NAME) LIKE ");
		sqlQuery.append("('" + CommonUtils.PERCENTAGE_SIGN );
		sqlQuery.append(empName.toUpperCase() );
		sqlQuery.append(CommonUtils.PERCENTAGE_SIGN + "') ");
		sqlQuery.append("AND NVL(ACTIVE_STATUS,0) = 1");
		
		System.out.println(sqlQuery+ " ++ sqlQuery");
		return sqlQuery.toString();

	}
	
	
	

	/**
	 * @ staffEmpListStmr Patient List
	 * 
	 * */
	public static String cardHolderListStm(String cardId) {
//		JSONObject json = new JSONObject(reqObj);
		StringBuilder sqlQuery = new StringBuilder();
		
		sqlQuery.append("SELECT CARD_NO, CARD_ID, ENTRY_DATE, SALUTATION, ");
		sqlQuery.append("FNAME, LNAME, GENDER, M_STATUS, ");
		sqlQuery.append("AGE_DD, AGE_MM, AGE_YY, DOB,EMAIL_PERSONAL, ");
		sqlQuery.append("BLOOD_GROUP, PHONE_MOBILE, PE_ADDR1, NVL(ACTIVE_STAT,1) AS ACTIVE_STAT ");
		sqlQuery.append("FROM V_BILLS_PATIENT_CARD  ");
		sqlQuery.append("WHERE CARD_ID = ");
		sqlQuery.append("'"+cardId+"'");

		System.out.println(sqlQuery+ " ++ sqlQuery");
		return sqlQuery.toString();

	}

	/**
	 * @ Admitted Patient List
	 * 
	 * */
	public static String ipdAdmissionPatientStmt(String reqObj) {
		JSONObject json = new JSONObject(reqObj);

		StringBuilder sqlQuery = new StringBuilder();
						
		sqlQuery.append("SELECT I.REG_NO AS REG_NO, O.HOSPITAL_NUMBER AS HOSPITAL_NUMBER, ");
		sqlQuery.append("O.SALUTATION AS SALUTATION, O.FNAME AS FNAME, O.LNAME AS LNAME, O.PATIENT_NAME AS PATIENT_NAME, ");
		sqlQuery.append("O.GENDER_DATA AS GENDER, O.M_STATUS_DATA AS M_STATUS, ");
		sqlQuery.append("O.AGE_DD AS AGE_DD, O.AGE_MM AS AGE_MM, O.AGE_YY AS AGE_YY, O.AGE AS AGE, O.DOB AS DOB, ");
		sqlQuery.append("O.BLOOD_GROUP AS BLOOD_GROUP, O.ADDRESS AS ADDRESS, O.PHONE_MOBILE AS PHONE_MOBILE, O.EMAIL AS EMAIL, ");
		
		sqlQuery.append("I.ADMISSION_NO AS ADMISSION_NO, I.ADMISSION_ID AS ADMISSION_ID, I.PRIMARY_DOCTOR_NO AS DOCTOR_NO, I.REF_DOCTOR_NO AS REF_DOCTOR_NO, ");
		sqlQuery.append("I.CURR_BED_NO AS BED_NO,I.BU_NO AS BU_NO ");
		sqlQuery.append("FROM IPD_ADMISSION I LEFT OUTER JOIN OPD_REGISTRATION O ON (I.REG_NO = O.REG_NO) ");
		sqlQuery.append("WHERE I.ADMISSION_ID = ");
		sqlQuery.append("'"+Def.getString(json, "admissionId")+"' ");

		return sqlQuery.toString();

	}
	
	public static String dayCareAdmissionPatient(String reqObj) {
		JSONObject json = new JSONObject(reqObj);

		StringBuilder sqlQuery = new StringBuilder();
				
		sqlQuery.append("SELECT D.DAYCARE_ADMISSION_NO ADMISSION_NO, D.DAYCARE_ADMISSION_ID ADMISSION_ID, D.REG_NO, ");
		sqlQuery.append("D.DUTY_DOCTOR_NO DOCTOR_NO, D.CURR_BED_NO BED_NO, D.BU_NO ");
		sqlQuery.append("FROM DAYCARE_ADMISSION D ");
		sqlQuery.append("WHERE DAYCARE_ADMISSION_ID = ");
		sqlQuery.append("'"+Def.getString(json, "admissionId")+"' ");

		return sqlQuery.toString();

	}

	public static String wardList(String flagName, String flagValue) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT ward_name, ward_no FROM ipds_ward where ")
				.append(flagName)
				.append("=")
				.append(flagValue);
		return sqlQuery.toString();
	}

	public static String primaryDoctorList() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT H.DOCTOR_NAME, H.DOCTOR_NO, H.DOCTOR_ID, H.DOC_DEGREE, H.PHONE_MOBILE, ")
				.append("D.BU_NO,INITCAP(F_GET_BU(D.BU_NO)) BU_NAME ")
				.append("FROM HPMS_IPD_PRIMARY_DOC_V H, HPMS_DOCTOR D ")
				.append("WHERE H.ACTIVE_STAT = 1 ")
				.append("AND H.DOCTOR_NO = D.DOCTOR_NO");
		return sqlQuery.toString();
	}

	public static String ipdDepartmentList() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT B.BU_NO, BU_ID, BU_NAME FROM HR_BU B,HR_BU_FLAG F ")
				.append("WHERE B.BU_NO = F.BU_NO ")
				.append("AND B.OG_NO = F.OG_NO ")
				.append("AND B.COMPANY_NO = F.COMPANY_NO ")
				.append("AND NVL(F.IPD_DEPARTMENT,0) = 1 ")
				.append("AND B.ACTIVE_STAT = 1 ")
				.append("ORDER BY 3");
		return sqlQuery.toString();
	}
	
}
