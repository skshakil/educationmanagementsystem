package com.mysoft.core.commonPackage;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientInfoDto {

	private Long   regNo;
	private String hospitalNo;
	private String salutation;
	private String fName;
	private String lName;		
	private String patientName;
	private String gender;
	private String maritalStatus;
	private String ageDD;
	private String ageMM;
	private String ageYY;
	private String age;
	private Date dob;
	private String bloodGroup;
	private String address;
	private String phoneMobile;
	private String email;
	
	private Long doctorNo;
	private String doctorId;
	private String doctorInfo;
	
	private Long refDoctorNo;
	
//	admission
	private Long admissionNo;
	private String admissionId;
	private Long bedNo;
	private Long buNo;
	
//	staff/employee
	private Long empNo;
	private String empId;
	private String empName;
	
//	corporate client
	private Long corClientNo;
	private String corClientId;
	private String corClientName;
	
//	Card holder
	private Long cardNo;
	private String cardId;
	private Date entryDate;
	private Long activeStatus;

//	Relation
	private Long relationNo;
	private String relationId;
	private String relationName;
	
	

}
