package com.mysoft.core.commonPackage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayModeDto {

	private Long payModeNo;
	private String payModeName;
	
}
