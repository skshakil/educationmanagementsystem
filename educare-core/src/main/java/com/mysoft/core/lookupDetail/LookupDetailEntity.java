package com.mysoft.core.lookupDetail;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "SA_LOOKUPDTL")
public class LookupDetailEntity extends BaseOraEntity implements Serializable{

	private static final long serialVersionUID = -4538971070304943261L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "LOOKUPDTL_NO")
	private Long id;
	 
//	@Column(name = "LOOKUPDTL_NO_PARENT")
	@Transient
	private Long lookupdtlNoParent;

	@Transient
	private Long notEqualId;
	
	@NotNull
	@Column(name = "LOOKUP_NO")
	private Long lookupNo;

	@Column(name = "SL_NO")
	private Integer slNo;
	  
	@NotNull
	@Column(name = "LOOKDTL_NAME",length = 100)
	private String dtlName;
	
	@Column(name = "DESCRIPTION",length = 4000)
	private String dtlDescription;
	         
	@NotNull
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus = 1;

	@Column(name = "LOOKDTL_NAME_MAP")
	private String lookdtlNameMap;
	
	
	@Transient List<Long> lookupNoList;
	@Transient List<Long> LookupDltNoList;
	
	@Transient String lookupdtlNoParentName;

//	public List<Long> getLookupNoList() {
//		return lookupNoList;
//	}
//
//	public void setLookupNoList(List<Long> lookupNoList) {
//		this.lookupNoList = lookupNoList;
//	}
	              
}
