package com.mysoft.core.lookupDetail;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.commonPackage.CommonService;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.CoreRegistrationType;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

import one.util.streamex.StreamEx;

@Repository
@Transactional
public class LookupDetailRepository extends BaseRepository {

	@Autowired
	private CommonService commonService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {

		DataTableResults<LookupDetailEntity> dataTableResults = null;
		Response response = new Response();
		LookupDetailEntity saLookupDtlEntity = new LookupDetailEntity();
		String lookupNo = request.getParameter("lookupNo");
		ArrayList<String> parameterNames = new ArrayList<String>();
		Enumeration enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String parameterName = (String) enumeration.nextElement();
			System.out.println(parameterName);
			parameterNames.add(parameterName);
		}

		if (lookupNo != null) {
			saLookupDtlEntity.setLookupNo(Long.parseLong(lookupNo));
		}

		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(saLookupDtlEntity);

		List gridList = new ArrayList<>();
		response = baseList(typedQuery(saLookupDtlEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request, Long lookupNo) {

		String ogNo = request.getParameter("ogNo");
		String companyNo = request.getParameter("companyNo");

		Response response = new Response();
		DataTableResults<LookupDetailEntity> dataTableResults = null;
		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		LookupDetailEntity saLookupDtlEntity = new LookupDetailEntity();

		if (lookupNo != null) {
			saLookupDtlEntity.setLookupNo(lookupNo);
		}
		if (!StringUtils.isEmpty(ogNo)) {
			saLookupDtlEntity.setOrganizationNo(Long.parseLong(ogNo));
		}
		if (!StringUtils.isEmpty(companyNo)) {
			saLookupDtlEntity.setCompanyNo(Long.parseLong(companyNo));
		}

		Long totalRowCount = totalCount(saLookupDtlEntity);
		List gridList = new ArrayList<>();
		response = baseList(typedQuery(saLookupDtlEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response salutationGridList(HttpServletRequest request) {
		return gridList(request, salutationLookupNo);
	}

	public Response specilizationGridList(HttpServletRequest request) {
		return gridList(request, specializationLookupNo);
	}

//	public Response rankGridList(HttpServletRequest request) {
//		return gridList(request, rankLookupNo);
//	}

//	public Response personalCategoryGridList(HttpServletRequest request) {
//		return gridList(request, personCategoryLookupNo);
//	}

//	public Response serviceCategoryGridList(HttpServletRequest request) {
//		return gridList(request, serviceCategoryLookupNo);
//	}

//	public Response unitGridList(HttpServletRequest request) {
//		return gridList(request, unitsLookupNo);
//	}

//	public Response corpGridList(HttpServletRequest request) {
//		return gridList(request, corpsLookupNo);
//	}

//	public Response eligibilityStatusGridList(HttpServletRequest request) {
//		return gridList(request, eligibilityLookupNo);
//	}

	public Response list(String reqObj) {
		LookupDetailEntity saLookupDtlEntity = null;
		if (null != reqObj) {
			saLookupDtlEntity = objectMapperReadValue(reqObj, LookupDetailEntity.class);
		}
		return baseList(criteriaQuery(saLookupDtlEntity));
	}

	public Response lookupDetailList() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();

//		lookupNoList.add(prefixLookupNo);
//		lookupNoList.add(unitsLookupNo);
//		lookupNoList.add(eligibilityLookupNo);
//		lookupNoList.add(serviceCategoryLookupNo);
		lookupNoList.add(nationalityLookupNo);
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(maritalStatusLookupNo);
		lookupNoList.add(religionLookupNo);
		lookupNoList.add(medicalCategoryLookupNo);
		lookupNoList.add(districtLookupNo);
		lookupNoList.add(relationLookupNo);
//		lookupNoList.add(jobTypeNo);
//		lookupNoList.add(employeeTypeNo);
//		lookupNoList.add(hrTypeNo);
//		lookupNoList.add(upazilaLookupNo);
		lookupNoList.add(countryLookupNo);

		return findLookupDetials(lookupNoList, userDetail);
	}

	@SuppressWarnings("unchecked")
	public Response ipdInitList() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();

		lookupNoList.add(patientCondition);
		lookupNoList.add(arrivalCondition);
		lookupNoList.add(accompainedBy);
		lookupNoList.add(genderLookupNo);
		lookupNoList.add(maritalStatusLookupNo);
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(religionLookupNo);
		lookupNoList.add(salutationLookupNo);
		lookupNoList.add(relationLookupNo);

		Map<String, Object> dataMap = new HashMap<String, Object>();

		Response res = findLookupDetials(lookupNoList, userDetail);
		if (res.isSuccess() && res.getObj() != null) {
			dataMap = (Map<String, Object>) res.getObj();
		}

		Response resPatientType = commonService.getPatientTypeListIpd();
		if (resPatientType.isSuccess()) {
			dataMap.put("patientTypeIpd", resPatientType.getItems());
		}
		Response resPayMode = commonService.getPayModeList();
		if (resPayMode.isSuccess()) {
			dataMap.put("payModeList", resPayMode.getItems());
		}
		Response resDeptList = commonService.ipdDepartmentList();
		if (resDeptList.isSuccess()) {
			dataMap.put("departmentList", resDeptList.getItems());
		}

		res.setObj(dataMap);
		return res;
	}

	public Response initRegistrationData() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();

		/*
		 * lookupNoList.add(prefixLookupNo); lookupNoList.add(unitsLookupNo);
		 * lookupNoList.add(eligibilityLookupNo);
		 * lookupNoList.add(serviceCategoryLookupNo);
		 */
		lookupNoList.add(nationalityLookupNo);
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(maritalStatusLookupNo);
		lookupNoList.add(religionLookupNo);
		lookupNoList.add(medicalCategoryLookupNo);
		lookupNoList.add(districtLookupNo);
		/*
		 * lookupNoList.add(relationListNo); lookupNoList.add(jobTypeNo);
		 * lookupNoList.add(employeeTypeNo); lookupNoList.add(hrTypeNo);
		 * lookupNoList.add(upazilaLookupNo); lookupNoList.add(countryLookupNo);
		 * lookupNoList.add(reLookupNo); lookupNoList.add(familyLookupNo);
		 * 
		 */

		return findLookupDetials(lookupNoList, userDetail);
	}

	public Response findLookupDetials(List<Long> lookupNoList, UserSignInContainter userDetail) {

		Response response = new Response();
		List<LookupDetailEntity> lookupDetailEntityList = new ArrayList<LookupDetailEntity>();
		Map<String, Object> lookupDetailMap = new HashMap<String, Object>();

		LookupDetailEntity lookupDetailEntity = new LookupDetailEntity();
		lookupDetailEntity.setCompanyNo(userDetail.getCompanyNo());
		lookupDetailEntity.setOrganizationNo(userDetail.getOrganizationNo());
		lookupDetailEntity.setLookupNoList(lookupNoList);
//should open after add jobtype view and salutation view
//		Response jobtypeNoList = viewEmpGradesService.list();
//		Response salutationList = coreViewHpmsSalutationService.findList();

		response = baseList(criteriaQuery(lookupDetailEntity));

		if (response.isSuccess() && response.getItems() != null) {

			lookupDetailEntityList = getListFromObject(response.getItems(), LookupDetailEntity.class);

			Map<Long, List<LookupDetailEntity>> groupByLookupNo = lookupDetailEntityList.stream()
					.collect(Collectors.groupingBy(LookupDetailEntity::getLookupNo));

			for (Long lookupNo : lookupNoList) {

				/*
				 * if (prefixLookupNo == lookupNo.longValue()) {
				 * lookupDetailMap.put("prefixList", groupByLookupNo.get(lookupNo)); } if
				 * (unitsLookupNo == lookupNo.longValue()) { lookupDetailMap.put("unitsList",
				 * groupByLookupNo.get(lookupNo)); } if (eligibilityLookupNo ==
				 * lookupNo.longValue()) { lookupDetailMap.put("eligibilityList",
				 * groupByLookupNo.get(lookupNo)); } if (personCategoryLookupNo ==
				 * lookupNo.longValue()) { lookupDetailMap.put("personCategoryList",
				 * groupByLookupNo.get(lookupNo)); } if (rankLookupNo == lookupNo.longValue()) {
				 * lookupDetailMap.put("rankCategoryList", groupByLookupNo.get(lookupNo)); } if
				 * (corpsLookupNo == lookupNo.longValue()) {
				 * lookupDetailMap.put("corpsCategoryList", groupByLookupNo.get(lookupNo)); }
				 */
				if (nationalityLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("nationalityList", groupByLookupNo.get(lookupNo));
				}
				if (bloodGroupLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("bloodGroupList", groupByLookupNo.get(lookupNo));
				}
				if (maritalStatusLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("maritalStatusList", groupByLookupNo.get(lookupNo));
				}
				if (religionLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("religionList", groupByLookupNo.get(lookupNo));

				}
				if (medicalCategoryLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("medicalCategoryList", groupByLookupNo.get(lookupNo));
				}
				if (districtLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("districtList", groupByLookupNo.get(lookupNo));
				}
				if (thanaLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("thanaList", groupByLookupNo.get(lookupNo));
				}
				if (relationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("relationList", groupByLookupNo.get(lookupNo));
				}
				/*
				 * if (serviceCategoryLookupNo == lookupNo.longValue()) {
				 * lookupDetailMap.put("serviceCategoryList", groupByLookupNo.get(lookupNo)); }
				 * if (priorityLookupNo == lookupNo.longValue()) {
				 * lookupDetailMap.put("priorityList", groupByLookupNo.get(lookupNo)); } if
				 * (jobTypeNo == lookupNo.longValue()) { lookupDetailMap.put("jobTypeList",
				 * groupByLookupNo.get(lookupNo)); }
				 */

//				if (jobtypeNoList.isSuccess() && jobtypeNoList.getItems() != null) {
//					lookupDetailMap.put("jobTypeList", jobtypeNoList.getItems());
//				}

				/*
				 * if (employeeTypeNo == lookupNo.longValue()) {
				 * lookupDetailMap.put("employeeTypeList", groupByLookupNo.get(lookupNo)); } if
				 * (hrTypeNo == lookupNo.longValue()) { lookupDetailMap.put("hrTypeList",
				 * groupByLookupNo.get(lookupNo)); } if (upazilaLookupNo ==
				 * lookupNo.longValue()) { lookupDetailMap.put("upazilaList",
				 * groupByLookupNo.get(lookupNo)); }
				 * 
				 */
				if (countryLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("countryList", groupByLookupNo.get(lookupNo));
				}
				if (genderLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("genderList", groupByLookupNo.get(lookupNo));
				}

				if (salutationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("salutationList", groupByLookupNo.get(lookupNo));
				}

//				if (salutationList.isSuccess() && salutationList.getItems() != null) {
//					lookupDetailMap.put("salutationList", salutationList.getItems());
//				}
				if (patientCondition == lookupNo.longValue()) {
					lookupDetailMap.put("patientCondition", groupByLookupNo.get(lookupNo));
				}
				if (arrivalCondition == lookupNo.longValue()) {
					lookupDetailMap.put("arrivalCondition", groupByLookupNo.get(lookupNo));
				}
				if (accompainedBy == lookupNo.longValue()) {
					lookupDetailMap.put("accompainedBy", groupByLookupNo.get(lookupNo));
				}
				if (specializationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("specializationList", groupByLookupNo.get(lookupNo));
				}

			}
		}
		Response response1 = new Response();
		response1.setObj(lookupDetailMap);

		return response1;
	}

	public Response findLookupDetials(List<Long> lookupNoList, Long companyNo, UserSignInContainter userDetail) {

		Response response = new Response();
		List<LookupDetailEntity> lookupDetailEntityList = new ArrayList<LookupDetailEntity>();
		Map<String, Object> lookupDetailMap = new HashMap<String, Object>();

		LookupDetailEntity lookupDetailEntity = new LookupDetailEntity();
		lookupDetailEntity.setCompanyNo(companyNo);
		lookupDetailEntity.setLookupNoList(lookupNoList);

		response = baseList(criteriaQuery(lookupDetailEntity));

		if (response.isSuccess() && response.getItems() != null) {

			lookupDetailEntityList = getListFromObject(response.getItems(), LookupDetailEntity.class);

			Map<Long, List<LookupDetailEntity>> groupByLookupNo = lookupDetailEntityList.stream()
					.collect(Collectors.groupingBy(LookupDetailEntity::getLookupNo));

			for (Long lookupNo : lookupNoList) {

				if (nationalityLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("nationalityList", groupByLookupNo.get(lookupNo));
				}
				if (bloodGroupLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("bloodGroupList", groupByLookupNo.get(lookupNo));
				}
				if (maritalStatusLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("maritalStatusList", groupByLookupNo.get(lookupNo));
				}
				if (religionLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("religionList", groupByLookupNo.get(lookupNo));

				}
				if (medicalCategoryLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("medicalCategoryList", groupByLookupNo.get(lookupNo));
				}
				if (districtLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("districtList", groupByLookupNo.get(lookupNo));
				}
				if (thanaLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("thanaList", groupByLookupNo.get(lookupNo));
				}
				if (relationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("relationList", groupByLookupNo.get(lookupNo));
				}
				if (countryLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("countryList", groupByLookupNo.get(lookupNo));
				}
				if (genderLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("genderList", groupByLookupNo.get(lookupNo));
				}
				if (salutationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("salutationList", groupByLookupNo.get(lookupNo));
				}
				if (patientCondition == lookupNo.longValue()) {
					lookupDetailMap.put("patientCondition", groupByLookupNo.get(lookupNo));
				}
				if (arrivalCondition == lookupNo.longValue()) {
					lookupDetailMap.put("arrivalCondition", groupByLookupNo.get(lookupNo));
				}
				if (accompainedBy == lookupNo.longValue()) {
					lookupDetailMap.put("accompainedBy", groupByLookupNo.get(lookupNo));
				}
				if (specializationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("specializationList", groupByLookupNo.get(lookupNo));
				}
				if (divisionLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("divisionList", groupByLookupNo.get(lookupNo));
				}
			}
		}
		Response response1 = new Response();
		response1.setObj(lookupDetailMap);

		return response1;
	}

	public Response findLookupDetialsWithParentName(List<Long> lookupNoList) {

		Response response = new Response();
		List<LookupDetailEntity> lookupDetailEntityList = new ArrayList<LookupDetailEntity>();
		List<LookupDetailEntity> lookupDetailEntityFilterList = new ArrayList<LookupDetailEntity>();
		List<LookupDetailEntity> lookupDetailEntityParentList = new ArrayList<LookupDetailEntity>();
		Map<String, Object> lookupDetailMap = new HashMap<String, Object>();

		LookupDetailEntity lookupDetailEntity = new LookupDetailEntity();
		lookupDetailEntity.setLookupNoList(lookupNoList);
		response = baseList(criteriaQuery(lookupDetailEntity));

		if (response.isSuccess() && response.getItems() != null) {

			lookupDetailEntityList = getListFromObject(response.getItems(), LookupDetailEntity.class);

			List<LookupDetailEntity> lookupDtlParent = StreamEx.of(lookupDetailEntityList)
					.distinct(LookupDetailEntity::getLookupdtlNoParent).toList();

			List<Long> parentNoList = lookupDtlParent.stream().map(LookupDetailEntity::getLookupdtlNoParent)
					.collect(Collectors.toList());
			parentNoList.removeIf(Objects::isNull);

			if (parentNoList != null && parentNoList.size() > 0) {

				Response response1 = new Response();

				LookupDetailEntity lookupDetailEntityP = new LookupDetailEntity();
				lookupDetailEntityP.setLookupDltNoList(parentNoList);
				response1 = baseList(criteriaQuery(lookupDetailEntityP));

				if (response1.isSuccess() && response1.getItems() != null) {

					lookupDetailEntityParentList = getListFromObject(response1.getItems(), LookupDetailEntity.class);

				}
			}

			for (LookupDetailEntity lookdtlEntity : lookupDetailEntityList) {

				if (lookdtlEntity.getLookupdtlNoParent() != null) {

					if (lookupDetailEntityParentList != null) {

						LookupDetailEntity plookdtlEntity = lookupDetailEntityParentList.stream()
								.filter(lookupdtl -> lookupdtl.getId().longValue() == lookdtlEntity
										.getLookupdtlNoParent().longValue())
								.findAny().orElse(null);

						if (plookdtlEntity != null) {
							lookdtlEntity.setLookupdtlNoParentName(plookdtlEntity.getDtlName());
						}
					}

				}
				lookupDetailEntityFilterList.add(lookdtlEntity);
			}

			Map<Long, List<LookupDetailEntity>> groupByLookupNo = lookupDetailEntityFilterList.stream()
					.collect(Collectors.groupingBy(LookupDetailEntity::getLookupNo));

			for (Long lookupNo : lookupNoList) {

				if (nationalityLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("nationalityList", groupByLookupNo.get(lookupNo));
				}
				if (bloodGroupLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("bloodGroupList", groupByLookupNo.get(lookupNo));
				}
				if (maritalStatusLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("maritalStatusList", groupByLookupNo.get(lookupNo));
				}
				if (religionLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("religionList", groupByLookupNo.get(lookupNo));
				}
				if (medicalCategoryLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("medicalCategoryList", groupByLookupNo.get(lookupNo));
				}
				if (districtLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("districtList", groupByLookupNo.get(lookupNo));
				}
				if (relationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("relationList", groupByLookupNo.get(lookupNo));
				}

				if (priorityLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("priorityList", groupByLookupNo.get(lookupNo));
				}
				if (countryLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("countryList", groupByLookupNo.get(lookupNo));
				}
				if (genderLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("genderList", groupByLookupNo.get(lookupNo));
				}
				if (salutationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("salutationList", groupByLookupNo.get(lookupNo));
				}
				if (specializationLookupNo == lookupNo.longValue()) {
					lookupDetailMap.put("specializationList", groupByLookupNo.get(lookupNo));
				}

			}

		}

		Response response1 = new Response();
		response1.setObj(lookupDetailMap);

		return response1;
	}

	public Response initDataFmailyRegistration() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();
		lookupNoList.add(districtLookupNo);
		lookupNoList.add(bloodGroupLookupNo);

		return findLookupDetials(lookupNoList, userDetail);
	}

	public Response initDataPolicy() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();
//		lookupNoList.add(familyLookupNo);
//		lookupNoList.add(reLookupNo);
//		lookupNoList.add(cneLookupNo);

		return findLookupDetials(lookupNoList, userDetail);
	}

	public Response initDataPolicyBill() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();

//		lookupNoList.add(serviceCategoryLookupNo);
//		lookupNoList.add(eligibilityLookupNo);
//		lookupNoList.add(personCategoryLookupNo);

		Map<String, Object> lookupDetailMap = new HashMap<String, Object>();

		Response response = new Response();

		response = findLookupDetials(lookupNoList, userDetail);

		if (response.getObj() != null) {

			lookupDetailMap = (Map<String, Object>) response.getObj();

			lookupDetailMap.put("registrationTypeList", CoreRegistrationType.getRegTypeList());
		}

		response.setObj(lookupDetailMap);

		return response;
	}

	public Response initDataCneRegistration() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();

		// lookupNoList.add(cneLookupNo);
//		lookupNoList.add(reLookupNo); // re will be cne after service holder Retirement service holder
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(districtLookupNo);

		return findLookupDetials(lookupNoList, userDetail);

	}

	public Response initDataRegistrationFilter() {

		List<Long> lookupNoList = new ArrayList<>();

		// lookupNoList.add(cneLookupNo);
//		lookupNoList.add(prefixLookupNo);
//		lookupNoList.add(unitsLookupNo);
//		lookupNoList.add(eligibilityLookupNo);
//		lookupNoList.add(personCategoryLookupNo);
//		lookupNoList.add(serviceCategoryLookupNo);
//		lookupNoList.add(rankLookupNo);
//		lookupNoList.add(corpsLookupNo);

		return findLookupDetialsWithParentName(lookupNoList);
	}

	public Response initDataReRegistration() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();
//		lookupNoList.add(reLookupNo);
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(districtLookupNo);

		return findLookupDetials(lookupNoList, userDetail);
	}

	public Response initDataMiscelRegistration() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(districtLookupNo);

		return findLookupDetials(lookupNoList, userDetail);
	}

	public Response opdAppointmentLookupDetailList() {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();
//		lookupNoList.add(serviceCategoryLookupNo);
		lookupNoList.add(priorityLookupNo);

		return findLookupDetials(lookupNoList, userDetail);

	}

	public Response initEmployeeSetupList(UserSignInContainter userDetail) {

		List<Long> lookupNoList = new ArrayList<>();
		lookupNoList.add(genderLookupNo);
		lookupNoList.add(maritalStatusLookupNo);
		// lookupNoList.add(hrTypeNo);
		// lookupNoList.add(employeeTypeNo);
		// lookupNoList.add(jobTypeNo);
		lookupNoList.add(districtLookupNo);
		// lookupNoList.add(upazilaLookupNo);
		lookupNoList.add(countryLookupNo);
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(religionLookupNo);
		lookupNoList.add(salutationLookupNo);
		lookupNoList.add(relationLookupNo);
		lookupNoList.add(specializationLookupNo);
		lookupNoList.add(thanaLookupNo);

		return findLookupDetials(lookupNoList, userDetail);
	}

	public Response initEmployeeSetupList(Long companyNo, UserSignInContainter userDetail) {

		List<Long> lookupNoList = new ArrayList<>();
		lookupNoList.add(genderLookupNo);
		lookupNoList.add(maritalStatusLookupNo);
		lookupNoList.add(districtLookupNo);
		// lookupNoList.add(upazilaLookupNo);
		lookupNoList.add(countryLookupNo);
		lookupNoList.add(bloodGroupLookupNo);
		lookupNoList.add(religionLookupNo);
		lookupNoList.add(salutationLookupNo);
		lookupNoList.add(relationLookupNo);
		lookupNoList.add(specializationLookupNo);
		lookupNoList.add(thanaLookupNo);
		lookupNoList.add(divisionLookupNo);

		return findLookupDetials(lookupNoList, companyNo, userDetail);
	}

	@SuppressWarnings("unchecked")
	public Response getEmrInitData(String reqObj) {
		UserSignInContainter userDetail = null;
		List<Long> lookupNoList = new ArrayList<>();
		lookupNoList.add(genderLookupNo);
		lookupNoList.add(salutationLookupNo);
		lookupNoList.add(bloodGroupLookupNo);

		Map<String, Object> lookupDetailMap = new HashMap<String, Object>();

		Response response = findLookupDetials(lookupNoList, userDetail);
		if (response.getObj() != null) {
			lookupDetailMap.putAll((Map<? extends String, ? extends Object>) response.getObj());
		}

		Response resPatientType = commonService.getPatientTypeListByFlagColumn("EMR_FLAG");
		if (resPatientType.getItems() != null && resPatientType.getItems().size() > 0) {
			lookupDetailMap.put("patientTypeList", resPatientType.getItems());
		}
		response.setObj(null);
		response.setModel(lookupDetailMap);

		return response;
	}

	/*
	 * @SuppressWarnings({"unchecked" }) private Map<String, Object>
	 * lookupDetailMap(Map<Long, List<LookupDetailEntity>> groupByLookupNo, Long
	 * lookupNo,String keyName, Map<String, Object> lookupDetailMap) {
	 * 
	 * return (Map<String, Object>) lookupDetailMap.put(keyName,
	 * groupByLookupNo.get(lookupNo)); }
	 */

//	public Response lookupDetailListByLookupParentNumber(Long lookupParentNo){
//
//
//		List<Long> lookupNoList = new ArrayList<>();
//		lookupNoList.add(prefixLookupNo);
//		lookupNoList.add(unitsLookupNo);
//		lookupNoList.add(eligibilityLookupNo);
//		lookupNoList.add(personCategoryLookupNo);
//		lookupNoList.add(rankLookupNo);
//		lookupNoList.add(corpsLookupNo);
//		
//		return findLookupDetials(lookupNoList);
//
//
//	}

	public Response lookupDetailListByLookupParentNumber(Long lookupParentNo) {

		Response lookupParentListResponse = new Response();
		List<Long> lookupNoList = new ArrayList<>();
//		lookupNoList.add(prefixLookupNo);
//		lookupNoList.add(unitsLookupNo);
//		lookupNoList.add(eligibilityLookupNo);
//		lookupNoList.add(personCategoryLookupNo);
//		lookupNoList.add(rankLookupNo);
//		lookupNoList.add(corpsLookupNo);

		Map<String, Object> lookupDetailMap = new HashMap<String, Object>();

		LookupDetailEntity saLookupDtlEntity = new LookupDetailEntity();
		saLookupDtlEntity.setLookupdtlNoParent(lookupParentNo);

		lookupParentListResponse = baseList(criteriaQuery(saLookupDtlEntity));

		if (lookupParentListResponse.isSuccess() && lookupParentListResponse.getItems() != null) {
			List<LookupDetailEntity> lookupDetailList = getListFromObject(lookupParentListResponse.getItems(),
					LookupDetailEntity.class);

			lookupDetailList.stream()
					.collect(Collectors.groupingBy(LookupDetailEntity::getLookupNo));

			for (Long lookupNo : lookupNoList) {
//				if (prefixLookupNo == lookupNo.longValue()) {
//					lookupDetailMap.put("prefixList", lookupDetailListGroupByLookupNo.get(lookupNo));
//				}
//				if (unitsLookupNo == lookupNo.longValue()) {
//					lookupDetailMap.put("unitsList", lookupDetailListGroupByLookupNo.get(lookupNo));
//				}
//				if (eligibilityLookupNo == lookupNo.longValue()) {
//					lookupDetailMap.put("eligibilityList", lookupDetailListGroupByLookupNo.get(lookupNo));
//				}
//				if (personCategoryLookupNo == lookupNo.longValue()) {
//					lookupDetailMap.put("personCategoryList", lookupDetailListGroupByLookupNo.get(lookupNo));
//				}
//				if (rankLookupNo == lookupNo.longValue()) {
//					lookupDetailMap.put("rankCategoryList", lookupDetailListGroupByLookupNo.get(lookupNo));
//				}
//				if (corpsLookupNo == lookupNo.longValue()) {
//					lookupDetailMap.put("corpsCategoryList", lookupDetailListGroupByLookupNo.get(lookupNo));
//				}
			}
		}

		Response response = new Response();
		response.setObj(lookupDetailMap);
		return response;
	}

	public Response listBylookupNo(Long lookupNo) {
		LookupDetailEntity saLookupDtlEntity = new LookupDetailEntity();
		saLookupDtlEntity.setLookupNo(lookupNo);
		return baseList(criteriaQuery(saLookupDtlEntity));
	}

	public Response listBylookupNoOgNoCompanyNo(Long lookupNo, Long ogNo, Long companyNo) {
		LookupDetailEntity saLookupDtlEntity = new LookupDetailEntity();
		saLookupDtlEntity.setLookupNo(lookupNo);
		saLookupDtlEntity.setOrganizationNo(ogNo);
		saLookupDtlEntity.setCompanyNo(companyNo);
		return baseList(criteriaQuery(saLookupDtlEntity));
	}

	public Response genderList() {

		return listBylookupNo(5007l);
	}

	public Response bloodGroups() {
		return listBylookupNo(1004l);
	}

	public Response salutationList() {
		return listBylookupNo(1005l);
	}

	public Response groupNameList() {
		return listBylookupNo(5030l);
	}

	public Response specializationList(Long ogNo, Long companyNo) {
		return listBylookupNoOgNoCompanyNo(specializationLookupNo, ogNo, companyNo);
	}
	
	public Response getSalutationList(Long ogNo, Long companyNo) {
		return listBylookupNoOgNoCompanyNo(salutationLookupNo, ogNo, companyNo);
	}

	public Response rankList() {
		Response response = new Response();
		List<Long> rank = new ArrayList<Long>();
//		rank.add(rankLookupNo);
		response = findLookupDetialsWithParentName(rank);
		return response;
	}

	public Response save(String reqObj, UserSignInContainter userDetails) {
		LookupDetailEntity saLookupDtlEntity = objectMapperReadValue(reqObj, LookupDetailEntity.class);

		saLookupDtlEntity.setCompanyNo(userDetails.getCompanyNo());
		saLookupDtlEntity.setSsCreator(userDetails.getUserId());
		saLookupDtlEntity.setSsCreatedOn(new Date());
		saLookupDtlEntity.setSsCreateSession(userDetails.getSessionNo());
		saLookupDtlEntity.setSsModifiedOn(new Date());
		saLookupDtlEntity.setSsModifier(userDetails.getUserId());
		saLookupDtlEntity.setSsModifiedSession(userDetails.getSessionNo());
		return baseOnlySave(saLookupDtlEntity);
	}
	
	public Response update(String reqObj, UserSignInContainter userDetails) {

		LookupDetailEntity saLookupDtlEntity = objectMapperReadValue(reqObj, LookupDetailEntity.class);
		LookupDetailEntity obj = findById(saLookupDtlEntity.getId());
		if (obj != null) {

			obj.setLookupdtlNoParent(saLookupDtlEntity.getLookupdtlNoParent());

			obj.setDtlName(saLookupDtlEntity.getDtlName());
			if (saLookupDtlEntity.getSlNo() != null) {
				obj.setSlNo(saLookupDtlEntity.getSlNo());
			}
			if (saLookupDtlEntity.getDtlDescription() != null) {
				obj.setDtlDescription(saLookupDtlEntity.getDtlDescription());
			}

			obj.setSsModifiedOn(new Date());
			obj.setSsModifier(userDetails.getUserId());
			obj.setSsModifiedSession(userDetails.getSessionNo());

			return baseUpdate(obj);
		}

		return getErrorResponse("Record not Found !!");

	}

	public Response saveSalutation(String reqObj, UserSignInContainter userDetails) {
		LookupDetailEntity saLookupDtlEntity = objectMapperReadValue(reqObj, LookupDetailEntity.class);
		
		Response resDupCheck = duplicateCheckJobTitile(saLookupDtlEntity);
		if (!resDupCheck.isSuccess()) {
			return getErrorResponse("Salutaion Duplicate Not Allowed!!");
		}

		saLookupDtlEntity.setSsCreator(userDetails.getUserId());
		saLookupDtlEntity.setSsCreatedOn(new Date());
		saLookupDtlEntity.setSsCreateSession(userDetails.getSessionNo());
		saLookupDtlEntity.setSsModifiedOn(new Date());
		saLookupDtlEntity.setSsModifier(userDetails.getUserId());
		saLookupDtlEntity.setSsModifiedSession(userDetails.getSessionNo());
		
		Response response = baseOnlySave(saLookupDtlEntity);
		return getSuccessResponse("Salutation Save Successful.", response);
	}

	public Response updateSalutation(String reqObj, UserSignInContainter userDetails) {
		LookupDetailEntity saLookupDtlEntity = objectMapperReadValue(reqObj, LookupDetailEntity.class);
		
		Response resDupCheck = duplicateCheckJobTitile(saLookupDtlEntity);
		if (!resDupCheck.isSuccess()) {
			return getErrorResponse("Salutaion Duplicate Not Allowed!!");
		}
		LookupDetailEntity foundSaLookupDtl = findById(saLookupDtlEntity.getId());
		if(foundSaLookupDtl != null) {
			saLookupDtlEntity.setSsModifiedOn(new Date());
			saLookupDtlEntity.setSsModifier(userDetails.getUserId());
			saLookupDtlEntity.setSsModifiedSession(userDetails.getSessionNo());
			
			Response response = baseUpdate(saLookupDtlEntity);
			return getSuccessResponse("Salutation Update Successfully.", response);
		}
		return getErrorResponse("Salutation Not Found!!");
	}
	
	public Response saveSpecilization(String reqObj, UserSignInContainter userDetails) {
		LookupDetailEntity saLookupDtlEntity = objectMapperReadValue(reqObj, LookupDetailEntity.class);
		
		Response resDupCheck = duplicateCheckJobTitile(saLookupDtlEntity);
		if (!resDupCheck.isSuccess()) {
			return getErrorResponse("Specilization Duplicate Not Allowed!!");
		}
		
		saLookupDtlEntity.setSsCreator(userDetails.getUserId());
		saLookupDtlEntity.setSsCreatedOn(new Date());
		saLookupDtlEntity.setSsCreateSession(userDetails.getSessionNo());
		saLookupDtlEntity.setSsModifiedOn(new Date());
		saLookupDtlEntity.setSsModifier(userDetails.getUserId());
		saLookupDtlEntity.setSsModifiedSession(userDetails.getSessionNo());
		
		Response response = baseOnlySave(saLookupDtlEntity);
		return getSuccessResponse("Specilization Save Successful.", response);
	}
	
	public Response updateSpecilization(String reqObj, UserSignInContainter userDetails) {
		LookupDetailEntity saLookupDtlEntity = objectMapperReadValue(reqObj, LookupDetailEntity.class);
		
		Response resDupCheck = duplicateCheckJobTitile(saLookupDtlEntity);
		if (!resDupCheck.isSuccess()) {
			return getErrorResponse("Specilization Duplicate Not Allowed!!");
		}
		LookupDetailEntity foundSaLookupDtl = findById(saLookupDtlEntity.getId());
		if(foundSaLookupDtl != null) {
			saLookupDtlEntity.setSsModifiedOn(new Date());
			saLookupDtlEntity.setSsModifier(userDetails.getUserId());
			saLookupDtlEntity.setSsModifiedSession(userDetails.getSessionNo());
			
			Response response = baseUpdate(saLookupDtlEntity);
			return getSuccessResponse("Specilization Update Successfully.", response);
		}
		return getErrorResponse("Specilization Not Found!!");
	}

	public Response detele(Long id) {

		LookupDetailEntity saLookupDtlEntity = findById(id);
		if (saLookupDtlEntity == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(saLookupDtlEntity);

	}

	public Response remove(Long id) {
		LookupDetailEntity saLookupDtlEntity = findById(id);
		if (saLookupDtlEntity == null) {
			return getErrorResponse("Record not found!");
		}
		saLookupDtlEntity.setActiveStatus(3);
		return baseRemove(saLookupDtlEntity);
	}

	public LookupDetailEntity findById(Long id) {
		LookupDetailEntity saLookupDtlEntity = new LookupDetailEntity();
		saLookupDtlEntity.setId(id);
		saLookupDtlEntity.setActiveStatus(0);
		Response response = baseFindById(criteriaQuery(saLookupDtlEntity));
		if (response.isSuccess()) {

			return getValueFromObject(response.getObj(), LookupDetailEntity.class);
		}
		return null;
	}
	
// 	------------Start duplicate check-------------------
	public Response duplicateCheckJobTitile(LookupDetailEntity reqobj) {

		LookupDetailEntity lookupDetailEntity = new LookupDetailEntity();

		if (reqobj.getOrganizationNo() != null && reqobj.getOrganizationNo() > 0) {
			lookupDetailEntity.setOrganizationNo(reqobj.getOrganizationNo());
		}
		if (reqobj.getCompanyNo() != null && reqobj.getCompanyNo() > 0) {
			lookupDetailEntity.setCompanyNo(reqobj.getCompanyNo());
		}
		if (reqobj.getId() != null && reqobj.getId() > 0) {
			lookupDetailEntity.setId(null); 
			lookupDetailEntity.setNotEqualId(reqobj.getId());
		}
		if (!StringUtils.isEmpty(reqobj.getDtlName())) {
			lookupDetailEntity.setDtlName(reqobj.getDtlName());
		}

		Long totalCount = totalCountWithDConjunction(lookupDetailEntity);

		if (totalCount.longValue() > 0) {
			return getErrorResponse("Duplicate Not Allowed!!");
		} else {
			return getSuccessResponse("Validated");
		}

	}

	private Long totalCountWithDConjunction(LookupDetailEntity filter) {

		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<LookupDetailEntity> root = from(LookupDetailEntity.class, criteriaQuery);

		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));
	}
// ----------- End duplicate check ------------------------
	

	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(LookupDetailEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		
		List<Order> orderList = new ArrayList();
		
		orderList.add(builder.asc(root.get("slNo")));
		
		criteria.orderBy(orderList);
		
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(LookupDetailEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ, LookupDetailEntity.class);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		if (dataTableInRQ.getOrder().getName() != null && !dataTableInRQ.getOrder().getName().isEmpty()) {
			if (dataTableInRQ.getOrder().getSortDir().equals("ASC")) {
				criteria.orderBy(builder.asc(root.get(dataTableInRQ.getOrder().getName())));
			} else {
				criteria.orderBy(builder.desc(root.get(dataTableInRQ.getOrder().getName())));
			}

		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(LookupDetailEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<LookupDetailEntity> root = from(LookupDetailEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(LookupDetailEntity filter, CriteriaBuilder builder,
			Root<LookupDetailEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			if (filter.getActiveStatus() > 0) {
				Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
				p.add(condition);
			}
			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
			if (filter.getLookupNo() != null && filter.getLookupNo() > 0) {
				Predicate condition = builder.equal(root.get("lookupNo"), filter.getLookupNo());
				p.add(condition);
			}

			if (filter.getLookupDltNoList() != null && filter.getLookupDltNoList().size() > 0) {
				Expression<Long> exp = root.get("id");
				Predicate condition = exp.in(filter.getLookupDltNoList());
				p.add(condition);
			}

			if (filter.getLookupdtlNoParent() != null && filter.getLookupdtlNoParent() > 0) {
				Predicate condition = builder.equal(root.get("lookupdtlNoParent"), filter.getLookupdtlNoParent());
				p.add(condition);
			}

			if (filter.getLookupNoList() != null && filter.getLookupNoList().size() > 0) {
				Expression<Long> exp = root.get("lookupNo");
				Predicate condition = exp.in(filter.getLookupNoList());
				p.add(condition);
			}
			if (filter.getLookupdtlNoParent() != null && filter.getLookupdtlNoParent() > 0) {
				Expression<Long> exp = root.get("lookupdtlNoParent");
				Predicate condition = exp.in(filter.getLookupdtlNoParent());
				p.add(condition);
			}
			if (filter.getOrganizationNo() != null && filter.getOrganizationNo() > 0) {
				Predicate condition = builder.equal(root.get("organizationNo"), filter.getOrganizationNo());
				p.add(condition);
			}
			if (filter.getCompanyNo() != null && filter.getCompanyNo() > 0) {
				Predicate condition = builder.equal(root.get("companyNo"), filter.getCompanyNo());
				p.add(condition);
			}
			if (filter.getDtlName() != null) {
				p.add(builder.equal(root.get("dtlName"), filter.getDtlName()));
			}
			if (filter.getNotEqualId() != null) {
				p.add(builder.notEqual(root.get("id"), filter.getNotEqualId()));
			}
		}

		return p;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(LookupDetailEntity.class);
	}
}
