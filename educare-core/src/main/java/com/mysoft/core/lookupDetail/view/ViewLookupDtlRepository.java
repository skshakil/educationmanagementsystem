package com.mysoft.core.lookupDetail.view;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Response;

@Repository	
@Transactional
public class ViewLookupDtlRepository extends BaseRepository  {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request, Long lookupNo) {

		String ogNo = request.getParameter("ogNo");
		String companyNo = request.getParameter("companyNo");

		DataTableResults<ViewLookupDtlEntity> dataTableResults = null;
		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Response response = new Response();
		
		ViewLookupDtlEntity viewDesignationEntity = new ViewLookupDtlEntity();
		if (!StringUtils.isEmpty(ogNo)) {
			viewDesignationEntity.setOrganizationNo(Long.parseLong(ogNo));
		}
		if (lookupNo != null) {
			viewDesignationEntity.setLookupNo(lookupNo);
		}
		if (!StringUtils.isEmpty(companyNo)) {
			viewDesignationEntity.setCompanyNo(Long.parseLong(companyNo));
//			setPramProcedureSearchNumber("k_parameter.pd_set_company_no",Long.parseLong(companyNo));
		}

		Long totalRowCount = totalCount(viewDesignationEntity);
		List gridList = new ArrayList<>();
		response = baseList(typedQuery(viewDesignationEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}
	
	public Response salutationGridList(HttpServletRequest request) {
		return gridList(request, salutationLookupNo);
	}

	public Response specilizationGridList(HttpServletRequest request) {
		return gridList(request, specializationLookupNo);
	}

	public ViewLookupDtlEntity findById(Long id) {
		ViewLookupDtlEntity viewDesignationEntity = new ViewLookupDtlEntity();
		viewDesignationEntity.setId(id);
		Response response = baseFindById(criteriaQuery(viewDesignationEntity));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), ViewLookupDtlEntity.class);
		}
		return null;
	}

	// ------------ Non API-------------------
	private Long totalCount(ViewLookupDtlEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<ViewLookupDtlEntity> root = from(ViewLookupDtlEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(ViewLookupDtlEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		List<Order> orderList = new ArrayList();
		
		orderList.add(builder.asc(root.get("slNo")));
		
		criteria.orderBy(orderList);
		
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(ViewLookupDtlEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ, ViewLookupDtlEntity.class);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}
		if (dataTableInRQ.getOrder().getName() != null && !dataTableInRQ.getOrder().getName().isEmpty()) {
			if (dataTableInRQ.getOrder().getSortDir().equals("ASC")) {
				criteria.orderBy(builder.asc(root.get(dataTableInRQ.getOrder().getName())));
			} else {
				criteria.orderBy(builder.desc(root.get(dataTableInRQ.getOrder().getName())));
			}
		}
		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(ViewLookupDtlEntity filter, CriteriaBuilder builder,
			Root<ViewLookupDtlEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {

			if (filter.getId() != null) {
				p.add(builder.equal(root.get("id"), filter.getId()));
			}
			if (filter.getLookupNo() != null) {
				p.add(builder.equal(root.get("lookupNo"), filter.getLookupNo()));
			}
			if (filter.getActiveStatus() != null) {
				p.add(builder.equal(root.get("activeStatus"), filter.getActiveStatus()));
			}
			if (filter.getCompanyNo() != null) {
				p.add(builder.equal(root.get("companyNo"), filter.getCompanyNo()));
			}
			if (filter.getOrganizationNo() != null) {
				p.add(builder.equal(root.get("organizationNo"), filter.getOrganizationNo()));
			}
		}
		return p;
	}

	@SuppressWarnings("rawtypes")
	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(ViewLookupDtlEntity.class);
		@SuppressWarnings("unused")
		CriteriaBuilder builder = super.builder;
		@SuppressWarnings("unused")
		CriteriaQuery criteria = super.criteria;
		@SuppressWarnings("unused")
		Root root = super.root;
	}

}
