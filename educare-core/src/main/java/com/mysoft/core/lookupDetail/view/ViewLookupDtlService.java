package com.mysoft.core.lookupDetail.view;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;

@Service
public class ViewLookupDtlService {

	@Autowired
	ViewLookupDtlRepository viewLookupDtlRepository;

	public Response salutationGridList(HttpServletRequest request) {
		return viewLookupDtlRepository.salutationGridList(request);
	}
	
	public Response specilizationGridList(HttpServletRequest request) {
		return viewLookupDtlRepository.specilizationGridList(request);
	}
	
	public ViewLookupDtlEntity findById(Long id) {
		return viewLookupDtlRepository.findById(id);
	}
}
