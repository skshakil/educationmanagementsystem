package com.mysoft.core.lookupDetail.view;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.Immutable;

import com.mysoft.core.base.BaseOraEntity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Immutable
@Table(name = "SA_LOOKUPDTL_V")
public class ViewLookupDtlEntity extends BaseOraEntity{		
	
	@Id
	@Column(name = "LOOKUPDTL_NO")
	private Long id;

	@Column(name = "LOOKUP_NO")
	private Long lookupNo;
	
	@Column(name = "LOOKUP_NAME")
	private String lookupName;

	@Column(name = "SL_NO")
	private Integer slNo;
	  
	@Column(name = "LOOKDTL_NAME")
	private String dtlName;
	
	@Column(name = "DESCRIPTION")
	private String dtlDescription;
	         
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus;

	@Column(name = "LOOKDTL_NAME_MAP")
	private String lookdtlNameMap;
	
	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = "OG_NAME")
	private String organizationName;

}
