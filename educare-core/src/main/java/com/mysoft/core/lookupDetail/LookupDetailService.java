package com.mysoft.core.lookupDetail;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Service
public class LookupDetailService {

	@Autowired
	private LookupDetailRepository lookupDetailRepository;

	public Response gridList(HttpServletRequest request) {
		return lookupDetailRepository.gridList(request);
	}

	public Response salutationGridList(HttpServletRequest request) {
		return lookupDetailRepository.salutationGridList(request);
	}
	
	public Response saveSalutation(String reqObj, UserSignInContainter user) {
		return lookupDetailRepository.saveSalutation(reqObj, user);
	}
	
	public Response updateSalutation(String reqObj, UserSignInContainter user) {
		return lookupDetailRepository.updateSalutation(reqObj, user);
	}

	public Response specilizationGridList(HttpServletRequest request) {
		return lookupDetailRepository.specilizationGridList(request);
	}
	
	public Response saveSpecilization(String reqObj, UserSignInContainter user) {
		return lookupDetailRepository.saveSpecilization(reqObj, user);
	}
	
	public Response updateSpecilization(String reqObj, UserSignInContainter user) {
		return lookupDetailRepository.updateSpecilization(reqObj, user);
	}
	
//	public Response rankGridList(HttpServletRequest request) {
//		return lookupDetailRepository.rankGridList(request);
//	}
//	
//	public Response personalCategoryGridList(HttpServletRequest request) {
//		return lookupDetailRepository.personalCategoryGridList(request);
//	}
//	
//	public Response serviceCategoryGridList(HttpServletRequest request) {
//		return lookupDetailRepository.serviceCategoryGridList(request);
//	}
//	
//	public Response unitGridList(HttpServletRequest request) {
//		return lookupDetailRepository.unitGridList(request);
//	}
//	
//	public Response corpGridList(HttpServletRequest request) {
//		return lookupDetailRepository.corpGridList(request);
//	}
//	
//	public Response eligibilityStatusGridList(HttpServletRequest request) {
//		return lookupDetailRepository.eligibilityStatusGridList(request);
//	}
	
	public Response listBylookupNo(Long lookupNo) {
		return lookupDetailRepository.listBylookupNo(lookupNo);
	}

	public Response list(String reqObj) {
		return lookupDetailRepository.list(reqObj);
	}

	public Response rankList() {
		return lookupDetailRepository.rankList();
	}
	
	
	public Response lookupDetailList() {
		return lookupDetailRepository.lookupDetailList();
	}
	
	public Response opdAppointmentLookupDetailList() {
		return lookupDetailRepository.opdAppointmentLookupDetailList();
	}
	
	public Response lookupDetailListByLookupParentNo(Long lookupParentNo) {
		return lookupDetailRepository.lookupDetailListByLookupParentNumber(lookupParentNo);
	}

	public Response findById(long id) {
		Response res = new Response();
		LookupDetailEntity obj = new LookupDetailEntity();
		obj = lookupDetailRepository.findById(id);
		res.setObj(obj);
		return res;
	}

	public Response save(String reqObj, UserSignInContainter userDetails) {
		return lookupDetailRepository.save(reqObj, userDetails);
	}

	public Response update(String reqObj, UserSignInContainter userDetails) {
		return lookupDetailRepository.update(reqObj, userDetails);
	}

	public Response delete(Long id) {
		return lookupDetailRepository.detele(id);
	}

	public Response remove(Long id) {
		return lookupDetailRepository.remove(id);
	}

	public Response genderList() {
		return lookupDetailRepository.genderList();
	}

	public Response bloodGroups() {
		return lookupDetailRepository.bloodGroups();
	}

	public Response salutationList() {
		return lookupDetailRepository.salutationList();
	}
	
	public Response findLookupDetials(List<Long> lookupNoList,UserSignInContainter userDetail) {
		
		return lookupDetailRepository.findLookupDetials(lookupNoList,userDetail);
	}
	
	public Response initRegistrationData() {
		return lookupDetailRepository.initRegistrationData();
	}
	
   public Response initDataFmailyRegistration() {
		
		return lookupDetailRepository.initDataFmailyRegistration();
	}
   
   public Response initDataCneRegistration() {
		
 		return lookupDetailRepository.initDataCneRegistration();
 	}
	
   public Response initDataReRegistration() {
	   
	   return lookupDetailRepository.initDataReRegistration();
   }
   public Response initDataMiscelRegistration() {

	   return lookupDetailRepository.initDataMiscelRegistration();
   }

	public Response initEmployeeSetup(UserSignInContainter userDetail) {
		return lookupDetailRepository.initEmployeeSetupList(userDetail);
	}
	public Response initEmployeeSetup(Long companyNo, UserSignInContainter userDetail) {
		return lookupDetailRepository.initEmployeeSetupList(companyNo,userDetail);
	}

   public Response initDataPolicyBill() {
		
		return lookupDetailRepository.initDataPolicyBill();
	}

   
   public Response initDataRegistrationFilter() {
		
 		return lookupDetailRepository.initDataRegistrationFilter();
 	}

   public Response initDataPolicy() {
		return lookupDetailRepository.initDataPolicy();
	}

	public Response ipdInitLists() {
		return lookupDetailRepository.ipdInitList();
	}
	
	public Response groupNameList() {
		return lookupDetailRepository.groupNameList();
	}
	
	public Response getEmrInitData(String reqObj) {
		return lookupDetailRepository.getEmrInitData(reqObj);
    }
	
	public Response specializationList(Long ogNo, Long companyNo) {
		return lookupDetailRepository.specializationList(ogNo, companyNo);
	}
	
	public Response getSalutationList(String reqObj) {
		JSONObject json = new JSONObject(reqObj);
		Long ogNo = Def.getLong(json, "ogNo");
		Long companyNo = Def.getLong(json, "companyNo");
		return lookupDetailRepository.getSalutationList(ogNo, companyNo);
	}
}
