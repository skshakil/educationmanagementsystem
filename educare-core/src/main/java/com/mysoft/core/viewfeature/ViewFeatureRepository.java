package com.mysoft.core.viewfeature;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.util.Response;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;



/**
 * @author Md. Jahurul Islam
 *
 */
@Repository
@Transactional
public class ViewFeatureRepository extends BaseRepository {

	public Response save(String reqObj) {
		ViewFeatureEntity fatureEntity = objectMapperReadValue(reqObj, ViewFeatureEntity.class);
		return baseOnlySave(fatureEntity);
	}

	public Response list(String reqObj) {
		ViewFeatureEntity fatureEntity = objectMapperReadValue(reqObj, ViewFeatureEntity.class);
		return baseList(criteriaQuery(fatureEntity));
	}
	
	public List<ViewFeatureEntity> findByUserId(Long userId) {
		
		List<ViewFeatureEntity>  featureEntityList = null;
		
		ViewFeatureEntity fatureEntity = new ViewFeatureEntity();
		fatureEntity.setUserNo(userId);
		Response response = baseList(criteriaQuery(fatureEntity));
		
		if(response.isSuccess() && response.getItems() !=null) {
			featureEntityList = getListFromObject(response.getItems(), ViewFeatureEntity.class);
		}
		
		return featureEntityList;
	}

	public Response update(String reqObj) {
		ViewFeatureEntity FeatureEntity = objectMapperReadValue(reqObj, ViewFeatureEntity.class);
		return baseSaveOrUpdate(FeatureEntity);
	}

	public ViewFeatureEntity findById(Long id) {
		ViewFeatureEntity FeatureEntity = new ViewFeatureEntity();
		FeatureEntity.setId(id);
		Response response = baseFindById(criteriaQuery(FeatureEntity));

		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), ViewFeatureEntity.class);
		}
		return null;
	}

	public Response delete(Long id) {
		ViewFeatureEntity FeatureEntity = findById(id);
		return baseDelete(FeatureEntity);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(ViewFeatureEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "unchecked" })
    private List<Predicate> criteriaCondition(ViewFeatureEntity filter, CriteriaBuilder builder, Root<ViewFeatureEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			if (filter.getHideFlag()!=null && filter.getHideFlag()) {
				Predicate condition = builder.equal(root.get("hideFlag"), filter.getHideFlag());
				p.add(condition);
			}
			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
			if (filter.getUserNo() != null && filter.getUserNo() > 0) {
				Predicate condition = builder.equal(root.get("userNo"), filter.getUserNo());
				p.add(condition);
			}

		}

		return p;
	}

	
	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(ViewFeatureEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

}
