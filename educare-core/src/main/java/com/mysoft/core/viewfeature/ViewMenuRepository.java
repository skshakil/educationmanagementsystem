package com.mysoft.core.viewfeature;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.util.Response;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;



/**
 * @author Md. Jahurul Islam
 *
 */
@Repository
@Transactional
public class ViewMenuRepository extends BaseRepository {

	public Response save(String reqObj) {
		ViewMenuEntity viewMenuEntity = objectMapperReadValue(reqObj, ViewMenuEntity.class);
		return baseOnlySave(viewMenuEntity);
	}

	public Response list(String reqObj) {
		ViewMenuEntity viewMenuEntity = objectMapperReadValue(reqObj, ViewMenuEntity.class);
		return baseList(criteriaQuery(viewMenuEntity));
	}
	
	public List<ViewMenuEntity> findByUserId(Long userId) {
		
		List<ViewMenuEntity>  viewMenuEntityList = null;
		
		ViewMenuEntity viewMenuEntity = new ViewMenuEntity();
		viewMenuEntity.setUserNo(userId);
		Response response = baseList(criteriaQuery(viewMenuEntity));
		
		if(response.isSuccess() && response.getItems() !=null) {
			viewMenuEntityList = getListFromObject(response.getItems(), ViewMenuEntity.class);
		}
		
		return viewMenuEntityList;
	}

	public Response update(String reqObj) {
		ViewMenuEntity viewMenuEntity = objectMapperReadValue(reqObj, ViewMenuEntity.class);
		return baseSaveOrUpdate(viewMenuEntity);
	}

	public ViewMenuEntity findById(Long id) {
		ViewMenuEntity viewMenuEntity = new ViewMenuEntity();
		viewMenuEntity.setId(id);
		Response response = baseFindById(criteriaQuery(viewMenuEntity));

		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), ViewMenuEntity.class);
		}
		return null;
	}

	public Response delete(Long id) {
		ViewMenuEntity viewMenuEntity = findById(id);
		return baseDelete(viewMenuEntity);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(ViewMenuEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
        p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "unchecked" })
    private List<Predicate> criteriaCondition(ViewMenuEntity filter, CriteriaBuilder builder, Root<ViewMenuEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			
	    	if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
	    	
			if (filter.getUserNo() != null && filter.getUserNo() > 0) {
				Predicate condition = builder.equal(root.get("userNo"), filter.getUserNo());
				p.add(condition);
			}

		}

		return p;
	}

	@SuppressWarnings({"rawtypes","unused"})
	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(ViewMenuEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

}
