
package com.mysoft.core.viewfeature;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import lombok.Getter;
import lombok.Setter;



/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
@Setter
@Entity
@Immutable
@Table(name="SA_USERACCESSCTRL_V")
public class ViewFeatureEntity {
	
	@Id
	@Column(name=" SUBMENU_NO", nullable=false)
	private Long id ;
	
	@Column(name="SUBMENU_ID",nullable=true)
	String subMenuId;
		
	@Column(name="SUBMENU_NAME",nullable=true)
	String subMenuName;
	
	@Column(name="MENU_NO",nullable=true)
	Long menuNo;
	
	@Column(name="MENU_NAME",nullable=true)
	String menuName;
	
	@Column(name="USER_NO",nullable=true)
	Long userNo;

	@Column(name = "COMPANY_NO")
	private Long companyNo;	
	
	@Column(name="HIDE_FLAG",nullable=true)
	Boolean hideFlag;
	
	@Column(name="CAN_VIEW",nullable=true)
	Boolean canView;
	
	@Column(name="CAN_CREATE",nullable=true)
	Boolean canCreate;
	
	@Column(name="CAN_MODIFY",nullable=true)
	Boolean canModifed;
	
	@Column(name="CAN_REMOVE",nullable=true)
	Boolean canRemove;

	}
