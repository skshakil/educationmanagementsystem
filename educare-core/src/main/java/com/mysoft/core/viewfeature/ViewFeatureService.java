package com.mysoft.core.viewfeature;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.mysoft.core.util.Response;
import org.springframework.stereotype.Service;


/**
 * @author Mohammad lockman
 *
 */

@Service
public class ViewFeatureService {
	@Autowired
	ViewFeatureRepository viewFeatureRepository;

	public Response save(String referal) {
		return viewFeatureRepository.save(referal);
	}

	public Response list(String reqObj) {
		return viewFeatureRepository.list(reqObj);
	}
	
	public List<ViewFeatureEntity> findByUserId(Long userId) {
		return viewFeatureRepository.findByUserId(userId);
	}

	public Response update(String reqObj) {

		return viewFeatureRepository.update(reqObj);
	}

	public Response delete(Long id) {
		return viewFeatureRepository.delete(id);
	}

}
