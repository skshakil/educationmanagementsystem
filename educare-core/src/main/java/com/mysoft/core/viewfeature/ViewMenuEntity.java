
package com.mysoft.core.viewfeature;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import lombok.Getter;
import lombok.Setter;



/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
@Setter
@Entity
@Immutable
@Table(name="SA_OBJECT_V_WEB")
public class ViewMenuEntity {
	
	@Id
	@Column(name="ID", insertable = false, updatable = false,  nullable=false)
	private Long id ;
	
	@Column(name="USER_NO", insertable = false, updatable = false)
	Long userNo;
	
	@Column(name="LVL", insertable = false, updatable = false)
	Integer treeLable;
	
	@Column(name="OBJECT_NO", insertable = false, updatable = false)
	Long menuNo;
	
	@Column(name="OBJECT_ID", insertable = false, updatable = false)
	String menuCode;
		
	@Column(name="OBJECT_NAME", insertable = false, updatable = false)
	String menuName;
	
	@Column(name="OBJECT_LINK",insertable = false, updatable = false)
	String menuLink;
	
	@Column(name="PARENT_OBJECT_NO",insertable = false, updatable = false)
	Long parentMenuNo;
	
	@Column(name="PARENT_OBJECT_NM",insertable = false, updatable = false)
	String parentMenuName;
	
	@Column(name="DISPLAY_ORDER",insertable = false, updatable = false)
	Long displayOrder;
	

	}
