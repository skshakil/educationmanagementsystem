package com.mysoft.core.viewfeature;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.mysoft.core.util.Response;
import org.springframework.stereotype.Service;


/**
 * @author Mohammad lockman
 *
 */

@Service
public class ViewMenuService {
	@Autowired
	ViewMenuRepository viewMenuRepository;

	public Response save(String referal) {
		return viewMenuRepository.save(referal);
	}

	public Response list(String reqObj) {
		return viewMenuRepository.list(reqObj);
	}
	
	public List<ViewMenuEntity> findByUserId(Long userId) {
		return viewMenuRepository.findByUserId(userId);
	}



}
