package com.mysoft.core.locationSetup;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import com.mysoft.core.base.BaseOraEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
@Setter
@Entity
@Table(name = "SA_LOCATION_SETUP")
public class LocationEntity extends BaseOraEntity implements Serializable {

	private static final long serialVersionUID = 3746039792618057221L;

	@Id
	@Column(nullable = false, name = "LOCATION_NO")
	Long id;

	@Column(nullable = false, name = "LOCATION_ID")
	String locationId;

	@Column(nullable = true, name = "LOCATION_NAME")
	String locationName;

	@Column(nullable = true, name = "DIVISION_NO")
	Long divisionNo;

	@Column(nullable = true, name = "DIST_FR_ORG")
	Long distForOrg;
	
	@Column(nullable = true, name = "SERVICE_TYPE")
	Long serviceType;

	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus = 1;

	@Column(name = "LOCATION_TYPE")
	private Long locationType;

	
}
