package com.mysoft.core.locationSetup;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Service
public class LocationService {

	@Autowired
	private LocationRepository locationRepository;

	public Response gridList(HttpServletRequest request) {
		return locationRepository.gridList(request);
	}

	public Response list(String reqObj) {
		return locationRepository.list(reqObj);
	}

	public Response save(String reqObj, Long companyNo, UserSignInContainter userDetails) {
		return locationRepository.save(reqObj, companyNo, userDetails);
	}

	public Response update(String reqObj, UserSignInContainter userDetails) {
		return locationRepository.update(reqObj, userDetails);
	}

	public Response delete(Long id) {
		return locationRepository.detele(id);
	}

	public Response remove(Long id) {
		return locationRepository.remove(id);
	}

	public LocationEntity findCompanyById(Long companyId) {

		return locationRepository.findById(companyId);
	}

	public Response findThana() {

		return locationRepository.findThana();
	}

	public Response findDistrict() {

		return locationRepository.findDistrict();
	}

	public Response findCountry() {

		return locationRepository.findCountry();
	}

	public Response findCountryDistricThana() {

		return locationRepository.findCountryDistricThana();
	}

}
