package com.mysoft.core.locationSetup;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Repository
@Transactional
public class LocationRepository extends BaseRepository {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {

		DataTableResults<LocationEntity> dataTableResults = null;
		Response response = new Response();
		LocationEntity LocationEntity = new LocationEntity();

		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(LocationEntity);

		List gridList = new ArrayList<>();
		response = baseList(typedQuery(LocationEntity, dataTableInRQ));

		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response list(String reqObj) {

		LocationEntity LocationEntity = null;
		if (null != reqObj) {
			LocationEntity = objectMapperReadValue(reqObj, LocationEntity.class);
		}
		return baseList(criteriaQuery(LocationEntity));
	}

	public Response save(String reqObj, Long companyNo, UserSignInContainter userDetails) {
		LocationEntity LocationEntity = objectMapperReadValue(reqObj, LocationEntity.class);

		LocationEntity.setId(companyNo);
		LocationEntity.setSsCreator(userDetails.getUserId());
		LocationEntity.setSsCreatedOn(new Date());
		LocationEntity.setSsCreateSession(userDetails.getSessionNo());

		return baseOnlySave(LocationEntity);
	}

	public Response update(String reqObj, UserSignInContainter userDetails) {

		LocationEntity LocationEntity = objectMapperReadValue(reqObj, LocationEntity.class);
		LocationEntity obj = findById(LocationEntity.getId());
		if (obj != null) {

			return baseUpdate(obj);
		}

		return getErrorResponse("Record not Found !!");

	}

	public Response detele(Long id) {

		LocationEntity LocationEntity = findById(id);
		if (LocationEntity == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(LocationEntity);

	}

	public Response remove(Long id) {
		LocationEntity LocationEntity = findById(id);
		if (LocationEntity == null) {
			return getErrorResponse("Record not found!");
		}
		LocationEntity.setActiveStatus(3);
		return baseRemove(LocationEntity);
	}

	public LocationEntity findById(Long id) {
		LocationEntity LocationEntity = new LocationEntity();
		LocationEntity.setId(id);
		Response response = baseFindById(criteriaQuery(LocationEntity));
		if (response.isSuccess()) {

			return getValueFromObject(response.getObj(), LocationEntity.class);
		}
		return null;
	}

	public Response findThana() {

		LocationEntity locationEntity = new LocationEntity();
		locationEntity.setLocationType(3L);

		Response response = baseList(criteriaQuery(locationEntity));

		return response;
	}

	public Response findDistrict() {

		LocationEntity locationEntity = new LocationEntity();
		locationEntity.setLocationType(2L);

		Response response = baseList(criteriaQuery(locationEntity));

		return response;
	}

	public Response findCountry() {

		LocationEntity locationEntity = new LocationEntity();
		locationEntity.setLocationType(0L);

		Response response = baseList(criteriaQuery(locationEntity));

		return response;
	}

	public Response findCountryDistricThana() {

		Map<String, Object> countryDistricThanaMap = new HashMap<String, Object>();
		Response countryResponse = findCountry();
		Response districtResponse = findDistrict();
		Response thanaResponse = findDistrict();

		if (countryResponse.isSuccess()) {
			countryDistricThanaMap.put("country", countryResponse.getItems());
		}

		if (districtResponse.isSuccess()) {
			countryDistricThanaMap.put("district", districtResponse.getItems());
		}

		if (thanaResponse.isSuccess()) {
			countryDistricThanaMap.put("thana", thanaResponse.getItems());
		}

		Response finalResponse = new Response();
		finalResponse.setObj(countryDistricThanaMap);

		return finalResponse;
	}

	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(LocationEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(LocationEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(LocationEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<LocationEntity> root = from(LocationEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(LocationEntity filter, CriteriaBuilder builder,
			Root<LocationEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			if (filter.getActiveStatus() > 0) {
				Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
				p.add(condition);
			}

			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}

			if (filter.getLocationType() != null && filter.getLocationType() >= 0) {
				Predicate condition = builder.equal(root.get("locationType"), filter.getLocationType());
				p.add(condition);
			}

		}

		return p;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	private void init() {

		initEntityManagerBuilderCriteriaQueryRoot(LocationEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;

	}

}
