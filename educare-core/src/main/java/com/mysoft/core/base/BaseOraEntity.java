package com.mysoft.core.base;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
abstract public class BaseOraEntity {

	@Column(name = "SS_CREATOR")
	private Long ssCreator;

	@Column(name = "SS_CREATED_ON")
	private Date ssCreatedOn;

	@Column(name = "SS_CREATED_SESSION")
	private Long ssCreateSession;

	@Column(name = "SS_MODIFIER")
	private Long ssModifier;

	@Column(name = "SS_MODIFIED_ON")
	private Date ssModifiedOn;

	@Column(name = "SS_MODIFIED_SESSION")
	private Long ssModifiedSession;

	@Column(name = "COMPANY_NO")
	private Long companyNo;

	@Column(name = "OG_NO")
	private Long organizationNo;

}
