package com.mysoft.core.base;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.io.FilenameUtils;
import org.hibernate.dialect.Dialect;
import org.hibernate.jpa.QueryHints;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.ibm.icu.text.SimpleDateFormat;
import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.PaginationCriteria;
import com.mysoft.core.util.CommonFunctions;
import com.mysoft.core.util.CommonUtils;
import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

public class BaseRepository implements CommonFunctions {

	private final Logger LOGGER = LoggerFactory.getLogger(BaseRepository.class);
	public CriteriaBuilder builder = null;
	public CriteriaQuery criteria = null;
	public Root root = null;
	@Autowired
	private EntityManager entityManager;
	@Autowired
	private Environment env;

	public Response baseOnlySave(Object obj) {
		Response response = new Response();
		try {
			entityManager.persist(obj);
			response.setObj(obj);
			return getSuccessResponse("Saved Successfully", response);
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" baseOnlySave Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Save fail !!");
		}

	}

	public Response baseBatchOnlySave(List<Object> objects) {
		Response response = new Response();
		int batchSize = batchSize();
		try {
			List<Object> items = new ArrayList<Object>();
			for (int i = 0; i < objects.size(); i++) {
				if (i > 0 && i % batchSize == 0) {
					entityManager.flush();
					entityManager.clear();

				}
				Object object = objects.get(i);
				entityManager.persist(object);
				entityManager.persist(object);
				items.add(object);

//				EntityManagerFactory emf = Persistence.createEntityManagerFactory(null);
//				EntityManager entityManager = emf.createEntityManager();
//				entityManager.getTransaction().begin();
//				//Object object = objects.get(i);
//				entityManager.persist(object);
//				entityManager.getTransaction().commit();

			}
			response.setItems(items);
			return getSuccessResponse("Saved Successfully", response);
		} catch (Exception e) {

			LOGGER.error(" baseBatchOnlySave Exception == " + e.getCause().getCause().getMessage());

			return getErrorResponse("Batch Save Fail !!");
		}
	}

	public Response baseBatchSaveOrUpdate(List<Object> objects) {
		Response response = new Response();
		int batchSize = batchSize();
		try {
			List<Object> items = new ArrayList<Object>();
			for (int i = 0; i < objects.size(); i++) {
				if (i > 0 && i % batchSize == 0) {
					entityManager.flush();
					entityManager.clear();
				}
				Object object = objects.get(i);
				entityManager.merge(object);
				items.add(object);
			}
			response.setItems(items);
			return getSuccessResponse("Update Successfully", response);
		} catch (Exception e) {
			LOGGER.error(" baseBatchSaveOrUpdate Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Batch Save Fail !!");
		}
	}

	public Response baseSaveOrUpdate(Object obj) {
		Response response = new Response();
		try {
			response.setObj(entityManager.merge(obj));
			return getSuccessResponse("Update Successfully", response);
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" baseSaveOrUpdate Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Save fail !!");
		}

	}

	public Response baseSaveOrUpdate1(Object obj) {
		Response response = new Response();
		try {
			entityManager.getTransaction().begin();
			response.setObj(entityManager.merge(obj));
			entityManager.getTransaction().commit();

			return getSuccessResponse("Update Successfully", response);
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" baseSaveOrUpdate1 Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Save fail !!");
		}

	}

	public Response baseUpdate(Object obj) {

		try {
			entityManager.merge(obj);
			return getSuccessResponse("Update Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			LOGGER.error(" baseUpdate Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Update fail !!");
		}

	}

	public Response baseRemove(Object obj) {

		try {
			entityManager.merge(obj);
			return getSuccessResponse("Remove Successfully");
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" baseRemove Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Update fail !!");
		}

	}

	public Response baseDelete(Object obj) {
		try {
			entityManager.remove(obj);
			return getSuccessResponse("Delete Successfully");
		} catch (Exception e) {
			LOGGER.error(" baseDelete Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Delete fail !!");
		}

	}

	public Response baseBatchDelete(String entityName, String columnName, @SuppressWarnings("rawtypes") List ids) {

		try {
			Query query = entityManager.createQuery("DELETE " + entityName + " WHERE " + columnName + " IN (:ids)");
			query.setParameter("ids", ids);
			query.executeUpdate();
			return getSuccessResponse("Deleted Successfully");
		} catch (Exception e) {
			LOGGER.error(" baseBatchDelete Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Delete fail !!");
		}

	}

	// TODO
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseDataList(CriteriaQuery criteria) {
		Response response = new Response();
		List list = null;
		try {
			list = entityManager.createQuery(criteria).getResultList();

			if (list.size() > 0) {
				response.setItems(list);
				return getSuccessResponse("Data found ", response);
			}

			return getSuccessResponse("Data Empty ");
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" baseDataList Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Data not found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseTupleSingleColumn(Map<String, String> columnMap, CriteriaQuery criteria) {

		Response response = new Response();
		Object obj = null;
		try {
			obj = entityManager.createQuery(criteria).setHint(QueryHints.HINT_READONLY, true).getSingleResult();
			if (obj != null) {
				Tuple tuple = (Tuple) obj;
				response.setObj(tuple.get(0));
				return getSuccessResponse("find data Successfully", response);
			}
			return getErrorResponse("Data not found !!");
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" baseTupleSingleColumn Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Data not found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseTupleList(Map<String, String> columnMap, CriteriaQuery criteria) {
		Response response = new Response();
		List list = null;
		try {
			list = entityManager.createQuery(criteria).setHint(QueryHints.HINT_READONLY, true).getResultList();

			if (list.size() > 0) {

				List itemList = new ArrayList<>();

				List<Tuple> tupleList = list;

				for (Tuple tuple : tupleList) {

					Map<String, Object> tupleMap = new HashMap<String, Object>();
					int index = 0;

					for (Map.Entry<String, String> columName : columnMap.entrySet()) {

						tupleMap.put(columName.getValue(), tuple.get(index));

						index++;

					}

					itemList.add(tupleMap);

				}
				response.setItems(itemList);
				return getSuccessResponse("Data found ", response);
			}

			return getSuccessResponse("Data Empty ");
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" baseTupleList Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Data not found !!");
		}

	}

	public <T> CriteriaQuery<Tuple> baseDistinctMultiSeletCriteria(CriteriaBuilder builder,
			CriteriaQuery<Tuple> criteria, Root<T> root, Map<String, String> columnNameMap,
			List<Predicate> pConjunction) {

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		List<Selection<?>> columnSelection = new LinkedList<Selection<?>>();

		for (Map.Entry<String, String> columName : columnNameMap.entrySet()) {
			columnSelection.add(root.get(columName.getValue()));
		}

		criteria.multiselect(columnSelection).distinct(true);

		return criteria;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseList(CriteriaQuery criteria) {
		Response response = new Response();
		List list = null;
		try {
			list = entityManager.createQuery(criteria).setHint(QueryHints.HINT_READONLY, true).getResultList();

			if (list.size() > 0) {
				response.setItems(list);
				return getSuccessResponse("Data found ", response);
			}

			return getSuccessResponse("Data Empty ");
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" BaseList Exception == " + e.getCause().getCause().getMessage());
			return getErrorResponse("Data not found !!");
		}

	}

	/*
	 * @SuppressWarnings({ "rawtypes", "unchecked" }) public Response
	 * baseTupleList(Map<String, String> columnMap, CriteriaQuery criteria) {
	 * Response response = new Response(); List list = null; try { list =
	 * entityManager.createQuery(criteria).setHint(QueryHints.HINT_READONLY,
	 * true).getResultList();
	 * 
	 * if (list.size() > 0) {
	 * 
	 * List itemList = new ArrayList<>();
	 * 
	 * List<Tuple> tupleList = list;
	 * 
	 * for (Tuple tuple : tupleList) {
	 * 
	 * Map<String, Object> tupleMap = new HashMap<String, Object>(); int index = 0;
	 * 
	 * for (Map.Entry<String, String> columName : columnMap.entrySet()) {
	 * 
	 * tupleMap.put(columName.getValue(), tuple.get(index));
	 * 
	 * index++;
	 * 
	 * }
	 * 
	 * itemList.add(tupleMap);
	 * 
	 * } response.setItems(itemList); return getSuccessResponse("Data found ",
	 * response); }
	 * 
	 * return getSuccessResponse("Data Empty "); } catch (Exception e) { // TODO:
	 * handle exception return getErrorResponse("Data not found !!"); }
	 * 
	 * }
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseFindByParent(CriteriaQuery criteria) {
		Response response = new Response();
		List list = null;
		try {
			list = entityManager.createQuery(criteria).setHint(QueryHints.HINT_READONLY, true).getResultList();

			if (list.size() > 0) {

				response.setItems(list);
				return getSuccessResponse("Data found ", response);
			}

			return getSuccessResponse("Data Empty ");
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not found !!");
		}

	}

	@SuppressWarnings({ "rawtypes" })
	public Response baseList(TypedQuery typedQuery) {
		Response response = new Response();
		List list = null;

		try {

			list = typedQuery.setHint(QueryHints.HINT_READONLY, true).getResultList();

			if (list.size() > 0) {

				response.setItems(list);
				return getSuccessResponse("Data found ", response);
			}

			return getSuccessResponse("Data Empty ");

		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> Response getMaxRecord(CriteriaQuery criteria) {

		Response response = new Response();
		try {

			CriteriaQuery<T> select = criteria.select(root);

			TypedQuery<T> typedQuery = entityManager.createQuery(select);
			typedQuery.setFirstResult(0);
			typedQuery.setMaxResults(1);
			Object obj = typedQuery.getSingleResult();
			if (obj != null) {
				response.setObj(obj);
				return getSuccessResponse("Data found ", response);
			}
			return getSuccessResponse("Data Empty ");
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> Response getMaxSecodRecord(CriteriaQuery criteria) {

		Response response = new Response();

		try {

			CriteriaQuery<T> select = criteria.select(root);

			TypedQuery<T> typedQuery = entityManager.createQuery(select);
			typedQuery.setFirstResult(1);
			typedQuery.setMaxResults(1);
			Object obj = typedQuery.getSingleResult();
			if (obj != null) {
				response.setObj(obj);
				return getSuccessResponse("Data found ", response);
			}
			return getSuccessResponse("Data Empty ");
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseFindById(CriteriaQuery criteria) {
		Response response = new Response();
		Object obj = null;
		try {
			obj = entityManager.createQuery(criteria).getSingleResult();
			response.setObj(obj);
			return getSuccessResponse("find data Successfully", response);
		} catch (NoResultException e) {
			// TODO: handle exception
			return getErrorResponse("Data not Found !!");
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("exception occured !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseSingleObject(CriteriaQuery criteria) {
		Response response = new Response();
		Object obj = null;
		try {
			obj = entityManager.createQuery(criteria).getSingleResult();
			response.setObj(obj);
			return getSuccessResponse("find data Successfully", response);
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not Found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> TypedQuery baseTypedQuery(CriteriaQuery criteria, DataTableRequest dataTableInRQ) {

		CriteriaQuery<T> select = criteria.select(root);
		TypedQuery<T> typedQuery = entityManager.createQuery(select);
		typedQuery.setFirstResult(dataTableInRQ.getStart());
		typedQuery.setMaxResults(dataTableInRQ.getLength());
		return typedQuery;
	}

	public Response findByAppImageInsubdir(String imageName, String subDir) {

		String fileApiUri = env.getProperty("file.api.url");

		String uri = fileApiUri + "/findAppImageInsubdir/" + imageName;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("subDir", subDir);

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		return new RestTemplate().postForObject(uri, requestEntity, Response.class);
	}

	public Response findCompanyImage(String imageName) {

		return findByAppImageInsubdir(imageName, "company_image");
	}
	
	public Response findNewspaperLogo(String imageName) {
		
		return findByAppImageInsubdir(imageName, "news_blog");
	}

	public Response findCompanyLogo(String logoName) {

		return findByAppImageInsubdir(logoName, "company_logo");

	}

	public Response findOgLogo(String logoName) {
		return findByAppImageInsubdir(logoName, "og_logo");
	}

	public Response findDocDigiSign(String digiSingName) {
		return findByAppImageInsubdir(digiSingName, "doc_digital_sign");
	}

	public Response findByCompanyLogo() {
		return findByAppImageInsubdir("companyLogo.png", "company1");
	}

//	public Response findNonLabReportImageByName(String imageName) {
//		return findByInSubdir(imageName, "non_lab_report");
//	}

	public Response findByInSubdir(String fileName, String subDir) {

		String fileApiUri = env.getProperty("file.api.url");

		String uri = fileApiUri + "/findPatientReport/" + fileName;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("subDir", subDir);

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		RestTemplate restTemplate = new RestTemplate();
		Response photoResponse = restTemplate.postForObject(uri, requestEntity, Response.class);

		return photoResponse;

	}

	public Response findByPatientReportFile(String fileName, String subDir) {

		return findByInSubdir(fileName, subDir);

	}

//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public Long baseFilterCountTypedQuery(CriteriaQuery criteria) {
//
//		Long totalRowCount = (Long) entityManager.createQuery(criteria).getSingleResult();
//
//		return totalRowCount;
//	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> TypedQuery baseTypedQuery(CriteriaQuery criteria) {

		CriteriaQuery<T> select = criteria.select(root);
		TypedQuery<T> typedQuery = entityManager.createQuery(select);

		return typedQuery;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response getNonReadonlyList(CriteriaQuery criteria) {
		Response response = new Response();
		Object obj = null;
		try {
			obj = entityManager.createQuery(criteria).getResultList();
			response.setItems((List) obj);
			return getSuccessResponse("find data Successfully", response);
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not Found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Object> entityManagerBuilderCriteriaQueryRoot(Class clazz) {

		Map<String, Object> entityManagerParams = new HashMap<String, Object>();

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(clazz);
		entityManagerParams.put("builder", builder);
		entityManagerParams.put("criteria", criteria);
		entityManagerParams.put("root", root);

		return entityManagerParams;

	}

	@SuppressWarnings({ "rawtypes" })
	public void initEntityManagerBuilderCriteriaQueryRoot(Class clazz) {
		criteriaRoot(clazz);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Root criteriaRoot(Class clazz) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(clazz);
		Root root = criteria.from(clazz);
		this.builder = builder;
		this.criteria = criteria;
		this.root = root;

		return root;
	}

	public <T> void totalCriteriaQuery(Class<T> clazz) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<T> root = criteria.from(clazz);

		this.builder = builder;
		this.criteria = criteria;
		this.root = root;

	}

	public CriteriaBuilder criteriaBuilder() {
		return entityManager.getCriteriaBuilder();
	}

	public CriteriaQuery<Long> longCriteriaQuery(CriteriaBuilder builder) {
		return builder.createQuery(Long.class);
	}

	public CriteriaQuery<Tuple> baseCiteriaQueryTuple(CriteriaBuilder builder) {
		return builder.createTupleQuery();
	}

	public <T> Root<T> from(Class<T> clazz, CriteriaQuery<Long> criteria) {
		return criteria.from(clazz);
	}

	public <T> Root<T> baseFrom(Class<T> clazz, CriteriaQuery<Tuple> criteria) {
		return criteria.from(clazz);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Root LongCriteriaQuery(Class clazz) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root root = criteria.from(clazz);
		this.builder = builder;
		this.criteria = criteria;
		this.root = root;
		return root;

	}

	/*
	 * @SuppressWarnings({ "rawtypes", "unchecked" }) public <T> Long
	 * totalCount(Class<T> clazz) {
	 *
	 * CriteriaBuilder builder = entityManager.getCriteriaBuilder(); CriteriaQuery
	 * criteria = builder.createQuery(Long.class);
	 * criteria.select(builder.count(criteria.from(clazz))); Long totalRowCount =
	 * (Long)entityManager.createQuery(criteria).getSingleResult();
	 *
	 * return totalRowCount;
	 *
	 * }
	 */

	public <T> String criteriaQuery(Class<T> clazz) {
		CriteriaQuery<T> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(clazz);
		Root<T> root = criteriaQuery.from(clazz);
		return criteriaQuery.select(root).toString();
	}

	public <T> void limitedCriteriaQuer(Class clazz) {
		Query limitedCriteriaQuery = entityManager.createQuery(criteriaQuery(clazz)).setFirstResult(0)
				.setMaxResults(10);
		// return limitedCriteriaQuery.getResultList();
	}

	/*
	 * public <T> Long totalCount(CriteriaBuilder builder, CriteriaQuery<Long>
	 * criteria, List<Predicate> p) {
	 *
	 * //CriteriaBuilder builder = entityManager.getCriteriaBuilder();
	 * //CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
	 *
	 * // criteria.select(builder.count(criteria.from(clazz)));
	 *
	 * if (!CollectionUtils.isEmpty(p)) {
	 *
	 * Predicate[] pArray = p.toArray(new Predicate[] {}); Predicate predicate =
	 * builder.and(pArray); criteria.where(predicate); }
	 *
	 *
	 * Long totalRowCount = entityManager.createQuery(criteria).getSingleResult();
	 *
	 * return totalRowCount;
	 *
	 * }
	 */

	public <T> Long totalCount(Class<T> clazz, List<Predicate> p) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);

		criteria.select(builder.count(criteria.from(clazz)));

		/*
		 * if (!CollectionUtils.isEmpty(p)) {
		 *
		 * Predicate[] pArray = p.toArray(new Predicate[] {}); Predicate predicate =
		 * builder.and(pArray); criteria.where(predicate); }
		 */

		Long totalRowCount = entityManager.createQuery(criteria).getSingleResult();

		return totalRowCount;

	}

	public <T> Long totalCount(CriteriaBuilder builder, CriteriaQuery<Long> criteria, Root<T> root,
			List<Predicate> pConjunction) {

		criteria.select(builder.count(root));

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		Long totalRowCount = entityManager.createQuery(criteria).getSingleResult();

		return totalRowCount;

	}

	public <T> CriteriaQuery<Tuple> baseMultiSeletCriteria(CriteriaBuilder builder, CriteriaQuery<Tuple> criteria,
			Root<T> root, Map<String, String> columnNameMap, List<Predicate> pConjunction) {

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		List<Selection<?>> columnSelection = new LinkedList<Selection<?>>();

		for (Map.Entry<String, String> columName : columnNameMap.entrySet()) {
			columnSelection.add(root.get(columName.getValue()));
		}

		criteria.multiselect(columnSelection);

		return criteria;

	}

	public <T> CriteriaQuery<Tuple> baseMultiSeletCriteria(CriteriaBuilder builder, CriteriaQuery<Tuple> criteria,
			Root<T> root, Map<String, String> columnNameMap, List<Predicate> pConjunction,
			List<Predicate> pDisJunction) {

		List<Predicate> pArrayJoin = new ArrayList<Predicate>();

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);

		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}

		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);
		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);
		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		List<Selection<?>> columnSelection = new LinkedList<Selection<?>>();

		for (Map.Entry<String, String> columName : columnNameMap.entrySet()) {

			if (columName.getKey().equals("unitNo")) {
				root.join(columName.getValue(), JoinType.LEFT);
			}

			if (columName.getKey().equals("serviceCategory")) {
				root.join(columName.getValue(), JoinType.LEFT);
			}

			if (columName.getKey().equals("rankNo")) {
				root.join(columName.getValue(), JoinType.LEFT);
			}

			columnSelection.add(root.get(columName.getValue()));
		}

		criteria.multiselect(columnSelection);

		return criteria;

	}

	public Long maxValue(CriteriaBuilder builder, CriteriaQuery<Long> criteria, Root root, List<Predicate> pConjunction,
			String columnName) {

		criteria.select(builder.max(root.get(columnName)));

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		Long maxVal = entityManager.createQuery(criteria).getSingleResult();

		return maxVal;

	}

	@SuppressWarnings("unchecked")
	public Date maxDate(CriteriaBuilder builder, CriteriaQuery<Date> criteria, @SuppressWarnings("rawtypes") Root root,
			List<Predicate> pConjunction, String columnName) {

		criteria.select(builder.max(root.get(columnName)));

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}

		Date maxDate = entityManager.createQuery(criteria).getSingleResult();

		return maxDate;

	}

	public <T> Long filterTotalCount(CriteriaBuilder builder, CriteriaQuery<Long> criteria, Root<T> root,
			List<Predicate> pArrayJoin) {

		criteria.select(builder.count(root));

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		Long totalRowCount = entityManager.createQuery(criteria).getSingleResult();

		return totalRowCount;

	}

	public <T> Long totalCount(CriteriaBuilder builder, CriteriaQuery<Long> criteria, Root<T> root,
			List<Predicate> pConjunction, List<Predicate> pDisjunction) {

		criteria.select(builder.count(root));

		List<Predicate> pArrayJoin = new ArrayList<Predicate>();

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}
		if (!CollectionUtils.isEmpty(pDisjunction)) {
			Predicate[] pArray = pDisjunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);
		}
		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);
		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		Long totalRowCount = entityManager.createQuery(criteria).getSingleResult();

		return totalRowCount;

	}

	@SuppressWarnings("unchecked")
	@Deprecated
	public <T> List<Predicate> dataTablefilter(DataTableRequest<T> dataTableInRQ) {

		PaginationCriteria paginationCriteria = dataTableInRQ.getPaginationRequest();
		paginationCriteria.getFilterBy().getMapOfFilters();

		List<Predicate> p = new ArrayList<Predicate>();

		if (!paginationCriteria.isFilterByEmpty()) {
			Iterator<Entry<String, String>> fbit = paginationCriteria.getFilterBy().getMapOfFilters().entrySet()
					.iterator();

			while (fbit.hasNext()) {
				Map.Entry<String, String> pair = fbit.next();
				if (!pair.getKey().equals("ssModifiedOn")) {
					p.add(builder.like(builder.lower(root.get(pair.getKey())),
							CommonUtils.PERCENTAGE_SIGN + pair.getValue().toLowerCase() + CommonUtils.PERCENTAGE_SIGN));
				}

			}

		}

		return p;

	}

	@SuppressWarnings("unchecked")
	public <T> List<Predicate> dataTablefilter(DataTableRequest<T> dataTableInRQ, Class clazz) {

		PaginationCriteria paginationCriteria = dataTableInRQ.getPaginationRequest();
		paginationCriteria.getFilterBy().getMapOfFilters();

		List<Predicate> p = new ArrayList<Predicate>();

		if (!paginationCriteria.isFilterByEmpty()) {
			Iterator<Entry<String, String>> fbit = paginationCriteria.getFilterBy().getMapOfFilters().entrySet()
					.iterator();
			Field[] fields = getClassFields(clazz);

			boolean isNumber = true;
			int executeCount = 0;

			while (fbit.hasNext()) {
				Map.Entry<String, String> pair = fbit.next();

				if (isNumber && executeCount == 0) {
					isNumber = isParsableNumber(pair.getValue());
					executeCount = 1;
				}

				String dataType = getClassFieldDataType(fields, pair.getKey());

				if (dataType != null && dataType.equals("class java.lang.String")) {
					p.add(builder.like(builder.lower(root.get(pair.getKey())),
							CommonUtils.PERCENTAGE_SIGN + pair.getValue().toLowerCase() + CommonUtils.PERCENTAGE_SIGN));
				}

				else if (dataType != null && dataType.equals("class java.lang.Long") && isNumber) {
					p.add(builder.equal(root.get(pair.getKey()), pair.getValue().trim()));

				}

			}

		}

		return p;

	}

	public Field[] getClassFields(Class claz) {
		return claz.getDeclaredFields();
	}

	public String getClassFieldDataType(Field[] fields, String fieldName) {

		for (Field field : fields) {
			if (field.getName().trim().equals(fieldName.trim())) {
				// System.out.println("field.getType() " + field.getType());
				return field.getType().toString();
			}

		}

		return null;

	}

	public <T> List<Predicate> dataTablefilter(DataTableRequest<T> dataTableInRQ, CriteriaBuilder builder, Root root) {

		PaginationCriteria paginationCriteria = dataTableInRQ.getPaginationRequest();
		paginationCriteria.getFilterBy().getMapOfFilters();

		List<Predicate> p = new ArrayList<Predicate>();

		if (!paginationCriteria.isFilterByEmpty()) {
			Iterator<Entry<String, String>> fbit = paginationCriteria.getFilterBy().getMapOfFilters().entrySet()
					.iterator();

			while (fbit.hasNext()) {
				Map.Entry<String, String> pair = fbit.next();
				if (!pair.getKey().equals("ssModifiedOn")) {
					System.out.println("pair.getKey() " + pair.getKey());
					p.add(builder.like(builder.lower(root.get(pair.getKey())),
							CommonUtils.PERCENTAGE_SIGN + pair.getValue().toLowerCase() + CommonUtils.PERCENTAGE_SIGN));
				}

			}

		}

		return p;

	}

	public <T> List<Predicate> dataTablefilter(DataTableRequest<T> dataTableInRQ, CriteriaBuilder builder, Root root,
			Class clazz) {

		PaginationCriteria paginationCriteria = dataTableInRQ.getPaginationRequest();
		paginationCriteria.getFilterBy().getMapOfFilters();

		List<Predicate> p = new ArrayList<Predicate>();

		if (!paginationCriteria.isFilterByEmpty()) {
			Iterator<Entry<String, String>> fbit = paginationCriteria.getFilterBy().getMapOfFilters().entrySet()
					.iterator();
			Field[] fields = getClassFields(clazz);

			boolean isNumber = true;
			int executeCount = 0;

			while (fbit.hasNext()) {
				Map.Entry<String, String> pair = fbit.next();

				if (isNumber && executeCount == 0) {
					isNumber = isParsableNumber(pair.getValue());
					executeCount = 1;
				}

				String dataType = getClassFieldDataType(fields, pair.getKey());

				if (dataType != null && dataType.equals("class java.lang.String")) {
					p.add(builder.like(builder.lower(root.get(pair.getKey())),
							CommonUtils.PERCENTAGE_SIGN + pair.getValue().toLowerCase() + CommonUtils.PERCENTAGE_SIGN));
				} else if (dataType != null && dataType.equals("class java.lang.Long") && isNumber) {
					p.add(builder.equal(root.get(pair.getKey()), pair.getValue().trim()));

				}

			}

		}

		return p;

	}

	@SuppressWarnings("unchecked")
	public <T> List<Predicate> basePredicate(Map<String, Object> fields) {

		List<Predicate> p = new ArrayList<Predicate>();

		Iterator<Entry<String, Object>> fv = fields.entrySet().iterator();

		while (fv.hasNext()) {
			Map.Entry<String, Object> pair = fv.next();

			if (pair.getValue() instanceof String) {
				p.add(builder.like(builder.lower(root.get(pair.getKey().trim())), CommonUtils.PERCENTAGE_SIGN
						+ ((String) pair.getValue()).trim().toLowerCase() + CommonUtils.PERCENTAGE_SIGN));
			}

			if (pair.getValue() instanceof Long) {
				p.add(builder.equal(root.get(pair.getKey()), pair.getValue()));
			}

		}

		return p;

	}

	@SuppressWarnings("unchecked")
	public <T> List<Predicate> basePredicate(Map<String, Object> fields, CriteriaBuilder builder, Root root) {

		List<Predicate> p = new ArrayList<Predicate>();

		Iterator<Entry<String, Object>> fv = fields.entrySet().iterator();

		while (fv.hasNext()) {
			Map.Entry<String, Object> pair = fv.next();

			if (pair.getValue() instanceof String) {
				p.add(builder.like(builder.lower(root.get(pair.getKey())), CommonUtils.PERCENTAGE_SIGN
						+ ((String) pair.getValue()).toLowerCase() + CommonUtils.PERCENTAGE_SIGN));
			}

			if (pair.getValue() instanceof Long) {
				p.add(builder.equal(root.get(pair.getKey()), pair.getValue()));
			}

		}

		return p;

	}

	@SuppressWarnings({ "rawtypes" })
	public <T> TypedQuery typedQuery(List<Predicate> pConjunctionParam, List<Predicate> pDisJunctionParam) {

		List<Predicate> pArrayJoin = new ArrayList<Predicate>();

		List<Predicate> pConjunction = pConjunctionParam;
		List<Predicate> pDisJunction = pDisJunctionParam;

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}
		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);
		}
		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);
		}
		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria);
	}

	public String storedProcedureGenerateId(Long ogNo, Long companyNo, String prefix) {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PD_GENARATE_ID")

				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(6, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT)

				.setParameter(1, prefix).setParameter(2, "REG_ID").setParameter(3, ogNo).setParameter(4, companyNo)
				.setParameter(5, "YYMM").setParameter(6, 6l);

		return (String) query.getOutputParameterValue(7);
	}

	public String storedProcedureGenerateId(Long ogNo, Long companyNo, String prefix, String tableName,
			String columnName) {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PD_GENARATE_ID")

				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(6, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT)

				.setParameter(1, prefix).setParameter(2, columnName).setParameter(3, ogNo).setParameter(4, companyNo)
				.setParameter(5, "YYMM").setParameter(6, 6l);

		return (String) query.getOutputParameterValue(7);

	}

	public Long storedProcedureGenerateNo(Long ogNo, Long companyNo, String sequenceName) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PD_GENARATE_NO") // K_GENERAL.PD_GENARATE_NO

				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(6, Long.class, ParameterMode.OUT)

				.setParameter(1, sequenceName).setParameter(2, ogNo).setParameter(3, companyNo).setParameter(4, "YY")
				.setParameter(5, 1l);
		try {
			query.execute();
		} catch (Exception e) {
			System.err.println("sequenceName: " + sequenceName);
		}

		return (Long) query.getOutputParameterValue(6);
	}

	public Long functionFdAutoNo(Long ogNo, Long companyNo, String tableName, String columnName, Long dataLength) {
		BigDecimal maxValue = null;
		maxValue = (BigDecimal) entityManager
				.createNativeQuery("SELECT FD_AUTO_NO(:pTable,:pColumn,:pOgNo,:pCompanyNo,:pDataLength) FROM DUAL")
				.setParameter("pTable", tableName).setParameter("pColumn", columnName)
				.setParameter("pOgNo", ogNo != null ? ogNo : "")
				.setParameter("pCompanyNo", companyNo != null ? companyNo : "").setParameter("pDataLength", dataLength)
				.getSingleResult();

		if (maxValue == null) {
			return null;
		}
		return maxValue.longValue();

	}

	public String functionFdUserName(Long userno) {
		String usrName = null;
		usrName = (String) entityManager.createNativeQuery("SELECT FD_USER_NAME(:pUserNo) FROM DUAL")
				.setParameter("pUserNo", userno).getSingleResult();
		if (usrName == null) {
			return null;
		}
		return usrName;
	}

	public Response storedProcedureCreateAnonymousUser(Long regNo, String HN, String gender, Long companyNo,
			Long sessionNo, String encodPassword, String plainPassord, String defaultPageLink, String featureCoad,
			String phoneNo, String patientName, Long userNo, Long ogNo, String photo) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("pd_anonymous_user")

				.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(6, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(7, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(8, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(9, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(10, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(11, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(12, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(13, Long.class, ParameterMode.OUT)
				.registerStoredProcedureParameter(14, String.class, ParameterMode.OUT)
				.registerStoredProcedureParameter(15, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(16, String.class, ParameterMode.IN)

				.setParameter(1, regNo).setParameter(2, HN).setParameter(3, gender).setParameter(4, phoneNo)
				.setParameter(5, patientName).setParameter(6, userNo).setParameter(7, companyNo)
				.setParameter(8, sessionNo).setParameter(9, encodPassword).setParameter(10, plainPassord)
				.setParameter(11, defaultPageLink).setParameter(12, featureCoad).setParameter(15, ogNo)
				.setParameter(16, photo);

		query.execute();

		Long action = query.getOutputParameterValue(13) != null ? (Long) query.getOutputParameterValue(13) : null;
		String message = query.getOutputParameterValue(14) != null ? (String) query.getOutputParameterValue(14) : "";

		Response response = new Response();
		Map<String, Object> resMap = new HashMap<String, Object>();

		resMap.put("action", action);
		resMap.put("message", message);

		response.setObj(resMap);

		if (action == 0) {
			return getErrorResponse("Save failed!!");
		}
		return response;
	}

	public String functionPatientAge(Date fromDate, Date toDate) {
		String from = null;
		String to = null;
		String age = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		if (null != fromDate) {
			from = dateFormat.format(fromDate);
		}
		if (null != toDate) {
			to = dateFormat.format(toDate);
		}

		age = (String) entityManager.createNativeQuery("SELECT fnc_cal_age(:from,:to) FROM DUAL")
				.setParameter("from", from).setParameter("to", to).getSingleResult();

		if (age == null) {
			return null;
		}
		return age;
	}

	public Connection getOraConnection() {

//		try {
//
//			Class.forName(env.getProperty("ora.driver"));
//
//		} catch (ClassNotFoundException e) {
//			System.out.println("Where is your Oracle JDBC Driver?");
//
//		}
//		Connection connection = null;
//		try {
//			connection = DriverManager.getConnection(env.getProperty("ora.url"), env.getProperty("ora.user"),
//					env.getProperty("ora.password"));
//
//		} catch (SQLException e) {
//			System.out.println("Connection Failed! Check output console");
//		}
//		if (connection != null) {
//			return connection;
//		} else {
//			System.out.println("Failed to make connection!");
//			return null;
//		}

		try {

			Class.forName(env.getProperty("spring.datasource.driver-class-name"));

		} catch (ClassNotFoundException e) {
			System.out.println("Where is your Oracle JDBC Driver?");

		}
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(env.getProperty("spring.datasource.url"),
					env.getProperty("spring.datasource.username"), env.getProperty("spring.datasource.password"));

		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
		}
		if (connection != null) {
			return connection;
		} else {
			System.out.println("Failed to make connection!");
			return null;
		}

	}

	public void finalyConStmRs(Connection con, Statement stm, ResultSet rs) {

		try {
			if (rs != null) {
				rs.close();
			}
			if (stm != null) {
				stm.close();
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void finalyConPstmRs(Connection con, PreparedStatement pstm, ResultSet rs) {

		try {
			if (rs != null) {
				rs.close();
			}
			if (pstm != null) {
				pstm.close();
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void finalyStmRs(Statement stm, ResultSet rs) {

		try {
			if (rs != null) {
				rs.close();
			}
			if (stm != null) {
				stm.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void finalyRs(ResultSet rs) {

		try {
			if (rs != null) {
				rs.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String[] jsonArryToStringQuotsArry(JSONArray arr) {
		String[] item = new String[arr.length()];
		for (int i = 0; i < arr.length(); ++i) {
			item[i] = "'" + arr.optString(i) + "'";
		}
		return item;
	}

	@SuppressWarnings("unused")
	private String[] jsonArryToStringArry(JSONArray arr) {
		String[] item = new String[arr.length()];
		for (int i = 0; i < arr.length(); ++i) {
			item[i] = arr.optString(i);
		}
		return item;
	}

	@SuppressWarnings("unused")
	private int[] jsonArryToIntArry(JSONArray arr) {
		int[] item = new int[arr.length()];
		for (int i = 0; i < arr.length(); ++i) {
			item[i] = arr.optInt(i);
		}
		return item;
	}

	@SuppressWarnings("unused")
	private Float[] jsonArryToFloatArry(JSONArray arr) {
		Float[] item = new Float[arr.length()];
		for (int i = 0; i < arr.length(); ++i) {
			item[i] = arr.optFloat(i);
		}
		return item;
	}
	/*
	 * public <T> List globalCtriteria(Conjunction objConjunction,Disjunction
	 * objDisjunction, Class<T> clazz, Predicate predicate) { Session sessoin =
	 * sessionFactory.getCurrentSession(); Criteria criteria1 =
	 * sessoin.createCriteria(clazz); if(objConjunction != null) {
	 * criteria1.add(objConjunction); } if(objDisjunction !=null) {
	 * criteria1.add(objDisjunction); }
	 *
	 *
	 * CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	 *
	 * //Create Criteria CriteriaQuery<T> criteria = builder.createQuery(clazz);
	 * Root<T> contactRoot = criteria.from(clazz);
	 * criteria.select(contactRoot).where(predicate);
	 *
	 *
	 *
	 * //Use criteria to query with session to fetch all contacts List<T> contacts =
	 * sessionFactory.createQuery(criteria).getResultList();
	 *
	 * Predicate likeRestriction = builder.and( builder.li(
	 * myObjectRoot.get("name"), "%string1"), builder.notLike(
	 * myObjectRoot.get("name"), "%string2") );
	 *
	 *
	 * return null; }
	 */

	protected int batchSize() {
		return Integer.valueOf(Dialect.DEFAULT_BATCH_SIZE);
	}

	public Response storedProcedureRegitValidate(String serviceHolderId, String regPersonType, Long regPersonTypeNo,
			Long insertUpdateType, UserSignInContainter userDetails) {

		String lookupdtlNo = String.valueOf(regPersonTypeNo);
		int index = 0;

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("pkg_cmh_policy.pd_regis_validate")

				.registerStoredProcedureParameter(++index, Long.class, ParameterMode.IN) // 1.organizationNo
				.registerStoredProcedureParameter(++index, Long.class, ParameterMode.IN) // 2.companyNo
				.registerStoredProcedureParameter(++index, String.class, ParameterMode.IN) // 3.service holder id
				.registerStoredProcedureParameter(++index, String.class, ParameterMode.IN) // 4.resPersonType
				.registerStoredProcedureParameter(++index, String.class, ParameterMode.IN) // 5.resPersonTypeNo
				.registerStoredProcedureParameter(++index, Long.class, ParameterMode.IN) // 5.resPersonTypeNo

				.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)
				.registerStoredProcedureParameter(++index, Integer.class, ParameterMode.OUT)
				.registerStoredProcedureParameter(++index, String.class, ParameterMode.OUT)

				.setParameter(1, userDetails.getOrganizationNo()).setParameter(2, userDetails.getCompanyNo())
				.setParameter(3, serviceHolderId).setParameter(4, regPersonType).setParameter(5, lookupdtlNo)
				.setParameter(6, insertUpdateType);

		query.execute();

		String generatedPersonalId = query.getOutputParameterValue(7) != null
				? (String) query.getOutputParameterValue(7)
				: null;
		Integer action = query.getOutputParameterValue(8) != null ? (Integer) query.getOutputParameterValue(8) : null;
		String message = query.getOutputParameterValue(9) != null ? (String) query.getOutputParameterValue(9) : "";

		Response response = new Response();
		Map<String, Object> resMap = new HashMap<String, Object>();

		resMap.put("generatedPersonalId", generatedPersonalId);
		resMap.put("action", action);
		resMap.put("message", message);

		response.setObj(resMap);

		return response;

	}

	public Long registrationPhotoNo(UserSignInContainter userDetails) {
		return storedProcedureGenerateNo(userDetails.getOrganizationNo(), userDetails.getCompanyNo(),
				"SEQ_REG_PHOTO_NO");
	}

	public Response findByPhotoName(String photoName) {

		String fileApiUri = env.getProperty("file.api.url");

		LOGGER.info("File api url " + fileApiUri);

		String uri = fileApiUri + "/findPhoto/" + photoName;
		RestTemplate restTemplate = new RestTemplate();

		Response photoResponse = restTemplate.getForObject(uri, Response.class);

		LOGGER.info("Photo response " + photoResponse);

		return photoResponse;
	}

	public Long appReportNoAutoSeq(Long ogNo, Long companyNO) {
		return functionFdAutoNo(ogNo, companyNO, "APP_REPORT", "RPT_NO", 6L);
	}

	public Long getRefPatientReqNo(Long ogNo, Long userCompanyNo) {
		return storedProcedureGenerateNo(ogNo, userCompanyNo, "SEQ_REF_PAT_REQ_NO");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response getListFindByIdReadonly(CriteriaQuery criteria) {
		Response response = new Response();
		Object obj = null;
		try {

			obj = entityManager.createQuery(criteria).setHint(QueryHints.HINT_READONLY, true).getResultList();
			response.setItems((List) obj);
			return getSuccessResponse("find data Successfully", response);
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not Found !!");
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseFindByIdReadOnly(CriteriaQuery criteria) {
		Response response = new Response();
		Object obj = null;
		try {

			obj = entityManager.createQuery(criteria).setHint(QueryHints.HINT_READONLY, true).getSingleResult();
			response.setObj(obj);
			return getSuccessResponse("find data Successfully", response);
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not Found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> TypedQuery baseTypedQuery(CriteriaQuery criteria, int start, int length) {

		CriteriaQuery<T> select = criteria.select(root);

		TypedQuery<T> typedQuery = entityManager.createQuery(select);
		typedQuery.setFirstResult(start);
		typedQuery.setMaxResults(length);

		return typedQuery;
	}

	public void finallyOutputStream(ByteArrayOutputStream baos) {

		if (baos != null) {
			try {
				baos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@SuppressWarnings({ "rawtypes" })
	public Response baseTop1(CriteriaQuery criteria) {
		Response response = new Response();
		List list = new ArrayList<>();

		response = baseTopNList(criteria, 0);

		if (response.isSuccess()) {
			list = response.getItems();

			if (list == null || list.size() <= 0) {
				return getSuccessResponse("Data Empty ");
			}

			response.setItems(null);
			response.setObj(list.get(0));

			return getSuccessResponse("find data Successfully", response);

		} else {
			return getErrorResponse("Data not found !!");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response baseTopNList(CriteriaQuery criteria, int topN) {

		Response response = new Response();
		List list = null;
		try {
			list = entityManager.createQuery(criteria).setFirstResult(topN).setHint(QueryHints.HINT_READONLY, true)
					.getResultList();

			if (list.size() > 0) {
				response.setItems(list);
				return getSuccessResponse("Data found ", response);
			}

			return getSuccessResponse("Data Empty ");
		} catch (Exception e) {
			// TODO: handle exception
			return getErrorResponse("Data not found !!");
		}
	}

	public Long storeNoAutoSeq(Long ogNo, Long companyNO) {

		return functionFdAutoNo(ogNo, companyNO, "IN_STORE", "STORE_NO", 6L);
	}

	public Long reportingDoctorSeqNoAuto(Long ogNo, Long companyNO) {
		return functionFdAutoNo(ogNo, companyNO, "PATHS_USER_REPORTING_DOCTOR", "USER_REPORTING_DOCTOR_NO", 6L);
	}

	public Long reportingPreparedOfficerSeqNoAuto(Long ogNo, Long companyNO) {
		return functionFdAutoNo(ogNo, companyNO, "PATHS_USER_REPORTING_PREPARED", "USER_REPORTING_PREPARED_NO", 6L);
	}

	public Long reportingScientificOfficerSeqNoAuto(Long ogNo, Long companyNO) {
		return functionFdAutoNo(ogNo, companyNO, "PATHS_USER_REPORTING_SIN_OFF", "USER_REPORTING_SIN_OFF_NO", 6L);
	}

	public Long tagDoctorSeqNoAuto(Long ogNo, Long companyNO) {
		return storedProcedureGenerateNo(ogNo, companyNO, "seq_user_misc_access_no");
	}

	public CriteriaQuery<Date> dateCriteriaQuery(CriteriaBuilder builder) {
		return builder.createQuery(Date.class);
	}

	public <T> Root<T> fromDate(Class<T> clazz, CriteriaQuery<Date> criteria) {
		return criteria.from(clazz);
	}

	public String findReportImg(String imageName) {
		try {
			Response res = findByAppImageInsubdir(imageName, "report_images");
			if (res.isSuccess() && res.getObj() != null) {
				return res.getObj().toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String findReportHeaderImg() {
		return findReportImg("img_header.png");
	}

	public String findReportFooterImg() {
		return findReportImg("img_footer.png");
	}

	public String getSmsIdByProcedure(Long ogNo, Long companyNo) {
		return storedProcedureGenerateId(ogNo, companyNo, "A", "PATH_SMS_REPORT_INFO", "SMS_ID");
	}

	public Long getSmsNoByProcedure(Long ogNo, Long companyNo) {
		return storedProcedureGenerateNo(ogNo, companyNo, "SEQ_SMS_NO");
	}

	public int setOgNoParameter4View(Long ogNo) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("K_PARAMETER.PD_SET_OG_NO");
		query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
		query.setParameter(1, ogNo);

		try {
			query.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		return 1;
	}

	public int setCompanyNoParameter4View(Long conpanyNo) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("K_PARAMETER.PD_SET_COMPANY_NO");
		query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
		query.setParameter(1, conpanyNo);

		try {
			query.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		return 1;
	}

	public Response storePhotoToFile(MultipartFile file, String customFileName) {

		String fileApiUri = env.getProperty("file.api.url") + "/uploadPhoto";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("file", file.getResource());
		body.add("customFileName", customFileName);

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		RestTemplate restTemplate = new RestTemplate();
		Response photoResponse = restTemplate.postForObject(fileApiUri, requestEntity, Response.class);

		return photoResponse;
	}

	public Response uploadFileAttachment(MultipartFile file, String fileName, String subDir) {

		String uri = env.getProperty("file.api.url") + "/uploadAppImagenInsubdir";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		String fileNam = FilenameUtils.removeExtension(fileName);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
		body.add("file", file.getResource());
		body.add("customFileName", fileNam);
		body.add("subDir", subDir);

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		return new RestTemplate().postForObject(uri, requestEntity, Response.class);
	}

	public Response uploadCompanyImage(MultipartFile file, String fileName) {
		return uploadFileAttachment(file, fileName, "company_image");
	}

	public Response uploadCompanyLogo(MultipartFile file, String fileName) {
		return uploadFileAttachment(file, fileName, "company_logo");
	}

	public Response uploadOgLogo(MultipartFile file, String fileName) {
		return uploadFileAttachment(file, fileName, "og_logo");
	}

	public Response uploadDocDigiSing(MultipartFile file, String fileName) {
		return uploadFileAttachment(file, fileName, "doc_digital_sign");
	}

	public Response uploadNewsBlogImage(MultipartFile file, String fileName) {
		return uploadFileAttachment(file, fileName, "news_blog");
	}

	public Response deleteFileAttachment(String fileName) {
		String uri = env.getProperty("file.api.url") + "/deleteAppImagenInsubdir";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
		body.add("fileName", fileName);
		body.add("subDir", "file_attachment");

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		return new RestTemplate().postForObject(uri, requestEntity, Response.class);
	}

	// ------New: for parameterized view----------
	public boolean setPramProcedureVariableAndFromToDate(String procedureName, String fromVariabe, String toVariabe,
			String fromDate, String toDate) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery(procedureName);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
		query.setParameter(1, fromVariabe);
		query.setParameter(2, toVariabe);
		query.setParameter(3, fromDate);
		query.setParameter(4, toDate);

		try {
			query.execute();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	// ------New: for parameterized view----------
	public boolean setPramProcedureFromToDate(String procedureName, Date fromDate, Date toDate) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery(procedureName);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		query.setParameter(1, dateFormat(fromDate, "dd/MM/yyyy"));
		query.setParameter(2, dateFormat(toDate, "dd/MM/yyyy"));

		try {
			query.execute();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	// ------New: for parameterized view----------
	public boolean setPramProcedureSearchText(String procedureName, String searchText) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery(procedureName);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, searchText);

		try {
			query.execute();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	// ------New: for parameterized view----------
	public boolean setPramProcedureSearchNumber(String procedureName, Long searchNumber) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery(procedureName);
		query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
		query.setParameter(1, searchNumber);

		try {
			query.execute();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	// ------New: for parameterized view----------
	public boolean setPramProcedureNumbers(Long num1, Long num2, Long num3, Long num4, Long num5) {

		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("k_parameter.PD_SET_NO");
		query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN);

		query.registerStoredProcedureParameter(3, Long.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(4, Long.class, ParameterMode.IN);

		query.registerStoredProcedureParameter(5, Long.class, ParameterMode.IN);

		query.setParameter(1, num1);
		query.setParameter(2, num2);
		query.setParameter(3, num3);
		query.setParameter(4, num4);
		query.setParameter(5, num5);

		try {
			query.execute();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	 public Response getLookupMList(Long lookupNo) {
			boolean errorFlag = false;
			Response response = new Response();
			Connection con = null;
			ResultSet rs = null;
			Statement stm = null;
			Map<String, Object> list = null;
			List<Object> dataList = new ArrayList<>();
			try {
				con = getOraConnection();
				stm = con.createStatement();
				rs = stm.executeQuery(typeQuryStm(lookupNo));

				while (rs.next()) {

					list = new HashMap<>();
					list.put("id", rs.getLong("lookupdtl_no"));
					list.put("name", rs.getString("lookdtl_name"));
					dataList.add(list);

				}
			} catch (SQLException e) {
				e.printStackTrace();
				errorFlag = true;
			} finally {
				finalyConStmRs(con, stm, rs);
			}

			if (errorFlag) {
				return getErrorResponse("data not found.");
			}
			response.setItems(dataList);

			return getSuccessResponse("data found.", response);
		}
	 
	 public String typeQuryStm(Long lookupNo){
		 StringBuilder str = new StringBuilder();
		 str.append("SELECT lookupdtl_no, lookup_no, lookdtl_name ");
		 str.append("FROM  sa_lookupdtl_m ");
		 str.append("WHERE lookup_no =");
		 str.append(lookupNo);
		 str.append(" AND active_stat = 1");
		 str.append(" ORDER BY sl_no");
		 
		 return str.toString();
	 }
}
