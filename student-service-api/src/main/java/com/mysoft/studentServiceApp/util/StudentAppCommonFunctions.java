package com.mysoft.studentServiceApp.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import com.mysoft.auth.user.MyUserDetails;
import com.mysoft.core.util.CommonFunctions;

public interface StudentAppCommonFunctions  extends CommonFunctions{

	default Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	default MyUserDetails userDetails() {

		return (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	default MyUserDetails userPrincipal() {

		return (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	default String accessToken() {

		String token = null;

		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();

		if (authentication instanceof OAuth2Authentication) {
			Object details = authentication.getDetails();
			if (details instanceof OAuth2AuthenticationDetails) {
				OAuth2AuthenticationDetails oauthsDetails = (OAuth2AuthenticationDetails) details;
				token = oauthsDetails.getTokenValue();
			}
		}

		return token;
	}

	long prefixLookupNo = 5033l;
	long unitsLookupNo = 5032l;
	long eligibilityLookupNo = 5036l;
	long serviceCategoryLookupNo = 5031l;
	long personCategoryLookupNo = 5037l;
	long rankLookupNo = 5038l;
	long corpsLookupNo = 5039l;
	long nationalityLookupNo = 5041l;
	long priorityLookupNo = 5047l;
	long bloodGroupLookupNo = 1004l;
	long maritalStatusLookupNo = 1084l;
	long religionLookupNo = 1006l;
	long medicalCategoryLookupNo = 5040l;
	long districtLookupNo = 1001l;
	long relationListNo = 1021l;
	long familyLookupNo = 5044l;
	long cneLookupNo = 5045l;
	long reLookupNo = 5046l;
}
