package com.mysoft.studentServiceApp.base;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysoft.auth.user.MyUserDetails;
import com.mysoft.core.base.BaseRepository;
import com.mysoft.core.util.NodeTree;
import com.mysoft.studentServiceApp.util.StudentAppCommonFunctions;

@Repository
@Transactional
public class StudentBaseRepository extends BaseRepository implements StudentAppCommonFunctions {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Autowired
	private Environment env;
	
	@Autowired
	private EntityManager entityManager;
	@Autowired
	private TokenStore tokenStore;

//	public Long sessionNo(Long ogNo, Long companyNo) {
//		
//		BigDecimal sessionNo  = null ; 
//		sessionNo = (BigDecimal) entityManager
//			    .createNativeQuery(
//			        "SELECT fd_session_no(:pOgNo, :pCompanyNo) FROM DUAL"
//			    )
//			    .setParameter("pOgNo", ogNo)
//			    .setParameter("pCompanyNo", companyNo)
//			    .getSingleResult();
//		
//		if(sessionNo == null) {
//			return null ;
//		}
//		return sessionNo.longValue();
//	
//	}
//	
//	public void userAccessLog(Long userNo, Long ogNo, Long companyNo, String ipAddress, Long sessionNo,String osNameParam,String browserNameParam,String macAddress) {
//		 String osName = osNameParam == null? "" : osNameParam;
//		 String browserName = browserNameParam == null? "" : browserNameParam;
//		 
//		 StoredProcedureQuery query = entityManager.createStoredProcedureQuery("pd_ins_session_web")
//			    
//			    
//				.registerStoredProcedureParameter(1,Long.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(2,Long.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(3,Long.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(4, String.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(5, String.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(6, String.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(7, String.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(8, String.class,ParameterMode.IN)
//			    .registerStoredProcedureParameter(9, String.class, ParameterMode.IN)
//			    .registerStoredProcedureParameter(10, Long.class, ParameterMode.IN)
//			    
//			    .setParameter(1, userNo)
//				.setParameter(2, ogNo)
//				.setParameter(3, companyNo)
//				.setParameter(4, osName)
//				.setParameter(5, "")
//				.setParameter(6, "")
//				.setParameter(7, macAddress)
//				.setParameter(8, browserName)
//				.setParameter(9, ipAddress)
//				.setParameter(10, sessionNo);
//
//				query.execute();
//				
//	}
//	
//	
//	public List<NodeTree> userMenuList(Long userNo,Long orgNo, Long companyNo, String objType) {
//		ResultSet rs = null;
//		List<NodeTree> menuList = new ArrayList<NodeTree>();
//	
// 		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("pd_user_access_java")
//	 	.registerStoredProcedureParameter(1,Long.class,ParameterMode.IN)
//	    .registerStoredProcedureParameter(2,Long.class,ParameterMode.IN)
//	    .registerStoredProcedureParameter(3,Long.class,ParameterMode.IN)
//	    .registerStoredProcedureParameter(4, String.class,ParameterMode.IN)
//	    .registerStoredProcedureParameter(5, Void.class,ParameterMode.REF_CURSOR)
//	    
//	    .setParameter(1, userNo)
//		.setParameter(2, orgNo)
//		.setParameter(3, companyNo)
//		.setParameter(4, objType);
//			
//		query.execute();
//		  
//		
//			try {
//				
//				rs = (ResultSet) query.getOutputParameterValue(5);
//				
//				while (rs.next()) {
//					menuList.add(new NodeTree(rs.getString("disp_val"), rs.getString("page_link"),rs.getString("ICONNAME"), rs.getLong("val"), rs.getLong("parent_item")));
//				}
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}finally {
//				finalyRs(rs);
//			}
//			
//			return menuList;
//	}
//	
//    private List<NodeTree> createTree(List<NodeTree> nodes) {
//    	
//    	List<NodeTree> menuList = new ArrayList<NodeTree>();
//    	
//        Map<Long, NodeTree> mapTmp = new HashMap<>();
//        
//        //Save all nodes to a map
//        for (NodeTree current : nodes) {
//            mapTmp.put(current.getChildId(), current);
//        }
//
//        //loop and assign parent/child relationships
//        for (NodeTree current : nodes) {
//            Long parentId = current.getParentId();
//
//            if (parentId != null) {
//            	NodeTree parent = mapTmp.get(parentId);
//                if (parent != null) {
//                    current.setParent(parent);
//                    parent.addChild(current);
//                    mapTmp.put(parentId, parent);
//                    mapTmp.put(current.getChildId(), current);
//                }
//            }
//
//        }
//
//    
//        //get the root
//        NodeTree root = null;
//        for (NodeTree node : mapTmp.values()) {
//            if(node.getParent() == null) {
//                root = node;
//                System.out.println(root);
//                menuList.add(root);
//               }
//        }
//
//        return menuList;
// 
//    }
//	
//	
//	
//	
//	
//	public void userNormalLogOut(Long sessionNo, Long userNo) {
//		
//		System.out.println(" ##### "+ userDetails().getUserId());
//		userAccessLogUpdate(sessionNo,userNo,"Normal");
//	}
//	
//		
//	public void userAdNormalLogOut(Long sessionNo, Long userNo) {
//		
//		userAccessLogUpdate(sessionNo,userNo,"Adnormal");
//
//	}
//	
//	private void userAccessLogUpdate(Long sessionNo, Long userNo, String logOutType) {
//		
//		StringBuilder updateSession = new StringBuilder();
//		
//		updateSession.append(" UPDATE SA_session");
//		updateSession.append(" Set SESSION_STAT = 0 ,");
//		
//		updateSession.append(" logout_type=");
//		updateSession.append("'"+logOutType+"' ,");
//		
//		updateSession.append(" LOGOUT_DT = ");
//		updateSession.append(" to_date('" + dateFormat(new Date(), "dd-MM-yyyy hh:mm:ss a") + "', 'DD-MM-RRRR HH:MI:SS AM')");
//		
//		updateSession.append(" WHERE USER_NO =");
//		updateSession.append(userNo);
//		updateSession.append(" AND SESSION_NO = ");
//		updateSession.append(sessionNo);
//		
//		entityManager.createQuery(updateSession.toString());
//	}
	

	public MyUserDetails userDetails() {

		MyUserDetails myUserDetails = userPrincipal();
		Map<String, Object> details = tokenStore.readAccessToken(accessToken()).getAdditionalInformation();
		
		if(null != details.get("sessionNo")) {
			myUserDetails.setSessionNo( ((Long) details.get("sessionNo")).longValue());
		}
	
		return myUserDetails;
	}

//	public Long featureSubmenuNo() {
//		return storedProcedureGenerateNo(userDetails().getCompanyNo(), "SEQ_SUBMENU_NO");
//	}

//	public Long employeeSeqNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_EMP_NO");
//	}
//
//	public Long userSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_USER_NO");
//	}
//
//	public Long grantCompanySequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_GR_COMPANY_NO");
//	}
//	
//	public Long grantCompanyfunctionFdAutoNo(Long ogNo, Long companuNo) {
//		return functionFdAutoNo(ogNo, companuNo, "sa_grantcompany","gr_company_no",6L);
//		
//	}
//	
//	public Long doctorSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_DOCTOR_NO");
//	}
//	
//	public Long getDoctorNoByfunction() {
//		return functionFdAutoNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(),"HPMS_DOCTOR","DOCTOR_NO",6L);
//		
//	}
//
//	public Long grantObjectSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_GR_OBJECT_NO");
//	}
//
//	public Long moduleSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_MENU_NO");
//	}

//	public Long specializationSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_SPECIALIZATION_NO");
//	}
//
//	public Long companySequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_COMPANY_NO");
//	}
//
//	public Long roleSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_ROLE_NO");
//	}
//	
//	public Long policyBillsSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_POLICY_BILL_NO");
//	}
//	public Long policySequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_POLICY_NO");
//	}
//	public Long policyRankYearSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_RANK_POLICY_NO");
//	}
//	public Long userGrantDepartmentSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_GRANTDEPT_NO");
//	}
//	public Long billUnitSequenceNo() {
//		return storedProcedureGenerateNo(userDetails().getOrganizationNo(), userDetails().getCompanyNo(), "SEQ_BILL_UNIT_NO");
//	}
	
	

}
