package com.mysoft.studentServiceApp.student;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysoft.core.util.Response;

@RestController
@RequestMapping("/api/student")
public class StudentController {

	@Autowired
	private StudentService studentService;

	
	@PostMapping("/list")
	public Response getAll(@RequestBody(required = false) String reqObj) {
		return studentService.list(reqObj);
	}


	@GetMapping("/gridList")
	public Response gridList(HttpServletRequest request) {
		return studentService.gridList(request);
	}

	@PostMapping("/save-info")
	public Response save(@RequestBody String reqObj) {
		return studentService.save(reqObj);
	}

	@PostMapping("/update-info")
	public Response update(@RequestBody String reqObj) {
		return studentService.update(reqObj);
	}

	@PostMapping("/delete-info")
	public Response delete(@RequestBody String reqObj) {
		return studentService.deleteStudentInfo(reqObj);
	}

}
