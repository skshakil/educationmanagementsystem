package com.mysoft.studentServiceApp.student;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.pagination.DataTableRequest;
import com.mysoft.core.pagination.DataTableResults;
import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.studentServiceApp.base.StudentBaseRepository;
import com.mysoft.studentServiceApp.student.studentAchievementInfo.StudentAchievementInfoEntity;
import com.mysoft.studentServiceApp.student.studentAchievementInfo.StudentAchievementInfoService;
import com.mysoft.studentServiceApp.student.studentDocumentInfo.StudentDocumentInfoEntity;
import com.mysoft.studentServiceApp.student.studentDocumentInfo.StudentDocumentInfoService;
import com.mysoft.studentServiceApp.student.studentEducationalInfo.StudentEducationalInfoEntity;
import com.mysoft.studentServiceApp.student.studentEducationalInfo.StudentEducationalInfoService;
import com.mysoft.studentServiceApp.student.studentExprienceInfo.StudentExprienceInfoEntity;
import com.mysoft.studentServiceApp.student.studentExprienceInfo.StudentExprienceInfoService;
import com.mysoft.studentServiceApp.student.studentFamilyInfo.StudentFamilyInfoEntity;
import com.mysoft.studentServiceApp.student.studentFamilyInfo.StudentFamilyInfoService;

@Repository
@Transactional
public class StudentRepository extends StudentBaseRepository {

	@Autowired
	private StudentAchievementInfoService achievementInfoService;

	@Autowired
	private StudentEducationalInfoService educationalInfoService;

	@Autowired
	private StudentExprienceInfoService exprienceInfoService;

	@Autowired
	private StudentFamilyInfoService familyInfoService;


	@Autowired
	private StudentDocumentInfoService studentDocumentInfoService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {

		DataTableResults<StudentEntity> dataTableResults = null;
		Response response = new Response();
		StudentEntity studentObj = new StudentEntity();
		studentObj.setActiveStatus(1);
		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		Long totalRowCount = totalCount(studentObj);
		List gridList = new ArrayList<>();
		response = baseList(typedQuery(studentObj, dataTableInRQ));
		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, gridList, gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	public Response list(String reqObj) {
		StudentEntity studentObj = new StudentEntity();
		studentObj.setActiveStatus(1);
		return baseList(criteriaQuery(studentObj));
	}

	public Response save(String reqObj) {
		boolean porcContinueFlg = true;
		Response response = new Response();
		StudentEntity studentObj = null;
		JSONObject json = new JSONObject(reqObj);
		String studentObjStr = Def.getString(json, "studentObj");
		if (studentObjStr != null && !studentObjStr.equals("{}")) {
			studentObj = objectMapperReadValue(studentObjStr, StudentEntity.class);
		}
		if (studentObj != null) {
//			String ID = "ST"+generateRandom(6);
//			studentObj.setStudentId(ID);

			studentObj.setSsCreator(userDetails().getUserId());
			studentObj.setSsCreatedOn(new Date());
			studentObj.setSsCreateSession(userDetails().getSessionNo());
			studentObj.setSsModifier(userDetails().getUserId());
			studentObj.setSsModifiedOn(new Date());
			studentObj.setSsModifiedSession(userDetails().getSessionNo());
			studentObj.setCompanyNo(userDetails().getCompanyNo());
			studentObj.setOrganizationNo(userDetails().getOrganizationNo());
			
			response = baseOnlySave(studentObj);
//			if(true) return response;
			if (response.isSuccess() && response.getObj() != null) {
				studentObj = (StudentEntity) response.getObj();

				String familyInfoListStr = Def.getString(json, "familyInfoList");
				if (familyInfoListStr != null && !familyInfoListStr.equals("[]")) {
					List<StudentFamilyInfoEntity> familyInfoList = null;
					familyInfoList = objectMapperReadArrayValue(familyInfoListStr, StudentFamilyInfoEntity.class);
					System.out.println(familyInfoList);
					if (familyInfoList != null) {
						boolean isSave = familyInfoService.saveFamilyMember(familyInfoList, studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(familyInfoList);
							response.setMessage("Family Info save failed !");
							return response;
						}
					}
				}
				String educationalInfoListStr = Def.getString(json, "educationalInfoList");
				if (educationalInfoListStr != null && !educationalInfoListStr.equals("[]")) {
					List<StudentEducationalInfoEntity> educationalInfoList = null;
					educationalInfoList = objectMapperReadArrayValue(educationalInfoListStr,
							StudentEducationalInfoEntity.class);
					if (educationalInfoList != null) {
						boolean isSave = educationalInfoService.saveStudentInfo(educationalInfoList,
								studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(educationalInfoList);
							response.setMessage("Educationl Info save failed !");
							return response;
						}
					}
				}
				String documentInfoListStr = Def.getString(json, "documentInfoList");
				if (documentInfoListStr != null && !documentInfoListStr.equals("[]")) {
					List<StudentDocumentInfoEntity> documentInfoList = null;
					documentInfoList = objectMapperReadArrayValue(documentInfoListStr, StudentDocumentInfoEntity.class);
					if (documentInfoList != null) {
						boolean isSave = studentDocumentInfoService.saveStudentInfo(documentInfoList,
								studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(documentInfoList);
							response.setMessage("Document Info save failed !");
							return response;
						}
					}
				}
				String exprienceListStr = Def.getString(json, "exprienceList");
				if (exprienceListStr != null && !exprienceListStr.equals("[]")) {
					List<StudentExprienceInfoEntity> exprienceList = null;
					exprienceList = objectMapperReadArrayValue(exprienceListStr, StudentExprienceInfoEntity.class);
					if (exprienceList != null) {
						boolean isSave = exprienceInfoService.saveStudentInfo(exprienceList, studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(exprienceList);
							response.setMessage("Exprience Info save failed !");
							return response;
						}
					}
				}
				String achievementInfoListStr = Def.getString(json, "achievementInfoList");
				if (achievementInfoListStr != null && !achievementInfoListStr.equals("[]")) {
					List<StudentAchievementInfoEntity> achievementInfoList = null;
					achievementInfoList = objectMapperReadArrayValue(achievementInfoListStr,
							StudentAchievementInfoEntity.class);
					if (achievementInfoList != null) {
						boolean isSave = achievementInfoService.saveStudentInfo(achievementInfoList,
								studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(achievementInfoList);
							response.setMessage("Achievement Info save failed !");
							return response;
						}
					}
				}
			}
			response.setObj(studentObj);
			response.setMessage("Student Information save successfully done !");
			return response;
		}
		response.setSuccess(false);
		response.setObj(studentObj);
		response.setMessage("Student info not found !");
		return response;

	}

	public Response update(String reqObj) {
		boolean porcContinueFlg = true;
		Response response = new Response();
		StudentEntity studentObj = null;
		JSONObject json = new JSONObject(reqObj);
		String studentObjStr = Def.getString(json, "studentObj");
		if (studentObjStr != null && !studentObjStr.equals("{}")) {
			studentObj = objectMapperReadValue(studentObjStr, StudentEntity.class);
		}
		StudentEntity obj = findById(studentObj.getId());
		if (obj != null) {

			obj.setStFName(studentObj.getStFName());
			obj.setStLName(studentObj.getStLName());
			obj.setFatherName(studentObj.getFatherName());
			obj.setGender(studentObj.getGender());

			obj.setSsModifier(userDetails().getUserId());
			obj.setSsModifiedOn(new Date());
			obj.setSsModifiedSession(userDetails().getSessionNo());
			response = baseUpdate(obj);
//			if(true) return response;
			if (response.isSuccess()) {
				String familyInfoListStr = Def.getString(json, "familyInfoList");
				if (familyInfoListStr != null && !familyInfoListStr.equals("[]")) {
					List<StudentFamilyInfoEntity> familyInfoList = null;
					familyInfoList = objectMapperReadArrayValue(familyInfoListStr, StudentFamilyInfoEntity.class);
					if (familyInfoList != null) {
						boolean isSave = familyInfoService.familyMamberSaveUpdateOrDelete(familyInfoList,
								studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(familyInfoList);
							response.setMessage("Family Info update failed !");
							return response;
						}
					}
				}
				String educationalInfoListStr = Def.getString(json, "educationalInfoList");
				if (educationalInfoListStr != null && !educationalInfoListStr.equals("[]")) {
					List<StudentEducationalInfoEntity> educationalInfoList = null;
					educationalInfoList = objectMapperReadArrayValue(familyInfoListStr,
							StudentEducationalInfoEntity.class);
					if (educationalInfoList != null) {
						boolean isSave = educationalInfoService.saveUpdateOrDelete(educationalInfoList,
								studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(educationalInfoList);
							response.setMessage("Educationl Info update failed !");
							return response;
						}
					}
				}
				String documentInfoListStr = Def.getString(json, "documentInfoList");
				if (documentInfoListStr != null && !documentInfoListStr.equals("[]")) {
					List<StudentDocumentInfoEntity> documentInfoList = null;
					documentInfoList = objectMapperReadArrayValue(documentInfoListStr, StudentDocumentInfoEntity.class);
					if (documentInfoList != null) {
						boolean isSave = studentDocumentInfoService.saveUpdateOrDelete(documentInfoList,
								studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(documentInfoList);
							response.setMessage("Document Info update failed !");
							return response;
						}
					}
				}
				String exprienceListStr = Def.getString(json, "exprienceList");
				if (exprienceListStr != null && !exprienceListStr.equals("[]")) {
					List<StudentExprienceInfoEntity> exprienceList = null;
					exprienceList = objectMapperReadArrayValue(exprienceListStr, StudentExprienceInfoEntity.class);
					if (exprienceList != null) {
						boolean isSave = exprienceInfoService.saveUpdateOrDelete(exprienceList, studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(exprienceList);
							response.setMessage("Exprience Info update failed !");
							return response;
						}
					}
				}
				String achievementInfoListStr = Def.getString(json, "achievementInfoList");
				if (achievementInfoListStr != null && !achievementInfoListStr.equals("[]")) {
					List<StudentAchievementInfoEntity> achievementInfoList = null;
					achievementInfoList = objectMapperReadArrayValue(achievementInfoListStr,
							StudentAchievementInfoEntity.class);
					if (achievementInfoList != null) {
						boolean isSave = achievementInfoService.saveUpdateOrDelete(achievementInfoList,
								studentObj.getId());
						if (!isSave) {
							porcContinueFlg = false;
							response.setSuccess(false);
							response.setObj(achievementInfoList);
							response.setMessage("Achivement Info update failed !");
							return response;
						}
					}
				}
				response.setObj(studentObj);
				response.setMessage("Student Information Update successfully done !");
				return response;
			}

		}
		return getErrorResponse("Update failed, Record not Found!");
	}

	public StudentEntity findById(Long id) {
		StudentEntity studentObj = new StudentEntity();
		studentObj.setId(id);
		studentObj.setActiveStatus(1);
		Response response = baseFindById(criteriaQuery(studentObj));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), StudentEntity.class);
		}
		return null;
	}

	public Response deleteStudentInfo(String reqObj) {
		boolean porcContinueFlg = true;
		Response response = new Response();
		JSONObject json = new JSONObject(reqObj);
		StudentEntity studentObj = null;
		if (reqObj != null && !reqObj.equals("{}")) {
			String studentObjStr = Def.getString(json, "studentObj");
			if (studentObjStr != null && !studentObjStr.equals("{}")) {
				studentObj = objectMapperReadValue(studentObjStr, StudentEntity.class);
			}
			StudentEntity obj = findById(studentObj.getId());
			if (obj != null) {

				obj.setActiveStatus(0);

				obj.setSsModifier(userDetails().getUserId());
				obj.setSsModifiedOn(new Date());
				obj.setSsModifiedSession(userDetails().getSessionNo());
				response = baseRemove(obj);

				if (response.isSuccess()) {
					response.setMessage("Student Information delete successfully done !");
					return response;
				}
			}
		}
		return getErrorResponse("Delete failed, Record not Found!");
	}
	
	public Response find(Long id) {
		StudentEntity studentObj = new StudentEntity();
		studentObj.setId(id);
		studentObj.setActiveStatus(1);
		return baseFindById(criteriaQuery(studentObj));
	}

	public Response detele(Long id) {
		StudentEntity studentObj = findById(id);
		if (studentObj == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(studentObj);
	}

	public Response remove(Long id) {
		StudentEntity studentObj = findById(id);
		if (studentObj == null) {
			return getErrorResponse("Record not found!");
		}
		studentObj.setActiveStatus(0);
		return baseRemove(studentObj);
	}

	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(StudentEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(StudentEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);

		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}

		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);

		}

		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);

		}

		criteria.where(pArrayJoin.toArray(new Predicate[0]));

		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private Long totalCount(StudentEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<StudentEntity> root = from(StudentEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));

	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(StudentEntity filter, CriteriaBuilder builder, Root<StudentEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {
			if (filter.getActiveStatus() > 0) {
				Predicate condition = builder.equal(root.get("activeStatus"), filter.getActiveStatus());
				p.add(condition);
			}

			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}

		}

		return p;
	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(StudentEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

}
