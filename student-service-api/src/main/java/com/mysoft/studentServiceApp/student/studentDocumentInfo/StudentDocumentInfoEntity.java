package com.mysoft.studentServiceApp.student.studentDocumentInfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ST_STUDENT_DOCUMENT_INFO")
public class StudentDocumentInfoEntity extends BaseOraEntity {

	@Id
	@Column(name = "STUDENT_DOCUMENT_NO")
	private Long id;

	@Column(name = "STUDENT_DOCUMENT_ID")
	private String studentDocumentId;

	@Column(name = "STUDENT_NO")
	private Long studentNo;

	@Column(name = "DOCUMENT_NAME")
	private String documentName;
	
	@Column(name = "DOCUMENT_TYPE")
	private Long documentType;
	

	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "FILE_PATH")
	private String filePath;
	
	
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus =1;
	
	
	@Transient
	private Integer isDeleted;

}
