package com.mysoft.studentServiceApp.student.studentFamilyInfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mysoft.core.base.BaseOraEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ST_STUDENT_FAMILY_INFO")
public class StudentFamilyInfoEntity extends BaseOraEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "STUDENT_FAMILY_NO")
	private Long id;

	@Column(name = "STUDENT_FAMILY_ID")
	private String studentFamilyId;

	@Column(name = "STUDENT_NO")
	private Long studentNo;

	@Column(name = "MEMBER_NAME")
	private String memberName;

	@Column(name = "RELATION_NO")
	private Long relationNo;

	@Column(name = "MOBILE_NO")
	private String mobileNo;

	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus =1;

	@Transient
	private Integer isDeleted;
	
	
}
