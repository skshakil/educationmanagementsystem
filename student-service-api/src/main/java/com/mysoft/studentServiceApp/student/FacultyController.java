package com.mysoft.studentServiceApp.student;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;
import com.mysoft.shared.faculty.facultyInfo.FacultyService;
import com.mysoft.studentServiceApp.base.StudentBaseController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/faculty")
public class FacultyController extends StudentBaseController{

    @Autowired
    private FacultyService facultyService;

    @GetMapping("/test")
    public String test() {
        return "Welcome to faculty section";
    }

    @PostMapping("/list")
    public Response getAll(@RequestBody(required = false) String reqObj) {
        return facultyService.list(reqObj);
    }

    @PostMapping("/gridlist")
    public Response gridDataList(HttpServletRequest request) {
        return facultyService.gridList(request);
    }

    @PostMapping("/save")
    public Response save(@RequestBody String reqObj) {
        UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
        return facultyService.save(reqObj, userDetails);
    }

    @PostMapping("/update")
    public Response update(@RequestBody String reqObj) {
        UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
        return facultyService.update(reqObj, userDetails);
    }

    @PostMapping("/delete")
    public Response delete(@RequestBody String reqObj) {
        UserSignInContainter userDetails = new UserSignInContainter(userDetails().getUserId(), userDetails().getCompanyNo(), userDetails().getOrganizationNo(), userDetails().getSessionNo(),userDetails().getUserTypeNo());
        return facultyService.deleteFacultyInfo(reqObj, userDetails);
    }
}
