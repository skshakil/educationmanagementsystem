package com.mysoft.studentServiceApp.student.studentAchievementInfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ST_STUDENT_ACHIEVEMENT_INFO")
public class StudentAchievementInfoEntity extends BaseOraEntity {

	@Id
	@Column(name = "STUDENT_ACHIEVEMENT_NO")
	private Long id;

	@Column(name = "STUDENT_ACHIEVEMENT_ID")
	private String studentAchivementId;

	@Column(name = "STUDENT_NO")
	private Long studentNo;

	@Column(name = "ACH_DESC")
	private String achDesc;
	
	@Column(name = "ORG_NAME")
	private String orgName;
	

	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "ACH_TITLE")
	private String achTitle;
	
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus =1;
	
	
	@Transient
	private Integer isDeleted;
	

}
