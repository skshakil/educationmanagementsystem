package com.mysoft.studentServiceApp.student.studentEducationalInfo;

import javax.persistence.*;

import com.mysoft.core.base.BaseOraEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ST_STUDENT_EDUCATION_INFO")
public class StudentEducationalInfoEntity extends BaseOraEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EDUCATION_NO")
	private Long id;

	@Column(name = "EDUCATION_ID")
	private String educationId;

	@Column(name = "STUDENT_NO")
	private Long studentNo;

	@Column(name = "COURSE_NAME")
	private String courseName;
	
	@Column(name = "ADMISSION_YEAR")
	private Long admissionYear;

	@Column(name = "INSTITUTION_NAME")
	private String institutionName;
	
	@Column(name = "MAJOR_SUBJECT")
	private String majorSubject;
	
	@Column(name = "BOARD_NAME")
	private String boardName;
	
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus =1;
	
	
	@Transient
	private Integer isDeleted;

}
