package com.mysoft.studentServiceApp.student.studentFamilyInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;

@Service
public class StudentFamilyInfoService {

	@Autowired
	private StudentFamilyInfoRepository studentInfoRepository;
	
	public Response findByStudentId(String reqObj) {
		return studentInfoRepository.findByStudentId(reqObj);
	}

	public Boolean saveFamilyMember(List<StudentFamilyInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveFamilyMember(batch, studentNo);
	}

	public Boolean familyMamberSaveUpdateOrDelete(List<StudentFamilyInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.familyMamberSaveUpdateOrDelete(batch, studentNo);
	}

}
