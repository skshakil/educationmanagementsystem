package com.mysoft.studentServiceApp.student.studentAchievementInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import com.mysoft.core.util.Response;
import com.mysoft.studentServiceApp.base.StudentBaseRepository;

@Repository
@Transactional
public class StudentAchievementInfoRepository extends StudentBaseRepository {

	public Boolean saveUpdateOrDelete(List<StudentAchievementInfoEntity> batch, Long studentNo) {
		Response response = null;
		Boolean flag = true;

		for (StudentAchievementInfoEntity obj : batch) {
			obj.setStudentNo(studentNo);
			if (obj.getIsDeleted() != null && obj.getIsDeleted() == 1) {
				response = removeById(obj.getId());
				if (!response.isSuccess()) {
					flag = false;
					break;
				}
			} else {
				if (obj.getId() != null) {
					response = updateObj(obj);
					if (!response.isSuccess()) {
						flag = false;
						break;
					}
				} else {
					response = saveObj(obj);
					if (!response.isSuccess()) {
						flag = false;
						break;
					}
				}
			}

		}

		return flag;
	}

	public Boolean saveStudentInfo(List<StudentAchievementInfoEntity> batch, Long studentNo) {
		Response response = null;
		boolean flag = true;
		for (StudentAchievementInfoEntity obj : batch) {
			obj.setStudentNo(studentNo);
			response = saveObj(obj);
			if (!response.isSuccess()) {
				flag = false;
				break;
			}
		}
		return flag;
	}

	public Response saveObj(StudentAchievementInfoEntity studentInfoObj) {
		studentInfoObj.setSsCreator(userDetails().getUserId());
		studentInfoObj.setSsCreatedOn(new Date());
		studentInfoObj.setSsCreateSession(userDetails().getSessionNo());
		studentInfoObj.setCompanyNo(userDetails().getCompanyNo());
		studentInfoObj.setOrganizationNo(userDetails().getOrganizationNo());
		return baseOnlySave(studentInfoObj);
	}

	public Response removeById(Long id) {
		StudentAchievementInfoEntity obj = findByIdObj(id);
		if (obj != null) {
			obj.setSsModifier(userDetails().getUserId());
			obj.setSsModifiedOn(new Date());
			obj.setSsModifiedSession(userDetails().getSessionNo());
			obj.setActiveStatus(0);
			return baseRemove(obj);
		}
		return getErrorResponse("Student info not found !");
	}

	public Response updateObj(StudentAchievementInfoEntity reqObj) {
		StudentAchievementInfoEntity obj = findByIdObj(reqObj.getId());
		if (obj != null) {

			obj.setAchTitle(reqObj.getAchDesc());
			obj.setAchDesc(reqObj.getAchDesc());
			obj.setAddress(reqObj.getAddress());
			obj.setOrgName(reqObj.getOrgName());

			obj.setSsModifier(userDetails().getUserId());
			obj.setSsModifiedOn(new Date());
			obj.setSsModifiedSession(userDetails().getSessionNo());

			return baseUpdate(obj);
		}
		return getErrorResponse("Record not Found !!");
	}

	public StudentAchievementInfoEntity findByIdObj(Long id) {
		StudentAchievementInfoEntity studentInfoObj = new StudentAchievementInfoEntity();
		studentInfoObj.setId(id);
		Response response = baseFindById(criteriaQuery(studentInfoObj));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), StudentAchievementInfoEntity.class);
		}
		return null;
	}

	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(StudentAchievementInfoEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(StudentAchievementInfoEntity filter, CriteriaBuilder builder,
			Root<StudentAchievementInfoEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {

			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
			if (filter.getStudentNo() != null && filter.getStudentNo() > 0) {
				Predicate condition = builder.equal(root.get("studentNo"), filter.getStudentNo());
				p.add(condition);
			}

		}

		return p;
	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(StudentAchievementInfoEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

}
