package com.mysoft.studentServiceApp.student.studentAchievementInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentAchievementInfoService {

	@Autowired
	private StudentAchievementInfoRepository studentInfoRepository;

	public Boolean saveStudentInfo(List<StudentAchievementInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveStudentInfo(batch, studentNo);
	}

	public Boolean saveUpdateOrDelete(List<StudentAchievementInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveUpdateOrDelete(batch, studentNo);
	}

}
