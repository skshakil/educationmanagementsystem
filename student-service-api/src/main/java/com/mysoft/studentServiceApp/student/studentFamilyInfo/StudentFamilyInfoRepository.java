package com.mysoft.studentServiceApp.student.studentFamilyInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.mysoft.core.util.Def;
import com.mysoft.core.util.Response;
import com.mysoft.studentServiceApp.base.StudentBaseRepository;
import com.mysoft.studentServiceApp.student.StudentEntity;

@Repository
@Transactional
public class StudentFamilyInfoRepository extends StudentBaseRepository {
	
	public Response findByStudentId(String reqObj) {
		StudentFamilyInfoEntity stFamilyInfoObj = new StudentFamilyInfoEntity();
		JSONObject json = new JSONObject(reqObj);
		Long studentNo = Def.getLong(json, "studentNo");
		if (studentNo == null) {
			return getErrorResponse("Student No not Found!");
		}
		stFamilyInfoObj.setStudentNo(studentNo);
		
		return baseList(criteriaQuery(stFamilyInfoObj));
	}

	public Boolean familyMamberSaveUpdateOrDelete(List<StudentFamilyInfoEntity> batch, Long studentNo) {
		Response response = null;
		Boolean flag = true;

		for (StudentFamilyInfoEntity obj : batch) {
			obj.setStudentNo(studentNo);
			if (obj.getIsDeleted() != null && obj.getIsDeleted() == 1) {
				response = removeById(obj.getId());
				if (!response.isSuccess()) {
					flag = false;
					break;
				}
			} else {
				if (obj.getId() != null) {
					response = updateObj(obj);
					if (!response.isSuccess()) {
						flag = false;
						break;
					}
				} else {
					response = saveObj(obj);
					if (!response.isSuccess()) {
						flag = false;
						break;
					}
				}
			}

		}

		return flag;
	}

	public Boolean saveFamilyMember(List<StudentFamilyInfoEntity> batch, Long studentNo) {
		Response response = null;
		boolean flag = true;
		for (StudentFamilyInfoEntity obj : batch) {
			obj.setStudentNo(studentNo);
			response = saveObj(obj);
			if (!response.isSuccess()) {
				flag = false;
				break;
			}
		}
		return flag;
	}

	public Response saveObj(StudentFamilyInfoEntity studentInfoObj) {
		studentInfoObj.setSsCreator(userDetails().getUserId());
		studentInfoObj.setSsCreatedOn(new Date());
		studentInfoObj.setSsCreateSession(userDetails().getSessionNo());
		studentInfoObj.setCompanyNo(userDetails().getCompanyNo());
		studentInfoObj.setOrganizationNo(userDetails().getOrganizationNo());
		return baseOnlySave(studentInfoObj);
	}

	public Response removeById(Long id) {
		StudentFamilyInfoEntity obj = findByIdObj(id);
		if (obj != null) {
			obj.setSsModifier(userDetails().getUserId());
			obj.setSsModifiedOn(new Date());
			obj.setSsModifiedSession(userDetails().getSessionNo());
			obj.setActiveStatus(0);
			return baseRemove(obj);
		}
		return getErrorResponse("Student info not found !");
	}

	public Response updateObj(StudentFamilyInfoEntity reqObj) {
		StudentFamilyInfoEntity obj = findByIdObj(reqObj.getId());
		if (obj != null) {

			obj.setMemberName(reqObj.getMemberName());
			obj.setRelationNo(reqObj.getRelationNo());
			obj.setMobileNo(reqObj.getMobileNo());
			obj.setAddress(reqObj.getAddress());

			obj.setSsModifier(userDetails().getUserId());
			obj.setSsModifiedOn(new Date());
			obj.setSsModifiedSession(userDetails().getSessionNo());

			return baseUpdate(obj);
		}
		return getErrorResponse("Record not Found !!");
	}

	public StudentFamilyInfoEntity findByIdObj(Long id) {
		StudentFamilyInfoEntity studentInfoObj = new StudentFamilyInfoEntity();
		studentInfoObj.setId(id);
		Response response = baseFindById(criteriaQuery(studentInfoObj));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), StudentFamilyInfoEntity.class);
		}
		return null;
	}

	// Non API
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(StudentFamilyInfoEntity filter) {
		init();

		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(StudentFamilyInfoEntity filter, CriteriaBuilder builder,
			Root<StudentFamilyInfoEntity> root) {

		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}

		List<Predicate> p = new ArrayList<Predicate>();

		if (filter != null) {

			if (filter.getId() != null && filter.getId() > 0) {
				Predicate condition = builder.equal(root.get("id"), filter.getId());
				p.add(condition);
			}
			if (filter.getStudentNo() != null && filter.getStudentNo() > 0) {
				Predicate condition = builder.equal(root.get("studentNo"), filter.getStudentNo());
				p.add(condition);
			}

		}

		return p;
	}

	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(StudentFamilyInfoEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

}
