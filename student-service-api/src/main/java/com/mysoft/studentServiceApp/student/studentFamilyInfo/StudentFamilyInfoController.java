package com.mysoft.studentServiceApp.student.studentFamilyInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysoft.core.util.Response;

@RestController
@RequestMapping("/api/student-family-info")
public class StudentFamilyInfoController {

	@Autowired
	private StudentFamilyInfoService studentInfoService;

	@PostMapping("/find-by-student-id")
	public Response getAll(@RequestBody String reqObj) {
		return studentInfoService.findByStudentId(reqObj);
	}

}
