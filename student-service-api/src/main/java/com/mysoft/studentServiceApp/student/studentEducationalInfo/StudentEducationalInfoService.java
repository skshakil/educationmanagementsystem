package com.mysoft.studentServiceApp.student.studentEducationalInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentEducationalInfoService {

	@Autowired
	private StudentEducationalInfoRepository studentInfoRepository;

	public Boolean saveStudentInfo(List<StudentEducationalInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveStudentInfo(batch, studentNo);
	}

	public Boolean saveUpdateOrDelete(List<StudentEducationalInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveUpdateOrDelete(batch, studentNo);
	}

}
