package com.mysoft.studentServiceApp.student.studentDocumentInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDocumentInfoService {

	@Autowired
	private StudentDocumentInfoRepository studentInfoRepository;

	public Boolean saveStudentInfo(List<StudentDocumentInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveStudentInfo(batch, studentNo);
	}

	public Boolean saveUpdateOrDelete(List<StudentDocumentInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveUpdateOrDelete(batch, studentNo);
	}

}
