package com.mysoft.studentServiceApp.student;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mysoft.core.base.BaseOraEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "ST_STUDENT")
public class StudentEntity extends BaseOraEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "STUDENT_NO")
	private Long id;

	@Column(name = "STUDENT_ID")
	private String studentId;

	@Column(name = "SALUTATION")
	private String salutation;

	@Column(name = "FNAME")
	private String stFName;

	@Column(name = "MNAME")
	private String stMName;

	@Column(name = "LNAME")
	private String stLName;

	@Column(name = "FATHER_NAME")
	private String fatherName;

	@Column(name = "MOTHER_NAME")
	private String motherName;

	@Column(name = "SPOUSE_NAME")
	private String spouseeName;

	@Column(name = "GENDER")
	private String gender;

	@Column(name = "DOB")
	private Date dob;

	@Column(name = "NATIONALITY")
	private String nationality;

	@Column(name = "PRE_ADDRESS")
	private String presentAddress;

	@Column(name = "PER_ADDRESS")
	private String permanentAddress;

	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus = 1;

}
