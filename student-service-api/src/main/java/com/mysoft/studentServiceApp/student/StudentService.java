package com.mysoft.studentServiceApp.student;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysoft.core.util.Response;
import com.mysoft.core.util.UserSignInContainter;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	public Response save(String reqObj) {
		return studentRepository.save(reqObj);
	}

	public Response list(String reqObj) {
		return studentRepository.list(reqObj);
	}

	public Response update(String reqObj) {
		return studentRepository.update(reqObj);
	}
	
	public Response deleteStudentInfo(String reqObj) {
		return studentRepository.deleteStudentInfo(reqObj);
	}

	public Response delete(Long id) {
		return studentRepository.remove(id);
	}

	public Response gridList(HttpServletRequest request) {
		return studentRepository.gridList(request);
	}

	public Response findByUserNo(Long id) {
		return studentRepository.find(id);
	}

}
