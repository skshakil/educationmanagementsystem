package com.mysoft.studentServiceApp.student.studentExprienceInfo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mysoft.core.base.BaseOraEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ST_STUDENT_EXPRIENCE_INFO")
public class StudentExprienceInfoEntity extends BaseOraEntity {

	@Id
	@Column(name = "STUDENT_EXPRIENCE_NO")
	private Long id;

	@Column(name = "STUDENT_EXPRIENCE_ID")
	private String studentExprienceId;

	@Column(name = "STUDENT_NO")
	private Long studentNo;

	@Column(name = "EXP_TITLE")
	private String expTitle;
	
	@Column(name = "EXP_DESC")
	private String expDesc;
	
	@Column(name = "EXP_INSTITUTE")
	private String expInstitute;
	
	@Column(name = "EXP_START_DATE")
	private Date expStartDate;
	
	@Column(name = "EXP_END_DATE")
	private Date expEndDate;
	
	@Column(name = "EXP_CERTIFIC_DATE")
	private Date expCertificDate;
	

	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "ACTIVE_STAT")
	private Integer activeStatus =1;
	
	
	@Transient
	private Integer isDeleted;


}
