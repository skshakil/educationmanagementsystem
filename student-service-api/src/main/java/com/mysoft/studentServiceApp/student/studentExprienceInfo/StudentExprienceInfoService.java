package com.mysoft.studentServiceApp.student.studentExprienceInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentExprienceInfoService {

	@Autowired
	private StudentExprienceInfoRepository studentInfoRepository;

	public Boolean saveStudentInfo(List<StudentExprienceInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveStudentInfo(batch, studentNo);
	}

	public Boolean saveUpdateOrDelete(List<StudentExprienceInfoEntity> batch, Long studentNo) {
		return studentInfoRepository.saveUpdateOrDelete(batch, studentNo);
	}

}
