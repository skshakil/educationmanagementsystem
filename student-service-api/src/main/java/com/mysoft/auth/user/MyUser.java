package com.mysoft.auth.user;


import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
@Setter
public class MyUser extends User {
	
	private static final long serialVersionUID = 1L;

	private  long userId;
	private Long agentId;
    private String name;
	private String fullName;
	//private SaCompany company;
	
	private Long companyNo;
	private String companyName;
	private Long organizationNo;
	private String organizationName;
	private Long sessionNo;
	private Long empNo;
	private Long doctorNo;
    private Long userBillUnitNo;
    private Long userBtbLabNo;
    private Integer userTypeNo;

	public MyUser(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
			long id, Long companyNo, String companyName) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.userId = id;
		this.companyNo = companyNo;
		this.companyName = companyName;
		
	}
	
	


}

