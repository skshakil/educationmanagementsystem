package com.mysoft.auth.user;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Md. Jahurul Islam
 *
 */
@Getter
@Setter
public class CoreUserEntity implements Serializable {
	private static final long serialVersionUID = -1537111554670173093L;

	private long id;

	private String userName;

	private String name;

	private String fullName;

	private String phone;

	private String email;

	private String password;

	private Date dateOfBirth;

	private String gender;

	private int activeStatus = 1;

	private boolean enabled;

	private boolean accountLocked;

	private boolean accountExpired;

	private boolean passwordExpired;

	private Date accountExpireDate;

	private Long companyNo;

	private Date dateCreated;

	private String createdBy;

	private Date lastUpdated;

	private String updatedBy;

	private String companyName;

	private Long doctorNo;

	private Long empNo;

	private String compnayName;

	private Long orgNo;

	private Long billUnitNo;

	private Long b2bLabNo;

}
